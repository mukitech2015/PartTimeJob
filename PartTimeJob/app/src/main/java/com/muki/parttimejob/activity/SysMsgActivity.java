package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.SysMsgAdapter;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/10/8.
 */
public class SysMsgActivity  extends AppCompatActivity implements PullBaseView.OnHeaderRefreshListener,
        PullBaseView.OnFooterRefreshListener {
    private RelativeLayout   sysMsgBackLat;
    private PullRecyclerView sysmsgPrv;
    private SysMsgAdapter sysMsgAdapter;
    private int  page=0;
    private List<AVObject>   avObjectList=new ArrayList<>();
    private LinearLayout  noSysMsgLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sysmsg);
        initViews();
    }
    @Override
    protected void onStart() {
        super.onStart();
        getSysNotes();
    }
    protected void initViews() {
        noSysMsgLat= (LinearLayout) findViewById(R.id.noSysMsgLat);
        sysMsgBackLat= (RelativeLayout) findViewById(R.id.sysMsgBackLat);
        sysMsgBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sysmsgPrv= (PullRecyclerView) findViewById(R.id.sysmsgPrv);
        sysmsgPrv.setLayoutManager(new LinearLayoutManager(this),this);
        sysmsgPrv.setOnFooterRefreshListener(this);
        sysmsgPrv.setOnHeaderRefreshListener(this);
    }
    public   void  setDateAdapter(List<AVObject> list){
        if (sysMsgAdapter==null){
            sysMsgAdapter=new SysMsgAdapter(SysMsgActivity.this,list);
            sysmsgPrv.setAdapter(sysMsgAdapter);
        }
        sysMsgAdapter.changeSysNoteAdapter(list);
        if(sysMsgAdapter.getItemCount()==0){
            sysmsgPrv.setVisibility(View.GONE);
            noSysMsgLat.setVisibility(View.VISIBLE);
        }else {
            sysmsgPrv.setVisibility(View.VISIBLE);
            noSysMsgLat.setVisibility(View.GONE);
        }
    }
    public  void  getSysNotes(  ){
        AVQuery<AVObject>   sysNoteAVQuery=new AVQuery<>("Sys_Notice");
        sysNoteAVQuery.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        sysNoteAVQuery.setMaxCacheAge(24 * 3600); //设置缓存有效期
        sysNoteAVQuery.orderByDescending("createdAt");
        sysNoteAVQuery.skip(page*10);// 跳过 10 条结果
        sysNoteAVQuery.limit(10);
        sysNoteAVQuery.whereEqualTo("user_pointer", LeanchatUser.getCurrentUser());
        sysNoteAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                  if(list!=null){
                      if(page==0){
                          avObjectList.clear();
                          avObjectList.addAll(list);
                          setDateAdapter(avObjectList);
                      }else{
                          avObjectList.addAll(avObjectList.size(),list);
                          setDateAdapter(avObjectList);
                      }
                  }
            }
        });
    }

    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                getSysNotes();
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }

    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                getSysNotes();
                view.onHeaderRefreshComplete();
            }
        }, 2000);
    }
}



