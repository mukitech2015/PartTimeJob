package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.MemeberAddAdapter;
import com.muki.parttimejob.manager.FriendsManager;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ConversationUtils;
import com.muki.parttimejob.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import cn.leancloud.chatkit.LCChatKit;

/**
 * 群聊对话拉人页面
 * Created by lzw on 14-10-11.
 * TODO: ConversationChangeEvent
 */
public class ConversationAddMembersActivity extends AVBaseActivity {
  protected RecyclerView recyclerView;
  private LinearLayoutManager layoutManager;
  private MemeberAddAdapter adapter;
  private AVIMConversation conversation;
  public static final int OK = 0;
  private RelativeLayout    addMemBackLat;
  private RelativeLayout  addMenLat;
  List<String> members;
  List<String>  checkedUsers;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.conversation_add_members_layout);
    addMemBackLat= (RelativeLayout) findViewById(R.id.addMemBackLat);
    addMenLat= (RelativeLayout) findViewById(R.id.addMemSureLat);
    recyclerView= (RecyclerView) findViewById(R.id.member_add_rv_list);
    members = new ArrayList<String>();
    members.add(LCChatKit.getInstance().getCurrentUserId());
    layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    adapter = new MemeberAddAdapter();
    recyclerView.setAdapter(adapter);
    addMemBackLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
              finish();
       }
     });
    addMenLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkedUsers = adapter.getCheckedIds();
        if(checkedUsers.size()==0){
           Toast.makeText(getApplicationContext(),"请添加成员",Toast.LENGTH_SHORT).show();
           return;
        }
        final EditText editText= new EditText(ConversationAddMembersActivity.this);
        new AlertDialog.Builder(ConversationAddMembersActivity.this).setTitle("请设置群聊名称").setIcon(
                R.drawable.ic_launcher).setView(editText).setPositiveButton("确定", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
                 if(!TextUtils.isEmpty(editText.getText().toString())){
                   addMembers(editText.getText().toString());
                 }else{
                   Toast.makeText(getApplicationContext(),"群聊名称不能为空",Toast.LENGTH_SHORT).show();
                   return;
                 }
          }
        }).show();
       }
    });
  }

  @Override
  protected void onStart() {
    super.onStart();
    setListData();
  }
  private void setListData() {
    FriendsManager.fetchFriends(false, new FindCallback<LeanchatUser>() {
      @Override
      public void done(List<LeanchatUser> list, AVException e) {
        if (filterException(e)) {
          adapter.setDataList(list);
          adapter.notifyDataSetChanged();
        }
      }
    });
  }

  public void updateName(final AVIMConversation conv, String newName, final AVIMConversationCallback callback) {
    conv.setName(newName);
    conv.updateInfoInBackground(new AVIMConversationCallback() {
      @Override
      public void done(AVIMException e) {
        if (e != null) {
          if (callback != null) {
              callback.done(e);
          }
        } else {
          if (callback != null) {
            callback.done(null);
          }
        }
      }
    });
  }

  private void addMembers(final String   gropuName) {
    final ProgressDialog dialog = showSpinnerDialog();
    if (checkedUsers.size() == 0) {
        finish();
    }else {
      ConversationUtils.createGroupConversation(members, new AVIMConversationCreatedCallback() {
        @Override
        public void done(AVIMConversation avimConversation, AVIMException e) {
          if(e==null){
            conversation = LCChatKit.getInstance().getClient().getConversation(avimConversation.getConversationId());
            conversation.addMembers(checkedUsers, new AVIMConversationCallback() {
              @Override
              public void done(AVIMException e) {
                dialog.dismiss();
                updateName(conversation,gropuName,new AVIMConversationCallback() {
                  @Override
                  public void done(AVIMException e) {
                if (filterException(e)) {
                    Utils.toast(R.string.conversation_inviteSucceed);
                    setResult(RESULT_OK);
                    finish();
                    }
                  }
                });
              }
            });
          }
        }
      });
      }
    }
}
