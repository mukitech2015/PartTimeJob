package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/9/13.
 */
public class JobListAdapter  extends BaseAdapter<JobListAdapter.ViewHolder> {
    DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
    public JobListAdapter(Context context, List<AVObject> listDatas) {
        super(context,listDatas);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_selectjob_item, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        Job  job= (Job) listDatas.get(position);
        Business  business= (Business) job.get("business_pointer");
        holder.select_company_Name_Tv.setText(business.getBusiness_Name());
        UpLoadImageUtil.loadCropImage(context,business.getComLogoUrl(),holder.companyLogoIv);
        SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
            holder.dateTv.setText(sdf.format(sf.parse(job.getJob_StartTime()+""))+"至"+sdf.format(sf.parse(job.getJob_EndTime()+"")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (business.businessStyle()){
            holder.busStyleIv.setImageResource(R.drawable.qiye);
        }else{
            holder.busStyleIv.setImageResource(R.drawable.geren);
        }
        if(business.IsVip()){
            holder.vipIv.setVisibility(View.VISIBLE);
        }else{
            holder.vipIv.setVisibility(View.INVISIBLE);
        }
        if(business.IsSalaryGuarantee()){
           holder.baozhangIv.setVisibility(View.VISIBLE);
        }else{
           holder.baozhangIv.setVisibility(View.INVISIBLE);
        }
        holder.recrutmentTitleTv.setText(job.getJob_RecruitName());
        holder.priceTv.setText(job.getJob_Price()+""+job.getJob_Unit());
        double  latitude =job.getGeoPoint().getLatitude();
        double  longitude=job.getGeoPoint().getLongitude();
        AVGeoPoint point = new AVGeoPoint(latitude,longitude);
        double distance=point.distanceInKilometersTo(new AVGeoPoint(AppConfig.mylatitude,AppConfig.mylongitude));
        holder.distanceTv.setText(decimalFormat.format(Double.valueOf(distance))+"千米");
        holder.locationTv.setText(job.getJob_Address());
    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView  recrutmentTitleTv;
        private  TextView   dateTv;
        private  TextView  priceTv;
        private  TextView  locationTv;
        private ImageView  companyLogoIv;
        private  TextView  select_company_Name_Tv;
        private  TextView  distanceTv;
        private  ImageView  baozhangIv;
        private  ImageView   vipIv;
        private  ImageView   busStyleIv;
        public ViewHolder(View view) {
           super(view);
            busStyleIv= (ImageView) view.findViewById(R.id.select_busStyleIv);
            vipIv= (ImageView) view.findViewById(R.id.select_vipIv);
            baozhangIv= (ImageView) view.findViewById(R.id.select_baozhangIv);
            distanceTv= (TextView) view.findViewById(R.id.select_distanceTv);
            select_company_Name_Tv= (TextView) view.findViewById(R.id.select_company_Name_Tv);
           companyLogoIv= (ImageView) view.findViewById(R.id.select_company_logo_lv);
           dateTv= (TextView) view.findViewById(R.id.select_date_tv);
           locationTv= (TextView) view.findViewById(R.id.select_location_tv);
           recrutmentTitleTv= (TextView) view.findViewById(R.id.select_recrutment_title_tv);
           priceTv= (TextView) view.findViewById(R.id.select_price_tv);
        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
        this.listDatas=listDatas;
        notifyDataSetChanged();
    }
}
