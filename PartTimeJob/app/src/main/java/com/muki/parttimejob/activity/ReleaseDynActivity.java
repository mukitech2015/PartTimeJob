package com.muki.parttimejob.activity;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.SaveCallback;
import com.baoyz.actionsheet.ActionSheet;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.PubSelectedImgsAdapter;
import com.muki.parttimejob.event.ReleDynEvent;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ImageUtil;
import com.muki.parttimejob.utils.Utils;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by muki on 2016/10/18.
 */

public class ReleaseDynActivity  extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout  cancelLat;
    private RelativeLayout  relSendLat;
    private EditText    releaseEt;
    private ImageView    imputIv;
    private GridView release_GridView;
    PubSelectedImgsAdapter pubSelectedImgsAdapter;
    ArrayList<String>  dynPics=new ArrayList<>();
    String  dynContent;
    private ProgressDialog  progressDialog;
    String dynType="公开";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.layout_release_dyn);
        initViews();
        progressDialog= Utils.createProgressDialog(this,"正在发布......");
    }
    protected void initViews() {
        release_GridView= (GridView) findViewById(R.id.releaseGv);
        cancelLat= (RelativeLayout) findViewById(R.id.relCanlelLat);
        relSendLat= (RelativeLayout) findViewById(R.id.relSendLat);
        releaseEt= (EditText) findViewById(R.id.releaseEt);
        releaseEt.addTextChangedListener(new  EditChangedListener());
        imputIv= (ImageView) findViewById(R.id.imputIv);
        cancelLat.setOnClickListener(this);
        relSendLat.setOnClickListener(this);
        imputIv.setOnClickListener(this);
         //单选框
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.dynRg);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.openRb) //rb3设定为正确答案
                {
                    dynType="公开";
                }else if(checkedId==R.id.anonymousRb){
                    dynType="匿名";
                }else if(checkedId==R.id.friSeeRb){
                    dynType="仅好友可见";
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
       switch (v.getId()) {
           case R.id.relCanlelLat:
                finish();
               break;
           case R.id.imputIv:
               setMutiSelectPicture();
               break;
           case R.id.relSendLat:
               dynContent=releaseEt.getText().toString().trim();
               if(TextUtils.isEmpty(dynContent)&&dynPics.size()==0){
                   Toast.makeText(ReleaseDynActivity.this,"请输入内容",Toast.LENGTH_SHORT).show();
                   return;
               }
               if (dynPics.size()>0){
                   try {
                       releDyn(dynContent,dynPics);
                   } catch (FileNotFoundException e) {
                       e.printStackTrace();
                   }
               }else{
                   try {
                       releDynWithOutPic(dynContent);
                   } catch (FileNotFoundException e) {
                       e.printStackTrace();
                   }
               }
               break;
       }
    }
    public  void  releDynWithOutPic( String  dynamic_content)throws FileNotFoundException{
        progressDialog.show();
        final Dynamic  dynamic=new Dynamic();
        dynamic.setDynamicContent(dynamic_content);
        if(dynType.equals("匿名")){
            dynamic.setDynJurisdiction(true);
        }else if(dynType.equals("公开")){
            dynamic.setDynJurisdiction(false);
        }else if(dynType.equals("仅好友可见")){
            dynamic.setOnlyFriendState(true);
        }
        dynamic.setDynamicUsers(LeanchatUser.getCurrentUser());
        dynamic.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    progressDialog.dismiss();
                    Toast.makeText(ReleaseDynActivity.this,"发布成功",Toast.LENGTH_SHORT).show();
                    releaseEt.setText("");
                    EventBus.getDefault().post(new ReleDynEvent("发送成功"));
                    finish();
                }
            }
        });
    }
    public  void   releDyn(String  dynamic_content, final ArrayList<String>  dynPics) throws FileNotFoundException {
        progressDialog.show();
        final Dynamic  dynamic=new Dynamic();
        dynamic.setDynamicContent(dynamic_content);
        if(dynType.equals("匿名")){
            dynamic.setDynJurisdiction(true);
        }else if(dynType.equals("公开")){
            dynamic.setDynJurisdiction(false);
        }else if(dynType.equals("仅好友可见")){
            dynamic.setOnlyFriendState(true);
        }
        dynamic.setDynamicUsers(LeanchatUser.getCurrentUser());
        for (int i=0;i<dynPics.size();i++) {
            final AVFile avFileBig=AVFile.withAbsoluteLocalPath("bigDynamicImage.png",dynPics.get(i));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // 获取图片的宽和高
            Bitmap bitmap = BitmapFactory.decodeFile(dynPics.get(i), options);
            ThumbnailUtils.extractThumbnail(bitmap,50,50);
            options.inJustDecodeBounds = false;
            Bitmap bitmapThum = BitmapFactory.decodeFile(dynPics.get(i), options);
            String newPath=ImageUtil.getAbsoluteImagePath(Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), bitmapThum , null,null)),this);
            final AVFile avFile=AVFile.withAbsoluteLocalPath("dynamicImage.png",newPath);
            try {
                AVObject.saveFileBeforeSave(Arrays.asList(avFile, avFileBig), false, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e==null){
                            final AVObject dynObj=new AVObject("Dynamic_image");
                            dynObj.put("dynamic_Thumbnail_Image",avFile);
                            dynObj.put("dynamic_Image",avFileBig);
                            AVObject.saveAllInBackground(Arrays.asList(dynObj), new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> dynamicRelation=dynamic.getRelation("dynamic_Image_relation");
                                    dynamicRelation.add(dynObj);
                                    dynamic.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            if(e==null){
                                                progressDialog.dismiss();
                                                Toast.makeText(ReleaseDynActivity.this,"发布成功",Toast.LENGTH_SHORT).show();
                                                releaseEt.setText("");
                                                dynPics.clear();
                                                pubSelectedImgsAdapter.notifyDataSetChanged();
                                                EventBus.getDefault().post(new ReleDynEvent("发送成功"));
                                                finish();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } catch (AVException e) {
                e.printStackTrace();
            }
        }
    }
    public    void  setMutiSelectPicture(){
        AndroidImagePicker.getInstance().pickMulti(ReleaseDynActivity.this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
            @Override
            public void onImagePickComplete(List<ImageItem> items) {
                if(items != null && items.size() > 0){
                    dynPics.clear();
                    for(ImageItem image:items){
                        dynPics.add(getAbsoluteImagePath(ImageUtil.Scal(image.path,getApplicationContext())));
                    }
                    pubSelectedImgsAdapter = new PubSelectedImgsAdapter(ReleaseDynActivity.this,dynPics,new PubSelectedImgsAdapter.OnItemClickClass() {
                        @Override
                        public void OnItemClick(View v, String filepath) {
                            dynPics.remove(filepath);
                            pubSelectedImgsAdapter.notifyDataSetChanged();
                        }
                    });
                    release_GridView.setAdapter(pubSelectedImgsAdapter);
                }
            }
        });
          }
    protected String getAbsoluteImagePath(Uri uri)
    {
        // can post image
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( uri,
                proj, // Which columns to return
                null, // WHERE clause; which rows to return (all rows)
                null, // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    class EditChangedListener implements TextWatcher {
        private CharSequence temp;//监听前的文本
        private int editStart;//光标开始位置
        private int editEnd;//光标结束位置
        private final int charMaxNum = 140;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            temp = s;
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {

            /** 得到光标开始和结束位置 ,超过最大数后记录刚超出的数字索引进行控制 */
            editStart = releaseEt.getSelectionStart();
            editEnd = releaseEt.getSelectionEnd();
            if (temp.length() > charMaxNum) {
                Toast.makeText(ReleaseDynActivity.this, "你输入的字数已经超过了限制", Toast.LENGTH_LONG).show();
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                releaseEt.setText(s);
                releaseEt.setSelection(tempSelection);
            }
        }
    }
}









