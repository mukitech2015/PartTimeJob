package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/10/8.
 */
public class SysMsgAdapter extends BaseAdapter<SysMsgAdapter.ViewHolder> {
    public SysMsgAdapter(Context context, List<AVObject> listDatas) {
        super(context,listDatas);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_sysmsg_parent, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        AVObject   noticeAvObj=listDatas.get(position);
        holder.noteMsgTv.setText(noticeAvObj.getString("notice_msg"));
        holder.noteTitleTv.setText(noticeAvObj.getString("notice_title"));
        SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd hh:mm");
            holder.timeTv.setText(sdf.format(sf.parse(noticeAvObj.getCreatedAt()+"")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public   void  changeSysNoteAdapter(List<AVObject> listDatas){
          this.listDatas=listDatas;
          notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return listDatas==null?0:listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView   timeTv;
        private TextView   noteMsgTv;
        private TextView   noteTitleTv;
        public ViewHolder(View view) {
            super(view);
            noteTitleTv= (TextView) view.findViewById(R.id.noteTitleTv);
            timeTv= (TextView) view.findViewById(R.id.msgTimeTv);
            noteMsgTv= (TextView) view.findViewById(R.id.noteMsgTv);
        }
    }
}






