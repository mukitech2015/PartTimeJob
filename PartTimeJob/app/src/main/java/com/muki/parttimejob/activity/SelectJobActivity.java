package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.JobListAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.widget.BaseAdapter;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/9/27.
 */
public class SelectJobActivity   extends  BaseActivity implements View.OnClickListener,PullBaseView.OnHeaderRefreshListener,
        PullBaseView.OnFooterRefreshListener{
    private PullRecyclerView selectJobRv;
    private LinearLayout  selectBackLay;
    private JobListAdapter squareAdapter;
    String  jobType;
    private TextView   selJobTitleTv;
    List<AVObject> joblist=new ArrayList<>();
    private int  page=0;
    private   LinearLayout  noSelectJobLat;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
       setContentView(R.layout.layout_selectjob_list);
    }
    @Override
    protected void initViews() {
         noSelectJobLat= (LinearLayout) findViewById(R.id.noSelectJobLat);
         selJobTitleTv= (TextView) findViewById(R.id.selJobTitleTv);
         selectJobRv= (PullRecyclerView) findViewById(R.id.selectJobRv);
         selectJobRv.setOnHeaderRefreshListener(this);
         selectJobRv.setOnFooterRefreshListener(this);
         selectJobRv.setLayoutManager(new LinearLayoutManager(this),this);
         selectBackLay= (LinearLayout) findViewById(R.id.selectBackLat);
         selectBackLay.setOnClickListener(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent  intent=getIntent();
        jobType=intent.getStringExtra("JOB_TYPE");
        if(jobType==null){
            return;
        }
        selJobTitleTv.setText(jobType);
        getListData(jobType);
    }
    @Override
    protected void initDatas() {

    }
    public  void   getListData(String   jobType){
       if (jobType.equals("附近兼职")){
           AVQuery<AVObject> query = new AVQuery<>("Job");
           final AVGeoPoint point = new AVGeoPoint(AppConfig.mylatitude, AppConfig.mylongitude);
           query.whereNear("job_DistanceGEO", point);
           query.limit(10);
           query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
           query.setMaxCacheAge(24 * 3600); //设置缓存有效期
           query.orderByDescending("createdAt");
           query.whereEqualTo("job_States",true);
           query.skip(page*10);// 跳过 10 条结果
           query.include("business_pointer");
           query.findInBackground(new FindCallback<AVObject>() {
               @Override
               public void done(List<AVObject> list, AVException e) {
                    if(list!=null){
                       if (page==0){
                           joblist.clear();
                           joblist.addAll(list);
                           setAdapter(joblist);
                       }else{
                           joblist.addAll(list.size(),list);
                           setAdapter(joblist);
                       }
                    }
               }
           });
       }else{
           AVQuery<AVObject> query = new AVQuery<>("Job");
           query.limit(10);
           query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
           query.setMaxCacheAge(24 * 3600); //设置缓存有效期
           query.orderByDescending("createdAt");
           query.whereEqualTo("job_States",true);
           query.skip(page*10);// 跳过 10 条结果
           query.include("business_pointer");
           if(jobType.equals("官方任务")){
               query.whereEqualTo("job_States",true);
           }
           if(jobType.equals("线上兼职")){
               query.whereEqualTo("job_IsAtHome",true);
           }
           if(jobType.equals("实习机会")){
               query.whereEqualTo("job_Internship",true);
           }
           query.findInBackground(new FindCallback<AVObject>() {
               @Override
               public void done(List<AVObject> list, AVException e) {
                   if(list!=null){
                       if (page==0){
                           joblist.clear();
                           joblist.addAll(list);
                           setAdapter(joblist);
                       }else{
                           joblist.addAll(list.size(),list);
                           setAdapter(joblist);
                       }
                   }
               }
           });
       }
    }
    public   void   setAdapter(List<AVObject> listDatas){
        if (squareAdapter==null){
            squareAdapter=new JobListAdapter(SelectJobActivity.this,listDatas);
            selectJobRv.setAdapter(squareAdapter);
        }else{
            squareAdapter.changeAdapterData(listDatas);
        }
        squareAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent  intent=new Intent(SelectJobActivity.this,JobDetailActivity.class);
                intent.putExtra("job", (Serializable)joblist.get(position));
                startActivity(intent);
            }
        });
        if(squareAdapter.getItemCount()==0){
           selectJobRv.setVisibility(View.GONE);
           noSelectJobLat.setVisibility(View.VISIBLE);
        }else{
            selectJobRv.setVisibility(View.VISIBLE);
            noSelectJobLat.setVisibility(View.GONE);
        }
    }
    @Override
    public void onClick(View v) {
      switch (v.getId()) {
          case  R.id.selectBackLat:
              this.finish();
              break;
        }
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                getListData(jobType);
                view.onHeaderRefreshComplete();
            }
        }, 2000);
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                getListData(jobType);
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
}











