package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/31.
 */

public class WithdrawalsActivity  extends AppCompatActivity {
    @Bind(R.id.wdNameTv)
    TextView  wdNameTv;
    @Bind(R.id.wdTauthIv)
    ImageView  wdTauthIv;
    @Bind(R.id.alipayEt)
    EditText  alipayEt;
    @Bind(R.id.alipayNextLat)
    RelativeLayout  alipayNextLat;
    @Bind(R.id.bankNameEt)
    EditText  bankNameEt;
    @Bind(R.id.bankAccountEt)
    EditText  bankAccountEt;
    @Bind(R.id.bankNextLat)
    RelativeLayout  bankNextLat;
    @Bind(R.id.widthDrawsBackLat)
    RelativeLayout  widthDrawsBackLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_withdrawals);
        ButterKnife.bind(this);
        widthDrawsBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alipayNextLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String  alipayStr=alipayEt.getText().toString().trim();
              if(isAuthentication()){
                 if (TextUtils.isEmpty(alipayStr)){
                     Toast.makeText(WithdrawalsActivity.this,"请输入支付宝账户",Toast.LENGTH_SHORT).show();
                     return;
                 }
                 LeanchatUser.getCurrentUser().setAlipayAccount(alipayStr);
                 LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                     @Override
                     public void done(AVException e) {
                         if(e==null){
                             Intent  intent=new Intent(WithdrawalsActivity.this,NextWidthDrawalsActivity.class);
                             intent.putExtra("WithdrawalsType","支付宝");
                             startActivity(intent);
                         }
                     }
                 });
              }
            }
        });
        bankNextLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String  bankName=bankNameEt.getText().toString().trim();
              String  bankAccount=bankAccountEt.getText().toString().trim();
              if(isAuthentication()){
                  if (TextUtils.isEmpty(bankName)){
                      Toast.makeText(WithdrawalsActivity.this,"请输入开户银行名称",Toast.LENGTH_SHORT).show();
                      return;
                  }
                  if (TextUtils.isEmpty(bankAccount)){
                      Toast.makeText(WithdrawalsActivity.this,"请输入银行卡账号",Toast.LENGTH_SHORT).show();
                      return;
                  }
                  LeanchatUser.getCurrentUser().setBankAccount(bankAccount);
                  LeanchatUser.getCurrentUser().setBankName(bankName);
                  LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                      @Override
                      public void done(AVException e) {
                          if(e==null){
                              Intent  intent=new Intent(WithdrawalsActivity.this,NextWidthDrawalsActivity.class);
                              intent.putExtra("WithdrawalsType","银行卡");
                              startActivity(intent);
                          }
                      }
                  });
              }
            }
        });
    }
    public  boolean   isAuthentication( ){
        if(LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION()!=1){
            Toast.makeText(this,"未通过身份验证",Toast.LENGTH_SHORT).show();
            return  false;
        }
        if (TextUtils.isEmpty(wdNameTv.getText().toString().trim())){
            Toast.makeText(this,"请到个人中心填写姓名",Toast.LENGTH_SHORT).show();
            return  false;
        }
        return  true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        getDatas();
    }
    public   void    getDatas( ){
        if(!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getUSER_FAMILY_NAME())){
             wdNameTv.setText(LeanchatUser.getCurrentUser().getUSER_FAMILY_NAME());
        }
        if(LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION()==1){
            wdTauthIv.setImageResource(R.drawable.withdrawl_tauthentication);
        }else{
            wdTauthIv.setImageResource(R.drawable.withdrawl_notauthentication);
        }
        if (!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getAlipayAccount())){
            alipayEt.setText(LeanchatUser.getCurrentUser().getAlipayAccount());
        }
        if(!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getBankName())){
            bankNameEt.setText(LeanchatUser.getCurrentUser().getBankName());
        }
        if(!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getBankAccount())){
            bankAccountEt.setText(LeanchatUser.getCurrentUser().getBankAccount());
        }
    }
}






