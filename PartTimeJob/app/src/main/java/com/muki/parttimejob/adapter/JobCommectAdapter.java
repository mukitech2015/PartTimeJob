package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.TimeTool;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/11/8.
 */

public class JobCommectAdapter extends BaseAdapter< JobCommectAdapter.ViewHolder> {

    public  JobCommectAdapter(Context context, List<AVObject> listDatas) {
        super(context,listDatas);
    }
    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new  ViewHolder(mInflater.inflate(R.layout.layout_jobcomt_item, parent, false));
    }
    @Override
    public void onBindViewHolder( ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
          AVObject  jobComtObj=listDatas.get(position);
          LeanchatUser   leanchatUser=jobComtObj.getAVObject("commenter");
          if(leanchatUser!=null){
              if(TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())){
                  holder.jobComtNameTv.setText(leanchatUser.getUSERNAME());
              }else{
                  holder.jobComtNameTv.setText(leanchatUser.getUSER_NICKNAME());
              }
              UpLoadImageUtil.loadCropImage(context,leanchatUser.getAvatarUrl(),holder.jobComtAvatarIv);
          }
        SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        SimpleDateFormat formatter = new    SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDatecurrenttime =  new  Date(System.currentTimeMillis());//获取当前时间
        String    strtime= null;
        try {
            strtime = formatter.format(sf.parse(jobComtObj.getCreatedAt()+""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String    strcurrenttime= formatter.format(curDatecurrenttime);
        holder.jobComtTimeTv.setText(TimeTool.getTime(strtime,strcurrenttime));
        holder.jobComtContentTv.setText(jobComtObj.getString("content"));
        holder.jobCommectRb.setRating((float)jobComtObj.getDouble("rating"));
    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView jobComtAvatarIv;
        private TextView jobComtNameTv;
        private TextView  jobComtTimeTv;
        private TextView  jobComtContentTv;
        private RatingBar   jobCommectRb;

        public ViewHolder(View view) {
            super(view);
             jobCommectRb= (RatingBar) view.findViewById(R.id.jobCommectRb);
             jobComtAvatarIv= (ImageView) view.findViewById(R.id.jobComtAvatarIv);
             jobComtContentTv= (TextView) view.findViewById(R.id.jobComtContentTv);
             jobComtNameTv= (TextView) view.findViewById(R.id.jobComtNameTv);
             jobComtTimeTv= (TextView) view.findViewById(R.id.jobComtTimeTv);
        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
           this.listDatas=listDatas;
          notifyDataSetChanged();
    }
}
