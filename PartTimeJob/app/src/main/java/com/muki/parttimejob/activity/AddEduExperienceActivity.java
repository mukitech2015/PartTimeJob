package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

import org.feezu.liuli.timeselector.TimeSelector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/1.
 */

public class AddEduExperienceActivity  extends AppCompatActivity {
    @Bind(R.id.addExperBackLat)
    LinearLayout  addExperBackLat;
    @Bind(R.id.experEduStartTv)
    TextView   experEduStartTv;
    @Bind(R.id.experEduStartLat)
    RelativeLayout  experEduStartLat;
    @Bind(R.id.experEduEndTv)
    TextView   experEduEndTv;
    @Bind(R.id.experEduEndLat)
    RelativeLayout  experEduEndLat;
    @Bind(R.id.experSchoolNameEt)
    EditText experSchoolNameEt;
    @Bind(R.id.experMajorEt)
    EditText  experMajorEt;
    @Bind(R.id.saveExperLat)
    RelativeLayout  saveExperLat;
    private TimeSelector timeSelector;
    private  int   witchTime=-1;
    private  String   eduId;
    Date getDateWithDateString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addeduexperirnce);
        ButterKnife.bind(this);
        addExperBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        timeSelector = new TimeSelector(this, new TimeSelector.ResultHandler() {
            @Override
            public void handle(String time) {
                if(witchTime==0){
                    experEduStartTv.setText(time.replaceAll("00:00",""));
                }else if(witchTime==1){
                    experEduEndTv.setText(time.replaceAll("00:00",""));
                }
            }
        }, "1950-01-01 00:00", "2022-12-31 00:00");
        experEduStartLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                witchTime=0;
                timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                timeSelector.show();
            }
        });
        experEduEndLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                witchTime=1;
                timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                timeSelector.show();
            }
        });
        saveExperLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  experSchoolName=experSchoolNameEt.getText().toString().trim();
                String  experMajor=experMajorEt.getText().toString().trim();
                String  experStartTime=experEduStartTv.getText().toString().trim()+" 00:00:00";
                String  experEndTime=experEduEndTv.getText().toString().trim()+" 00:00:00";
                if (TextUtils.isEmpty(experSchoolName)){
                    Toast.makeText(AddEduExperienceActivity.this,"请填写学校名称",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(experStartTime)){
                    Toast.makeText(AddEduExperienceActivity.this,"请填写入学时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(experEndTime)){
                    Toast.makeText(AddEduExperienceActivity.this,"请填写毕业时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(getDateWithDateString(experEndTime+"00:00:00").getTime()<=getDateWithDateString(experStartTime+" 00:00:00").getTime()){
                    Toast.makeText(AddEduExperienceActivity.this,"毕业时间不能小于等于入学时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(experMajor)){
                    Toast.makeText(AddEduExperienceActivity.this,"请填写专业",Toast.LENGTH_SHORT).show();
                    return;
                }
                saveEduExper(experSchoolName,experMajor,experStartTime,experEndTime);
            }
        });
    }
   public   void    saveEduExper(final String  experSchoolName, final String  experMajor, final String  experStartTime, final String  experEndTime ){
       if(TextUtils.isEmpty(eduId)){
           final AVObject   avObject=new AVObject("User_Info");
           avObject.put("user_Info_Record_Address",experSchoolName);
           avObject.put("user_Info_Start_Date",getDateWithDateString(experStartTime));
           avObject.put("user_Info_End_Date",getDateWithDateString(experEndTime));
           avObject.put("user_Info_Record_Type","教育经历");
           avObject.put("user_Info_Record",experMajor);
           avObject.saveInBackground(new SaveCallback() {
               @Override
               public void done(AVException e) {
                   if (e==null){
                       AVRelation<AVObject> relation = LeanchatUser.getCurrentUser().getRelation("user_Info_relation");// 新建一个 AVRelation
                       relation.add(avObject);
                       LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                           @Override
                           public void done(AVException e) {
                               finish();
                           }
                       });
                   }
               }
           });
       }else{
           AVObject  practiceObj=AVObject.createWithoutData("User_Info",eduId);
           practiceObj.fetchInBackground(new GetCallback<AVObject>() {
               @Override
               public void done(AVObject avObject, AVException e) {
                   if(e==null){
                       avObject.put("user_Info_Record_Address",experSchoolName);
                       avObject.put("user_Info_Start_Date",getDateWithDateString(experStartTime));
                       avObject.put("user_Info_End_Date",getDateWithDateString(experEndTime));
                       avObject.put("user_Info_Record_Type","教育经历");
                       avObject.put("user_Info_Record",experMajor);
                       avObject.saveInBackground(new SaveCallback() {
                           @Override
                           public void done(AVException e) {
                               finish();
                           }
                       });
                   }
               }
           });
       }
   }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent=getIntent();
        if (intent!=null){
            String  startTime=intent.getStringExtra("user_Info_Start_Date");
            String  endTime=intent.getStringExtra("user_Info_End_Date");
            String   major=intent.getStringExtra("user_Info_Record");
            String   school=intent.getStringExtra("user_Info_Record_Address");
            eduId=intent.getStringExtra("objectId");
            experSchoolNameEt.setText(school);
            experMajorEt.setText(major);
            experEduEndTv.setText(endTime);
            experEduStartTv.setText(startTime);
        }
    }
}









