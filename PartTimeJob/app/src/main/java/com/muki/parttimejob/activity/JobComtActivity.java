package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.JobCommectAdapter;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/8.
 */

public class JobComtActivity  extends AppCompatActivity implements PullBaseView.OnHeaderRefreshListener,
        PullBaseView.OnFooterRefreshListener{
    @Bind(R.id.jobComtBackLat)
    LinearLayout  jobComtBackLat;
    @Bind(R.id.jobCommentPv)
    PullRecyclerView  jobCommentPv;
    JobCommectAdapter   jobCommectAdapter;
    Job  job;
    int page=0;
    @Bind(R.id.noJobComtLat)
     LinearLayout   noJobComtLat;
    private  List<AVObject>  jobComtList=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_jobcomment);
        ButterKnife.bind(this);
        jobCommentPv.setLayoutManager(new LinearLayoutManager(this),this);
        jobCommentPv.setOnHeaderRefreshListener(this);
        jobCommentPv.setOnFooterRefreshListener(this);
        jobComtBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });

    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                getJobComments(job);
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                getJobComments(job);
                view.onHeaderRefreshComplete();
            }
        }, 2000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent=getIntent();
        job= (Job) intent.getSerializableExtra("job");
        getJobComments(job);
    }
    public   void    setJobCommentAdapter(List<AVObject> list){
         if(jobCommectAdapter==null){
             jobCommectAdapter=new JobCommectAdapter(this,list);
             jobCommentPv.setAdapter(jobCommectAdapter);
         }
         jobCommectAdapter.changeAdapterData(list);
         if(jobCommectAdapter.getItemCount()==0){
             jobCommentPv.setVisibility(View.GONE);
             noJobComtLat.setVisibility(View.VISIBLE);
         }else {
             jobCommentPv.setVisibility(View.VISIBLE);
             noJobComtLat.setVisibility(View.GONE);
         }
    }
    public    void   getJobComments(final Job   job){
        AVObject todoFolder = AVObject.createWithoutData("Job",job.getObjectId());
        AVRelation<AVObject> relation = todoFolder.getRelation("job_Comment_relation");
        AVQuery<AVObject> query = relation.getQuery();
        query.limit(10);
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.orderByDescending("createdAt");
        query.include("commenter");
        query.skip(page*10);// 跳过 10 条结果
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                  if (list!=null){
                      if(page==0){
                          jobComtList.clear();
                          jobComtList.addAll(list);
                          setJobCommentAdapter(jobComtList);
                      }else{
                          jobComtList.addAll(jobComtList.size(),list);
                          setJobCommentAdapter(jobComtList);
                      }
                  }
            }
        });
    }
}




