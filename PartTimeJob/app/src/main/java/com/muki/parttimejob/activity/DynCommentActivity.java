package com.muki.parttimejob.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.DynCommentAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/10/25.
 */

public class DynCommentActivity   extends   BaseActivity implements View.OnClickListener,PullBaseView.OnFooterRefreshListener,PullBaseView.OnHeaderRefreshListener{
     private LinearLayout dynComtBackLat;
     private PullRecyclerView  dynCommentPrv;
     private RelativeLayout  dynComtLat;
     private DynCommentAdapter   dynCommentAdapter;
     private List<AVObject>   dynComtObjList=new ArrayList<>();
     private    int    page=0;
     Dynamic dynamic;
    private   final int SEND_COMMENT=1001;
    private   LinearLayout   noDynComtLat;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
       setContentView(R.layout.layout_comment_content);
    }
    @Override
    protected void initViews() {
        noDynComtLat= (LinearLayout) findViewById(R.id.noDynComtLat);
        dynComtLat= (RelativeLayout) findViewById(R.id.dynComtLat);
        dynComtLat.setOnClickListener(this);
        dynComtBackLat= (LinearLayout) findViewById(R.id.dynComtBackLat);
        dynComtBackLat.setOnClickListener(this);
        dynCommentPrv= (PullRecyclerView) findViewById(R.id.dynCommentPrv);
        dynCommentPrv.setOnHeaderRefreshListener(this);
        dynCommentPrv.setOnFooterRefreshListener(this);
        dynComtBackLat.setOnClickListener(this);
        dynCommentPrv.setLayoutManager(new LinearLayoutManager(DynCommentActivity.this),DynCommentActivity.this);
    }
    @Override
    protected void initDatas() {

    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent  intent=getIntent();
        dynamic= (Dynamic) intent.getSerializableExtra("dynamic");
        getDynComtDatas(dynamic.getObjectId());
    }
    public   void   getDynComtDatas(String  dynamicId){
        AVObject todoFolder = AVObject.createWithoutData("Dynamic",dynamicId);
        AVRelation<AVObject> relation = todoFolder.getRelation("dynamic_Comment_relation");
        AVQuery<AVObject> query = relation.getQuery();
        query.include("dynamic_Comment_User_pointer");
        query.orderByDescending("createdAt");
        query.skip(page*10);// 跳过 10 条结果
        query.limit(10);
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                 if(list!=null){
                     if(page==0){
                         dynComtObjList.clear();
                         dynComtObjList.addAll(list);
                         setDynCommentAdapter(dynComtObjList);
                     }else{
                         dynComtObjList.addAll(dynComtObjList.size(),list);
                         setDynCommentAdapter(dynComtObjList);
                     }
                 }
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.dynComtBackLat:
             finish();
             break;
            case R.id.dynComtLat:
                Intent intent_sendComt = new Intent(this, SendCommentActivity.class);
                intent_sendComt .putExtra(AppConfig.NeedCallback, true);
                intent_sendComt.putExtra("dynamic", (Serializable) dynamic);
                startActivityForResult(intent_sendComt ,SEND_COMMENT);
             break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if(requestCode==SEND_COMMENT) {
            getDynComtDatas(dynamic.getObjectId());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public  void   setDynCommentAdapter(List<AVObject> list){
           if(dynCommentAdapter==null){
               dynCommentAdapter=new DynCommentAdapter(DynCommentActivity.this,list);
               dynCommentPrv.setAdapter(dynCommentAdapter);
           }else{
               dynCommentAdapter.changeAdapterData(list);
           }
           if(dynCommentAdapter.getItemCount()==0){
               dynCommentPrv.setVisibility(View.GONE);
               noDynComtLat.setVisibility(View.VISIBLE);
           }else{
                dynCommentPrv.setVisibility(View.VISIBLE);
                noDynComtLat.setVisibility(View.GONE);
           }
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page++;
                getDynComtDatas(dynamic.getObjectId());
                view.onFooterRefreshComplete();
            }
        }, 1000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page=0;
                getDynComtDatas(dynamic.getObjectId());
                view.onHeaderRefreshComplete();
            }
        }, 1000);
    }
}
