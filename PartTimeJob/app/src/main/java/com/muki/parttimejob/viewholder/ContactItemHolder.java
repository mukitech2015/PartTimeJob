package com.muki.parttimejob.viewholder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.event.ContactItemClickEvent;
import com.muki.parttimejob.event.ContactItemLongClickEvent;
import com.muki.parttimejob.model.ContactItem;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.utils.UserCacheUtils;

import cn.leancloud.chatkit.viewholder.LCIMCommonViewHolder;
import de.greenrobot.event.EventBus;

/**
 * Created by wli on 15/11/24.
 */
public class ContactItemHolder extends LCIMCommonViewHolder<ContactItem> {

  TextView alpha;
  TextView nameView;
  ImageView avatarView;

  public ContactItem contactItem;

  public ContactItemHolder(Context context, ViewGroup root) {
    super(context, root, R.layout.common_user_item);
    initView();
  }
  public void initView() {
    alpha = (TextView)itemView.findViewById(R.id.alpha);
    nameView = (TextView)itemView.findViewById(R.id.tv_friend_name);
    avatarView = (ImageView)itemView.findViewById(R.id.img_friend_avatar);
    itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        UserCacheUtils.cacheUser(contactItem.user);
        EventBus.getDefault().post(new ContactItemClickEvent(contactItem.user.getObjectId(),nameView.getText().toString()));
      }
    });
    itemView.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        EventBus.getDefault().post(new ContactItemLongClickEvent(contactItem.user.getObjectId()));
        return true;
      }
    });
  }
  @Override
  public void bindData(ContactItem memberItem) {
    contactItem = memberItem;
    alpha.setVisibility(memberItem.initialVisible ? View.VISIBLE : View.GONE);
    if (!TextUtils.isEmpty(memberItem.sortContent)) {
      alpha.setText(String.valueOf(Character.toUpperCase(memberItem.sortContent.charAt(0))));
    } else {
      alpha.setText("");
    }
    UpLoadImageUtil.loadCropImage(getContext(),memberItem.user.getAvatarUrl(),avatarView);
    if(TextUtils.isEmpty(memberItem.user.getUSER_NICKNAME())){
      nameView.setText(memberItem.user.getUsername());
    }else{
      nameView.setText(memberItem.user.getUSER_NICKNAME());
    }
  }
  public static ViewHolderCreator HOLDER_CREATOR = new ViewHolderCreator<ContactItemHolder>() {
    @Override
    public ContactItemHolder createByViewGroupAndType(ViewGroup parent, int viewType) {
      return new ContactItemHolder(parent.getContext(), parent);
    }
  };
}
