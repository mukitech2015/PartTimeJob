package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.entity.ImageInfo;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.util.List;

/**
 * Created by muki on 2016/10/31.
 */

public class ScrollLvAdapter extends  RecyclerView.Adapter<ScrollLvAdapter.ViewHolder>{
    List<ImageInfo>   imageInfoList;
    Context  context;
    public ScrollLvAdapter(List<ImageInfo> imageInfoList, Context   context) {
        this.imageInfoList=imageInfoList;
        this.context=context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pic_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

            if (position==imageInfoList.size()){
                holder.imageview_item.setImageResource(R.drawable.imput_image);
            }else{
                UpLoadImageUtil.loadImage(context,imageInfoList.get(position).getThumbnailUrl(),holder.imageview_item);
            }

       holder.imageview_item.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (onRecycleViewItemClickListener!=null){
                   onRecycleViewItemClickListener.OnItemClick(holder.imageview_item,position);
               }
           }
       });
       holder.imageview_item.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View v) {
               if(onRecycleViewItemLongClickListener!=null){
                   onRecycleViewItemLongClickListener.OnItemLongClick(holder.imageview_item,position);
               }
               return false;
           }
       });
    }
    public  interface OnRecycleViewItemClickListener{
        void OnItemClick(View view, int position);
    }
    private OnRecycleViewItemClickListener  onRecycleViewItemClickListener;
    public void  setOnItemClickListener(OnRecycleViewItemClickListener   onRecycleViewItemClickListener ){
        this.onRecycleViewItemClickListener=onRecycleViewItemClickListener;
    }
    public  interface OnRecycleViewItemLongClickListener{
        void OnItemLongClick(View view, int position);
    }
    private OnRecycleViewItemLongClickListener onRecycleViewItemLongClickListener;
    public void  setOnItemLongClickListener(OnRecycleViewItemLongClickListener onRecycleViewItemLongClickListener ){
        this.onRecycleViewItemLongClickListener=onRecycleViewItemLongClickListener;
    }
    @Override
    public int getItemCount() {
        return  imageInfoList.size()+1;
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        public  ImageView imageview_item;
        public ViewHolder(View view) {
            super(view);
            imageview_item= (ImageView) view.findViewById(R.id.imageview_item);
        }
    }
    public   void    changeAdapter(List<ImageInfo> imageInfoList){
        this.imageInfoList=imageInfoList;
        notifyDataSetChanged();
    }
}
