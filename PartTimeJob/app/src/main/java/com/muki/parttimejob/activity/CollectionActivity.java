package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.CollectionAdapter;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.widget.BaseAdapter;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/28.
 */

public class CollectionActivity extends AppCompatActivity implements View.OnClickListener, PullBaseView.OnHeaderRefreshListener,
        PullBaseView.OnFooterRefreshListener {
    @Bind(R.id.mycollectionBackLat)
    LinearLayout mycollectionBackLat;
    @Bind(R.id.myCollectionJobLv)
    PullRecyclerView myCollectionJobLv;
    @Bind(R.id.noCollectionLat)
    LinearLayout noCollectionLat;
    private CollectionAdapter collectionAdapter;
    private static int page = 0;
    private ArrayList<AVObject> avObjects = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_collection);
        ButterKnife.bind(this);
        mycollectionBackLat.setOnClickListener(this);
        myCollectionJobLv.setOnHeaderRefreshListener(this);
        myCollectionJobLv.setOnFooterRefreshListener(this);
        myCollectionJobLv.setLayoutManager(new LinearLayoutManager(this), this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mycollectionBackLat:
                finish();
                break;
            case R.id.remCollectLat:
                int  mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                showCancelCollDlg(mIndex);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCollectionListDatas();
    }
    android.app.AlertDialog.Builder cancelCollDlg;
    public void showCancelCollDlg(final int position) {
        if (cancelCollDlg == null) {
            cancelCollDlg = new AlertDialog.Builder(this);
        }
        cancelCollDlg.setIcon(R.drawable.ic_launcher)
                .setTitle("提示")
                .setMessage("是否取消收藏?")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        avObjects.get(position).put("state", false);
                        avObjects.get(position).saveInBackground(new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                if (e == null) {
                                    avObjects.remove(position);
                                    collectionAdapter.changeAdapterData(avObjects);
                                }
                            }
                        });
                    }
                }).show();
    }
    public void setAdapter(final List<AVObject> listDatas) {
        if (collectionAdapter == null) {
            collectionAdapter = new CollectionAdapter(CollectionActivity.this, listDatas,this);
            myCollectionJobLv.setAdapter(collectionAdapter);
        } else {
            collectionAdapter.changeAdapterData(listDatas);
        }
        if (collectionAdapter.getItemCount() == 0) {
            myCollectionJobLv.setVisibility(View.GONE);
            noCollectionLat.setVisibility(View.VISIBLE);
        } else {
            myCollectionJobLv.setVisibility(View.VISIBLE);
            noCollectionLat.setVisibility(View.GONE);
        }
        collectionAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                final Job jobObj = avObjects.get(position).getAVObject("job_pointer");
                jobObj.fetchInBackground("business_pointer", new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject avObject, AVException e) {
                        Intent intent = new Intent(CollectionActivity.this, JobDetailActivity.class);
                        intent.putExtra("job", (Serializable) avObject);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    public void getCollectionListDatas() {
        AVQuery<AVObject> avObjectAVQuery = new AVQuery<>("User_Job_Follow");
        avObjectAVQuery.whereEqualTo("user_pointer", LeanchatUser.getCurrentUser());
        avObjectAVQuery.whereEqualTo("state", true);
        avObjectAVQuery.orderByDescending("createdAt");
        avObjectAVQuery.skip(page * 10);// 跳过 10 条结果
        avObjectAVQuery.limit(10);
        avObjectAVQuery.include("job_pointer");
        avObjectAVQuery.include("business_pointer");
        avObjectAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (list != null) {
                    if (page == 0) {
                        avObjects.clear();
                        avObjects.addAll(list);
                        setAdapter(avObjects);
                    } else {
                        avObjects.addAll(avObjects.size(), list);
                        setAdapter(avObjects);
                    }
                }
            }
        });
    }

    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //execute the task
                page++;
                getCollectionListDatas();
                view.onFooterRefreshComplete();
            }
        }, 1000);
    }

    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //execute the task
                page = 0;
                getCollectionListDatas();
                view.onHeaderRefreshComplete();
            }
        }, 1000);
    }
}
