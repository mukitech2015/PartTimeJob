package com.muki.parttimejob.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.ContactsAdapter;
import com.muki.parttimejob.event.ContactItemClickEvent;
import com.muki.parttimejob.event.ContactItemLongClickEvent;
import com.muki.parttimejob.event.ContactRefreshEvent;
import com.muki.parttimejob.event.InvitationEvent;
import com.muki.parttimejob.event.MemberLetterEvent;
import com.muki.parttimejob.manager.FriendsManager;
import com.muki.parttimejob.model.AddRequestManager;
import com.muki.parttimejob.model.LeanchatUser;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.leancloud.chatkit.utils.LCIMConstants;
import de.greenrobot.event.EventBus;

/**
 * Created by muki on 2016/10/13.
 */

public class MyFriendActivity   extends    AVBaseActivity  {
    @Bind(R.id.activity_square_members_srl_list)
    protected SwipeRefreshLayout refreshLayout;
    @Bind(R.id.activity_square_members_rv_list)
    protected RecyclerView recyclerView;
    private ContactsAdapter itemAdapter;
    LinearLayoutManager layoutManager;
    private RelativeLayout  myFdBackLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myfriend);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        myFdBackLat= (RelativeLayout) findViewById(R.id.myFdBackLat);
        myFdBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        itemAdapter = new ContactsAdapter();
        recyclerView.setAdapter(itemAdapter);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMembers(true);
            }
        });
     }
    @Override
    protected void onStart() {
        super.onStart();
        getMembers(true);
    }
    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void getMembers(final boolean isforce) {
        FriendsManager.fetchFriends(isforce, new FindCallback<LeanchatUser>() {
            @Override
            public void done(List<LeanchatUser> list, AVException e) {
                refreshLayout.setRefreshing(false);
                itemAdapter.setUserList(list);
                itemAdapter.notifyDataSetChanged();
            }
        });
    }

    public void showDeleteDialog(final String memberId) {
        new AlertDialog.Builder(this).setMessage(R.string.contact_deleteContact)
                .setPositiveButton(R.string.common_sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog dialog1 = showSpinnerDialog();
                        LeanchatUser.getCurrentUser().removeFriend(memberId, new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                dialog1.dismiss();
                                if (filterException(e)) {
                                    getMembers(true);
                                }
                            }
                        });
                    }
                }).setNegativeButton(R.string.chat_common_cancel, null).show();
    }

    public void onEvent(ContactRefreshEvent event) {
        getMembers(true);
    }
    public void onEvent(InvitationEvent event) {
        AddRequestManager.getInstance().unreadRequestsIncrement();

    }
    public void onEvent(ContactItemClickEvent event) {
        Intent intent = new Intent(this, ChatRoomActivity.class);
        intent.putExtra(LCIMConstants.PEER_ID, event.memberId);
        intent.putExtra("CHAT_TITLE",event.toName);
        intent.putExtra("TO_USERID",event.memberId);
        startActivity(intent);
        this.finish();
    }
    public void onEvent(ContactItemLongClickEvent event) {
        showDeleteDialog(event.memberId);
    }

    /**
     * 处理 LetterView 发送过来的 MemberLetterEvent
     * 会通过 MembersAdapter 获取应该要跳转到的位置，然后跳转
     */
    public void onEvent(MemberLetterEvent event) {
        Character targetChar = Character.toLowerCase(event.letter);
        if (itemAdapter.getIndexMap().containsKey(targetChar)) {
            int index = itemAdapter.getIndexMap().get(targetChar);
            if (index > 0 && index < itemAdapter.getItemCount()) {
                // 此处 index + 1 是因为 ContactsAdapter 有 header
                layoutManager.scrollToPositionWithOffset(index + 1, 0);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
    @Override
    public void onResume() {
        super.onResume();
    }
}








