package com.muki.parttimejob.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.SelectJobActivity;
import com.muki.parttimejob.adapter.HeaderChannelAdapter;
import com.muki.parttimejob.entity.ChannelEntity;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ToastUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sunfusheng on 16/4/20.
 */
public class HeaderChannelView extends HeaderViewInterface<List<ChannelEntity>> {
    private int number =0;
    private boolean isRunning=true;
    private  String[]  users=new String[5];
    @Bind(R.id.gv_channel)
    FixedGridView gvChannel;
    @Bind(R.id.textview_auto_roll)
    AutoVerticalScrollTextView  verticalScrollTV;
    Context  context;
    public HeaderChannelView(Activity context) {
        super(context);
        this.context=context;
    }
    @Override
    protected void getView(List<ChannelEntity> list, ListView listView) {
        View view = mInflate.inflate(R.layout.header_channel_layout, listView, false);
        ButterKnife.bind(this, view);
        dealWithTheView(list);
        listView.addHeaderView(view);
        getUserArray();
    }
    private void dealWithTheView(final List<ChannelEntity> list) {
        if (list == null || list.size() < 2) return;
        int size = list.size();
        if (size <= 4) {
            gvChannel.setNumColumns(size);
        } else if (size == 6) {
            gvChannel.setNumColumns(3);
        } else if (size == 8) {
            gvChannel.setNumColumns(4);
        } else {
            return;
        }
        final HeaderChannelAdapter adapter = new HeaderChannelAdapter(mActivity, list);
        gvChannel.setAdapter(adapter);
        gvChannel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToActivity(adapter.getItem(position).getTitle());
            }
        });
    }
    public  void   goToActivity(String  jobType){
        Intent intent=new Intent(context, SelectJobActivity.class);
        intent.putExtra("JOB_TYPE",jobType);
        context.startActivity(intent);
    }
    public  void   getUserArray( ){
        AVQuery<LeanchatUser> query = new AVQuery<>("_User");
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.orderByDescending("createdAt");
        query.limit(5);// 最多返回 10 条结果
        query.findInBackground(new FindCallback<LeanchatUser>() {
            @Override
            public void done(List<LeanchatUser> list, AVException e) {
                if(list!=null){
                    for (int  i=0;i<list.size();i++){
                        if (TextUtils.isEmpty(list.get(i).getUSER_NICKNAME())){
                            users[i]=list.get(i).getUSER_NICKNAME();
                        }else{
                            users[i]=list.get(i).getObjectId().substring(0,12);
                        }
                    }new Thread(){
                        @Override
                        public void run() {
                            while (isRunning){
                                SystemClock.sleep(3000);
                                handler.sendEmptyMessage(199);
                            }
                        }
                    }.start();
                }
            }
        });
    }
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 199) {
                verticalScrollTV.next();
                number++;
                verticalScrollTV.setText(users[number%users.length]+"报名试玩了App");
            }
        }
    };
}
