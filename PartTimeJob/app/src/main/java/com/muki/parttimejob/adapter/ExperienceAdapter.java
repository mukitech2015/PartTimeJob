package com.muki.parttimejob.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by muki on 2016/10/31.
 */

public class ExperienceAdapter   extends BaseAdapter {
    private List<AVObject> objectList;
    private Context context;
    private LayoutInflater mInflater;
    public  ExperienceAdapter(List<AVObject> objectList,Context context){
        this.objectList=objectList;
        this.context=context;
        mInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return objectList==null?0:objectList.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return  position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewholder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.layout_exper_item, parent, false);
            viewholder=new ViewHolder();
            convertView.setTag(viewholder);
        }else{
            viewholder = (ViewHolder)convertView.getTag();//取出ViewHolder对象
        }
        viewholder.experDateTv= (TextView) convertView.findViewById(R.id.experDateTv);
        viewholder.experNameTv= (TextView) convertView.findViewById(R.id.experNameTv);
        viewholder.experContentTv= (TextView) convertView.findViewById(R.id.experContentTv);
        viewholder.experNameTv.setText(objectList.get(position).getString("user_Info_Record_Address"));
        SimpleDateFormat formatter = new    SimpleDateFormat("yyyy-MM-dd");
        viewholder.experContentTv.setText(objectList.get(position).getString("user_Info_Record"));
        Date start_time=objectList.get(position).getDate("user_Info_Start_Date");
        Date end_time=objectList.get(position).getDate("user_Info_End_Date");
        if(start_time!=null&&end_time!=null){
            viewholder.experDateTv.setText(formatter.format(start_time)+"—"+formatter.format(end_time));
        }
        return convertView;
    }
    public    void   changeAdapterData(List<AVObject> objectList){
        this.objectList=objectList;
        notifyDataSetChanged();
    }
    class  ViewHolder{
        private TextView  experDateTv;
        private TextView  experNameTv;
        private TextView  experContentTv;
    }
}
