package com.muki.parttimejob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.entity.City;

import java.util.ArrayList;
import java.util.List;

/**
 * author zaaach on 2016/1/26.
 */
public class HotCityGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<City> mCities;
    public HotCityGridAdapter(Context context) {
        this.mContext = context;
        mCities = new ArrayList<>();
        mCities.add(new City("北京",R.drawable.home_location_beijing));
        mCities.add(new City("上海",R.drawable.home_location_shanghai));
        mCities.add(new City("广州",R.drawable.home_location_guangzhou));
        mCities.add(new City("深圳",R.drawable.home_location_shenzhen));
        mCities.add(new City("杭州",R.drawable.home_location_hangzhou));
        mCities.add(new City("天津",R.drawable.home_location_tianjin));
    }
    @Override
    public int getCount() {
        return mCities == null ? 0 : mCities.size();
    }
    @Override
    public String getItem(int position) {
        return mCities == null ? null : mCities.get(position).getName();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        HotCityViewHolder holder;
        if (view == null){
            view = LayoutInflater.from(mContext).inflate(R.layout.item_hot_city_gridview, parent, false);
            holder = new HotCityViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tv_hot_city_name);
            holder.cityImgIv= (ImageView) view.findViewById(R.id.cityImgIv);
            view.setTag(holder);
        }else{
            holder = (HotCityViewHolder) view.getTag();
        }
        holder.cityImgIv.setImageResource(mCities.get(position).getCityImg());
        holder.name.setText(mCities.get(position).getName());
        return view;
    }
    public static class HotCityViewHolder{
        TextView name;
        ImageView cityImgIv;
    }
}
