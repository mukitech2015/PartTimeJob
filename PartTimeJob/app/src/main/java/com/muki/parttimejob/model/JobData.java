package com.muki.parttimejob.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by muki on 2016/12/5.
 */

public class JobData implements Serializable {

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    private Job  job;
    private Boolean   job_IsAtHome;

    public Boolean getJob_IsAtHome() {
        return job_IsAtHome;
    }

    public void setJob_IsAtHome(Boolean job_IsAtHome) {
        this.job_IsAtHome = job_IsAtHome;
    }

    private boolean isNoData = false;
    private int height;
    private String  jobType;

    public Date getJobDate() {
        return jobDate;
    }

    public void setJobDate(Date jobDate) {
        this.jobDate = jobDate;
    }

    private Date  jobDate;
    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    private String   jobName;
    private double  latitude;
    public   JobData(){

    }
    public JobData(Job job, Date  jobDate,String   jobType,String   jobName, double latitude, double longitude, int hot, String address, String businessName, boolean businessStyle, boolean isSalGuee, boolean isVip, Date endTime, Date startTime, String businessLogo) {
        this.jobDate=jobDate;
        this.jobName=jobName;
        this.job=job;
        this.latitude = latitude;
        this.longitude = longitude;
        this.hot = hot;
        this.address = address;
        this.businessName = businessName;
        this.businessStyle = businessStyle;
        this.isSalGuee = isSalGuee;
        this.isVip = isVip;
        this.endTime = endTime;
        this.startTime = startTime;
        this.businessLogo = businessLogo;
        this.jobType=jobType;
    }

    private double  longitude;

    private int  hot;
    private String  address;
    private String  businessName;
    private boolean  businessStyle;
    private boolean  isSalGuee;
    private boolean  isVip;
    private Date  startTime;
    private Date  endTime;
    private String businessLogo;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public boolean isBusinessStyle() {
        return businessStyle;
    }

    public void setBusinessStyle(boolean businessStyle) {
        this.businessStyle = businessStyle;
    }

    public boolean isSalGuee() {
        return isSalGuee;
    }

    public void setSalGuee(boolean salGuee) {
        isSalGuee = salGuee;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isNoData() {
        return isNoData;
    }

    public void setNoData(boolean noData) {
        isNoData = noData;
    }

}














