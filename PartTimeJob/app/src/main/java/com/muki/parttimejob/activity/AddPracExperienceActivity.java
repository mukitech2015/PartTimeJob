package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

import org.feezu.liuli.timeselector.TimeSelector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/1.
 */

public class AddPracExperienceActivity extends AppCompatActivity {
     @Bind(R.id.practiceStartTimeTv)
     TextView  practiceStartTimeTv;
    @Bind(R.id.practiceStartTimeLat)
    RelativeLayout  practiceStartTimeLat;
    @Bind(R.id.practiceEndTimeTv)
    TextView  practiceEndTimeTv;
    @Bind(R.id.practiceEndTimeLat)
    RelativeLayout  practiceEndTimeLat;
    @Bind(R.id.practicePositionEt)
     EditText   practicePositionEt;
    @Bind(R.id.practiceContentEt)
    EditText  practiceContentEt;
    @Bind(R.id.practiceSaveLat)
    RelativeLayout  practiceSaveLat;
    @Bind(R.id.practiceBackLat)
    LinearLayout  practiceBackLat;
    private TimeSelector timeSelector;
    private  int   witchTime=-1;
    private  String  practiceId;
    Date getDateWithDateString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent   intent=getIntent();
        if (intent!=null){
            String  startTime=intent.getStringExtra("user_Info_Start_Date");
            String  endTime=intent.getStringExtra("user_Info_End_Date");
            String  practiceContent=intent.getStringExtra("user_Info_Record");
            String  practicePosition=intent.getStringExtra("user_Info_Record_Address");
            practiceId=intent.getStringExtra("objectId");
            practiceStartTimeTv.setText(startTime);
            practiceEndTimeTv.setText(endTime);
            practiceContentEt.setText(practiceContent);
            practicePositionEt.setText(practicePosition);
        }
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_addpartexperirnce);
        ButterKnife.bind(this);
        practiceBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        timeSelector = new TimeSelector(this, new TimeSelector.ResultHandler() {
            @Override
            public void handle(String time) {
                  if(witchTime==0){
                      practiceStartTimeTv.setText(time.replaceAll("00:00",""));
                  }else if (witchTime==1){
                      practiceEndTimeTv.setText(time.replaceAll("00:00",""));
                  }
            }
        }, "1950-01-01 00:00", "2022-12-31 00:00");
        practiceStartTimeLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                witchTime=0;
                timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                timeSelector.show();
            }
        });
        practiceEndTimeLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                witchTime=1;
                timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                timeSelector.show();
            }
        });
        practiceSaveLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  practicePosition=practicePositionEt.getText().toString().trim();
                String  practiceContent=practiceContentEt.getText().toString().trim();
                String  practiceStartTime=practiceStartTimeTv.getText().toString().trim()+" 00:00:00";
                String  practiceEndTime=practiceEndTimeTv.getText().toString().trim()+" 00:00:00";
                if (TextUtils.isEmpty(practicePosition)){
                    Toast.makeText(AddPracExperienceActivity.this,"请填写职位",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty( practiceContent)){
                    Toast.makeText(AddPracExperienceActivity.this,"请填写工作内容",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(practiceStartTime)){
                    Toast.makeText(AddPracExperienceActivity.this,"请填写工作开始时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(practiceEndTime)){
                    Toast.makeText(AddPracExperienceActivity.this,"请填写工作结束时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(getDateWithDateString(practiceEndTime+"00:00:00").getTime()<=getDateWithDateString(practiceStartTime+" 00:00:00").getTime()){
                    Toast.makeText(AddPracExperienceActivity.this,"工作结束时间不能小于等于工作开始时间",Toast.LENGTH_SHORT).show();
                    return;
                }
                savePracExper(practicePosition,practiceContent,practiceStartTime,practiceEndTime);
            }
        });
    }
    public  void   savePracExper(final String  practicePosition, final String  practiceContent , final String  practiceStartTime , final String  practiceEndTime  ){
         if(TextUtils.isEmpty(practiceId)){
             final AVObject   avObject=new AVObject("User_Info");
             avObject.put("user_Info_Record_Address",practicePosition);
             avObject.put("user_Info_Start_Date",getDateWithDateString(practiceStartTime));
             avObject.put("user_Info_End_Date",getDateWithDateString(practiceEndTime));
             avObject.put("user_Info_Record_Type","实践经历");
             avObject.put("user_Info_Record",practiceContent);
             avObject.saveInBackground(new SaveCallback() {
                 @Override
                 public void done(AVException e) {
                     if (e==null){
                         AVRelation<AVObject> relation = LeanchatUser.getCurrentUser().getRelation("user_Info_relation");// 新建一个 AVRelation
                         relation.add(avObject);
                         LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                             @Override
                             public void done(AVException e) {
                                 finish();
                             }
                         });
                     }else{
                         Log.e("User_Info_Record_Date",e.getMessage()+"");
                     }
                 }
             });
         }else{
             AVObject  practiceObj=AVObject.createWithoutData("User_Info",practiceId);
             practiceObj.fetchInBackground(new GetCallback<AVObject>() {
                 @Override
                 public void done(AVObject avObject, AVException e) {
                      if(e==null){
                          avObject.put("user_Info_Record_Address",practicePosition);
                          avObject.put("user_Info_Start_Date",getDateWithDateString(practiceStartTime));
                          avObject.put("user_Info_End_Date",getDateWithDateString(practiceEndTime));
                          avObject.put("user_Info_Record_Type","实践经历");
                          avObject.put("user_Info_Record",practiceContent);
                          avObject.saveInBackground(new SaveCallback() {
                              @Override
                              public void done(AVException e) {
                                  finish();
                              }
                          });
                      }
                 }
             });
         }
    }
}









