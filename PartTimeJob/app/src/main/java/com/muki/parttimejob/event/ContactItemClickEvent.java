package com.muki.parttimejob.event;

/**
 * Created by wli on 15/11/26.
 */
public class ContactItemClickEvent {
  public String memberId;
  public String toName;
  public ContactItemClickEvent(String id,String toName) {
    memberId = id;
    this.toName=toName;
  }
}
