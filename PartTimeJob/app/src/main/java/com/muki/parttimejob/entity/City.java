package com.muki.parttimejob.entity;

/**
 * author zaaach on 2016/1/26.
 */
public class City {
    private String name;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
    public City(String name,String  str) {
        this.name = name;
        this.str=str;
    }
    private String  str;
    public int getCityImg() {
        return cityImg;
    }
    public void setCityImg(int cityImg) {
        this.cityImg = cityImg;
    }

    private  int  cityImg;

    public City() {}

    public City(String name, int  cityImg) {
        this.name = name;
        this.cityImg =cityImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
