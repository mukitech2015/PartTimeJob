package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.baoyz.actionsheet.ActionSheet;
import com.bumptech.glide.Glide;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.ScrollLvAdapter;
import com.muki.parttimejob.entity.ImageInfo;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ImageUtil;
import com.muki.parttimejob.utils.TABOperation;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.FullyLinearLayoutManager;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;

import org.feezu.liuli.timeselector.TimeSelector;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by muki on 2016/9/21.
 */
public class UserInfoNextActivity   extends AppCompatActivity implements View.OnClickListener{
    private EditText univerEt;
    private EditText  colleagueEt;
    private EditText  majorEt;
    private TextView eduTimeTv;
    private TextView  eduTv;
    private ImageView stuCardIv;
    private RelativeLayout  educationLat;
    private TimeSelector timeSelector;
    private  RelativeLayout   eduTimeLat;
    private  RelativeLayout  submitRat;
    private   String  education;
    private String major;
    private String eduTime;
    private String school;
    private String university;
    private String stuCard;
    private  String   stuCardUrl;
    private  RelativeLayout   nextInfoBackLat;
    private ScrollLvAdapter scrollLvAdapter;
    List<ImageInfo>   imageObjList=new ArrayList<>();
    List<AVObject>    avObjectList=new ArrayList<>();
    /**
     * ============================================================================================
     */
    String   userAvatar;
    String   sex;
    String   phone;
    String   userHeight;
    String   userWeight;
    String  nickName;
    String  birth;
    int   age;
    RecyclerView   othetCertificateRv;
    TextView    schoolIsAuthenTv;
    private  int   selectWhich=-1;
    private  List<String>  certificateList=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_userinfo_next);
        initViews();
        getDatas();
    }
    public void  getCertificateImages( ){
        AVRelation<AVObject> relation =LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
        AVQuery<AVObject> query = relation.getQuery();
        query.whereEqualTo("user_Image_Type","其他证书");
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
               if(e==null){
                   avObjectList.clear();
                   avObjectList.addAll(list);
                   imageObjList.clear();
                   for(AVObject avObject:avObjectList){
                       ImageInfo  imageInfo=new ImageInfo();
                       imageInfo.setBigImageUrl(avObject.getAVFile("user_Image").getUrl());
                       imageInfo.setThumbnailUrl(avObject.getAVFile("user_Thumbnail_Image").getUrl());
                       imageObjList.add(imageInfo);
                   }
                   setScrollLvAdapter(imageObjList);
               }
            }
        });
    }
    int  schoolIsAuthenState=LeanchatUser.getCurrentUser().getUSER_SCHOOL_ISAUTHENTICATION();
    public  void    getDatas(  ){
        colleagueEt.setText(LeanchatUser.getCurrentUser().getUSER_COLLEGE());
        univerEt.setText(LeanchatUser.getCurrentUser().getUSER_SCHOOL());
        majorEt.setText(LeanchatUser.getCurrentUser().getUSER_MAJOR());
        eduTimeTv.setText(LeanchatUser.getCurrentUser().getUSER_ADMISSION());
        eduTv.setText(LeanchatUser.getCurrentUser().getUSER_DEGREE());
        if(schoolIsAuthenState==0){
            schoolIsAuthenTv.setText("未认证");
        }else if(schoolIsAuthenState==1){
            schoolIsAuthenTv.setText("已认证");
            colleagueEt.setFocusable(false);
            univerEt.setFocusable(false);
            majorEt.setFocusable(false);
        }else if(schoolIsAuthenState==2){
            schoolIsAuthenTv.setText("审核中");
            colleagueEt.setFocusable(false);
            univerEt.setFocusable(false);
            majorEt.setFocusable(false);
        }else if(schoolIsAuthenState==3){
            schoolIsAuthenTv.setText("审核未通过");
        }
        AVRelation<AVObject> user_image_infoAVRelation=LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
        AVQuery<AVObject> query =user_image_infoAVRelation.getQuery();
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(e==null){
                    for (int i=0;i<list.size();i++){
                        if(list.get(i).getString("user_Image_Type").equals("学生证")){
                            AVFile avFile=list.get(i).getAVFile("user_Image");
                            stuCardUrl=avFile.getUrl();
                            UpLoadImageUtil.loadImage(UserInfoNextActivity.this,avFile.getUrl(),stuCardIv);
                        }
                    }
                }
            }
        });
        getCertificateImages();
    }
    protected void initViews() {
        othetCertificateRv= (RecyclerView) findViewById(R.id.othetCertificateRv);
        FullyLinearLayoutManager linearLayoutManager=new FullyLinearLayoutManager(this);
        othetCertificateRv.setNestedScrollingEnabled(false);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        othetCertificateRv.setLayoutManager(linearLayoutManager);
        schoolIsAuthenTv= (TextView) findViewById(R.id.schoolIsAuthenTv);
        submitRat= (RelativeLayout) findViewById(R.id.userinfo_submit_lat);
        eduTimeLat= (RelativeLayout) findViewById(R.id.userinfo_eduTime_lat);
        educationLat= (RelativeLayout) findViewById(R.id.userinfo_education_lat);
        stuCardIv= (ImageView) findViewById(R.id.userinfo_stuCardIv);
        univerEt= (EditText) findViewById(R.id.userinfo_univerEt);
        colleagueEt= (EditText) findViewById(R.id.userinfo_colleagueEt);
        majorEt= (EditText) findViewById(R.id.userinfo_majorEt);
        eduTimeTv= (TextView) findViewById(R.id.userinfo_eduTimeTv);
        eduTv= (TextView) findViewById(R.id.userinfo_eduTv);
        schoolIsAuthenTv= (TextView) findViewById(R.id.schoolIsAuthenTv);
        nextInfoBackLat= (RelativeLayout) findViewById(R.id.nextInfoBackLat);
        nextInfoBackLat.setOnClickListener(this);
        stuCardIv.setOnClickListener(this);
        submitRat.setOnClickListener(this);
        educationLat.setOnClickListener(this);
        eduTimeLat.setOnClickListener(this);
        schoolIsAuthenTv.setOnClickListener(this);
        timeSelector = new TimeSelector(this, new TimeSelector.ResultHandler() {
            @Override
            public void handle(String time) {
                eduTimeTv.setText(time.replaceAll("00:00",""));
            }
        }, "2005-01-01 00:00", "2018-12-31 00:00");
    }
    public    void   setScrollLvAdapter(final List<ImageInfo>   imageInfoList ){
        if(scrollLvAdapter==null){
            scrollLvAdapter=new ScrollLvAdapter(imageInfoList,UserInfoNextActivity.this);
            othetCertificateRv.setAdapter(scrollLvAdapter);
        }else{
            scrollLvAdapter.changeAdapter(imageInfoList);
        }
        scrollLvAdapter.setOnItemLongClickListener(new ScrollLvAdapter.OnRecycleViewItemLongClickListener() {
            @Override
            public void OnItemLongClick(View view, int position) {
                if (position!=imageInfoList.size()) {
                    deleteOthetCert(position);
                }
            }
        });
        scrollLvAdapter.setOnItemClickListener(new ScrollLvAdapter.OnRecycleViewItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                if (position==imageInfoList.size()) {
                    selectWhich=1;
                    setMutiSelectPicture();
                }
            }
        });
    }
    public   void   deleteOthetCert(final int  position){
        new  AlertDialog.Builder(this).setIcon(R.drawable.ic_launcher)
                .setTitle("删除照片")
                .setMessage("确定删除该照片")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        avObjectList.get(position).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(AVException e) {
                                if(e==null){
                                    imageObjList.remove(imageObjList.get(position));
                                    scrollLvAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent=getIntent();
        userAvatar=intent.getStringExtra("userAvatar");
        sex=intent.getStringExtra("sex");
        phone=intent.getStringExtra("phone");
        userHeight=intent.getStringExtra("userHeight");
        userWeight=intent.getStringExtra("userWeight");
        nickName=intent.getStringExtra("nickName");
        birth=intent.getStringExtra("birth");
        age=intent.getIntExtra("age",0);
    }
    public    void  setMutiSelectPicture(){
        if(selectWhich==0){
            AndroidImagePicker.getInstance().pickSingle(UserInfoNextActivity.this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
                @Override
                public void onImagePickComplete(List<ImageItem> items) {
                    if(items != null && items.size() > 0){
                        Glide.with(UserInfoNextActivity.this).load(items.get(0).path).into(stuCardIv);
                        stuCard=items.get(0).path;
                    }
                }
            });
        }else if(selectWhich==1){
            AndroidImagePicker.getInstance().setSelectLimit(4);
            AndroidImagePicker.getInstance().pickMulti(UserInfoNextActivity.this, true,  new AndroidImagePicker.OnImagePickCompleteListener() {
                @Override
                public void onImagePickComplete(List<ImageItem> items) {
                    if(items != null && items.size() > 0){
                        certificateList.clear();
                        for (ImageItem  imageItem:items){
                            certificateList.add(imageItem.path);
                        }
                        List<ImageInfo>  imageInfoList=new ArrayList<>();
                        for (String  image:certificateList){
                            ImageInfo  imageInfo=new ImageInfo();
                            imageInfo.setBigImageUrl(image);
                            imageInfo.setThumbnailUrl(image);
                            imageInfoList.add(imageInfo);
                        }
                        imageInfoList.addAll(0,imageObjList);
                        setScrollLvAdapter(imageInfoList);
                    }
                }
            });
        }
    }
    public    void  setMutiSelectEducation(){
        ActionSheet.createBuilder(UserInfoNextActivity.this,getSupportFragmentManager())
                .setCancelButtonTitle("取消")
                .setOtherButtonTitles("博士", "研究生","本科","大专","中专","其他")
                .setCancelableOnTouchOutside(true)
                .setListener(new ActionSheet.ActionSheetListener() {
                    @Override
                    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

                    }
                    @Override
                    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                        switch (index){
                            case  0:
                                eduTv.setText("博士");
                                break;
                            case  1:
                                eduTv.setText("研究生");
                                break;
                            case  2:
                                eduTv.setText("本科");
                                break;
                            case  3:
                                eduTv.setText("大专");
                                break;
                            case  4:
                                eduTv.setText("中专");
                                break;
                            case  5:
                                eduTv.setText("其他");
                                break;
                        }
                    }
                }) .show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.userinfo_stuCardIv:
                selectWhich=0;
                setMutiSelectPicture();
                break;
            case  R.id.userinfo_education_lat:
                setMutiSelectEducation();
                break;
            case R.id.userinfo_eduTime_lat:
                timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                timeSelector.show();
                break;
            case  R.id.userinfo_submit_lat:
                try {
                    saveOtherCers(certificateList);
                    submitUserInfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case  R.id.nextInfoBackLat:
                finish();
                break;
        }
    }
    public   void   saveOtherCers(List<String>  stringList) throws FileNotFoundException {
        for (String  string:stringList){
            final AVFile avFileBig=AVFile.withAbsoluteLocalPath("bigImage.png",string);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // 获取图片的宽和高
            Bitmap bitmap = BitmapFactory.decodeFile(string, options);
            ThumbnailUtils.extractThumbnail(bitmap,50,50);
            options.inJustDecodeBounds = false;
            Bitmap bitmapThum = BitmapFactory.decodeFile(string, options);
            String newPath= ImageUtil.getAbsoluteImagePath(Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(), bitmapThum , null,null)),UserInfoNextActivity.this);
            final AVFile avFile=AVFile.withAbsoluteLocalPath("Image.png",newPath);
            try {
                AVObject.saveFileBeforeSave(Arrays.asList(avFile, avFileBig), false, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e==null){
                            final AVObject imageObj=new AVObject("User_Image_Info");
                            imageObj.put("user_Thumbnail_Image",avFile);
                            imageObj.put("user_Image",avFileBig);
                            imageObj.put("user_Image_Type","其他证书");
                            imageObj.put("user_pointer",LeanchatUser.getCurrentUser());
                            AVObject.saveAllInBackground(Arrays.asList(imageObj), new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> userImageInfoRelation=LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
                                    userImageInfoRelation.add(imageObj);
                                    LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            if(e==null){
                                                getCertificateImages();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } catch (AVException e) {
                e.printStackTrace();
            }
        }
    }
    public   void   submitUserInfo(  ) throws AVException, FileNotFoundException {
        university=univerEt.getText().toString().trim();
        education=eduTv.getText().toString().trim();
        eduTime=eduTimeTv.getText().toString().trim();
        major=majorEt.getText().toString().trim();
        school=colleagueEt.getText().toString().trim();
        if(TextUtils.isEmpty(university)){
            Toast.makeText(UserInfoNextActivity.this,"请填写学校",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(school)){
            Toast.makeText(UserInfoNextActivity.this,"请填写学院",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(major)){
            Toast.makeText(UserInfoNextActivity.this,"请填写专业",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(eduTime)){
            Toast.makeText(UserInfoNextActivity.this,"请填写入学时间",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(education)){
            Toast.makeText(UserInfoNextActivity.this,"请填写学历",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!TextUtils.isEmpty(stuCard)){
            saveStuCard(stuCard);
        }
        if (!TextUtils.isEmpty(userAvatar)){
            LeanchatUser.getCurrentUser().saveAvatar(userAvatar, new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if(e!=null){
                        Log.e("exception",e.getMessage());
                    }
                }
            });
        }
        TABOperation.updateUserInfo(this,university,eduTime,education,major,school,sex, userHeight, userWeight, nickName,birth);
    }
    public  void  saveStuCard( String    stuCard) throws FileNotFoundException {
        final AVFile stuCardFile=AVFile.withAbsoluteLocalPath("image.png",stuCard);
        stuCardFile.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    final AVObject  stuCardObj = new AVObject("User_Image_Info");
                    stuCardObj.put("user_Image_Type", "学生证");
                    stuCardObj.put("user_Image", stuCardFile);
                    stuCardObj.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            AVRelation<AVObject> relation =LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");// 新建一个 AVRelation
                            relation.add(stuCardObj);
                            LeanchatUser.getCurrentUser().saveInBackground();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
