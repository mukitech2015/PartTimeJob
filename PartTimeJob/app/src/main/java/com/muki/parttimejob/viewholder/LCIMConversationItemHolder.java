//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.muki.parttimejob.viewholder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVCallback;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMConversationQuery;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.AVIMReservedMessageType;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avos.avoscloud.im.v2.callback.AVIMMessagesQueryCallback;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ChatRoomActivity;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.LCIMConversationUtils;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cn.leancloud.chatkit.LCChatMessageInterface;
import cn.leancloud.chatkit.R.drawable;
import cn.leancloud.chatkit.R.id;
import cn.leancloud.chatkit.R.layout;
import cn.leancloud.chatkit.R.string;
import cn.leancloud.chatkit.cache.LCIMConversationItemCache;
import cn.leancloud.chatkit.event.LCIMConversationItemLongClickEvent;
import cn.leancloud.chatkit.utils.LCIMConstants;
import cn.leancloud.chatkit.utils.LCIMLogUtils;
import cn.leancloud.chatkit.viewholder.LCIMCommonViewHolder;
import de.greenrobot.event.EventBus;


public class LCIMConversationItemHolder extends LCIMCommonViewHolder {
    ImageView avatarView;
    TextView unreadView;
    TextView messageView;
    TextView timeView;
    TextView nameView;
    RelativeLayout avatarLayout;
    LinearLayout contentLayout;
    private String toName;
    public static ViewHolderCreator HOLDER_CREATOR = new ViewHolderCreator() {
        public LCIMConversationItemHolder createByViewGroupAndType(ViewGroup parent, int viewType) {
            return new LCIMConversationItemHolder(parent);
        }
    };

    public LCIMConversationItemHolder(ViewGroup root) {
        super(root.getContext(), root, layout.lcim_conversation_item);
        this.initView();
    }
    public void initView() {
        this.avatarView = (ImageView) this.itemView.findViewById(id.conversation_item_iv_avatar);
        this.nameView = (TextView) this.itemView.findViewById(id.conversation_item_tv_name);
        this.nameView.setTextSize(18);
        this.timeView = (TextView) this.itemView.findViewById(id.conversation_item_tv_time);
        this.unreadView = (TextView) this.itemView.findViewById(id.conversation_item_tv_unread);
        this.messageView = (TextView) this.itemView.findViewById(id.conversation_item_tv_message);
        this.avatarLayout = (RelativeLayout) this.itemView.findViewById(id.conversation_item_layout_avatar);
        this.contentLayout = (LinearLayout) this.itemView.findViewById(id.conversation_item_layout_content);
    }

    public void bindData(Object o) {
        this.reset();
        final AVIMConversation conversation = (AVIMConversation) o;
        if (null != conversation) {
            this.updateUnreadCount(conversation);
            this.updateLastMessageByConversation(conversation);
            if (null == conversation.getCreatedAt()) {
                Log.e("CONVERSATION", conversation.getConversationId());
                AVIMClient client = AVIMClient.getInstance( LeanchatUser.getCurrentUserId());
                AVIMConversationQuery query = client.getQuery();
                query.whereEqualTo("objectId",conversation.getConversationId());
                query.findInBackground(new AVIMConversationQueryCallback(){
                    @Override
                    public void done(List<AVIMConversation> convs,AVIMException e){
                        if(e==null){
                            if(convs!=null && !convs.isEmpty()){
                                AVIMConversation conv = convs.get(0);
                                LCIMConversationItemHolder.this.updateName(conv);
                                LCIMConversationItemHolder.this.updateIcon(conv);
                            }
                        }
                    }
                });

            } else {
                this.updateName(conversation);
                this.updateIcon(conversation);
            }
            this.itemView.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View v) {
                    Builder builder = new Builder(LCIMConversationItemHolder.this.getContext());
                    builder.setIcon(R.drawable.ic_launcher).setTitle("删除会话").setMessage("确定删除该会话吗？")
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EventBus.getDefault().post(new LCIMConversationItemLongClickEvent(conversation));
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            });
        }
    }

    private void reset() {
        this.avatarView.setImageResource(0);
        this.nameView.setText("");
        this.timeView.setText("");
        this.messageView.setText("");
        this.unreadView.setVisibility(View.INVISIBLE);
    }

    private void updateName(AVIMConversation conversation) {
        LCIMConversationUtils.getConversationName(conversation, new AVCallback<String>() {
            @Override
            protected void internalDone0(final String s, AVException e) {
                if (e == null) {
                    toName = s;
                    AVQuery<LeanchatUser> userAVQuery = AVQuery.getQuery(LeanchatUser.class);
                    userAVQuery.whereEqualTo("username", s);
                    userAVQuery.getFirstInBackground(new GetCallback<LeanchatUser>() {
                        @Override
                        public void done(LeanchatUser leanchatUser, AVException e) {
                            if (leanchatUser != null) {
                                if (!TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())) {
                                    toName = leanchatUser.getUSER_NICKNAME();
                                    LCIMConversationItemHolder.this.nameView.setText(leanchatUser.getUSER_NICKNAME());
                                } else {
                                    LCIMConversationItemHolder.this.nameView.setText(toName);
                                }
                            } else {
                                 LCIMConversationItemHolder.this.nameView.setText(toName);
                            }
                        }
                    });
                }
            }
        });
    }

    private void updateUnreadCount(AVIMConversation conversation) {
        int num = LCIMConversationItemCache.getInstance().getUnreadCount(conversation.getConversationId());
        this.unreadView.setText(num + "");
        this.unreadView.setVisibility(num > 0 ? View.VISIBLE : View.INVISIBLE);
    }
    private void updateLastMessageByConversation(final AVIMConversation conversation) {
        conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
            public void done(AVIMMessage avimMessage, AVIMException e) {
                if (null != avimMessage) {
                    LCIMConversationItemHolder.this.updateLastMessage(avimMessage);
                } else {
                    conversation.queryMessages(1, new AVIMMessagesQueryCallback() {
                        public void done(List<AVIMMessage> list, AVIMException e) {
                            if (null != e) {
                                LCIMLogUtils.logException(e);
                            }
                            if (null != list && !list.isEmpty()) {
                                AVIMMessage message = (AVIMMessage) list.get(0);
                                LCIMConversationItemHolder.this.updateLastMessage(message);
                            }
                        }
                    });
                }
            }
        });
    }
    public void getFromUserAvatar(AVIMMessage message) {
        if (null != message) {
            AVQuery<LeanchatUser> queryUser = AVObject.getQuery(LeanchatUser.class);
            queryUser.whereEqualTo("objectId", message.getFrom());
            queryUser.findInBackground(new FindCallback<LeanchatUser>() {
                @Override
                public void done(List<LeanchatUser> results, AVException e) {
                    if (results != null) {
                        for (LeanchatUser leanchatUser : results) {
                                UpLoadImageUtil.loadCropImage(getContext(), leanchatUser.getAvatarUrl(), LCIMConversationItemHolder.this.avatarView);
                            if (!TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())) {
                                nameView.setText(leanchatUser.getUSER_NICKNAME());
                                toName = leanchatUser.getUSER_NICKNAME();
                            } else {
                                toName = leanchatUser.getUSERNAME();
                                nameView.setText(leanchatUser.getUSERNAME());
                            }
                        }
                    }
                }
            });
        }
    }

    private void updateIcon(final AVIMConversation conversation) {
        if (null != conversation) {
            this.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    LCIMConversationItemHolder.this.onConversationItemClick(conversation);
                }
            });
            if (!conversation.isTransient() && conversation.getMembers().size() <= 2) {
                LCIMConversationUtils.getConversationPeerIcon(conversation, new AVCallback<String>() {
                    @Override
                    protected void internalDone0(String s, AVException e) {
                        if (e != null) {
                            LCIMLogUtils.logException(e);
                        }
                        if (!TextUtils.isEmpty(s)) {
                            UpLoadImageUtil.loadCropImage(getContext(), s, LCIMConversationItemHolder.this.avatarView);
                        } else {
                            LCIMConversationItemHolder.this.avatarView.setImageResource(drawable.lcim_default_avatar_icon);
                        }
                    }
                });
            } else {
                this.avatarView.setImageResource(drawable.lcim_group_icon);
            }
        }
    }

    private static CharSequence getMessageeShorthand(Context context, AVIMMessage message) {
        if (message instanceof AVIMTypedMessage) {
            AVIMReservedMessageType type = AVIMReservedMessageType.getAVIMReservedMessageType(((AVIMTypedMessage) message).getMessageType());
            switch (type.ordinal()) {
                case 1:
                    return ((AVIMTextMessage) message).getText();
                case 2:
                    return context.getString(string.lcim_message_shorthand_image);
                case 4:
                    return context.getString(string.lcim_message_shorthand_location);
                case 3:
                    return context.getString(string.lcim_message_shorthand_audio);
                default:
                    String shortHand = "";
                    if (message instanceof LCChatMessageInterface) {
                        LCChatMessageInterface messageInterface = (LCChatMessageInterface) message;
                        shortHand = messageInterface.getShorthand();
                    }
                    if (TextUtils.isEmpty(shortHand)) {
                        shortHand = context.getString(string.lcim_message_shorthand_unknown);
                    }
                    return shortHand;
            }
        } else {
            return message.getContent();
        }
    }

    private void updateLastMessage(AVIMMessage message) {
        if (null != message) {
            Date date = new Date(message.getTimestamp());
            SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
            this.timeView.setText(format.format(date));
            this.messageView.setText(getMessageeShorthand(this.getContext(), message));
            if (!message.getFrom().equals(LeanchatUser.getCurrentUserId())) {
                getFromUserAvatar(message);
            }
        }
    }
    private void onConversationItemClick(AVIMConversation conversation) {
        List<String> stringList = conversation.getMembers();
        String toUserId = "";
        for (String userId : stringList) {
            if (!userId.equals(LeanchatUser.getCurrentUserId())) {
                toUserId = userId;
            }
        }
        Intent conversationIntent = new Intent(this.getContext(), ChatRoomActivity.class);
        conversationIntent.putExtra("CHAT_TITLE", toName);
        conversationIntent.putExtra("TO_USERID", toUserId);
        conversationIntent.putExtra(LCIMConstants.CONVERSATION_ID, conversation.getConversationId());
        this.getContext().startActivity(conversationIntent);
    }
}
