package com.muki.parttimejob.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/31.
 */

public class WebActivity extends AppCompatActivity{
    @Bind(R.id.webView)
    WebView  webView;
    @Bind(R.id.webBackLat)
    LinearLayout   webBackLat;
    @Bind(R.id.webTitleTv)
    TextView   webTitleTv;
    @Bind(R.id.shareWebAppLat)
    RelativeLayout   shareWebAppLat;
    public ArrayList<SnsPlatform> platforms = new ArrayList<SnsPlatform>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_web);
        ButterKnife.bind(this);
        initPlatforms();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new InJavaScriptLocalObj(), "java_obj");
        webView.setWebViewClient(new BaseWebViewClient ());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                // TODO Auto-generated method stub
                webTitleTv.setText(title);
                super.onReceivedTitle(view, title);
            }
        });
        webBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });
        shareWebAppLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ShareAction(WebActivity.this).setDisplayList(SHARE_MEDIA.QQ,SHARE_MEDIA.QZONE,SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                        .withTitle("啄米兼职")
                        .withText("快和我一起做兼职吧")
                        .withMedia(new UMImage(WebActivity.this,R.drawable.ic_launcher))
                        .withTargetUrl(AppConfig.SHARE_APP)
                        .setCallback(umShareListener)
                        .open();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent  intent=getIntent();
        String  webUrl=intent.getStringExtra("WEB_URL");
        if (webUrl.equals(AppConfig.SHARE_APP)){
            shareWebAppLat.setVisibility(View.VISIBLE);
        }else{
            shareWebAppLat.setVisibility(View.GONE);
        }
        webView.loadUrl(webUrl);
    }
    private  class  BaseWebViewClient   extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            view.loadUrl("javascript:window.java_obj.getSource('<head>'+" +
                    "document.getElementsByTagName('html')[0].innerHTML+'</head>');");
            super.onPageFinished(view, url);
        }
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }
    }
    final class InJavaScriptLocalObj {
        @JavascriptInterface
        public void getSource(String html) {
            Log.e("html=", html);
        }
    }
    private void initPlatforms(){
        platforms.clear();
        for (SHARE_MEDIA e : SHARE_MEDIA.values()) {
            if (!e.toString().equals(SHARE_MEDIA.GENERIC.toString())){
                platforms.add(e.toSnsPlatform());
            }
        }
    }
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Log.d("plat","platform"+platform);

        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {

            if(t!=null){
                Log.d("throw","throw:"+t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {

        }
    };
}



