package com.muki.parttimejob.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.MainActivity;
import com.muki.parttimejob.activity.SearchJobActivity;
import com.muki.parttimejob.activity.SelectActivity;
import com.muki.parttimejob.adapter.SquireMainAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.entity.ChannelEntity;
import com.muki.parttimejob.entity.FilterData;
import com.muki.parttimejob.entity.FilterEntity;
import com.muki.parttimejob.event.SelectCityEvent;
import com.muki.parttimejob.model.BannerImage;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.JobData;
import com.muki.parttimejob.utils.ColorUtil;
import com.muki.parttimejob.utils.DensityUtil;
import com.muki.parttimejob.utils.ModelUtil;
import com.muki.parttimejob.widget.CircleRefreshLayout;
import com.muki.parttimejob.widget.FilterView;
import com.muki.parttimejob.widget.HeaderBannerView;
import com.muki.parttimejob.widget.HeaderChannelView;
import com.muki.parttimejob.widget.HeaderDividerView;
import com.muki.parttimejob.widget.HeaderFilterView;
import com.muki.parttimejob.widget.SmoothListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by muki on 2016/9/13.
 */
public class SquareFragment  extends Fragment implements SmoothListView.ISmoothListViewListener  {
    private  View   view;
    @Bind(R.id.listView)
    SmoothListView smoothListView;
    @Bind(R.id.filterView)
    public   FilterView filterView;
    @Bind(R.id.rl_bar)
    RelativeLayout rlBar;
    @Bind(R.id.view_title_bg)
    View viewTitleBg;
    @Bind(R.id.view_action_more_bg)
    View viewActionMoreBg;
    @Bind(R.id.fl_action_more)
    FrameLayout flActionMore;
    private Context mContext;
    @Bind(R.id.select_city_layout)
    RelativeLayout selectCityLat;
    private  final  int  CITY_RESULT=1000;
    @Bind(R.id.select_cityname_tv)
    TextView cityNameTv;
    private int mScreenHeight; // 屏幕高度
    private List<BannerImage> bannerList = new ArrayList<>(); // 广告数据
    private List<ChannelEntity> channelList = new ArrayList<>(); // 频道数据
    private List<JobData> joblList = new ArrayList<>(); // 频道数据
    private HeaderBannerView headerBannerView; // 广告视图
    private HeaderChannelView headerChannelView; // 频道视图
    private HeaderDividerView headerDividerView; // 分割线占位图
    private HeaderFilterView headerFilterView; // 分类筛选视图
    private FilterData filterData; // 筛选数据
    public SquireMainAdapter mAdapter; // 主页数据
    private View itemHeaderBannerView; // 从ListView获取的广告子View
    private View itemHeaderFilterView; // 从ListView获取的筛选子View
    private boolean isScrollIdle = true; // ListView是否在滑动
    private boolean isStickyTop = false; // 是否吸附在顶部
    private boolean isSmooth = false; // 没有吸附的前提下，是否在滑动
    private int titleViewHeight = 65; // 标题栏的高度
    private int filterPosition = -1; // 点击FilterView的位置：分类(0)、排序(1)、筛选(2)
    private int bannerViewHeight = 180; // 广告视图的高度
    private int bannerViewTopMargin; // 广告视图距离顶部的距离
    private int filterViewPosition = 4; // 筛选视图的位置
    private int filterViewTopMargin; // 筛选视图距离顶部的距离
    MainActivity   mainActivity;
    private CircleRefreshLayout mRefreshLayout ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_fg_squire, null);
        ButterKnife.bind(this,view);
        mContext=getActivity();
        mainActivity= (MainActivity) getActivity();
        initData();
        initView();
        initListener();
        EventBus.getDefault().register(this);
        return  view;
    }
    @Override
    public void onStart() {
        super.onStart();
    }
    private void initData() {
          mScreenHeight = DensityUtil.getWindowHeight(getActivity());
          filterData = new FilterData();
          filterData.setJobCitys(ModelUtil.getJobCityData(getActivity(),AppConfig.myCity));
          filterData.setJobTypes(ModelUtil.getJobTypeData());
          filterData.setJobOthers(ModelUtil.getJobOtherData());
        // 广告数据
          bannerList = ModelUtil.getBannerData();
        // 频道数据
          channelList = ModelUtil.getChannelDatas();
          joblList=ModelUtil.getAllJobDatas(AppConfig.myCity);
          getJobReceList();
    }
    private void initView() {
        mRefreshLayout = (CircleRefreshLayout) view.findViewById(R.id.refresh_layout);
        cityNameTv= (TextView) view.findViewById(R.id.select_cityname_tv);
        // 设置广告数据
        headerBannerView = new HeaderBannerView(getActivity());
        headerBannerView.fillView(bannerList, smoothListView);
        // 设置频道数据
        headerChannelView = new HeaderChannelView(getActivity());
        headerChannelView.fillView(channelList, smoothListView);
        // 设置分割线
        headerDividerView = new HeaderDividerView(getActivity());
        headerDividerView.fillView("", smoothListView);
        // 设置假FilterView数据
        headerFilterView = new HeaderFilterView(getActivity());
        headerFilterView.fillView(new Object(), smoothListView);
        // 设置真FilterView数据
        filterView.setFilterData(getActivity(), filterData);
        filterView.setVisibility(View.GONE);
        cityNameTv.setText(AppConfig.myCity);
        filterView.tvCityTitle.setText(AppConfig.myCity);
        headerFilterView.getFilterView().tvCityTitle.setText(AppConfig.myCity);
   //      设置ListView数据
        mAdapter = new SquireMainAdapter(getActivity(), joblList);
        smoothListView.setAdapter(mAdapter);
        filterViewPosition = smoothListView.getHeaderViewsCount() - 1;
        mainActivity.setSelectPage(new MainActivity.SelectPageListener() {
            @Override
            public void onSelectPageAction() {
                filterView.setVisibility(View.GONE);
                rlBar.getBackground().setAlpha(50);
            }
        });
    }
    public List<Job> jobRList=new ArrayList<>();
    public  void  getJobReceList(  ){
        AVQuery<Job> query = new AVQuery<>("Job");
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.include("business_pointer");
        final AVGeoPoint point = new AVGeoPoint(AppConfig.mylatitude, AppConfig.mylongitude);
        query.whereNear("job_DistanceGEO", point);
        query.limit(10);// 最多返回 10 条结果
        query.findInBackground(new FindCallback<Job>() {
            @Override
            public void done(List<Job> list, AVException e) {
                if(e==null){
                    jobRList.clear();
                    jobRList.addAll(list);
                }
            }
        });
    }
    private void initListener() {
        mRefreshLayout.setOnRefreshListener(new CircleRefreshLayout.OnCircleRefreshListener() {
            @Override
            public void completeRefresh() {
            }
            @Override
            public void refreshing() {
                mRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        initData();
                        mRefreshLayout.finishRefreshing();
                    }
                }, 1000);
            }
        });
        // (假的ListView头部展示的)筛选视图点击
        headerFilterView.setOnFilterClickListener(new HeaderFilterView.OnFilterClickListener() {
            @Override
            public void onFilterClick(int position) {
                filterPosition = position;
                isSmooth = true;
                smoothListView.smoothScrollToPositionFromTop(filterViewPosition, DensityUtil.dip2px(mContext, titleViewHeight));
            }
        });
        // (真正的)筛选视图点击
        filterView.setOnFilterClickListener(new FilterView.OnFilterClickListener() {
            @Override
            public void onFilterClick(int position) {
                if (isStickyTop) {
                    filterPosition = position;
                    filterView.show(position);
                    if (titleViewHeight - 3 > filterViewTopMargin || filterViewTopMargin > titleViewHeight + 3) {
                        smoothListView.smoothScrollToPositionFromTop(filterViewPosition, DensityUtil.dip2px(mContext, titleViewHeight));
                    }
                }
            }
        });
        filterView.setOnItemJobCityClickListener(new FilterView.OnItemJobCityClickListener() {
            @Override
            public void onItemJobCityClick(FilterEntity rightEntity) {
                  fillAdapter(ModelUtil.getJobCityData(rightEntity,AppConfig.myCity));
            }
        });
        filterView.setOnItemJobTypeClickListener(new FilterView.OnItemJobTypeClickListener() {
            @Override
            public void onItemJobTypeClick(FilterEntity entity) {
                  fillAdapter(ModelUtil.getJobTypeData(entity,AppConfig.myCity));
            }
        });
       filterView.setOnItemJobOtherClickListener(new FilterView.OnItemJobOtherClickListener() {
           @Override
           public void onItemJobOtherClick(FilterEntity entity) {
               if (entity.getKey().equals("最新发布")){
                   fillAdapter(ModelUtil.getNewReleJobDatas());
               }else{
                   fillAdapter(ModelUtil.getReceJobDatas(SquareFragment.this));
               }
           }
       });
        selectCityLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent    intent=new Intent(getActivity(),SelectActivity.class);
                startActivity(intent);
            }
        });
        flActionMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchIntent=new Intent(getActivity(), SearchJobActivity.class);
                startActivity(searchIntent);
            }
        });
        smoothListView.setSmoothListViewListener(this);
        smoothListView.setOnScrollListener(new SmoothListView.OnSmoothScrollListener() {
            @Override
            public void onSmoothScrolling(View view) {}

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                isScrollIdle = (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE);
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (isScrollIdle && bannerViewTopMargin < 0) return;
                // 获取广告头部View、自身的高度、距离顶部的高度
                if (itemHeaderBannerView == null) {
                    itemHeaderBannerView = smoothListView.getChildAt(1-firstVisibleItem);
                }
                if (itemHeaderBannerView != null) {
                    bannerViewTopMargin = DensityUtil.px2dip(mContext, itemHeaderBannerView.getTop());
                    bannerViewHeight = DensityUtil.px2dip(mContext, itemHeaderBannerView.getHeight());
                }
                // 获取筛选View、距离顶部的高度
                if (itemHeaderFilterView == null) {
                    itemHeaderFilterView = smoothListView.getChildAt(filterViewPosition - firstVisibleItem);
                }
                if (itemHeaderFilterView != null) {
                    filterViewTopMargin = DensityUtil.px2dip(mContext, itemHeaderFilterView.getTop());
                }
                // 处理筛选是否吸附在顶部
                if (filterViewTopMargin <= titleViewHeight || firstVisibleItem > filterViewPosition) {
                    isStickyTop = true; // 吸附在顶部
                    filterView.setVisibility(View.VISIBLE);
                } else {
                    isStickyTop = false; // 没有吸附在顶部
                    filterView.setVisibility(View.GONE);
                }
                if (isSmooth && isStickyTop) {
                    isSmooth = false;
                    filterView.show(filterPosition);
                }
                // 处理标题栏颜色渐变
                handleTitleBarColorEvaluate();
            }
        });
    }
    // 处理标题栏颜色渐变
    private void handleTitleBarColorEvaluate() {
        float fraction;
        if (bannerViewTopMargin > 0) {
            fraction = 1f - bannerViewTopMargin * 1f / 60;
            if (fraction < 0f)
                fraction = 0f;

            if( fraction < 0.5f){
                fraction = 0.5f;
            }
            rlBar.getBackground().setAlpha( (int)(fraction * 100));
            return ;
        }
        float space = Math.abs(bannerViewTopMargin) * 1f;
        fraction = space / (bannerViewHeight - titleViewHeight);
        if (fraction < 0f) fraction = 0f;
        if (fraction > 1f) fraction = 1f;
        rlBar.setAlpha(1f);
        if (fraction >= 1f || isStickyTop) {
            isStickyTop = true;
            viewTitleBg.setAlpha(0f);
            viewActionMoreBg.setAlpha(0f);
            rlBar.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            viewTitleBg.setAlpha(1f - fraction);
            viewActionMoreBg.setAlpha(1f - fraction);
            rlBar.setBackgroundColor(ColorUtil.getNewColorByStartEndColor(mContext, fraction, R.color.transparent, R.color.colorPrimary));
        }
    }
    public void onEvent(SelectCityEvent event) {
        String  cityName=event.getCity();
        cityNameTv.setText(cityName);
        filterView.tvCityTitle.setText(cityName);
        headerFilterView.getFilterView().tvCityTitle.setText(cityName);
        joblList=ModelUtil.getAllJobDatas(cityName);
        mAdapter = new SquireMainAdapter(getActivity(), joblList);
        smoothListView.setAdapter(mAdapter);
        filterData.setJobCitys(ModelUtil.getJobCityData(getActivity(),cityName));
        filterView.setFilterData(getActivity(), filterData);
    }
    private void fillAdapter(List<JobData> list) {
        if (list == null || list.size() == 0) {
            List<JobData>  jobList=ModelUtil.getAllJobDatas(AppConfig.myCity);
            List<JobData>   recomList=new ArrayList<>();
            for (JobData  jobData:jobList){
               if (jobData.getHot()!=0&&jobData.getJob().getBoolean("job_IsAtHome")){
                   recomList.add(jobData);
               }
            }
            mAdapter.setData(recomList);
        } else {
            mAdapter.setData(list);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        headerBannerView.enqueueBannerLoopMessage();
    }
    @Override
    public void onStop() {
        super.onStop();
        headerBannerView.removeBannerLoopMessage();
    }
}













