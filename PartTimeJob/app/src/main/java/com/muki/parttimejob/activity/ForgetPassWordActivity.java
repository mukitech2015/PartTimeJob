package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.utils.SmsSender;
import com.muki.parttimejob.utils.TABOperation;

import java.util.Random;

/**
 * Created by muki on 2016/10/18.
 */

public class ForgetPassWordActivity  extends BaseActivity implements View.OnClickListener{
private  LinearLayout    fgPassBackLat;
private  RelativeLayout   verificationCodeLat;
private  RelativeLayout   saveLat;
private EditText   phoneEt;
private EditText   passwordEt;
private EditText   verificationCodeEt;
protected boolean needCallback;
private TextView   verificationTv;
private TextView   changePwTv;
private TimeCount time;
        long rnd;
        String  passwordStr;
        Random random = new Random();
class TimeCount extends CountDownTimer {
    public TimeCount(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }
    @Override
    public void onFinish() {// 计时完毕
        verificationTv.setText("获取验证码");
        verificationCodeLat.setClickable(true);
    }
    @Override
    public void onTick(long millisUntilFinished) {// 计时过程
        verificationTv.setClickable(false);//防止重复点击
        verificationTv.setText(millisUntilFinished / 1000 + "S");
    }
}
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_forget_password);
        Bundle bundle = getIntent().getExtras();
        if(bundle == null)
            return;
        needCallback = bundle.getBoolean(AppConfig.NeedCallback, false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent  intent=getIntent();
        String  title=intent.getStringExtra("CHANGE_PW_TITLE");
        changePwTv.setText(title);
    }
    @Override
    protected void initViews() {
        changePwTv= (TextView) findViewById(R.id.changePwTv);
        verificationTv= (TextView) findViewById(R.id.fgPass_verification_tv);
        verificationCodeLat= (RelativeLayout) findViewById(R.id.fgPass_code_lat);
        saveLat= (RelativeLayout) findViewById(R.id.fgPass_lat);
        verificationCodeEt= (EditText) findViewById(R.id.fgPass_code_et);
        phoneEt= (EditText) findViewById(R.id.fgPass_phone_et);
        passwordEt= (EditText) findViewById(R.id.fgPass_password_et);
        fgPassBackLat= (LinearLayout) findViewById(R.id.fgPass_back_lat);
        fgPassBackLat.setOnClickListener(this);
        saveLat.setOnClickListener(this);
        verificationCodeLat.setOnClickListener(this);
        time = new  TimeCount(60000, 1000);
        phoneEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                } else {
                    if (!phoneEt.getText().toString().trim().equals("")){
                        if (isMobileNO(phoneEt.getText().toString().trim()) == false)
                        {
                            Toast.makeText(ForgetPassWordActivity.this,"手机号格式错误",Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }
            }
        });
    }
    @Override
    protected void initDatas() {

    }
    String  phoneStr;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fgPass_back_lat:
                finish();
                break;
            case  R.id.fgPass_code_lat:
                phoneStr=phoneEt.getText().toString().trim();
                passwordStr=passwordEt.getText().toString().trim();
                if (TextUtils.isEmpty(phoneStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isMobileNO(phoneStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"手机号格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(passwordStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isPassWordNo(passwordStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"密码格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                rnd = random.nextInt(9999)%(9999-1000+1)+1000;
                Thread thread=new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        SmsSender sender = new SmsSender(1400014630, "777f8d474ec29f52ea8a21479edbcb2d");
                        sender.sendMsg("86",phoneStr, "此验证码只用于注册啄米兼职App，"+rnd+"（验证码）,请勿泄露.",rnd);
                        time.start();
                    }
                });
                thread.start();
                break;
            case  R.id.fgPass_lat:
                phoneStr=phoneEt.getText().toString().trim();
                passwordStr=passwordEt.getText().toString().trim();
                String  verificationCodeStr=verificationCodeEt.getText().toString().trim();
                if (TextUtils.isEmpty(phoneStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(passwordStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isPassWordNo(passwordStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"密码格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(verificationCodeStr)){
                    Toast.makeText(ForgetPassWordActivity.this,"请输入验证码",Toast.LENGTH_SHORT).show();
                    return;
                }
//                if(!verificationCodeStr.equals(rnd+"")){
//                    Toast.makeText(ForgetPassWordActivity.this,"验证码不正确",Toast.LENGTH_SHORT).show();
//                    return;
//                }
                TABOperation.changePassWord(ForgetPassWordActivity.this,phoneStr,passwordStr);
                break;
        }
    }
    public static boolean isPassWordNo(String passwords){
        String telRegex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";
        if (TextUtils.isEmpty(passwords)) return false;
        else return passwords.matches(telRegex);
    }
    public static boolean isMobileNO(String mobiles) {
        String telRegex = "[1][358]\\d{9}";//"[1]"代表�?1位为数字1�?"[358]"代表第二位可以为3�?5�?8中的�?个，"\\d{9}"代表后面是可以是0�?9的数字，�?9位�??
        if (TextUtils.isEmpty(mobiles)) return false;
        else return mobiles.matches(telRegex);
    }
}
