package com.muki.parttimejob.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by muki on 2016/10/19.
 */

public class TimeTool {
    public static  String  getTime(String  time,String  currentTime ){
        String dateStart = time;
        String dateStop = currentTime;
        long diff = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //毫秒ms
            diff = d2.getTime() - d1.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }

        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        if(diffDays>=10){
            return  time;
        }else{
            if(diffDays>=1){
                return  diffDays+"天前";
            }else{
                if(diffHours>=1){
                    return diffHours+"小时前";
                }else{
                    if(diffMinutes>=1){
                        return  diffMinutes+"分钟前";
                    }else{
                        return  "刚刚";
                    }
                }
            }
        }
    }
}
