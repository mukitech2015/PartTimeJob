package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.muki.parttimejob.R;
import com.muki.parttimejob.viewholder.LCIMConversationItemHolder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.leancloud.chatkit.LCChatKit;
import cn.leancloud.chatkit.adapter.LCIMCommonListAdapter;

import cn.leancloud.chatkit.cache.LCIMConversationItemCache;
import cn.leancloud.chatkit.event.LCIMConversationItemLongClickEvent;
import cn.leancloud.chatkit.event.LCIMIMTypeMessageEvent;
import cn.leancloud.chatkit.event.LCIMOfflineMessageCountChangeEvent;
import cn.leancloud.chatkit.view.LCIMDividerItemDecoration;
import de.greenrobot.event.EventBus;


/**
 * Created by muki on 2016/10/13.
 */

public class LCIMConversationListActivity  extends   AVBaseActivity{
    protected RecyclerView recyclerView;
    protected LCIMCommonListAdapter<AVIMConversation> itemAdapter;
    protected LinearLayoutManager layoutManager;
    private  RelativeLayout privateBackLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_conversation_list);
        initViews();
    }
    @Override
    protected void onStart() {
        super.onStart();
        this.updateConversationList();
    }
    public  void  initViews(  ){
         privateBackLat= (RelativeLayout) findViewById(R.id.privateBackLat);
         privateBackLat.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 finish();
             }
         });
         this.recyclerView = (RecyclerView)findViewById(R.id.fragment_conversation_srl_view);
         this.layoutManager = new LinearLayoutManager(this);
         this.recyclerView.setLayoutManager(this.layoutManager);
         this.recyclerView.addItemDecoration(new LCIMDividerItemDecoration(this));
         this.itemAdapter = new LCIMCommonListAdapter(LCIMConversationItemHolder.class);
         this.recyclerView.setAdapter(this.itemAdapter);
         EventBus.getDefault().register(this);
     }
    public void onEvent(LCIMIMTypeMessageEvent event) {
           this.updateConversationList();
     }
    public void onEvent(LCIMConversationItemLongClickEvent event) {
        if(event.conversation!=null) {
            String conversationId = event.conversation.getConversationId();
            LCIMConversationItemCache.getInstance().deleteConversation(conversationId);
            updateConversationList();
        }
    }
    private void updateConversationList(){
        List convIdList = LCIMConversationItemCache.getInstance().getSortedConversationList();
        ArrayList conversationList = new ArrayList();
        Iterator i$ = convIdList.iterator();
        while(i$.hasNext()) {
            String convId = (String)i$.next();
            conversationList.add(LCChatKit.getInstance().getClient().getConversation(convId));
        }
        this.itemAdapter.setDataList(conversationList);
        this.itemAdapter.notifyDataSetChanged();
      }
    public void onEvent(LCIMOfflineMessageCountChangeEvent updateEvent) {
        this.updateConversationList();
    }
    @Override
    protected void onResume() {
        super.onResume();
        this.updateConversationList();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
