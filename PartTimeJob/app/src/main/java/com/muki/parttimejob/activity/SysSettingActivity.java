package com.muki.parttimejob.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVQuery;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ToastUtils;
import com.muki.parttimejob.utils.UpdateStatus;
import com.muki.parttimejob.utils.UpdateVersionUtil;
import com.muki.parttimejob.utils.Utils;
import com.muki.parttimejob.utils.VersionInfo;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;

import java.util.ArrayList;

/**
 * Created by muki on 2016/9/27.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SysSettingActivity    extends  BaseActivity   implements View.OnClickListener {
    private LinearLayout sysBackLat;
    private RelativeLayout logoutLat;
    private boolean needCallback;
    private RelativeLayout aboutLat;
    private RelativeLayout  updateLat;
    private RelativeLayout  clearCacheLat;
    private ProgressDialog   progressDialog;
    private  RelativeLayout  changePasswordLat;
    private  RelativeLayout  notiftSetLat;
    private  RelativeLayout   shareAppLat;
    public ArrayList<SnsPlatform> platforms = new ArrayList<SnsPlatform>();
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_system_act);
        Bundle bundle = getIntent().getExtras();
        if (bundle == null)
            return;
        needCallback = bundle.getBoolean(AppConfig.NeedCallback, false);
    }
    @Override
    protected void initViews() {
        progressDialog= Utils.createProgressDialog(this,"正在清除缓存......");
        shareAppLat= (RelativeLayout) findViewById(R.id.shareAppLat);
        notiftSetLat= (RelativeLayout) findViewById(R.id.notiftSetLat);
        changePasswordLat= (RelativeLayout) findViewById(R.id.changePasswordLat);
        clearCacheLat= (RelativeLayout) findViewById(R.id.clearCacheLat);
        updateLat= (RelativeLayout) findViewById(R.id.updateLat);
        aboutLat = (RelativeLayout) findViewById(R.id.aboutLat);
        logoutLat = (RelativeLayout) findViewById(R.id.logoutLat);
        sysBackLat = (LinearLayout) findViewById(R.id.sysBackLat);
        sysBackLat.setOnClickListener(this);
        logoutLat.setOnClickListener(this);
        aboutLat.setOnClickListener(this);
        clearCacheLat.setOnClickListener(this);
        changePasswordLat.setOnClickListener(this);
        notiftSetLat.setOnClickListener(this);
        shareAppLat.setOnClickListener(this);
        updateLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateVersionUtil.checkVersion(SysSettingActivity.this,new UpdateVersionUtil.UpdateListener() {
                    @Override
                    public void onUpdateReturned(int updateStatus, VersionInfo versionInfo) {
                        //判断回调过来的版本检测状态
                        switch (updateStatus) {
                            case UpdateStatus.YES:
                                //弹出更新提示
                                UpdateVersionUtil.showDialog(SysSettingActivity.this,versionInfo);
                                break;
                            case UpdateStatus.NO:
                                //没有新版本
                                ToastUtils.showToast(getApplicationContext(), "已经是最新版本了");
                                break;
                            case UpdateStatus.NOWIFI:
                                //当前是非wifi网络
                                ToastUtils.showToast(getApplicationContext(), "只有在wifi下更新");
                                break;
                            case UpdateStatus.ERROR:
                                //检测失败
                                ToastUtils.showToast(getApplicationContext(), "检测失败，请稍后重试");
                                break;
                            case UpdateStatus.TIMEOUT:
                                //链接超时
                                ToastUtils.showToast(getApplicationContext(), "链接超时，请检查网络设置");
                                break;
                        }
                    }
                });
            }
        });
    }
    @Override
    protected void initDatas() {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notiftSetLat:
                Intent   intentNotiftSet=new Intent(this,ProfileNotifySettingActivity.class );
                startActivity(intentNotiftSet);
                 break;
            case R.id.aboutLat:
                Intent intentWebAbout = new Intent(SysSettingActivity.this, WebActivity.class);
                intentWebAbout.putExtra("WEB_URL", AppConfig.ABOUT);
                startActivity(intentWebAbout);
                break;
            case R.id.sysBackLat:
                this.finish();
                break;
            case R.id.logoutLat:
                showLogoutDg();
                break;
            case R.id.clearCacheLat:
                progressDialog.show();
                new ClearCacheTask().execute();
                break;
            case R.id.changePasswordLat:
                Intent  intent=new Intent(SysSettingActivity.this,ForgetPassWordActivity.class);
                intent.putExtra("CHANGE_PW_TITLE","修改密码");
                startActivity(intent);
                break;
            case R.id.shareAppLat:
                Intent intentShare = new Intent(SysSettingActivity.this, WebActivity.class);
                intentShare .putExtra("WEB_URL",AppConfig.SHARE_APP);
                startActivity(intentShare );
                break;
        }
    }

    class  ClearCacheTask   extends AsyncTask<Void,Object,Object> {
        @Override
        protected Object doInBackground(Void... params) {
            AVQuery.clearAllCachedResults();
            return null;
        }
        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressDialog.dismiss();
            Toast.makeText(SysSettingActivity.this,"缓存已清除",Toast.LENGTH_SHORT).show();
        }
    }
    AlertDialog.Builder dialogLoginOut;
    public  void   showLogoutDg(  ){
        if(dialogLoginOut==null){
            dialogLoginOut=new AlertDialog.Builder(SysSettingActivity.this);
        }
        dialogLoginOut.setTitle("退出登录");
        dialogLoginOut.setIcon(R.drawable.ic_launcher);
        dialogLoginOut.setMessage("确定要退出登录吗？");
        dialogLoginOut.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LeanchatUser.logOut();
                Intent intent = new Intent(SysSettingActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }).show();
    }
}




