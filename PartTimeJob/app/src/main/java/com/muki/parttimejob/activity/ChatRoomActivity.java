package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVCallback;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.fragment.LCIMConversationFragment;
import com.muki.parttimejob.model.ConversationType;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.ConversationUtils;
import com.muki.parttimejob.utils.LCIMConversationUtils;

import java.util.Arrays;
import java.util.Map;

import cn.leancloud.chatkit.LCChatKit;
import cn.leancloud.chatkit.cache.LCIMConversationItemCache;
import cn.leancloud.chatkit.utils.LCIMConstants;
import cn.leancloud.chatkit.utils.LCIMLogUtils;

/**
 * Created by lzw on 15/4/24.
 */
public class ChatRoomActivity extends AppCompatActivity {
  private AVIMConversation conversation;
  public static final int QUIT_GROUP_REQUEST = 200;
  protected LCIMConversationFragment conversationFragment;
  public TextView chatTitleTv;
  public RelativeLayout chatBackLat;
  private  RelativeLayout  chatDetailLat;
  ConversationType conversationType;
  private   String   toUserObj;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.lcim_conversation_activity);
    this.conversationFragment = (LCIMConversationFragment)this.getSupportFragmentManager().findFragmentById(cn.leancloud.chatkit.R.id.fragment_chat);
    chatDetailLat= (RelativeLayout) findViewById(R.id.chatDetailLat);
    chatTitleTv= (TextView) findViewById(R.id.chatTitleTv);
    chatBackLat= (RelativeLayout) findViewById(R.id.chatRoomBackLat);
    chatBackLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
             finish();
         }
    });
    chatDetailLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent=new Intent(ChatRoomActivity.this, ContactPersonInfoActivity.class);
        intent.putExtra(Constants.LEANCHAT_USER_ID, toUserObj);
        startActivity(intent);
      }
    });
  }
  private void initByIntent(final Intent intent) {
    LCChatKit.getInstance().open(LeanchatUser.getCurrentUserId(), new AVIMClientCallback() {
      @Override
      public void done(AVIMClient avimClient, AVIMException e) {
        if( null ==  LCChatKit.getInstance().getClient()) {
             showToast("please login first!");
             finish();
        } else {
             Bundle extras = intent.getExtras();
          if(null != extras) {
            if(extras.containsKey(LCIMConstants.PEER_ID)) {
              getConversation(extras.getString(LCIMConstants.PEER_ID));
            } else if(extras.containsKey(LCIMConstants.CONVERSATION_ID)) {
              String conversationId = extras.getString(LCIMConstants.CONVERSATION_ID);
              updateConversation(LCChatKit.getInstance().getClient().getConversation(conversationId));
            } else {
              finish();
            }
          }
        }
      }
    });
  }
  protected void getConversation(String memberId) {
    LCChatKit.getInstance().getClient().createConversation(Arrays.asList(new String[]{memberId}), "", (Map)null, false, true, new AVIMConversationCreatedCallback() {
      public void done(AVIMConversation avimConversation, AVIMException e) {
        if(null != e) {
          showToast(e.getMessage());
        } else {
          conversation=avimConversation;
          updateConversation(avimConversation);
        }
      }
    });
  }
  @Override
  protected void onStart() {
    super.onStart();
    Intent  intent=getIntent();
    if(intent!=null){
      chatTitleTv.setText(intent.getStringExtra("CHAT_TITLE"));
      toUserObj =intent.getStringExtra("TO_USERID");
      initByIntent(this.getIntent());
    }
  }
  @Override
  protected void onResume() {
    super.onResume();
  }
  protected void updateConversation(AVIMConversation conversation) {
    if(null != conversation) {
      ChatRoomActivity.this.conversation=conversation;
      conversationType = ConversationUtils.typeOfConversation(conversation);
      Log.e("conversationType",conversationType+"");
      this.conversationFragment.setConversation(conversation);
      LCIMConversationItemCache.getInstance().clearUnread(conversation.getConversationId());
      LCIMConversationUtils.getConversationName(conversation, new AVCallback(){
        @Override
        protected void internalDone0(Object o, AVException e) {
          if(null != e) {
            LCIMLogUtils.logException(e);
          }
        }
      });
    }
  }
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
        case QUIT_GROUP_REQUEST:
          finish();
          break;
      }
    }
  }
  private void showToast(String content) {
    Toast.makeText(this, content,Toast.LENGTH_SHORT).show();
  }
}