package com.muki.parttimejob.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.Toast;

import com.muki.parttimejob.JobApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static int compare_date(String DATE1, String DATE2) {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    try {
      Date dt1 = df.parse(DATE1);
      Date dt2 = df.parse(DATE2);
      if (dt1.getTime() > dt2.getTime()) {
        System.out.println("dt1 在dt2前");
        return 1;
      } else if (dt1.getTime() < dt2.getTime()) {
        System.out.println("dt1在dt2后");
        return -1;
      } else {
        return 0;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return 0;
  }
  public static void toast(Context context, String str) {
    Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
  }

  public static void showInfoDialog(Activity cxt, String msg, String title) {
    AlertDialog.Builder builder = getBaseDialogBuilder(cxt);
    builder.setMessage(msg)
        .setPositiveButton(cxt.getString(com.muki.parttimejob.R.string.chat_utils_right), null)
        .setTitle(title)
        .show();
  }

  public static AlertDialog.Builder getBaseDialogBuilder(Activity ctx) {
    return new AlertDialog.Builder(ctx).setTitle(com.muki.parttimejob.R.string.chat_utils_tips);
  }

  public static void fixAsyncTaskBug() {
    // android bug
    new AsyncTask<Void, Void, Void>() {

      @Override
      protected Void doInBackground(Void... params) {
        return null;
      }
    }.execute();
  }

  public static void toast(int id) {
    toast(JobApplication.getInstance(), id);
  }

  public static void toast(String s) {
    toast(JobApplication.getInstance(), s);
  }

  public static void toast(Context cxt, int id) {
    Toast.makeText(cxt, id, Toast.LENGTH_SHORT).show();
  }
  public static boolean doubleEqual(double a, double b) {
    return Math.abs(a - b) < 1E-8;
  }

  public static String getPrettyDistance(double distance) {
    if (distance < 1000) {
      int metres = (int) distance;
      return String.valueOf(metres) + JobApplication.getInstance().getString(com.muki.parttimejob.R.string.discover_metres);
    } else {
      String num = String.format("%.1f", distance / 1000);
      return num + JobApplication.getInstance().getString(com.muki.parttimejob.R.string.utils_kilometres);
    }
  }

  public static ProgressDialog showSpinnerDialog(Activity activity) {
    //activity = modifyDialogContext(activity);
    ProgressDialog dialog = new ProgressDialog(activity);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setCancelable(true);
    dialog.setMessage(JobApplication.getInstance().getString(com.muki.parttimejob.R.string.sending));
    if (!activity.isFinishing()) {
      dialog.show();
    }
    return dialog;
  }

  public static boolean filterException(Exception e) {
    if (e != null) {
      toast(e.getMessage());
      return false;
    } else {
      return true;
    }
  }

  public static void saveBitmap(String filePath, Bitmap bitmap) {
    File file = new File(filePath);
    if (!file.getParentFile().exists()) {
      file.getParentFile().mkdirs();
    }
    FileOutputStream out = null;
    try {
      out = new FileOutputStream(file);
      if (bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)) {
        out.flush();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        out.close();
      } catch (Exception e) {
      }
    }
  }
  /**
   *
   * @Title: createProgressDialog
   * @Description: 创建ProgressDialog
   * @param  context
   * @param msg
   * @return ProgressDialog
   */
  public static ProgressDialog createProgressDialog(final Context context, final String msg)
  {
    final ProgressDialog dialog = new ProgressDialog(context);
    dialog.setMessage(msg);
    dialog.setCanceledOnTouchOutside(false);
    return dialog;
  }
}
