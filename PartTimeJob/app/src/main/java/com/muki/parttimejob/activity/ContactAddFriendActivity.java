package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.AddFriendAdapter;
import com.muki.parttimejob.manager.FriendsManager;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.UserCacheUtils;
import com.muki.parttimejob.widget.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by muki on 2016/10/12.
 */

public class ContactAddFriendActivity   extends AVBaseActivity  {
    @Bind(R.id.search_user_rv_layout)
    protected RecyclerView recyclerView;
    @Bind(R.id.searchNameEdit)
    EditText searchNameEdit;
    private String searchName = "";
    private AddFriendAdapter   addFriendAdapter;
    private RelativeLayout  addFrdLat;
    LinearLayout   searchNodataLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_add_friend_activity);
        init();
    }
    private void init() {
        searchNodataLat= (LinearLayout) findViewById(R.id.searchNodataLat);
       addFrdLat= (RelativeLayout) findViewById(R.id.addFrdLat);
         recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SpacesItemDecoration(this, LinearLayout.VERTICAL));
        addFrdLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public  void   setSerchFriendAdapter(List<LeanchatUser> avObjectList){
        if(addFriendAdapter==null){
          addFriendAdapter=new AddFriendAdapter(recyclerView,avObjectList,this);
          recyclerView.setAdapter(addFriendAdapter);
        }else{
          addFriendAdapter.changeAddFriendAdapter(avObjectList);
        }
        if(addFriendAdapter.getItemCount()==0){
            searchNodataLat.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            recyclerView.setVisibility(View.VISIBLE);
            searchNodataLat.setVisibility(View.GONE);
        }
    }
    private void loadMoreFriend(  ) {
        AVQuery<LeanchatUser> q = LeanchatUser.getQuery(LeanchatUser.class);
        q.whereContains(LeanchatUser.USERNAME, searchName);
        LeanchatUser user = LeanchatUser.getCurrentUser();
        List<String> friendIds = new ArrayList<String>(FriendsManager.getFriendIds());
        friendIds.add(user.getObjectId());
        q.whereNotContainedIn(Constants.OBJECT_ID, friendIds);
        q.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        q.findInBackground(new FindCallback<LeanchatUser>() {
            @Override
            public void done(List<LeanchatUser> list, AVException e) {
                if(e==null){
                    UserCacheUtils.cacheUsers(list);
                    setSerchFriendAdapter(list);
                }
             }
        });
    }
    @OnClick(R.id.searchBtn)
    public void search(View view) {
          searchName = searchNameEdit.getText().toString();
          if(TextUtils.isEmpty(searchName)){
              Toast.makeText(ContactAddFriendActivity.this,"请输入手机号码",Toast.LENGTH_SHORT).show();
              return;
          }
          loadMoreFriend();
    }
}







