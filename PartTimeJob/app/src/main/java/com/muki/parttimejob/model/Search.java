package com.muki.parttimejob.model;

import org.litepal.crud.DataSupport;

/**
 * Created by muki on 2016/11/11.
 */

public class Search  extends DataSupport {
    private  String string;
    public  Search(String   string){
       super();
       this.string=string;
    }
    public String getString() {
        return string;
    }
    public void setString(String string) {
        this.string = string;
    }
}
