package com.muki.parttimejob.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.WheelView;
import com.pizidea.imagepicker.AndroidImagePicker;

import org.feezu.liuli.timeselector.TimeSelector;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by muki on 2016/9/17.
 */
public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout upLoadAdatarLat;
    private ImageView userAvatarIv;
    private EditText nickNameEt;
    private TextView sexTv;
    String userAvatar;
    String nickName;
    String sex = "-1";
    String sexStr;
    String userHeight;
    String userWeight;
    String birhday;
    private int whichPrcture = -1;
    private RelativeLayout submitRat;
    private RelativeLayout selectSexRet;
    private TextView renzhengTv;
    private RelativeLayout userInfoBackLat;
    private TextView userHeightTv;
    private TextView userWeightTv;
    private RelativeLayout userWeightLat;
    private RelativeLayout userHeightLat;
    private ArrayList<String> user_height = new ArrayList<String>();
    private ArrayList<String> user_weight = new ArrayList<String>();
    String idState;
    private   RelativeLayout    idAuthenLat;
    private   TextView  birthTv;
    private   TextView  ageTv;
    private  RelativeLayout   birthLat;
    private TimeSelector timeSelector;
    int  age;
    String  birthStr;
    SimpleDateFormat formatter = new    SimpleDateFormat("yyyy-MM-dd");
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_userinfo);
        birthLat= (RelativeLayout) findViewById(R.id.birthLat);
        birthTv= (TextView) findViewById(R.id.userinfo_birthTv);
        ageTv= (TextView) findViewById(R.id.userinfo_ageTv);
        idAuthenLat= (RelativeLayout) findViewById(R.id.idAuthenLat);
        userWeightLat = (RelativeLayout) findViewById(R.id.userWeightLat);
        userHeightLat = (RelativeLayout) findViewById(R.id.userHeightLat);
        userHeightTv = (TextView) findViewById(R.id.userHeightTv);
        userWeightTv = (TextView) findViewById(R.id.userWeightTv);
        userInfoBackLat = (RelativeLayout) findViewById(R.id.userInfoBackLat);
        renzhengTv = (TextView) findViewById(R.id.renzhengTv);
        selectSexRet = (RelativeLayout) findViewById(R.id.userinfo_sexLat);
        nickNameEt = (EditText) findViewById(R.id.userinfo_nickNameEt);
        userAvatarIv = (ImageView) findViewById(R.id.userinfo_userAvatar_iv);
        sexTv = (TextView) findViewById(R.id.userinfo_sexTv);
        submitRat = (RelativeLayout) findViewById(R.id.userinfo_nextSubmit);
        upLoadAdatarLat = (RelativeLayout) findViewById(R.id.upLoad_avatar_lat);
        birthLat.setOnClickListener(this);
        upLoadAdatarLat.setOnClickListener(this);
        submitRat.setOnClickListener(this);
        selectSexRet.setOnClickListener(this);
        userInfoBackLat.setOnClickListener(this);
        userWeightLat.setOnClickListener(this);
        userHeightLat.setOnClickListener(this);
        idAuthenLat.setOnClickListener(this);
        for (int i = 120; i <= 250; i++) {
            user_height.add(i + "cm");
        }
        for (int i = 25; i <= 180; i++) {
            user_weight.add(i + "kg");
        }
        getData();
        timeSelector = new TimeSelector(this, new TimeSelector.ResultHandler() {
            @Override
            public void handle(String time) {
                     birthStr=  time.replaceAll("00:00","");
                     birthTv.setText(birthStr);
                 try {
                     age=getAge(getDateWithDateString(time.replaceAll("00:00","")));
                 } catch (Exception e) {
                    e.printStackTrace();
                 }
                  ageTv.setText(age+"");
            }
        }, "1990-01-01 00:00", "2018-12-31 00:00");

    }
    Date getDateWithDateString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    public  int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            }else{
                age--;
            }
        }
        return age;
    }
    public void getData() {
        UpLoadImageUtil.loadCropImage(this, LeanchatUser.getCurrentUser().getAvatarUrl(), userAvatarIv);
        nickNameEt.setText(LeanchatUser.getCurrentUser().getUSER_NICKNAME());
        if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION() == 0) {
            renzhengTv.setText("未认证");
        } else if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION() == 1) {
            renzhengTv.setText("已认证");
        } else if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION() == 2) {
            renzhengTv.setText("审核中");
        } else if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION() == 3) {
            renzhengTv.setText("审核未通过");
        }
        userWeightTv.setText(LeanchatUser.getCurrentUser().getUserWeight());
        userHeightTv.setText(LeanchatUser.getCurrentUser().getUserHeight());
        if (LeanchatUser.getCurrentUser().getUSER_SEX().equals("0")) {
            sexTv.setText("女");
        } else if (LeanchatUser.getCurrentUser().getUSER_SEX().equals("1")) {
            sexTv.setText("男");
        }
            idState = renzhengTv.getText().toString();
        if (LeanchatUser.getCurrentUser().getUserBirth()!=null){
            birthTv.setText(formatter.format(LeanchatUser.getCurrentUser().getUserBirth()));
        }
        try {
            age=getAge(LeanchatUser.getCurrentUser().getUserBirth());
        } catch (Exception e) {
            e.printStackTrace();
        }
          ageTv.setText(age+"");
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.birthLat:
                if(idState.equals("已认证")||idState.equals("审核中")){
                    Toast.makeText(UserInfoActivity.this, "处于认证状态无法修改该信息", Toast.LENGTH_SHORT).show();
                }else{
                    timeSelector.setMode(TimeSelector.MODE.YMD);//只显示 年月日
                    timeSelector.show();
                }
                break;
            case R.id.idAuthenLat:
                if(idState.equals("已认证")||idState.equals("审核中")){
                    Toast.makeText(UserInfoActivity.this, "处于认证状态无法修改该信息", Toast.LENGTH_SHORT).show();
                }else{
                    Intent  intent=new Intent(UserInfoActivity.this,IdAuthentication.class);
                    startActivity(intent);
               }
                break;
            case R.id.userInfoBackLat:
                finish();
                break;
            case R.id.upLoad_avatar_lat:
                whichPrcture = 0;
                setMutiSelectPicture();
                break;
            case R.id.userinfo_nextSubmit:
                try {
                    Next();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (AVException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.userinfo_sexLat:
                if (idState.equals("已认证") || idState.equals("审核中")) {
                    Toast.makeText(UserInfoActivity.this, "处于认证状态无法修改该信息", Toast.LENGTH_SHORT).show();
                } else {
                    showSelectSexDialog();
                }
                break;
            case R.id.userHeightLat:
                SelecterTitleDialog(user_height, "身高", userHeightTv, 45);
                break;
            case R.id.userWeightLat:
                SelecterTitleDialog(user_weight, "体重", userWeightTv, 35);
                break;
        }
    }
    public void setMutiSelectPicture() {
        if (whichPrcture == 0) {
            AndroidImagePicker.getInstance().pickAndCrop(UserInfoActivity.this, true, 120, new AndroidImagePicker.OnImageCropCompleteListener() {
                @Override
                public void onImageCropComplete(Bitmap bmp, float ratio, String path) {
                    UpLoadImageUtil.loadCropImage(getApplicationContext(), path, userAvatarIv);
                    userAvatar = path;
                }
            });
        }
    }

    public void Next() throws FileNotFoundException, AVException {
        userHeight = userHeightTv.getText().toString();
        userWeight = userWeightTv.getText().toString();
        nickName = nickNameEt.getText().toString().trim();
        birhday=birthTv.getText().toString().trim();
        sexStr = sexTv.getText().toString();
        if (TextUtils.isEmpty(userAvatar) && TextUtils.isEmpty(LeanchatUser.getCurrentUser().getAvatarUrl())) {
            Toast.makeText(UserInfoActivity.this, "请上传头像", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(nickName)) {
            Toast.makeText(UserInfoActivity.this, "请填写昵称", Toast.LENGTH_SHORT).show();
            return;
        }
        if (nickName.contains("兼职")) {
            Toast.makeText(UserInfoActivity.this, "昵称不能含有兼职字样", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(sexStr)) {
            Toast.makeText(UserInfoActivity.this, "请选择性别", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(userHeight)) {
            Toast.makeText(UserInfoActivity.this, "请填写身高", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(userWeight)) {
            Toast.makeText(UserInfoActivity.this, "请填写体重", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(birhday)) {
            Toast.makeText(UserInfoActivity.this, "请填写生日", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(UserInfoActivity.this, UserInfoNextActivity.class);
        intent.putExtra("userAvatar", userAvatar);
        intent.putExtra("birth", birthStr);
        intent.putExtra("age",age);
        intent.putExtra("sex", sex);
        intent.putExtra("nickName", nickName);
        intent.putExtra("userHeight", userHeight);
        intent.putExtra("userWeight", userWeight);
        startActivity(intent);
    }
    private AlertDialog selectSexDlg;

    private void showSelectSexDialog() {
        if (selectSexDlg == null) {
            selectSexDlg = new AlertDialog.Builder(this).create();
        }
        selectSexDlg.show();
        Window window = selectSexDlg.getWindow();
        window.setContentView(R.layout.layout_sex_dialog);
        window.findViewById(R.id.man_lat).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                sex = "1";
                sexTv.setText("男");
                selectSexDlg.dismiss();
            }
        });
        window.findViewById(R.id.woman_lat).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                sex = "0";
                sexTv.setText("女");
                selectSexDlg.dismiss();
            }
        });
    }

    private String choose_item;
    private int mindex;

    public void SelecterTitleDialog(final List<String> string, String title, final TextView textview, final int index) {
        View outerView = LayoutInflater.from(this).inflate(R.layout.wheel_view, null);
        WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
        wv.setOffset(2);
        wv.setItems(string);
        wv.setSeletion(index);
        mindex = index;
        wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                choose_item = item;
                mindex = selectedIndex;
            }
        });
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setView(outerView)
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface v, int position) {
                        // TODO Auto-generated method stub
                        if (mindex == 45 && textview.getId() == userHeightTv.getId()) {
                            textview.setText("165cm");
                        } else if (mindex == 35 && textview.getId() == userWeightTv.getId()) {
                            textview.setText("60kg");
                        } else {
                            textview.setText(choose_item);
                        }
                    }
                }).show();
    }
}