package com.muki.parttimejob.activity;

import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.FragmentViewPagerAdapter;
import com.muki.parttimejob.fragment.FindFragment;
import com.muki.parttimejob.fragment.MessageFragment;
import com.muki.parttimejob.fragment.MineFragment;
import com.muki.parttimejob.fragment.SquareFragment;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.PermissionsUtil;

import java.util.ArrayList;
import java.util.List;

import cn.leancloud.chatkit.LCChatKit;
import cn.leancloud.chatkit.cache.LCIMConversationItemCache;



public class MainActivity extends  FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    public ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    private SquareFragment squareFragment;
    private MessageFragment messageFragment = new MessageFragment();
    private FindFragment findFragment = new FindFragment();
    private MineFragment mineFragment = new MineFragment();
    public static ViewPager viewPager;
    protected static final String TAG = "MainActivity";
    private int currentTabIndex = 0;
    private static int indexPager;
    private TextView squire_icon_tv;
    private TextView message_icon_tv;
    private TextView find_icon_tv;
    private TextView mine_icon_tv;
    private ImageView squire_icon_iv;
    private ImageView message_icon_iv;
    private ImageView find_icon_iv;
    private ImageView mine_icon_iv;
    private LinearLayout squire_layout;
    private LinearLayout message_layout;
    private LinearLayout find_layout;
    private LinearLayout mine_layout;
    View layout;//搜索
    private final int REQUEST_CODE = 0;
    TextView  newMsgTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.activity_main);
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        Rect frame = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        TypedArray actionbarSizeTypedArray = this.obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        float h = actionbarSizeTypedArray.getDimension(0, 0);
        initViews();
        squareFragment = new SquareFragment();
        fragments.add(squareFragment);
        fragments.add(messageFragment);
        fragments.add(findFragment);
        fragments.add(mineFragment);
        FragmentViewPagerAdapter adapter = new FragmentViewPagerAdapter(getFragmentManager(), viewPager, fragments);
        adapter.setOnExtraPageChangeListener(new FragmentViewPagerAdapter.OnExtraPageChangeListener() {
            @Override
            public void onExtraPageSelected(int i) {
                setTabSelection(i);
            }
        });
        setTabSelection(0);
        viewPager.setCurrentItem(0);
    }
    @Override
    protected void onStart() {
        super.onStart();
        refresh();
    }
    public void initViews() {
        if (Build.VERSION.SDK_INT >= 23) {
            PermissionsUtil.checkAndRequestPermissions(this);
        }
        if(LeanchatUser.getCurrentUserId()!=null){
            LCChatKit.getInstance().open(LeanchatUser.getCurrentUserId()==null?null:LeanchatUser.getCurrentUserId(), new AVIMClientCallback() {
                @Override
                public void done(AVIMClient avimClient, AVIMException e) {

                }
            });
        }
        newMsgTv= (TextView) findViewById(R.id.newMsgTv);
        viewPager = (ViewPager) findViewById(R.id.content);
        message_icon_tv = (TextView) findViewById(R.id.message_icon_tv);
        squire_icon_tv= (TextView) findViewById(R.id.squire_icon_tv);
        find_icon_tv = (TextView) findViewById(R.id.find_icon_tv);
        mine_icon_tv = (TextView) findViewById(R.id.mine_icon_tv);
        squire_icon_iv= (ImageView) findViewById(R.id.squire_icon_iv);
        message_icon_iv = (ImageView) findViewById(R.id.message_icon_iv);
        mine_icon_iv = (ImageView) findViewById(R.id.mine_icon_iv);
        find_icon_iv = (ImageView) findViewById(R.id.find_icon_iv);
        squire_layout = (LinearLayout) findViewById(R.id.squire_layout);
        find_layout = (LinearLayout) findViewById(R.id.find_layout);
        message_layout= (LinearLayout) findViewById(R.id.message_layout);
        mine_layout = (LinearLayout) findViewById(R.id.mine_layout);
        find_layout.setOnClickListener(this);
        message_layout.setOnClickListener(this);
        squire_layout.setOnClickListener(this);
        mine_layout.setOnClickListener(this);
    }
    private void clearSelection() {
        squire_icon_tv.setTextColor(Color.parseColor("#82858b"));
        mine_icon_tv.setTextColor(Color.parseColor("#82858b"));
        find_icon_tv.setTextColor(Color.parseColor("#82858b"));
        message_icon_tv.setTextColor(Color.parseColor("#82858b"));
        message_icon_iv.setImageResource(R.drawable.news_nor);
        find_icon_iv.setImageResource(R.drawable.find_nor);
        squire_icon_iv.setImageResource(R.drawable.home_nor);
        mine_icon_iv.setImageResource(R.drawable.mine_nor);
    }
    @Override
    protected void onPause() {
        super.onPause();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    private void refresh() {
        int num = 0;
        List<String> convIdList = LCIMConversationItemCache.getInstance().getSortedConversationList();
        for (String  conId:convIdList){
            num =num+LCIMConversationItemCache.getInstance().getUnreadCount(conId);
        }
        newMsgTv.setVisibility(num>0? View.VISIBLE : View.GONE);
    }
    @Override
    protected void onResume() {
        super.onResume();
        // 缺少权限时, 进入权限配置页面

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.squire_layout:
                setTabSelection(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.message_layout:
                setTabSelection(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.find_layout:
                setTabSelection(2);
                viewPager.setCurrentItem(2);
                break;
            case R.id.mine_layout:
                setTabSelection(3);
                viewPager.setCurrentItem(3);
                break;

        }
    }
    private long exitTime = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }
    public void setTabSelection(int index) {
        indexPager = index;
        clearSelection();
        switch (index) {
            case 0:
                squire_icon_iv.setImageResource(R.drawable.home_press);
                squire_icon_tv.setTextColor(Color.parseColor("#ff3c00"));
                break;
            case 1:
                if (selectPageListener!=null){
                    selectPageListener.onSelectPageAction();
                }
                message_icon_iv.setImageResource(R.drawable.news_press);
                message_icon_tv.setTextColor(Color.parseColor("#e51c23"));
                break;
            case 2:
                if (selectPageListener!=null){
                    selectPageListener.onSelectPageAction();
                }
                find_icon_iv.setImageResource(R.drawable.find_press);
                find_icon_tv.setTextColor(Color.parseColor("#e51c23"));
                break;
            case 3:
                if (selectPageListener!=null){
                    selectPageListener.onSelectPageAction();
                }
                mine_icon_iv.setImageResource(R.drawable.mine_press);
                mine_icon_tv.setTextColor(Color.parseColor("#e51c23"));
                break;
        }
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }
    @Override
    public void onPageSelected(int position) {
        setTabSelection(position);
    }
    @Override
    public void onPageScrollStateChanged(int state) {

    }
    public  interface   SelectPageListener{
        public   void   onSelectPageAction( );
    }
    public   SelectPageListener  selectPageListener;
    public  void   setSelectPage(SelectPageListener  selectPageListener){
        this.selectPageListener=selectPageListener;
    }
}






