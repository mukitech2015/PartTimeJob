package com.muki.parttimejob.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.DynamicAdapter;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.Dynamic_Follow;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ButtonUtils;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/26.
 */

public class MyDynamicActivity    extends   BaseActivity  implements View.OnClickListener,PullBaseView.OnHeaderRefreshListener,PullBaseView.OnFooterRefreshListener{
    @Bind(R.id.mypullFdDynRv)
    PullRecyclerView  mypullFdDynRv;
    @Bind(R.id.myDynBackLat)
    RelativeLayout   myDynBackLat;
    DynamicAdapter   dynamicAdapter;
    List<AVObject>  objectListCopy=new ArrayList<AVObject>();
    int page=0;
    @Bind(R.id.noMyDynLat)
    LinearLayout noMyDynLat;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_act_mydyn);
        ButterKnife.bind(this);
        mypullFdDynRv.setOnHeaderRefreshListener(this);
        mypullFdDynRv.setOnFooterRefreshListener(this);
        mypullFdDynRv.setLayoutManager(new LinearLayoutManager(this),this);
    }
    @Override
    protected void initViews() {
        myDynBackLat.setOnClickListener(this);
    }
    @Override
    protected void initDatas() {

    }
    @Override
    protected void onStart() {
        super.onStart();
        getMyDynamics();
    }
    public   void   getMyDynamics(   ){
        AVQuery<AVObject> dynamicAVQuery= new AVQuery<>("Dynamic");
        dynamicAVQuery.orderByDescending("createdAt");
        dynamicAVQuery.whereEqualTo("dynamic_Users_pointer", LeanchatUser.getCurrentUser());
        dynamicAVQuery.skip(page*10);// 跳过 10 条结果
        dynamicAVQuery.limit(10);
        dynamicAVQuery.include("dynamic_Users_pointer");
        dynamicAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e==null){
                    if (page==0){
                        objectListCopy.clear();
                        objectListCopy.addAll(list);
                        setDynAdapter(objectListCopy);
                    }else{
                        objectListCopy.addAll(objectListCopy.size(),list);
                        setDynAdapter(objectListCopy);
                    }
                }
            }
        });
    }
    public  void   setDynAdapter(List<AVObject> list){
        if(dynamicAdapter==null){
            dynamicAdapter=new DynamicAdapter(MyDynamicActivity.this, list,this);
            mypullFdDynRv.setAdapter(dynamicAdapter);
        }else{
            dynamicAdapter.changeAdapterData(list);
        }
        if(dynamicAdapter.getItemCount()==0){
             mypullFdDynRv.setVisibility(View.GONE);
             noMyDynLat.setVisibility(View.VISIBLE);
        }else{
             mypullFdDynRv.setVisibility(View.VISIBLE);
             noMyDynLat.setVisibility(View.GONE);
        }
    }
    int  mIndex;
    android.app.AlertDialog.Builder builder;
    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.myDynBackLat:
                 finish();
                 break;
             case R.id.dynShareLat:
                 mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                 new ShareAction(this).setDisplayList(SHARE_MEDIA.QQ,SHARE_MEDIA.QZONE,SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                         .withTitle("分享动态")
                         .withText(objectListCopy.get(mIndex).getString("dynamic_content"))
                         .withMedia(new UMImage(this,R.drawable.ic_launcher))
                         .withTargetUrl("http://www.cloudconfs.com/part/index.php/Share/Dynamic/view?objectId="+objectListCopy.get(mIndex).getObjectId())
                         .setCallback(umShareListener)
                         .open();
                 break;
             case R.id.dynDeleteLat:
                 mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                 if(builder==null){
                     builder=new android.app.AlertDialog.Builder(this);
                 }
                 builder.setIcon(R.drawable.ic_launcher);
                 builder.setTitle("删除");
                 builder.setMessage("确定要删除动态么？");
                 builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {

                     }
                 }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         objectListCopy.get(mIndex).deleteInBackground(new DeleteCallback() {
                             @Override
                             public void done(AVException e) {
                                 if (e==null){
                                     objectListCopy.remove(mIndex);
                                     dynamicAdapter.notifyDataSetChanged();
                                 }
                             }
                         });
                     }
                 }).show();
                 break;
             case R.id.find_dynComtLat:
                 mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                 Intent dynComeIntent=new Intent(this, DynCommentActivity.class);
                 dynComeIntent.putExtra("dynamic", (Serializable) objectListCopy.get(mIndex));
                 startActivity(dynComeIntent);
                 break;
             case   R.id.dynLoveLat:
                 if( !ButtonUtils.isFastDoubleClick(R.id.find_dynComtLat)){
                     String   viewTagStr= String.valueOf(v.getTag());
                     if(viewTagStr.contains("null")) {
                         createLovePoint(LeanchatUser.getCurrentUser(), (Dynamic) objectListCopy.get(Integer.parseInt(viewTagStr.replace("null",""))));
                     }else if(viewTagStr.contains("false")){
                         setLovePoint(Integer.parseInt(viewTagStr.replace("false","")));
                     }else if(viewTagStr.contains("true")){
                         setLovePoint(Integer.parseInt(viewTagStr.replace("true","")));
                     }
                 }
                 break;
         }
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page++;
                getMyDynamics();
                view.onFooterRefreshComplete();
            }
        }, 1000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page=0;
                getMyDynamics();
                view.onHeaderRefreshComplete();
            }
        }, 1000);
    }
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Log.d("plat","platform"+platform);
        }
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {

            if(t!=null){
                Log.d("throw","throw:"+t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {

        }
    };
    public   void   setLovePoint(int  position){
        final Dynamic dynamic= (Dynamic) objectListCopy.get(position);
        AVQuery<Dynamic_Follow>  dynamic_followAVQuery=AVQuery.getQuery(Dynamic_Follow.class);
        dynamic_followAVQuery.whereEqualTo("dynamic_pointer",dynamic);
        dynamic_followAVQuery.whereEqualTo("user_pointer",LeanchatUser.getCurrentUser());
        dynamic_followAVQuery.getFirstInBackground(new GetCallback<Dynamic_Follow>() {
            @Override
            public void done(Dynamic_Follow dynamic_follow, AVException e) {
                if(dynamic_follow!=null){
                    if (dynamic_follow.getState()){
                        dynamic_follow.setState(false);
                        dynamic_follow.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                dynamic.setFollowCount(dynamic.getFollowCount()-1);
                                dynamic.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if(e==null){
                                            dynamicAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });

                            }
                        });
                    }else{
                        dynamic_follow.setState(true);
                        dynamic_follow.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                dynamic.setFollowCount(dynamic.getFollowCount()+1);
                                dynamic.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if(e==null){
                                            dynamicAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });

                            }
                        });
                    }
                }
            }
        });
    }
    public     void  createLovePoint(LeanchatUser  leanchatUser, final Dynamic  dynamic) {
        final Dynamic_Follow dynamic_follow = new Dynamic_Follow();
        dynamic_follow.setDynamic_pointer(dynamic);
        dynamic_follow.setUser_pointer(leanchatUser);
        dynamic_follow.setState(true);
        dynamic_follow.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    dynamic.setFollowCount(1);
                    dynamic.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if(e==null){
                                dynamicAdapter.notifyDataSetChanged();
                            }
                        }
                    });

                } else {
                    dynamic_follow.setState(false);
                }
            }
        });
    }
}
