package com.muki.parttimejob.model;


import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.FollowCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SignUpCallback;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by wli on 15/9/30.
 * 自定义的 AVUser
 */
public class LeanchatUser extends AVUser {
  public static final String USERNAME = "username";
  public static final String AVATAR = "user_HeadImage";
  public static final String LOCATION = "location";
  public static final String INSTALLATION = "installation";
  public static final String USER_COLLEGE= "user_College";
  public static final String USER_LEVEL= "user_Level";
  public static final String USER_SEX= "user_Sex";
  public static final String USER_ADMISSION= "user_AdmissionDate";
  public static final String USER_DEGREE= "user_Degree";
  public static final String MOBILEPHONENUMBER= "mobilePhoneNumber";
  public static final String USER_MAJOR= "user_Major";
  public static final String USER_SCHOOL_ISAUTHENTICATION= "user_School_IsAuthentication";
  public static final String USER_EVALUATION="user_evaluation";
  public static final String USER_FAMILY_NAME="user_Family_Name";
  public static final String USER_SCHOOL= "user_School";
  public static final String USER_NICKNAME= "user_NickName";
  public static final String USER_ID_ISAUTHENTCATION= "user_ID_IsAuthentication";
  public static final String USER_IDCARD= "user_IDcard";




  public String  getUSER_IDCARD( ){
    return   getString(USER_IDCARD);
  }
  public void setUSER_IDCARD(String user_IDcard) {
    put(USER_IDCARD,user_IDcard);
  }

  public int getUSER_ID_ISAUTHENTCATION( ){
    return   getInt(USER_ID_ISAUTHENTCATION);
  }
  public void  setUSER_ID_ISAUTHENTCATION(int   i){
     put(USER_ID_ISAUTHENTCATION,i);
  }
  public void setUSER_NICKNAME(String user_NickName) {
    put(USER_NICKNAME,user_NickName);
  }
  public String getUSER_NICKNAME( ){
    return getString(USER_NICKNAME);
  }
  public void setUSERNAME (String username) {
    put(USERNAME ,username);
  }
  public String getUSERNAME ( ){
    return getString(USERNAME);
  }
  public void setUSER_DEGREE(String user_Degree) {
    put(USER_DEGREE,user_Degree);
  }
  public String getUSER_DEGREE( ){
    return getString(USER_DEGREE);
  }
  public void setUSER_SCHOOL(String user_School) {
    put(USER_SCHOOL,user_School);
  }
  public String getUSER_SCHOOL( ){
    return getString(USER_SCHOOL);
  }
  public String getUSER_FAMILY_NAME( ){
    return getString(USER_FAMILY_NAME);
  }
  public void setUSER_FAMILY_NAME(String user_Family_Name) {
    put(USER_FAMILY_NAME,user_Family_Name);
  }
  public String getUSER_EVALUATION( ){
    return getString(USER_EVALUATION);
  }
  public void setUSER_EVALUATION(String user_evaluation) {
    put(USER_EVALUATION,user_evaluation);
  }
  public int getUSER_SCHOOL_ISAUTHENTICATION( ){
    return getInt(USER_SCHOOL_ISAUTHENTICATION);
  }
  public void setUSER_SCHOOL_ISAUTHENTICATION(int user_School_IsAuthentication) {
    put(USER_SCHOOL_ISAUTHENTICATION,user_School_IsAuthentication);
  }
  public String getUSER_MAJOR( ){
    return getString(USER_MAJOR);
  }
  public void setUSER_MAJOR(String user_Major) {
    put(USER_MAJOR,user_Major);
  }
  public String getMOBILEPHONENUMBER( ){
    return getString(MOBILEPHONENUMBER);
  }
  public void setMOBILEPHONENUMBER(String mobilePhoneNumber) {
    put(MOBILEPHONENUMBER,mobilePhoneNumber);
  }

  public String getUSER_ADMISSION( ){
    return getString(USER_ADMISSION);
  }
  public void setUSER_ADMISSION(String user_AdmissionDate) {
    put(USER_ADMISSION,user_AdmissionDate);
  }
  public String getUSER_SEX( ) {
    return getString(USER_SEX);
  }
  public void setUSER_SEX(String user_Sex) {
    put(USER_SEX,user_Sex);
  }
  public String getUSER_LEVEL( ){
    return getString(USER_LEVEL);
  }
  public void setUSER_LEVEL(String user_Level) {
    put(USER_LEVEL,user_Level);
  }
  public String getUSER_COLLEGE( ){
    return getString(USER_COLLEGE);
  }
  public void setUSER_COLLEGE(String user_College) {
    put(USER_COLLEGE,user_College);
  }
  public static String getCurrentUserId () {
    LeanchatUser currentUser = getCurrentUser(LeanchatUser.class);
    return (null != currentUser ? currentUser.getObjectId() : null);
  }

  public String getAvatarUrl() {
    AVFile file = getAVFile(AVATAR);
    if (file != null) {
      return file.getUrl();
    }
    return "";
  }
  public void saveAvatar(String path, final SaveCallback saveCallback) {
    final AVFile file;
    try {
      file = AVFile.withAbsoluteLocalPath("avatar.png", path);
      put(AVATAR, file);
      file.saveInBackground(new SaveCallback() {
        @Override
        public void done(AVException e) {
          if (null == e) {
             saveInBackground(saveCallback);
          } else {
            if (null != saveCallback) {
              saveCallback.done(e);
            }
          }
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  public static LeanchatUser getCurrentUser() {
    return AVUser.getCurrentUser(LeanchatUser.class);
  }
  public void updateInstallation(AVInstallation  installation ){
    put(INSTALLATION, installation);
  }
  public AVGeoPoint getGeoPoint() {
    return getAVGeoPoint(LOCATION);
  }
  public void setGeoPoint(AVGeoPoint point) {
    put(LOCATION, point);
  }
  public static void signUpByNameAndPwd(String name, String password, SignUpCallback callback) {
    AVUser user = new AVUser();
    user.setUsername(name);
    user.setPassword(password);
    user.signUpInBackground(callback);
  }
  public void removeFriend(String friendId, final SaveCallback saveCallback) {
    unfollowInBackground(friendId, new FollowCallback() {
      @Override
      public void done(AVObject object, AVException e) {
        if (saveCallback != null) {
            saveCallback.done(e);
        }
      }
    });
  }
  public void findFriendsWithCachePolicyfollowee(AVQuery.CachePolicy cachePolicy, FindCallback<LeanchatUser> findCallback) {
    AVQuery<LeanchatUser> q = null;
    try {
      q = followeeQuery(LeanchatUser.class);
    } catch (Exception e) {
    }
    q.setCachePolicy(cachePolicy);
    q.setMaxCacheAge(TimeUnit.MINUTES.toMillis(1));
    q.findInBackground(findCallback);
  }
  public void findFriendsWithCachePolicyfollower(AVQuery.CachePolicy cachePolicy, FindCallback<LeanchatUser> findCallback) {
    AVQuery<LeanchatUser> q = null;
    try {
      q = followerQuery(LeanchatUser.class);
      q.include("user_HeadImage");
    } catch (Exception e) {
    }
    q.setCachePolicy(cachePolicy);
    q.setMaxCacheAge(TimeUnit.MINUTES.toMillis(1));
    q.findInBackground(findCallback);
  }

  public   void  setUserExperience( Integer  user_experience ){
    put("user_experience",user_experience);
  }
  public   Integer  getUserExperience( ){
      return   getInt("user_ability");
  }
  public   void  setUserAbility( Integer  user_ability ){
    put("user_ability",user_ability);
  }
  public   Integer  getUserAbility( ){
      return   getInt("user_ability");
  }

  public   void  setUserComprehensive( Integer  user_comprehensive){
    put("user_comprehensive",user_comprehensive);
  }
  public   Integer  getUserComprehensive( ){
    return   getInt("user_comprehensive");
  }

  public  void   setAlipayAccount(String   user_AliPay_Account){
      put("user_AliPay_Account",user_AliPay_Account);
  }
  public  String   getAlipayAccount( ){
     return   getString("user_AliPay_Account");
  }
  public   Integer  getBalance( ){
    return getInt("user_Balance");
  }
  public   void  setBalance(Integer  user_Balance ){
    put("user_Balance",user_Balance);
  }
  public  void  setBankAccount(String   bankAccount){
     put("user_Bank_Account",bankAccount);
  }
  public  String  getBankAccount( ){
     return  getString("user_Bank_Account");
  }
  public  void  setBankName(String   bankName){
    put("user_Bank_Name",bankName);
  }
  public  String  getBankName( ){
    return  getString("user_Bank_Name");
  }
  public  void   setUserHeight(String  userHeight){
     put("user_Height",userHeight);
  }
  public   String  getUserHeight( ){
     return   getString("user_Height");
  }
  public  void   setUserWeight(String  userWeight){
    put("userWeight",userWeight);
  }
  public   String  getUserWeight( ){
    return   getString("userWeight");
  }
  public  void   setUserStandNum(int  user_StandNum){
    put("user_StandNum",user_StandNum);
  }
  public  int  getUserStandNum( ){
    return   getInt("user_StandNum");
  }

  public  void   setUserBirth(Date user_birth){
    put("birthday",user_birth);
  }
  public  Date  getUserBirth( ){
    return   getDate("birthday");
  }
  public  Date  getBanDate( ){
    return   getDate("BanList");
  }
  public  Boolean  getUserState( ){
    return   getBoolean("user_State");
  }
}






