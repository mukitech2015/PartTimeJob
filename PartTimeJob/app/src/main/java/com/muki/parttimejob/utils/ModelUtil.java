package com.muki.parttimejob.utils;


import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.muki.parttimejob.JobApplication;
import com.muki.parttimejob.R;
import com.muki.parttimejob.entity.ChannelEntity;
import com.muki.parttimejob.entity.FilterEntity;
import com.muki.parttimejob.fragment.SquareFragment;
import com.muki.parttimejob.model.BannerImage;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.CityData;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.JobData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 好吧，让你找到了，这是假的数据源
 *
 * Created by sunfusheng on 16/4/22.
 */
public class ModelUtil {
    // 广告数据
    public static List<BannerImage> getBannerData() {
        List<BannerImage> adList = new ArrayList<>();
        adList.clear();
        adList.addAll(JobApplication.getInstance().bannerList);
        return adList;
    }
    public  static   List<JobData>   getAllJobDatas(String   cityStr){
       List<Job>  jobList=JobApplication.getInstance().jobList;
       List<JobData>   jobDataList=new ArrayList<>();
       for (Job   job:jobList){
           if (job.getJob_Address().contains(cityStr)){
               Business business=job.getAVObject("business_pointer");
               jobDataList.add(new JobData(job,job.getCreatedAt(),job.getJob_RecruitType(),job.getJob_RecruitName(),job.getGeoPoint().getLatitude(),job.getGeoPoint().getLongitude() ,job.jobHot(),job.getJob_Address(),business.getBusiness_Name(),business.businessStyle(),business.IsSalaryGuarantee(),business.IsVip(),job.getJob_EndTime(),job.getJob_StartTime(),business.getComLogoUrl()));
           }
       }
       if(jobDataList.size()==0){
           for (Job  job:jobList){
               if (job.jobHot()!=0&&job.getBoolean("job_IsAtHome")){
                   Business business=job.getAVObject("business_pointer");
                   jobDataList.add(new JobData(job,job.getCreatedAt(),job.getJob_RecruitType(),job.getJob_RecruitName(),job.getGeoPoint().getLatitude(),job.getGeoPoint().getLongitude() ,job.jobHot(),job.getJob_Address(),business.getBusiness_Name(),business.businessStyle(),business.IsSalaryGuarantee(),business.IsVip(),job.getJob_EndTime(),job.getJob_StartTime(),business.getComLogoUrl()));
               }
           }
       }
        return jobDataList;
    }
    public  static   List<JobData>   getNewReleJobDatas(   ){
        List<Job>  jobList=JobApplication.getInstance().jobNewReleList;
        List<JobData>   jobDataList=new ArrayList<>();
        for (Job   job:jobList){
            Business business=job.getAVObject("business_pointer");
            jobDataList.add(new JobData( job,job.getCreatedAt(),job.getJob_RecruitType(),job.getJob_RecruitName(),job.getGeoPoint().getLatitude(),job.getGeoPoint().getLongitude() ,job.jobHot(),job.getJob_Address(),business.getBusiness_Name(),business.businessStyle(),business.IsSalaryGuarantee(),business.IsVip(),job.getJob_EndTime(),job.getJob_StartTime(),business.getComLogoUrl()));
        }
        return jobDataList;
    }

    public  static   List<JobData>   getReceJobDatas(SquareFragment squareFragment){
        List<Job>  jobList=squareFragment.jobRList;
        List<JobData>   jobDataList=new ArrayList<>();
        for (Job   job:jobList){
            Business business=job.getAVObject("business_pointer");
            jobDataList.add(new JobData(job,job.getCreatedAt(),job.getJob_RecruitType(),job.getJob_RecruitName(),job.getGeoPoint().getLatitude(),job.getGeoPoint().getLongitude() ,job.jobHot(),job.getJob_Address(),business.getBusiness_Name(),business.businessStyle(),business.IsSalaryGuarantee(),business.IsVip(),job.getJob_EndTime(),job.getJob_StartTime(),business.getComLogoUrl()));
        }
        return jobDataList;
    }
    public static String getJson(Context context, String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open(fileName),"GBK"));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public static List<ChannelEntity> getChannelDatas( ){
        List<ChannelEntity> channelList = new ArrayList<>(); // 频道数据
        channelList.clear();
        channelList.add(new ChannelEntity("官方任务", R.drawable.task));
        channelList.add(new ChannelEntity("附近兼职",R.drawable.near));
        channelList.add(new ChannelEntity("线上兼职",R.drawable.curtilage));
        channelList.add(new ChannelEntity("实习机会",R.drawable.talent));
        return   channelList;
    }
    // 兼职城市
    public static List<FilterEntity> getJobCityData(Context  context,String city) {
        List<FilterEntity> list = new ArrayList<>();
        list.clear();
        List<CityData>  cityDataList=getAllCityDatas(context);
        for (CityData  cityData:cityDataList){
          if(city.equals(cityData.getCityName())){
             for (int i=0;i<cityData.getRegions().size();i++){
                 list.add(new FilterEntity(cityData.getRegions().get(i),i+"" ));
             }
              list.add(0,new FilterEntity(cityData.getCityName(),"0"));
          }
        }
        return list;
    }
    public static List<CityData>  getAllCityDatas(Context context){
        JSONArray jsonArray= JSON.parseArray(getJson(context,"city.json"));
        List<CityData>  cityDatas=new ArrayList<>();
        for (int i=0;i<jsonArray.size();i++){
            com.alibaba.fastjson.JSONObject jsonObject=jsonArray.getJSONObject(i);
            JSONArray   jsonArrayCity=jsonObject.getJSONArray("city");
            for (int z=0;z<jsonArrayCity.size();z++){
                CityData  cityData=new CityData();
                List<String>  stringList=new ArrayList<>();
                com.alibaba.fastjson.JSONObject jsonObjectCity=jsonArrayCity.getJSONObject(z);
                cityData.setCityName(jsonObjectCity.getString("name"));
                JSONArray  jsonArrayArea=jsonObjectCity.getJSONArray("area");
                stringList.clear();
                for (int j=0;j<jsonArrayArea.size();j++){
                    stringList.add((String) jsonArrayArea.get(j));
                }
                cityData.setRegions(stringList);
                cityDatas.add(cityData);
            }
        }
        return cityDatas;
    }
    // 兼职类型
    public static List<FilterEntity> getJobTypeData() {
        List<FilterEntity> list = new ArrayList<>();
        list.clear();
        list.addAll(JobApplication.getInstance().jobTypes);
        return list;
    }
    // 兼职
    public static List<FilterEntity> getJobOtherData() {
        List<FilterEntity> list = new ArrayList<>();
        list.add(new FilterEntity("最新发布", "1"));
        list.add(new FilterEntity("离我最近", "2"));
        return list;
    }
    // ListView城市数据
    public static List<JobData> getJobCityData(FilterEntity rightEntity,String  cityStr) {
        List<JobData> list =getAllJobDatas(cityStr);
        List<JobData> jobList = new ArrayList<>();
        for (int i=0; i<list.size(); i++) {
            if (list.get(i).getAddress().contains(rightEntity.getKey())) {
                jobList.add(list.get(i));
            }
        }
        return jobList;
    }

    public static List<JobData> getJobTypeData(FilterEntity entity,String  cityStr) {
        List<JobData> list =getAllJobDatas(cityStr);
        List<JobData> jobList = new ArrayList<>();
        int size = list.size();
        for (int i=0; i<size; i++) {
            if (!TextUtils.isEmpty(list.get(i).getJobType())){
                if (list.get(i).getJobType().contains(entity.getKey())) {
                    jobList.add(list.get(i));
                }
            }
        }
        if(entity.getKey().equals("兼职类型")){
            return   list;
        }
        return jobList;
    }
    // ListView排序数据
    public static List<JobData> getJObTypeData(FilterEntity entity,String  cityStr) {
        List<JobData> list =getAllJobDatas(cityStr);
        List<JobData> jobList = new ArrayList<>();
        int size = list.size();
        for (int i=0; i<size; i++) {
            if (list.get(i).getJobType().contains(entity.getKey())) {
                jobList.add(list.get(i));
            }
        }
        return jobList;
    }
    // ListView筛选数据
    public static List<JobData> getJobOtherData(FilterEntity entity,SquareFragment squareFragment) {
        List<JobData> jobList = new ArrayList<>();
        if (entity.getKey().equals("最新发布")){
            jobList=getNewReleJobDatas();
        }else if(entity.getKey().equals("离我最近")){
            jobList=getReceJobDatas(squareFragment);
        }
        return jobList;
    }
    // 暂无数据
    public static List<JobData> getNoDataEntity(int height) {
        List<JobData> list = new ArrayList<>();
        JobData entity = new JobData();
        entity.setNoData(true);
        entity.setHeight(height);
        list.add(entity);
        return list;
    }
}
