package com.muki.parttimejob.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.ExperienceAdapter;
import com.muki.parttimejob.adapter.ScrollLvAdapter;
import com.muki.parttimejob.entity.ImageInfo;
import com.muki.parttimejob.event.ReleaseStyleImageEvent;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ImageUtil;
import com.muki.parttimejob.widget.FullyLinearLayoutManager;
import com.muki.parttimejob.widget.NoScrollListView;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by muki on 2016/12/2.
 */

public class MyResumeActivity  extends AppCompatActivity {
    private RecyclerView userStyleRv;
    private  RelativeLayout resumeBackLat;
    private ScrollLvAdapter styleImageAdapter;
    NoScrollListView eduLv;
    NoScrollListView  practiceLv;
    ExperienceAdapter eduAdapter;
    ExperienceAdapter  practiceAdapter;
    RelativeLayout eduLat;
    RelativeLayout  practiceLat;
    RelativeLayout  selfEvaluationLat;
    List<AVObject> edulist=new ArrayList<>();
    List<AVObject> priceList=new ArrayList<>();
    List<AVObject>   imageObjList=new ArrayList<>();
    List<ImageInfo>   imageInfoList=new ArrayList<>();
    List<String>   strings=new ArrayList<String>();
    SimpleDateFormat formatter = new    SimpleDateFormat("yyyy-MM-dd");
    TextView evaluationTv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_resume);
        initViews();
        EventBus.getDefault().register(this);
    }
    public void onEventMainThread(ReleaseStyleImageEvent event) {
        try {
            saveStyleImage(event.getImages());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        initDatas();
    }

    public  void  initDatas(  ){
        getExperienceDatas("教育经历");
        getExperienceDatas("实践经历");
        getStyleImages();
        evaluationTv.setText(LeanchatUser.getCurrentUser().getUSER_EVALUATION()+"");
    }
    public  void  initViews(  ){
        evaluationTv= (TextView)findViewById(R.id.evaluationTv);
        resumeBackLat= (RelativeLayout) findViewById(R.id.resumeBackLat);
        userStyleRv= (RecyclerView)findViewById(R.id.userStyleRv);
        FullyLinearLayoutManager linearLayoutManager=new FullyLinearLayoutManager(this);
        userStyleRv.setNestedScrollingEnabled(false);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        userStyleRv.setLayoutManager(linearLayoutManager);
        selfEvaluationLat= (RelativeLayout) findViewById(R.id.selfEvaluationLat);
        eduLv= (NoScrollListView)findViewById(R.id.eduLv);
        practiceLv= (NoScrollListView) findViewById(R.id.practiceLv);
        eduLat= (RelativeLayout)findViewById(R.id.addEduExperLat);
        practiceLat= (RelativeLayout)findViewById(R.id.addPractExperLat);
        selfEvaluationLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent    selfEvaluationIntent=new Intent(MyResumeActivity.this,EvaluationActivity.class);
                startActivity(selfEvaluationIntent);
            }
        });
        resumeBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        eduLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent=new Intent(MyResumeActivity.this, AddEduExperienceActivity.class);
                startActivity(intent);
            }
        });
        practiceLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent=new Intent(MyResumeActivity.this,AddPracExperienceActivity.class);
                startActivity(intent);
            }
        });

    }

    AlertDialog.Builder builder;
    public  void  showDelete(final AVObject avObject, final View view){
        if(builder==null){
            builder=new AlertDialog.Builder(this);
        }
        builder.setTitle("删除");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage("确定要删除么？");
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                avObject.deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(AVException e) {
                        if(e==null){
                            if(view.getId()==eduLv.getId()){
                                edulist.remove(avObject);
                                eduAdapter.notifyDataSetChanged();
                            }else if (view.getId()==practiceLv.getId()){
                                priceList.remove(avObject);
                                practiceAdapter.notifyDataSetChanged();
                            }else{
                                imageObjList.remove(avObject);
                                styleImageAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
            }
        }).show();
    }
    public   void  getExperienceDatas(final String   user_Info_Record_Type){
        AVRelation<AVObject> relation = LeanchatUser.getCurrentUser().getRelation("user_Info_relation");
        AVQuery<AVObject> query = relation.getQuery();
        if (user_Info_Record_Type.equals("教育经历")){
            query.whereEqualTo("user_Info_Record_Type","教育经历");
        }else{
            query.whereEqualTo("user_Info_Record_Type","实践经历");
        }
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(list!=null) {
                    if(user_Info_Record_Type.equals("教育经历")){
                        edulist.clear();
                        edulist.addAll(list);
                        setEduAdapter(edulist);
                    }else{
                        priceList.clear();
                        priceList.addAll(list);
                        setPractiveAdapter(priceList);
                    }
                }
            }
        });
    }
    public  void  setPractiveAdapter(final List<AVObject> list){
        if(practiceAdapter==null){
            practiceAdapter=new ExperienceAdapter(list,this);
            practiceLv.setAdapter( practiceAdapter);
        }
        practiceAdapter.changeAdapterData(list);
        practiceLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                showDelete(list.get(i),practiceLv);
                return true;
            }
        });
        practiceLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(MyResumeActivity.this,AddPracExperienceActivity.class);
                intent.putExtra("objectId",list.get(position).getObjectId());
                intent.putExtra("user_Info_End_Date",formatter.format(list.get(position).getDate("user_Info_End_Date")));
                intent.putExtra("user_Info_Start_Date",formatter.format(list.get(position).getDate("user_Info_Start_Date")));
                intent.putExtra("user_Info_Record_Address",list.get(position).getString("user_Info_Record_Address"));
                intent.putExtra("user_Info_Record",list.get(position).getString("user_Info_Record"));
                startActivity(intent);
            }
        });
    }
    public  void  setEduAdapter(final List<AVObject> list ){
        if( eduAdapter==null){
            eduAdapter=new ExperienceAdapter(list,this);
            eduLv.setAdapter(eduAdapter);
        }
        eduAdapter.changeAdapterData(list);
        eduLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent  intent=new Intent(MyResumeActivity.this,AddEduExperienceActivity.class);
                intent.putExtra("objectId",list.get(position).getObjectId());
                intent.putExtra("user_Info_End_Date",formatter.format(list.get(position).getDate("user_Info_End_Date")));
                intent.putExtra("user_Info_Start_Date",formatter.format(list.get(position).getDate("user_Info_Start_Date")));
                intent.putExtra("user_Info_Record_Address",list.get(position).getString("user_Info_Record_Address"));
                intent.putExtra("user_Info_Record",list.get(position).getString("user_Info_Record"));
                startActivity(intent);
            }
        });
        eduLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                showDelete(list.get(i),eduLv);
                return true;
            }
        });
    }
    public void  getStyleImages( ){
        AVRelation<AVObject> relation =LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
        AVQuery<AVObject> query = relation.getQuery();
        query.whereEqualTo("user_Image_Type","个人风采");
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
               if(list!=null){
                   imageObjList.clear();
                   imageObjList.addAll(list);
                   imageInfoList.clear();
                   for(AVObject avObject:imageObjList){
                       ImageInfo  imageInfo=new ImageInfo();
                       imageInfo.setBigImageUrl(avObject.getAVFile("user_Image").getUrl());
                       imageInfo.setThumbnailUrl(avObject.getAVFile("user_Thumbnail_Image").getUrl());
                       imageInfoList.add(imageInfo);
                   }
                   setStyleImageAdapter(imageInfoList);
               }
            }
        });
    }

    public  void  saveStyleImage(List<String>   stringList) throws FileNotFoundException {
        for (int i = 0; i < stringList.size(); i++) {
            final String  bigImagePath=stringList.get(i);
            final AVFile avFileBig=AVFile.withAbsoluteLocalPath("bigImage.png",stringList.get(i));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            // 获取图片的宽和高
            Bitmap bitmap = BitmapFactory.decodeFile(stringList.get(i), options);
            ThumbnailUtils.extractThumbnail(bitmap,50,50);
            options.inJustDecodeBounds = false;
            Bitmap bitmapThum = BitmapFactory.decodeFile(stringList.get(i), options);
            final String newPath= ImageUtil.getAbsoluteImagePath(Uri.parse(MediaStore.Images.Media.insertImage(MyResumeActivity.this.getContentResolver(), bitmapThum , null,null)),MyResumeActivity.this);
            final AVFile avFile=AVFile.withAbsoluteLocalPath("Image.png",newPath);
            try {
                AVObject.saveFileBeforeSave(Arrays.asList(avFile, avFileBig), false, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e==null){
                            final AVObject userImageObj=new AVObject("User_Image_Info");
                            userImageObj.put("user_Thumbnail_Image",avFile);
                            userImageObj.put("user_Image",avFileBig);
                            userImageObj.put("user_Image_Type","个人风采");
                            userImageObj.put("user_pointer",LeanchatUser.getCurrentUser());
                            AVObject.saveAllInBackground(Arrays.asList(userImageObj), new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> userImageInfoRelation=LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
                                    userImageInfoRelation.add(userImageObj);
                                    LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            if(e==null){
                                                getStyleImages();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } catch (AVException e) {
                e.printStackTrace();
            }
        }
    }
    public   void  setStyleImageAdapter(final List<ImageInfo>   imageInfoList){
        if (styleImageAdapter==null){
            styleImageAdapter=new ScrollLvAdapter(imageInfoList,MyResumeActivity.this);
            userStyleRv.setAdapter(styleImageAdapter);
        }else{
            styleImageAdapter.changeAdapter(imageInfoList);
        }
        styleImageAdapter.setOnItemLongClickListener(new ScrollLvAdapter.OnRecycleViewItemLongClickListener() {
            @Override
            public void OnItemLongClick(View view, final int position) {
                new  AlertDialog.Builder(MyResumeActivity.this).setIcon(R.drawable.ic_launcher)
                        .setTitle("删除照片")
                        .setMessage("确定删除该照片")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                imageObjList.get(position).deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if(e==null){
                                            imageInfoList.remove(imageInfoList.get(position));
                                            styleImageAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                            }
                        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
        styleImageAdapter.setOnItemClickListener(new ScrollLvAdapter.OnRecycleViewItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                if (position==imageInfoList.size()){
                    AndroidImagePicker.getInstance().pickMulti(MyResumeActivity.this,true, new AndroidImagePicker.OnImagePickCompleteListener() {
                        @Override
                        public void onImagePickComplete(List<ImageItem> items) throws FileNotFoundException {
                            if(items != null && items.size() > 0){
                                strings.clear();
                                for (ImageItem  imageItem:items){
                                    strings.add(imageItem.path);
                                }
                                saveStyleImage(strings);
                            }
                        }
                    });
                }else{
                    Intent intent = new Intent(MyResumeActivity.this, ImagePreviewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ImagePreviewActivity.IMAGE_INFO, (Serializable) imageInfoList);
                    bundle.putInt(ImagePreviewActivity.CURRENT_ITEM, position);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            }
        });
    }
}
