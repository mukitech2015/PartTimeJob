package com.muki.parttimejob.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.muki.parttimejob.JobApplication;
import com.muki.parttimejob.utils.FileUtil;

import java.io.File;
import java.util.Set;

/**
 * Created by muki on 2016/9/12.
 */
public class AppConfig {
    public  static  double  mylatitude;
    public  static  double  mylongitude;
    public  static  String  myCity="";
    public  static  String  myAddress;
    public static final String OBJECT_ID = "objectId";
    public static final int PAGE_SIZE = 10;
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    public static final int ORDER_UPDATED_AT = 1;
    public static final int ORDER_DISTANCE = 0;

    private static final String LEANMESSAGE_CONSTANTS_PREFIX = "com.avoscloud.leanchatlib.";

    public static final String INVITATION_ACTION = "invitation_action";

    public static final String LEANCHAT_USER_ID = getPrefixConstant("leanchat_user_id");

    public static final String INTENT_KEY = getPrefixConstant("intent_key");
    public static final String INTENT_VALUE = getPrefixConstant("intent_value");

    //Notification
    public static final String NOTOFICATION_TAG = getPrefixConstant("notification_tag");
    public static final String NOTIFICATION_SINGLE_CHAT = AppConfig.getPrefixConstant("notification_single_chat");
    public static final String NOTIFICATION_GROUP_CHAT = AppConfig.getPrefixConstant("notification_group_chat");
    public static final String NOTIFICATION_SYSTEM = AppConfig.getPrefixConstant("notification_system_chat");

    public static String getPrefixConstant(String str) {
        return LEANMESSAGE_CONSTANTS_PREFIX + str;
    }
    public   static   final  String   ACTION_CANCEL_REG="取消报名";
    public    static  final  String   ACTION_WDW_AND_CANCEL="结款/取消";
    public    static  final  String   ACTION_WAIT_MER_PAY="等待商户付款";
    public    static   final String   ACTION_RETURN_POST="重新提交";
    public    static   final String   ACTION_COMMT="评价";
    public    static   final String   ACTION_COMPLETE="已完成";
    public    static   final String   ACTION_ALREADY_CANCEL="已取消";
    public    static   final String   ACTION_NOT_PASS="未通过";
    public   static  final String     REG_IN="报名中";
    public   static  final String     AlREADY_PASSWORD="已通过";
    public   static  final String  WAIT_SETTLE="待结款";
    public   static   final  String  COMMT="待评价";
    public   static   final  String  COMPLETE="已完成";
    public   static   final   String   YUZHIGONGZI="http://www.cloudconfs.com/part/index.php/Share/Anticipate/view?objectId=";
    public   static   final   String   FINDJOB="http://www.cloudconfs.com/part/index.php/Share/Searching/view?objectId=";
    public   static   final   String   JIESUANTONG="http://parttime.leanapp.cn/about/jiesuantong";
    public    static  final   String   SHIYONG="http://www.cloudconfs.com/part/index.php/Share/Manual/index";
    public    static  final   String   BUSINESS="http://www.cloudconfs.com/part/index.php/Share/Business_infor/view?objectId=";
    public    static   final String   ABOUT="http://www.cloudconfs.com/part/index.php/Share/About";
    public  static  final  String   SHARE_APP="http://www.cloudconfs.com/part/index.php/Share/Software/share";
    public final static String NeedCallback = "NeedCallback";
    public final static int  ANONYMOUS_LAT =1;
    public final static int  FRIEND_LAT = 2;
    public final static int   SQUARE_LAT = 0;
    public final static int   MORE_IV = 3;
    private static AppConfig appConfig;

    private SharedPreferences preferences;

    /**
     * 是否是测试环境.
     */
    public static final boolean DEBUG = false;

    /**
     * App根目录.
     */
    public String APP_PATH_ROOT;

    private AppConfig() {
        preferences = JobApplication.getInstance().getSharedPreferences("PART_TIME_JOB", Context.MODE_PRIVATE);

        APP_PATH_ROOT = FileUtil.getRootPath().getAbsolutePath() + File.separator + "PART_TIME_JOB";
        FileUtil.initDirectory(APP_PATH_ROOT);
    }

    public static AppConfig getInstance() {
        if (appConfig == null)
            appConfig = new AppConfig();
        return appConfig;
    }

    public void putInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public void putString(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public void putLong(String key, long value) {
        preferences.edit().putLong(key, value).commit();
    }

    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    public void putFloat(String key, float value) {
        preferences.edit().putFloat(key, value).commit();
    }

    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public void putStringSet(String key, Set<String> value) {
        preferences.edit().putStringSet(key, value).commit();
    }

    public Set<String> getStringSet(String key, Set<String> defValue) {
        return preferences.getStringSet(key, defValue);
    }

}
