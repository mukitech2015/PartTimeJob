package com.muki.parttimejob.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.DynMsgAdapter;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/10/25.
 */

public class DynMsgActivity extends   BaseActivity implements View.OnClickListener,PullBaseView.OnFooterRefreshListener,PullBaseView.OnHeaderRefreshListener{
     private LinearLayout dynMsgBackLat;
     private PullRecyclerView  dynMsgPrv;
     private DynMsgAdapter dynMsgAdapter;
     private List<AVObject>   dynMsgObjList=new ArrayList<>();
     private    int    page=0;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
       setContentView(R.layout.layout_msg_list);
    }
    @Override
    protected void initViews() {
        dynMsgBackLat= (LinearLayout) findViewById(R.id.dynMsgBackLat);
        dynMsgBackLat.setOnClickListener(this);
        dynMsgPrv= (PullRecyclerView) findViewById(R.id.dynMsgPrv);
        dynMsgPrv.setOnHeaderRefreshListener(this);
        dynMsgPrv.setOnFooterRefreshListener(this);
        dynMsgPrv.setOnClickListener(this);
        dynMsgPrv.setLayoutManager(new LinearLayoutManager(DynMsgActivity.this),DynMsgActivity.this);
    }
    @Override
    protected void initDatas() {

    }
    @Override
    protected void onStart() {
        super.onStart();
        getDynNewComtDatas();
    }
    public   void   getDynNewComtDatas(){
          AVQuery<AVObject>   avObjectAVQuery=new AVQuery<>("Dynamic_Comment");
          avObjectAVQuery.whereEqualTo("isRead",false);
          avObjectAVQuery.whereEqualTo("dynamic_Reply_User_pointer", LeanchatUser.getCurrentUser());
          avObjectAVQuery.include("dynamic_Comment_User_pointer");
          avObjectAVQuery.include("dynamic_Comment");
          avObjectAVQuery.orderByDescending("createdAt");
          avObjectAVQuery.skip(page*10);// 跳过 10 条结果
          avObjectAVQuery.limit(10);
          avObjectAVQuery.findInBackground(new FindCallback<AVObject>() {
              @Override
              public void done(List<AVObject> list, AVException e) {
                  if (e==null){
                        for (AVObject  avObject:list){
                          avObject.put("isRead",true);
                          avObject.saveInBackground();
                         }
                      if(page==0){
                          dynMsgObjList.clear();
                          dynMsgObjList.addAll(list);
                          setDynMsgAdapter(dynMsgObjList);
                      }else{
                          dynMsgObjList.addAll(dynMsgObjList.size(),list);
                          setDynMsgAdapter(dynMsgObjList);
                      }
                  }
              }
          });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.dynMsgBackLat:
             finish();
             break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public  void   setDynMsgAdapter(List<AVObject> list){
           if(dynMsgAdapter==null){
               dynMsgAdapter=new DynMsgAdapter(DynMsgActivity.this,list);
               dynMsgPrv.setAdapter(dynMsgAdapter);
           }else{
               dynMsgAdapter.changeAdapterData(list);
           }
        }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page++;
                getDynNewComtDatas();
                view.onFooterRefreshComplete();
            }
        }, 1000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page=0;
                getDynNewComtDatas();
                view.onHeaderRefreshComplete();
            }
        }, 1000);
    }
}
