package com.muki.parttimejob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.model.Search;

import java.util.List;

/**
 * Created by muki on 2016/11/11.
 */

public class JobHistoryListAdapter   extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Search>   stringList;
    public  JobHistoryListAdapter(Context context,List<Search>   stringList){
        this.context=context;
        this.stringList=stringList;
        layoutInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return stringList==null?0:stringList.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.layout_search_job_history, null);
            holder.hisStrTv= (TextView) convertView.findViewById(R.id.hisStrTv);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
            holder.hisStrTv.setText(stringList.get(position).getString());
        return convertView;
    }
    class  Holder{
        private TextView  hisStrTv;
    }
    public  void  changeAdapterData(List<Search>   stringList){
        this.stringList=stringList;
        notifyDataSetChanged();
    }
}
