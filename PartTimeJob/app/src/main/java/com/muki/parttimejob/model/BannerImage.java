package com.muki.parttimejob.model;

/**
 * Created by muki on 2016/12/7.
 */

public class BannerImage {

   public  BannerImage(String  imageUrl,String  bannerUrl){
        this.bannerUrl=bannerUrl;
        this.imageUrl=imageUrl;
    }
   private String  imageUrl;
    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String  bannerUrl;
}
