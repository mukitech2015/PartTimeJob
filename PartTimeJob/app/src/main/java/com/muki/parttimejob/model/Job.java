package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by muki on 2016/10/10.
 */

@AVClassName("Job")
public class Job  extends AVObject implements Serializable {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public static final String  JOB_ENDTIME= "job_EndTime";
    public static final String  JOB_RESPONSIBILITY= "job_Responsibility";
    public static final String  JOB_PRICE= "job_Price";
    public static final String  JOB_SPECIALEXPLANATION= "job_SpecialExplanation";
    public static final String  JOB_INDUSTY= "job_Industy";
    public static final String  JOB_SEXPREFERENCE= "job_SexPreference";
    public static final String  JOB_RECRUITTYPE="job_RecruitType";
    public static final String  JOB_REGDEADLINE="job_RegDeadLine";
    public static final String  JOB_Difficulty="job_Difficulty";
    public static final String  JOB_WEEKENDJOB="job_WeekendJob";
    public static final String  JOB_WORKDATE="job_WorkDate";
    public static final String  JOB_MROSE="job_Mrose";
    public static final String  JOB_CITY="job_City";
    public static final String  JOB_RECRUITNAME="job_RecruitName";
    public static final String  JOB_CONTACT="job_Contact";
    public static final String  JOB_SALARY_LEVEL="job_Salary_Level";
    public static final String  JOB_STARTTIME="job_StartTime";
    public static final String  JOB_CERTIFICATE="job_Certificate";
    public static final String  JOB_UNIT="job_Unit";
    public static final String  JOB_RECRUITNUM="job_RecruitNum";
    public static final String  JOB_ADDRESS="job_Address";
    public static final String  JOB_SALARY_INFO="job_Salary_Info";
    public static final String BUSINESS_POINTER="business_pointer";
    public static final String JOB_DISTANCEGEO="job_DistanceGEO";
    public static final String JOB_REQUIREMENTS="job_Requirements";


    public  String  getJob_Requirements( ){
        return    getString(JOB_REQUIREMENTS);
    }
    public  void    setJob_Requirements(String job_Requirements){
        put(JOB_REQUIREMENTS,job_Requirements);
    }
    public AVGeoPoint getGeoPoint() {
        return getAVGeoPoint(JOB_DISTANCEGEO);
    }
    public void setGeoPoint(AVGeoPoint point) {
        put(JOB_DISTANCEGEO, point);
    }
    public  String  getJob_Salary_Info( ){
        return    getString(JOB_SALARY_INFO);
    }
    public  void    setJob_Salary_Info(String job_Salary_Info){
        put(JOB_SALARY_INFO,job_Salary_Info);
    }
    public  String  getJob_Address( ){
        return    getString(JOB_ADDRESS);
    }
    public  void    setJob_Address(String job_Address){
        put(JOB_ADDRESS,job_Address);
    }

    public  String  getJob_RecruitNum( ){
        return    getString(JOB_RECRUITNUM);
    }
    public  void    setJob_RecruitNum(String job_RecruitNum){
        put(JOB_RECRUITNUM,job_RecruitNum);
    }

    public  String  getJob_Unit( ){
        return    getString(JOB_UNIT);
    }
    public  void    setJob_Unit(String job_Unit){
        put(JOB_UNIT,job_Unit);
    }
    public  String  getJob_Certificate( ){
        return    getString(JOB_CERTIFICATE);
    }
    public  void    setJob_Certificate(String job_Certificate){
        put(JOB_CERTIFICATE,job_Certificate);
    }

    public Date getJob_StartTime( ){
        return    getDate(JOB_STARTTIME);
    }
    public  void    setJob_StartTime(Date job_StartTime){
        put(JOB_STARTTIME,job_StartTime);
    }
    public  String  getJob_Salary_Level( ){
        return    getString(JOB_SALARY_LEVEL);
    }
    public  void    setJob_Salary_Level(String job_Salary_Level){
        put(JOB_SALARY_LEVEL,job_Salary_Level);
    }
    public  String  getJob_Contact( ){
        return    getString(JOB_CONTACT);
    }
    public  void    setJob_Contact(String  job_Contact){
        put(JOB_CONTACT,job_Contact);
    }
    public  String  getJob_RecruitName( ){
        return    getString(JOB_RECRUITNAME);
    }
    public  void    setJob_RecruitName(String  job_RecruitName){
        put(JOB_RECRUITNAME,job_RecruitName);
    }
    public  String  getJob_City( ){
        return    getString(JOB_CITY);
    }
    public  void    setJob_City(String  job_City){
        put(JOB_CITY,job_City);
    }
    public  String  getJob_Mrose( ){
        return    getString(JOB_MROSE);
    }
    public  void    setJob_Mrose(String  job_Mrose){
        put(JOB_MROSE,job_Mrose);
    }
    public  String  getJob_WorkDate( ){
        return    getString(JOB_WORKDATE);
    }
    public  void    setJob_WorkDate(String   job_WorkDate){
        put(JOB_WORKDATE,job_WorkDate);
    }
    public  String  getJob_WeekendJob( ){
        return    getString(JOB_WEEKENDJOB);
    }
    public  void    setJob_WeekendJob(String   job_WeekendJob){
        put(JOB_WEEKENDJOB,job_WeekendJob);
    }
    public  Date  getJob_RegDeadLine( ){
        return    getDate(JOB_REGDEADLINE);
    }
    public  void    setJob_RegDeadLine(Date  job_RegDeadLine){
        put(JOB_REGDEADLINE,job_RegDeadLine);
    }
    public  String  getJob_Difficulty( ){
        return    getString(JOB_Difficulty);
    }
    public  void    setJob_Difficulty(String   job_Difficulty){
        put(JOB_Difficulty,job_Difficulty);
    }
    public  String  getJob_RecruitType( ){
        return    getString(JOB_RECRUITTYPE);
    }
    public  void    setJob_RecruitType(String   job_RecruitType){
        put(JOB_RECRUITTYPE,job_RecruitType);
    }
    public  String  getJob_SexPreference( ){
        return    getString(JOB_SEXPREFERENCE);
    }
    public  void    setJob_SexPreference(String   job_SexPreference){
        put(JOB_SEXPREFERENCE,job_SexPreference);
    }
    public  String  getJob_Industy( ){
        return    getString(JOB_INDUSTY);
    }
    public  void    setJob_Industy(String   job_Industy){
        put(JOB_INDUSTY,job_Industy);
    }
    public  String  getJob_SpecialExplanation( ){
        return    getString(JOB_SPECIALEXPLANATION);
    }
    public  void    setJob_SpecialExplanation(String   job_SpecialExplanation){
        put(JOB_SPECIALEXPLANATION,job_SpecialExplanation);
    }
    public  double  getJob_Price( ){
        return    getDouble(JOB_PRICE);
    }
    public  void    setJob_Price(double  job_Price){
        put(JOB_PRICE,job_Price);
    }
    public Job() {

    }
    public  String  getJob_Responsibility( ){
        return    getString(JOB_RESPONSIBILITY);
    }
    public  void    setJob_Responsibility(String   job_Responsibility){
        put(JOB_RESPONSIBILITY,job_Responsibility);
    }
    public  Date  getJob_EndTime( ){
         return    getDate(JOB_ENDTIME);
    }
    public  void    setJob_EndTime(Date   job_EndTime){
        put(JOB_ENDTIME,job_EndTime);
    }
    public  int   jobHot(){
        return  getInt("job_Hot");
    }
    public  boolean   jobState(){
        return   getBoolean("job_States");
    }
}













