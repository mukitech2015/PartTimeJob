package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

/**
 * Created by muki on 2016/10/31.
 */
@AVClassName("BudgetHistory")
public class BudgetHistory extends AVObject{
    public static final Creator CREATOR = AVObjectCreator.instance;
    public   void  setBudgetMoney(double  budget_Money){
      put("budget_Money",budget_Money);
    }
    public  double   getBudgetMoney(){
        return  getDouble("budget_Money");
    }
    public   void  setBudgetDescription(String  budget_Description){
        put("budget_Description",budget_Description);
    }
    public  String   getBudgetDescription( ){
        return  getString("budget_Description");
    }
    public   void  setUserPointer(LeanchatUser  leanchatUser ){
        put("user_pointer",leanchatUser);
    }
    public   LeanchatUser  getUserPointer( ){
        return getAVObject("user_pointer");
    }
    public   void setCurrentBalance(double  currentBalance){
        put("current_Balance",currentBalance);
    }
    public   double   getCurrentBalance( ){
        return  getDouble("current_Balance");
    }
    public   void setBudgetWithdrawApply(BudgetWithdrawApply  budgetWithdrawApply){
        put("budgetWithdrawApply_pointer",budgetWithdrawApply);
    }
    public   BudgetWithdrawApply   getBudgetWithdrawApply( ) {
        return getAVObject("budgetWithdrawApply_pointer");
    }
}




