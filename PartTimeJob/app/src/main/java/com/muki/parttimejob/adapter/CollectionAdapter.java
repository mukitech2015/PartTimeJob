package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.GetCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/10/28.
 */

public class CollectionAdapter  extends BaseAdapter<CollectionAdapter .ViewHolder> {
    DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
    private View.OnClickListener  onClickListener;
    public CollectionAdapter (Context context, List<AVObject> listDatas,View.OnClickListener  onClickListener) {
        super(context,listDatas);
        this.onClickListener=onClickListener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_collection_item, parent, false));
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        AVObject  avObject=listDatas.get(position);
        final Job  jobObj=avObject.getAVObject("job_pointer");
        jobObj.fetchInBackground("business_pointer",new GetCallback<AVObject>() {
            @Override
            public void done(AVObject avObject, AVException e) {
                   if (e==null){
                       Job  job= (Job) avObject;
                       SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
                       SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
                       try {
                           holder.dateTv.setText(sdf.format(sf.parse(job.getJob_StartTime()+""))+"至"+sdf.format(sf.parse(job.getJob_EndTime()+"")));
                       } catch (ParseException e1) {
                           e1.printStackTrace();
                       }
                       holder.recrutmentTitleTv.setText(job.getJob_RecruitName());
                       holder.priceTv.setText(job.getJob_Price()+""+job.getJob_Unit());
                       double  latitude =job.getGeoPoint().getLatitude();
                       double  longitude=job.getGeoPoint().getLongitude();
                       AVGeoPoint point = new AVGeoPoint(latitude,longitude);
                       double distance=point.distanceInKilometersTo(new AVGeoPoint(AppConfig.mylatitude,AppConfig.mylongitude));
                       holder.locationTv.setText(job.getJob_Address());
                       holder.distanceTv.setText(decimalFormat.format(Double.valueOf(distance))+"千米");
                       Business  business= (Business) job.get("business_pointer");
                       UpLoadImageUtil.loadCropImage(context,business.getComLogoUrl(),holder.companyLogoIv);
                       if(business.IsVip()){
                           holder.vipIv.setVisibility(View.VISIBLE);
                       }else{
                           holder.vipIv.setVisibility(View.INVISIBLE);
                       }
                       if(business.IsSalaryGuarantee()){
                           holder.baozhangIv.setVisibility(View.VISIBLE);
                       }else{
                           holder.baozhangIv.setVisibility(View.INVISIBLE);
                       }
                       holder.remCollectLat.setTag(position);
                       holder.remCollectLat.setOnClickListener(onClickListener);
                   }
            }
        });

    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView recrutmentTitleTv;
        private  TextView   dateTv;
        private  TextView  priceTv;
        private  TextView  locationTv;
        private ImageView companyLogoIv;
        private TextView   distanceTv;
        private  ImageView   vipIv;
        private  ImageView   baozhangIv;
        private RelativeLayout  remCollectLat;
        public ViewHolder(View view) {
            super(view);
            remCollectLat= (RelativeLayout) view.findViewById(R.id.remCollectLat);
            baozhangIv= (ImageView) view.findViewById(R.id.collection_baozhangIv);
            vipIv= (ImageView) view.findViewById(R.id.collection_vipIv);
            priceTv= (TextView) view.findViewById(R.id.collection_price_tv);
            distanceTv= (TextView) view.findViewById(R.id.collection_distanceTv);
            companyLogoIv= (ImageView) view.findViewById(R.id.collection_company_logo_lv);
            dateTv= (TextView) view.findViewById(R.id.collection_date_tv);
            locationTv= (TextView) view.findViewById(R.id.collection_location_tv);
            recrutmentTitleTv= (TextView) view.findViewById(R.id.collection_recrutment_title_tv);
            priceTv= (TextView) view.findViewById(R.id.collection_price_tv);
        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
        this.listDatas=listDatas;
        notifyDataSetChanged();
    }
}
