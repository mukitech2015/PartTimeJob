package com.muki.parttimejob.utils;

import android.text.TextUtils;
import android.util.Log;

import com.avos.avoscloud.AVCallback;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.leancloud.chatkit.LCChatKit;
import cn.leancloud.chatkit.LCChatKitUser;
import cn.leancloud.chatkit.cache.LCIMProfileCache;

/**
 * Created by muki on 2016/10/17.
 */

public class LCIMConversationUtils {
    public LCIMConversationUtils() {


    }
    public static void getConversationName(AVIMConversation conversation, final AVCallback<String> callback) {
        if(null != callback) {
            if(null == conversation) {
                callback.internalDone((String) null, new AVException(new Throwable("conversation can not be null!")));
            } else {
                if(conversation.isTransient()) {
                    Log.e("conversation.getName()",conversation.getName()+"==");
                    callback.internalDone(conversation.getName(), (AVException)null);
                } else if(2 == conversation.getMembers().size()) {
                    String peerId = getConversationPeerId(conversation);
                    LCIMProfileCache.getInstance().getUserName(peerId, callback);
                } else if(!TextUtils.isEmpty(conversation.getName())) {
                    callback.internalDone(conversation.getName(), (AVException)null);
                } else {
                    LCIMProfileCache.getInstance().getCachedUsers(conversation.getMembers(), new AVCallback<List<LCChatKitUser>>() {
                        @Override
                        protected void internalDone0(List<LCChatKitUser>  lcimUserProfiles, AVException e) {
                            ArrayList nameList = new ArrayList();
                            if(null != lcimUserProfiles) {
                                Iterator i$ = lcimUserProfiles.iterator();
                                while(i$.hasNext()) {
                                    LCChatKitUser userProfile = (LCChatKitUser)i$.next();
                                    nameList.add(userProfile.getUserName());
                                }
                            }
                            callback.internalDone(TextUtils.join(",", nameList), e);
                        }
                    });
                }
            }
        }
    }
    public static void getConversationPeerIcon(AVIMConversation conversation, AVCallback<String> callback) {
        if(null != conversation && !conversation.isTransient() && !conversation.getMembers().isEmpty()) {
            String peerId = getConversationPeerId(conversation);
            if(1 == conversation.getMembers().size()) {
                peerId = (String)conversation.getMembers().get(0);
            }
            LCIMProfileCache.getInstance().getUserAvatar(peerId, callback);
        } else {
            callback.internalDone((String) null, new AVException(new Throwable("cannot find icon!")));
        }
    }
    private static String getConversationPeerId(AVIMConversation conversation) {
        if(null != conversation && 2 == conversation.getMembers().size()) {
            String currentUserId = LCChatKit.getInstance().getCurrentUserId();
            String firstMemeberId = (String)conversation.getMembers().get(0);
            return (String)conversation.getMembers().get(firstMemeberId.equals(currentUserId)?1:0);
        } else {
            return "";
        }
    }
}
