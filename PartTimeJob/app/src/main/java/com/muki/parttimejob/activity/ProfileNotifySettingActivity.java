package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.muki.parttimejob.R;

/**
 * Created by muki on 2016/11/9.
 */

public class ProfileNotifySettingActivity extends AppCompatActivity{
    LinearLayout   setNotiftLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setting_notify_layout);
        setNotiftLat= (LinearLayout) findViewById(R.id.setNotiftLat);
        setNotiftLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}








