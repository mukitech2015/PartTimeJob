package com.muki.parttimejob.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ChatRoomActivity;
import com.muki.parttimejob.activity.ContactAddFriendActivity;
import com.muki.parttimejob.activity.ContactNewFriendActivity;
import com.muki.parttimejob.activity.LCIMConversationListActivity;
import com.muki.parttimejob.activity.MainActivity;
import com.muki.parttimejob.activity.MyFriendActivity;
import com.muki.parttimejob.activity.SysMsgActivity;
import com.muki.parttimejob.model.AddRequestManager;

import java.util.List;

import cn.leancloud.chatkit.cache.LCIMConversationItemCache;
import cn.leancloud.chatkit.utils.LCIMConstants;

/**
 * Created by muki on 2016/9/13.
 */
public class MessageFragment extends Fragment implements View.OnClickListener{
    private  View   view;
    LinearLayout assistantLat;
    private LinearLayout sysMsgLat;
    private  LinearLayout  merchantsLat;
    private  LinearLayout  privateChatLat;
    private LinearLayout  newFriLat;
    MainActivity    mainActivity;
    private ImageView    msgTipsView;
    private ImageView    chatMsgIv;
    private  LinearLayout  addFriLat;
    private  LinearLayout  myMsgFriLat;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_fg_message,null);
        initViews();
        return  view;
    }

    public void bindData(Object o) {
        final AVIMConversation conversation = (AVIMConversation)o;
        if(null != conversation) {
            if(null == conversation.getCreatedAt()) {
               refresh();
            }
        }
    }
    public  void  initViews(){
         mainActivity= (MainActivity) getActivity();
         myMsgFriLat= (LinearLayout) view.findViewById(R.id.myMsgFriLat);
         addFriLat= (LinearLayout) view.findViewById(R.id.addMsgFriLat);
         chatMsgIv= (ImageView) view.findViewById(R.id.iv_msgChat_Tv);
         msgTipsView= (ImageView) view.findViewById(R.id.iv_msg_tips);
         newFriLat= (LinearLayout) view.findViewById(R.id.newFriLat);
         assistantLat= (LinearLayout) view.findViewById(R.id.assistantLat);
         sysMsgLat= (LinearLayout) view.findViewById(R.id.sysMsgLat);
         merchantsLat= (LinearLayout) view.findViewById(R.id.merchantsLat);
         privateChatLat= (LinearLayout) view.findViewById(R.id.privateChatLat);
         assistantLat.setOnClickListener(this);
         sysMsgLat.setOnClickListener(this);
         merchantsLat.setOnClickListener(this);
         privateChatLat.setOnClickListener(this);
         newFriLat.setOnClickListener(this);
         addFriLat.setOnClickListener(this);
         myMsgFriLat.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.myMsgFriLat:
                 Intent  intent_myFriend=new Intent(getActivity(),MyFriendActivity.class);
                 startActivity(intent_myFriend);
                 break;
             case R.id.assistantLat:
                 Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
                 intent.putExtra(LCIMConstants.PEER_ID, "58327eb05f7cff004f27c13a");
                 intent.putExtra("CHAT_TITLE","兼职助手");
                 intent.putExtra("TO_USERID","58327eb05f7cff004f27c13a");
                 startActivity(intent);
                 break;
             case R.id.sysMsgLat:
                Intent  intent_msg=new Intent(getActivity(), SysMsgActivity.class);
                startActivity(intent_msg);
                 break;
             case R.id.merchantsLat:
                 Intent intentMerchant = new Intent(getActivity(), ChatRoomActivity.class);
                 intentMerchant.putExtra(LCIMConstants.PEER_ID, "58327eb05f7cff004f27c13a");
                 intentMerchant.putExtra("TO_USERID","58327eb05f7cff004f27c13a");
                 intentMerchant.putExtra("CHAT_TITLE","兼职助手");
                 startActivity(intentMerchant);
                 break;
             case R.id.privateChatLat:
                 Intent  intent_private=new Intent(getActivity(), LCIMConversationListActivity.class);
                 startActivity(intent_private);
                 break;
             case R.id.newFriLat:
                 Intent newFriIntent = new Intent(getActivity(), ContactNewFriendActivity.class);
                 startActivity(newFriIntent);
                 break;
             case R.id.addMsgFriLat:
                 Intent  intent_AddFriend=new Intent(getActivity(), ContactAddFriendActivity.class);
                 startActivity(intent_AddFriend);
                 break;
           }
      }
    private void updateNewRequestBadge() {
        msgTipsView.setVisibility(AddRequestManager.getInstance().hasUnreadRequests() ? View.VISIBLE : View.GONE);
    }
    private void refresh() {
        int num = 0;
        List<String> convIdList = LCIMConversationItemCache.getInstance().getSortedConversationList();
        for (String  conId:convIdList){
            num =num+LCIMConversationItemCache.getInstance().getUnreadCount(conId);
        }
        chatMsgIv.setVisibility(num>0? View.VISIBLE : View.GONE);
        AddRequestManager.getInstance().countUnreadRequests(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                updateNewRequestBadge();
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }
}





