package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactPersonInfoActivity;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.TimeTool;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/10/25.
 */
public class DynMsgAdapter extends BaseAdapter<DynMsgAdapter.ViewHolder> {

    public DynMsgAdapter(Context context, List<AVObject> listDatas) {
        super(context, listDatas);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_msg_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        AVObject dynMsgObj = listDatas.get(position);
        holder.dynMsgContentTv.setText(dynMsgObj.getString("dynamic_Comment_Content"));
        final LeanchatUser leanchatUser = dynMsgObj.getAVObject("dynamic_Comment_User_pointer");
        if (leanchatUser != null) {
            if (TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())) {
                holder.dynMsgNameTv.setText(leanchatUser.getUSERNAME());
            } else {
                holder.dynMsgNameTv.setText(leanchatUser.getUSER_NICKNAME());
            }
            UpLoadImageUtil.loadCropImage(context, leanchatUser.getAvatarUrl(), holder.dynMsgAvatarIv);
        }
        SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Date curDatecurrenttime = new Date(System.currentTimeMillis());//获取当前时间
        try {
            String strtime = formatter.format(sf.parse(dynMsgObj.getCreatedAt() + ""));
            String strcurrenttime = formatter.format(curDatecurrenttime);
            holder.dynMsgTimeTv.setText(TimeTool.getTime(strtime, strcurrenttime));
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Dynamic dynamic = dynMsgObj.getAVObject("dynamic_Comment");
        Log.e(" dynamic", dynamic.getObjectId()+"");
        if(dynamic!=null){
            if (!TextUtils.isEmpty(dynamic.getDynamicContent())){
                holder.dynMsgTv.setVisibility(View.VISIBLE);
                holder.dynMsgTv.setText(dynamic.getDynamicContent());
            }else{
                holder.dynMsgTv.setVisibility(View.GONE);
            }
            AVRelation<AVObject> relation = dynamic.getRelation("dynamic_Image_relation");
            AVQuery<AVObject> query = relation.getQuery();
            query.include("dynamic_Image");
            query.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(final List<AVObject> list, AVException e) {
                    if (e==null&&list.size()>0) {
                        holder.dynPicIv.setVisibility(View.VISIBLE);
                        UpLoadImageUtil.loadFilletImage(context, list.get(0).getAVFile("dynamic_Thumbnail_Image").getUrl(), holder.dynPicIv);
                    }else{
                        holder.dynPicIv.setVisibility(View.GONE);
                    }
                }
            });
            holder.dynMsgAvatarIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!leanchatUser.getObjectId().equals(LeanchatUser.getCurrentUserId())){
                        Intent intent=new Intent(context, ContactPersonInfoActivity.class);
                        intent.putExtra(Constants.LEANCHAT_USER_ID, leanchatUser.getObjectId());
                        context.startActivity(intent);
                    }else{
                        Toast.makeText(context,"不能查看自己",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView dynMsgAvatarIv;
        private TextView dynMsgNameTv;
        private TextView dynMsgTimeTv;
        private TextView dynMsgContentTv;
        private ImageView dynPicIv;
        private TextView  dynMsgTv;
        public ViewHolder(View view) {
            super(view);
            dynMsgTv= (TextView) view.findViewById(R.id.dynMsgTv);
            dynPicIv = (ImageView) view.findViewById(R.id.dynMsgIv);
            dynMsgAvatarIv = (ImageView) view.findViewById(R.id.dynMsgAvatarIv);
            dynMsgContentTv = (TextView) view.findViewById(R.id.dynMsgContentTv);
            dynMsgNameTv = (TextView) view.findViewById(R.id.dynMsgNameTv);
            dynMsgTimeTv = (TextView) view.findViewById(R.id.dynMsgtTimeTv);
        }
    }

    public void changeAdapterData(List<AVObject> listDatas) {
        this.listDatas = listDatas;
        notifyDataSetChanged();
    }
}
