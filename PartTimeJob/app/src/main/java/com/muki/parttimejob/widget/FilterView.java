package com.muki.parttimejob.widget;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.FilterOneAdapter;
import com.muki.parttimejob.entity.FilterData;
import com.muki.parttimejob.entity.FilterEntity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sunfusheng on 16/4/20.
 */
public class FilterView extends LinearLayout implements View.OnClickListener {

    @Bind(R.id.tv_citys_title)
    public TextView tvCityTitle;
    @Bind(R.id.iv_category_arrow)
    ImageView ivCategoryArrow;
    @Bind(R.id.tv_type_title)
    TextView tvTypeTitle;
    @Bind(R.id.iv_sort_arrow)
    ImageView ivSortArrow;
    @Bind(R.id.tv_other_title)
    TextView tvOtherTitle;
    @Bind(R.id.iv_filter_arrow)
    ImageView ivFilterArrow;
    @Bind(R.id.ll_category)
    LinearLayout llCategory;
    @Bind(R.id.ll_sort)
    LinearLayout llSort;
    @Bind(R.id.ll_filter)
    LinearLayout llFilter;
    @Bind(R.id.lv_right)
    ListView lvRight;
    @Bind(R.id.ll_head_layout)
    LinearLayout llHeadLayout;
    @Bind(R.id.ll_content_list_view)
    LinearLayout llContentListView;
    @Bind(R.id.view_mask_bg)
    View viewMaskBg;
    private Context mContext;
    private Activity mActivity;
    private int filterPosition = -1;
    private int lastFilterPosition = -1;
    public static final int POSITION_CATEGORY = 0; // 分类的位置
    public static final int POSITION_SORT = 1; // 排序的位置
    public static final int POSITION_FILTER = 2; // 筛选的位置

    private boolean isShowing = false;
    private int panelHeight;
    private FilterData filterData;
    private FilterOneAdapter cityAdapter;
    private FilterOneAdapter typeAdapter;
    private FilterOneAdapter otherAdapter;
    private FilterEntity selectJobCityEntity;
    private FilterEntity selectJobTypeEntity;
    private FilterEntity selecteJobOtherEntity;
    public FilterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FilterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.view_filter_layout, this);
        ButterKnife.bind(this, view);
        initView();
        initListener();
    }

    private void initView() {
        viewMaskBg.setVisibility(GONE);
        llContentListView.setVisibility(GONE);
    }

    private void initListener() {
        llCategory.setOnClickListener(this);
        llSort.setOnClickListener(this);
        llFilter.setOnClickListener(this);
        viewMaskBg.setOnClickListener(this);
        llContentListView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_category:
                filterPosition = 0;
                if (onFilterClickListener != null) {
                    onFilterClickListener.onFilterClick(filterPosition);
                }
                break;
            case R.id.ll_sort:
                filterPosition = 1;
                if (onFilterClickListener != null) {
                    onFilterClickListener.onFilterClick(filterPosition);
                }
                break;
            case R.id.ll_filter:
                filterPosition = 2;
                if (onFilterClickListener != null) {
                    onFilterClickListener.onFilterClick(filterPosition);
                }
                break;
            case R.id.view_mask_bg:
                hide();
                break;
        }
    }

    // 复位筛选的显示状态
    public void resetFilterStatus() {
        tvCityTitle.setTextColor(mContext.getResources().getColor(R.color.colorGray));
        ivCategoryArrow.setImageResource(R.drawable.home_down_arrow);

        tvTypeTitle.setTextColor(mContext.getResources().getColor(R.color.colorGray));
        ivSortArrow.setImageResource(R.drawable.home_down_arrow);

        tvOtherTitle.setTextColor(mContext.getResources().getColor(R.color.colorGray));
        ivFilterArrow.setImageResource(R.drawable.home_down_arrow);
    }
    // 复位所有的状态
    public void resetAllStatus() {
        resetFilterStatus();
        hide();
    }
    // 设置分类数据
    private void setJobCityAdapter() {
        lvRight.setVisibility(VISIBLE);
        cityAdapter = new FilterOneAdapter(mContext, filterData.getJobCitys());
        lvRight.setAdapter( cityAdapter );
        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectJobCityEntity= filterData.getJobCitys().get(position);
                cityAdapter .setSelectedEntity(selectJobCityEntity);
                if (onItemJobCityClickListener != null) {
                    onItemJobCityClickListener.onItemJobCityClick(selectJobCityEntity);
                    tvCityTitle.setText(selectJobCityEntity.getKey());
                }
               hide();
            }
        });
    }
    // 设置排序数据
    private void setJobTypeAdapter() {
        lvRight.setVisibility(VISIBLE);
        typeAdapter = new FilterOneAdapter(mContext, filterData.getJobTypes());
        lvRight.setAdapter( typeAdapter);
        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectJobTypeEntity = filterData.getJobTypes().get(position);
                typeAdapter.setSelectedEntity(selectJobTypeEntity);
                if (onItemJobCityClickListener != null) {
                    onItemJobTypeClickListener.onItemJobTypeClick(selectJobTypeEntity);
                    tvTypeTitle.setText(selectJobTypeEntity.getKey());
                }
                hide();
            }
        });
    }
    // 设置筛选数据
    private void setJobOtherAdapter() {
        lvRight.setVisibility(VISIBLE);
        otherAdapter = new FilterOneAdapter(mContext, filterData.getJobOthers());
        lvRight.setAdapter(otherAdapter);
        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecteJobOtherEntity = filterData.getJobOthers().get(position);
                otherAdapter.setSelectedEntity(selecteJobOtherEntity);
                if (onItemJobOtherClickListener != null) {
                    onItemJobOtherClickListener.onItemJobOtherClick(selecteJobOtherEntity);
                    tvOtherTitle.setText(selecteJobOtherEntity.getKey());
                }
                hide();
            }
        });
    }

    // 动画显示
    public void show(int position) {
        if (isShowing && lastFilterPosition == position) return;
        resetFilterStatus();
        rotateArrowUp(position);
        rotateArrowDown(lastFilterPosition);
        lastFilterPosition = position;
        switch (position) {
            case POSITION_CATEGORY:
                tvCityTitle.setTextColor(mActivity.getResources().getColor(R.color.colorPrimary));
                ivCategoryArrow.setImageResource(R.drawable.home_down_arrow_red);
                setJobCityAdapter();
                break;
            case POSITION_SORT:
                tvTypeTitle.setTextColor(mActivity.getResources().getColor(R.color.colorPrimary));
                ivSortArrow.setImageResource(R.drawable.home_down_arrow_red);
                setJobTypeAdapter();
                break;
            case POSITION_FILTER:
                tvOtherTitle.setTextColor(mActivity.getResources().getColor(R.color.colorPrimary));
                ivFilterArrow.setImageResource(R.drawable.home_down_arrow_red);
                setJobOtherAdapter();
                break;
        }

        if (isShowing) return ;
        isShowing = true;
        viewMaskBg.setVisibility(VISIBLE);
        llContentListView.setVisibility(VISIBLE);
        llContentListView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                llContentListView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                panelHeight = llContentListView.getHeight();
                ObjectAnimator.ofFloat(llContentListView, "translationY", -panelHeight, 0).setDuration(200).start();
            }
        });
    }

    // 隐藏动画
    public void hide() {
        isShowing = false;
        resetFilterStatus();
        rotateArrowDown(filterPosition);
        rotateArrowDown(lastFilterPosition);
        filterPosition = -1;
        lastFilterPosition = -1;
        viewMaskBg.setVisibility(View.GONE);
        ObjectAnimator.ofFloat(llContentListView, "translationY", 0, -panelHeight).setDuration(200).start();
    }

    // 旋转箭头向上
    private void rotateArrowUp(int position) {
        switch (position) {
            case POSITION_CATEGORY:
                rotateArrowUpAnimation(ivCategoryArrow);
                break;
            case POSITION_SORT:
                rotateArrowUpAnimation(ivSortArrow);
                break;
            case POSITION_FILTER:
                rotateArrowUpAnimation(ivFilterArrow);
                break;
        }
    }

    // 旋转箭头向下
    private void rotateArrowDown(int position) {
        switch (position) {
            case POSITION_CATEGORY:
                rotateArrowDownAnimation(ivCategoryArrow);
                break;
            case POSITION_SORT:
                rotateArrowDownAnimation(ivSortArrow);
                break;
            case POSITION_FILTER:
                rotateArrowDownAnimation(ivFilterArrow);
                break;
        }
    }

    // 旋转箭头向上
    public static void rotateArrowUpAnimation(final ImageView iv) {
        if (iv == null) return;
        RotateAnimation animation = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(200);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        iv.startAnimation(animation);
    }

    // 旋转箭头向下
    public static void rotateArrowDownAnimation(final ImageView iv) {
        if (iv == null) return;
        RotateAnimation animation = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(200);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        iv.startAnimation(animation);
    }

    // 设置筛选数据
    public void setFilterData(Activity activity, FilterData filterData) {
        this.mActivity = activity;
        this.filterData = filterData;
    }
    // 是否显示
    public boolean isShowing() {
        return isShowing;
    }

    public int getFilterPosition() {
        return filterPosition;
    }

    // 筛选视图点击
    private OnFilterClickListener onFilterClickListener;
    public void setOnFilterClickListener(OnFilterClickListener onFilterClickListener) {
        this.onFilterClickListener = onFilterClickListener;
    }
    public interface OnFilterClickListener {
        void onFilterClick(int position);
    }

    // 分类Item点击
    private OnItemJobCityClickListener onItemJobCityClickListener;
    public void setOnItemJobCityClickListener(OnItemJobCityClickListener onItemJobCityClickListener) {
        this.onItemJobCityClickListener=onItemJobCityClickListener;
    }
    public interface OnItemJobCityClickListener {
        void onItemJobCityClick( FilterEntity rightEntity);
    }

    // 排序Item点击
    private OnItemJobTypeClickListener onItemJobTypeClickListener;
    public void setOnItemJobTypeClickListener(OnItemJobTypeClickListener onItemJobTypeClickListener) {
        this.onItemJobTypeClickListener=onItemJobTypeClickListener;
    }
    public interface OnItemJobTypeClickListener {
        void onItemJobTypeClick(FilterEntity entity);
    }
    // 筛选Item点击
    private OnItemJobOtherClickListener onItemJobOtherClickListener;
    public void setOnItemJobOtherClickListener(OnItemJobOtherClickListener onItemJobOtherClickListener) {
        this.onItemJobOtherClickListener=onItemJobOtherClickListener;
    }
    public interface OnItemJobOtherClickListener  {
        void onItemJobOtherClick(FilterEntity entity);
    }
}
