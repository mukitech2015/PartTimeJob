package com.muki.parttimejob.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.HotSetAdapter;
import com.muki.parttimejob.adapter.JobHistoryListAdapter;
import com.muki.parttimejob.adapter.SquireAdapter;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.Search;

import org.litepal.crud.DataSupport;
import org.litepal.tablemanager.Connector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by muki on 2016/9/26.
 */
public class SearchJobActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int REQUEST_CODE_JOB= 1001;
    private ListView mListView;
    private ListView mResultListView;
    private EditText searchBox;
    private ImageView clearBtn;
    private ImageView backBtn;
    private ViewGroup emptyView;
    private JobHistoryListAdapter jobHistoryListAdapter;
    private SquireAdapter squireAdapter;
    private LinearLayout  selJobBackLat;
    private  final  int  JOB_RESULT=1000;
    private List<Search>   stringList=new ArrayList<>();
    private List<Job>   jobList=new ArrayList<>();
    private SQLiteDatabase db;
    private View  viewHeader;
    private View  viewFooter;
    private GridView   searchHotGv;
    private HotSetAdapter    hotSetAdapter;
    private  ImageView   clearHisIv;
    private  List<String>   hotKey=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search);
        initView();
        initData();

    }
    private void initData() {
        jobHistoryListAdapter = new JobHistoryListAdapter(this,stringList);
        squireAdapter = new SquireAdapter(this,jobList);
        mListView.setAdapter(jobHistoryListAdapter);
        mResultListView.setAdapter(squireAdapter);
        db = Connector.getDatabase();
        stringList=DataSupport.findAll(Search.class);
        jobHistoryListAdapter.changeAdapterData(stringList);
        AVQuery<AVObject>   query=new AVQuery<>("Hot_Search");
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
               if(list!=null){
                    hotKey.clear();
                    for(AVObject avObject:list){
                    hotKey.add(avObject.getString("hot_key"));
                    }
                    setSearchHotAdapter(hotKey);
               }
            }
        });
    }
    public   void   setSearchHotAdapter(List<String>   strings){
       if(hotSetAdapter==null){
           hotSetAdapter=new HotSetAdapter(SearchJobActivity.this,strings);
           searchHotGv.setAdapter(hotSetAdapter);
       }else{
           hotSetAdapter.changeSearchHotAdapter(strings);
       }
       searchHotGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               searchBox.setText(hotKey.get(i));
          }
       });
    }
    private void initView() {
        selJobBackLat= (LinearLayout) findViewById(R.id.selJobBackLat);
        selJobBackLat.setOnClickListener(this);
        mListView = (ListView) findViewById(R.id.listview_Job_history);
        viewHeader=LayoutInflater.from(this).inflate(R.layout.layout_history_header,null);
        viewFooter=LayoutInflater.from(this).inflate(R.layout.layout_history_footer,null);
        searchHotGv= (GridView)findViewById(R.id.searchHotGv);
        clearHisIv= (ImageView) viewHeader.findViewById(R.id.searchClearIv);
        clearHisIv.setOnClickListener(this);
        mListView.addHeaderView(viewHeader);
        mListView.addFooterView(viewFooter);
        searchBox = (EditText) findViewById(R.id.et_searchJob);
        searchBox.setFocusable(true);
        searchBox.setFocusableInTouchMode(true);
        searchBox.requestFocus();
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                      String keyword = s.toString();
                if (TextUtils.isEmpty(keyword)) {
                      clearBtn.setVisibility(View.GONE);
                      emptyView.setVisibility(View.GONE);
                      mResultListView.setVisibility(View.GONE);
                      searchHotGv.setVisibility(View.GONE);
                } else {
                      clearBtn.setVisibility(View.VISIBLE);
                      searchHotGv.setVisibility(View.VISIBLE);
                      mListView.setVisibility(View.GONE);
                      mResultListView.setVisibility(View.VISIBLE);
                      searchJob(keyword);
                }
            }
        });
        emptyView = (ViewGroup) findViewById(R.id.job_empty_view);
        mResultListView = (ListView) findViewById(R.id.listview_Job_result);
        mResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentdetail=new Intent(SearchJobActivity.this, JobDetailActivity.class);
                intentdetail.putExtra("job", (Serializable) jobList.get(position));
                startActivity(intentdetail);
            }
        });
        clearBtn = (ImageView) findViewById(R.id.iv_searchJob_clear);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchBox.setText("");
                searchHotGv.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.VISIBLE);
                clearBtn.setVisibility(View.GONE);
                emptyView.setVisibility(View.GONE);
                mResultListView.setVisibility(View.GONE);
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(stringList.size()>0){
                    searchBox.setText(stringList.get(position-1).getString());
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.selJobBackLat:
                finish();
                break;
            case  R.id.searchClearIv:
                DataSupport.deleteAll(Search.class);
                stringList=DataSupport.findAll(Search.class);
                jobHistoryListAdapter.changeAdapterData(stringList);
                break;
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
   public   void   searchJob(final String   secrchContent){
       AVQuery<Job>   addressQuery=AVQuery.getQuery(Job.class);
       addressQuery.whereContains("job_Address",secrchContent);
       AVQuery<Job>   nameQuery=AVQuery.getQuery(Job.class);
       nameQuery.whereContains("job_RecruitName",secrchContent);
       AVQuery<Job> query = AVQuery.or(Arrays.asList(addressQuery,nameQuery));
       query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
       query.setMaxCacheAge(24 * 3600); //设置缓存有效期
       query.orderByDescending("createdAt");
       query.whereContains("job_RecruitName",secrchContent);
       query.include("business_pointer");
       query.findInBackground(new FindCallback<Job>() {
           @Override
           public void done(List<Job> list, AVException e) {
               if (list== null ||list.size() == 0) {
                   searchHotGv.setVisibility(View.GONE);
                   emptyView.setVisibility(View.VISIBLE);
               } else {
                   new Search(secrchContent).save();
                   jobList=list;
                   squireAdapter.changeAdapterData(list);
                   emptyView.setVisibility(View.GONE);
               }
           }
       });
   }
}





