package com.muki.parttimejob.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/12/3.
 */

public class CityData {
    public List<String> getRegions() {
        return regions;
    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    String  cityName;
    private List<String>  regions=new ArrayList<>();

}
