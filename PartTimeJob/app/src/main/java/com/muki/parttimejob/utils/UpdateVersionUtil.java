package com.muki.parttimejob.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.muki.parttimejob.R;

import java.io.File;
import java.lang.reflect.Method;

/**
 * 
 * @author	wenjie
 *	版本更新的工具类
 */
public class UpdateVersionUtil{
	/**
	 * 接口回调
	 * @author wenjie
	 *
	 */
	public interface UpdateListener{
		void onUpdateReturned(int updateStatus, VersionInfo versionInfo);
	}
	
	public UpdateListener updateListener;
	
	public void setUpdateListener(UpdateListener updateListener) {
		this.updateListener = updateListener;
	}
	/**
	 * 网络测试 检测版本
	 * @param context 上下文
	 */
	public static void checkVersion(final Context context, final UpdateListener updateListener) {
		final int clientVersionCode = ApkUtils.getVersionCode(context);
		AVQuery<AVObject> versionAVQuery = new AVQuery<>("VersionAndroid");
		versionAVQuery.getFirstInBackground(new GetCallback<AVObject>() {
			@Override
			public void done(AVObject avObject, AVException e) {
				if (avObject != null) {
					int serverVersionCode = avObject.getInt("VersionCode");
					String url = avObject.getString("url");
					String deac = avObject.getString("desc");
                    String  versionSize=avObject.getString("versionSize");
					String  versionName=avObject.getString("versionName");
                    String  id=avObject.getObjectId();
					VersionInfo mVersionInfo=new VersionInfo();
					mVersionInfo.setId(id);
					mVersionInfo.setDownloadUrl(url);
					mVersionInfo.setVersionCode(serverVersionCode);
					mVersionInfo.setVersionDesc(deac);
					mVersionInfo.setVersionName(versionName);
					mVersionInfo.setVersionSize(versionSize);
					if (clientVersionCode!=serverVersionCode) {
						int i = NetworkUtil.checkedNetWorkType(context);
						if (i == NetworkUtil.NOWIFI) {
							updateListener.onUpdateReturned(UpdateStatus.NOWIFI, mVersionInfo);
						} else if (i == NetworkUtil.WIFI) {
							updateListener.onUpdateReturned(UpdateStatus.YES, mVersionInfo);
						}
					} else {
						updateListener.onUpdateReturned(UpdateStatus.NO, null);
					}
				}else{
					updateListener.onUpdateReturned(UpdateStatus.NO, null);
				}
			}
		});
	}
	/**
	 * 弹出新版本提示
	 * @param context 上下文
	 * @param versionInfo 更新内容
	 */
	public static void showDialog(final Context context, final VersionInfo versionInfo){
		final Dialog dialog = new AlertDialog.Builder(context).create();
		final File file = new File(SDCardUtils.getRootDirectory()+"/updateVersion/haojianzhi.apk");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);//
		dialog.show();
		View view = LayoutInflater.from(context).inflate(R.layout.version_update_dialog, null);
		dialog.setContentView(view);
		final Button btnOk = (Button) view.findViewById(R.id.btn_update_id_ok);
		Button btnCancel = (Button) view.findViewById(R.id.btn_update_id_cancel);
		TextView tvContent = (TextView) view.findViewById(R.id.tv_update_content);
		TextView tvUpdateTile = (TextView) view.findViewById(R.id.tv_update_title);
		final TextView tvUpdateMsgSize = (TextView) view.findViewById(R.id.tv_update_msg_size);
		tvContent.setText(versionInfo.getVersionDesc());
		tvUpdateTile.setText("最新版本："+versionInfo.getVersionName());
		if(file.exists() && file.getName().equals("haojianzhi.apk")){
			tvUpdateMsgSize.setText("新版本已经下载，是否安装？");
		}else{
			tvUpdateMsgSize.setText("新版本大小："+versionInfo.getVersionSize());
		}
		btnOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(v.getId() == R.id.btn_update_id_ok){
					//新版本已经下载
					if(file.exists() && file.getName().equals("haojianzhi.apk")){
						Intent intent = ApkUtils.getInstallIntent(file);
						context.startActivity(intent);
					}else{
						//没有下载，则开启服务下载新版本
						Intent intent = new Intent(context,UpdateVersionService.class);
						intent.putExtra("downloadUrl", versionInfo.getDownloadUrl());
						context.startService(intent);
					}
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}
	/**
	 * 收起通知栏
	 * @param context
	 */
	public static void collapseStatusBar(Context context) {
		try{
			Object statusBarManager = context.getSystemService(Context.ACCESSIBILITY_SERVICE);
			Method collapse;
			if (Build.VERSION.SDK_INT <= 16){
				collapse = statusBarManager.getClass().getMethod("collapse"); 
			}else{ 
				collapse = statusBarManager.getClass().getMethod("collapsePanels"); 
			} 
			collapse.invoke(statusBarManager);
		}catch (Exception localException){
			localException.printStackTrace();
		} 
	}
}
