package com.muki.parttimejob.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.entity.City;
import com.muki.parttimejob.entity.LocateState;
import com.muki.parttimejob.widget.WrapHeightGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * author zaaach on 2016/1/26.
 */
public class CityListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private List<City> mCities;
    private OnCityClickListener onCityClickListener;
    private int locateState = LocateState.LOCATING;
    private String locatedCity;
    public CityListAdapter(Context mContext, List<City> mCities) {
        this.mContext = mContext;
        this.mCities = mCities;
        this.inflater = LayoutInflater.from(mContext);
        if (mCities == null){
            mCities = new ArrayList<>();
        }
        mCities.add(0, new City("定位", "0"));
        mCities.add(1, new City("热门", "1"));
    }
    /**
     * 更新定位状态
     * @param state
     */
    public void updateLocateState(int state, String city){
        this.locateState = state;
        this.locatedCity = city;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
      if(position==0){
          view = inflater.inflate(R.layout.view_locate_city, parent, false);
          ViewGroup container = (ViewGroup) view.findViewById(R.id.layout_locate);
          TextView state = (TextView) view.findViewById(R.id.tv_located_city);
          switch (locateState) {
              case LocateState.LOCATING:
                  state.setText(("正在定位..."));
                  break;
              case LocateState.FAILED:
                  state.setText("定位失败");
                  break;
              case LocateState.SUCCESS:
                  state.setText(locatedCity);
                  break;
          }
          container.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if (locateState == LocateState.FAILED){
                      //重新定位
                      if (onCityClickListener != null){
                          onCityClickListener.onLocateClick();
                      }
                  }else if (locateState == LocateState.SUCCESS){
                      //返回定位城市
                      if (onCityClickListener != null){
                          onCityClickListener.onCityClick(locatedCity);
                      }
                  }
              }
          });
      }else if(position==1){
          view = inflater.inflate(R.layout.view_hot_city, parent, false);
          WrapHeightGridView gridView = (WrapHeightGridView) view.findViewById(R.id.gridview_hot_city);
          final HotCityGridAdapter hotCityGridAdapter = new HotCityGridAdapter(mContext);
          gridView.setAdapter(hotCityGridAdapter);
          gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                  if (onCityClickListener != null){
                      Log.e("aaaaaaaaaaaaaa","dddddddddd");
                      onCityClickListener.onCityClick(hotCityGridAdapter.getItem(position));
                  }
              }
          });
      }
        return view;
    }
    public void setOnCityClickListener(OnCityClickListener listener){
        this.onCityClickListener = listener;
    }
    public interface OnCityClickListener{
        void onCityClick(String name);
        void onLocateClick();
    }
}
