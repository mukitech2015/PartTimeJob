package com.muki.parttimejob.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.CollectionActivity;
import com.muki.parttimejob.activity.MyCommtActivity;
import com.muki.parttimejob.activity.MyDynamicActivity;
import com.muki.parttimejob.activity.MyJobActivity;
import com.muki.parttimejob.activity.MyResumeActivity;
import com.muki.parttimejob.activity.SysSettingActivity;
import com.muki.parttimejob.activity.UserInfoActivity;
import com.muki.parttimejob.activity.WalletActivity;
import com.muki.parttimejob.activity.WebActivity;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.UpLoadImageUtil;

/**
 * Created by muki on 2016/9/13.
 */
public class MineFragment extends Fragment  implements View.OnClickListener {
    private View   view;
    private LinearLayout   mineInfoLat;
    private  RelativeLayout  myJobLat;
    private  RelativeLayout   myWallentLat;
    private  RelativeLayout   myCollectionLat;
    private  RelativeLayout   myDynLat;
    private  TextView   nameTv;
    private  TextView   schoolTv;
    private  TextView   phoneTv;
    private   ImageView avatarIv;
    private  LinearLayout  bmLat;
    private  LinearLayout  tgLat;
    private  LinearLayout  jsLat;
    private  LinearLayout  pjLat;
    private  LinearLayout  wcLat;
    RelativeLayout  myResumeLat;
    RelativeLayout   findJobLat;
    private  TextView   fg_authenTv;
    private  ImageView  sexIconIv;
    RelativeLayout  myCommtLat;
    private RelativeLayout    settingLat;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_fg_mine,null);
        initViews();
        return  view;
    }
    public  void  initViews( ){
         myResumeLat= (RelativeLayout) view.findViewById(R.id.myResumeLat);
         myCommtLat= (RelativeLayout) view.findViewById(R.id.myCommtLat);
         fg_authenTv= (TextView) view.findViewById(R.id.fg_authenTv);
         settingLat= (RelativeLayout) view.findViewById(R.id.settingLat);
         findJobLat= (RelativeLayout) view.findViewById(R.id.findJobLat);
         sexIconIv= (ImageView) view.findViewById(R.id.sexIconIv);
        bmLat= (LinearLayout) view.findViewById(R.id.fg_bmLat);
        tgLat= (LinearLayout) view.findViewById(R.id.fg_tgLat);
        jsLat= (LinearLayout) view.findViewById(R.id.fg_jsLat);
        pjLat= (LinearLayout) view.findViewById(R.id.fg_pjLat);
        wcLat= (LinearLayout) view.findViewById(R.id.fg_wcLat);
        findJobLat.setOnClickListener(this);
        bmLat.setOnClickListener(this);
        tgLat.setOnClickListener(this);
        jsLat.setOnClickListener(this);
        pjLat.setOnClickListener(this);
        wcLat.setOnClickListener(this);
        myResumeLat.setOnClickListener(this);
        myCommtLat.setOnClickListener(this);
        settingLat.setOnClickListener(this);
        avatarIv= (ImageView) view.findViewById(R.id.fg_avatarIv);
        nameTv= (TextView) view.findViewById(R.id.fg_nameTv);
        schoolTv= (TextView) view.findViewById(R.id.fg_school_tv);
        phoneTv= (TextView) view.findViewById(R.id.fg_phNumberTv);
        myCollectionLat= (RelativeLayout) view.findViewById(R.id.myCollectionLat);
        myWallentLat= (RelativeLayout) view.findViewById(R.id.myWallentLat);
        myJobLat= (RelativeLayout) view.findViewById(R.id.myJobLat);
        myJobLat.setOnClickListener(this);
        myCollectionLat.setOnClickListener(this);
        myWallentLat.setOnClickListener(this);
        mineInfoLat= (LinearLayout) view.findViewById(R.id.mine_info_lat);
        myDynLat= (RelativeLayout) view.findViewById(R.id.myDynLat);
        mineInfoLat.setOnClickListener(this);
        myDynLat.setOnClickListener(this);
    }

    public  void  getDatas( ){
        new Thread(new Runnable() {
            public void run() {
                try {
                    LeanchatUser.getCurrentUser().fetch();
                    handler.sendEmptyMessage(1001);
                } catch (AVException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1001:
                    if(!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getUSER_NICKNAME())){
                        nameTv.setText(LeanchatUser.getCurrentUser().getUSER_NICKNAME());
                    }else{
                        nameTv.setText(LeanchatUser.getCurrentUserId().substring(0,8));
                    }
                    if (LeanchatUser.getCurrentUser().getUSER_SEX().equals("1")){
                        sexIconIv.setImageResource(R.drawable.man_icon);
                    }else{
                        sexIconIv.setImageResource(R.drawable.woman_icon);
                    }
                    schoolTv.setText(LeanchatUser.getCurrentUser().getUSER_SCHOOL());
                    phoneTv.setText(LeanchatUser.getCurrentUser().getMobilePhoneNumber());
                    UpLoadImageUtil.loadCropImage(getActivity(),LeanchatUser.getCurrentUser().getAvatarUrl(),avatarIv);
                   int  idAuthStare= LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION();
                    if(idAuthStare==0){
                        fg_authenTv.setText("未认证");
                    }else if(idAuthStare==1){
                        fg_authenTv.setText("已认证");
                    }else if(idAuthStare==2){
                        fg_authenTv.setText("审核中");
                    }else{
                        fg_authenTv.setText("审核未通过");
                    }
                    break;
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        getDatas();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case  R.id.myResumeLat:
                Intent  intentmyResume=new Intent(getActivity(), MyResumeActivity.class);
                startActivity(intentmyResume);
                 break;
            case R.id.myCommtLat:
                Intent  intentmyCommt=new Intent(getActivity(), MyCommtActivity.class);
                startActivity(intentmyCommt);
                break;
            case R.id.settingLat:
               Intent  intentSetting=new Intent(getActivity(), SysSettingActivity.class);
               startActivity(intentSetting);
                break;
            case   R.id.myJobLat:
                    Intent   intent_myjob=new Intent(getActivity(), MyJobActivity.class);
                    startActivity(intent_myjob);
                break;
            case R.id.mine_info_lat:
                   Intent   intent_userInfo=new Intent(getActivity(),UserInfoActivity.class);
                   startActivity(intent_userInfo);
                break;
              case   R.id.myWallentLat:
                      Intent   intent_myWallent=new Intent(getActivity(), WalletActivity.class);
                      startActivity(intent_myWallent);
                   break;
            case R.id.myCollectionLat:
                    Intent   intent_collection=new Intent(getActivity(),CollectionActivity.class);
                    startActivity(intent_collection);
                break;
            case R.id.myDynLat:
                    Intent   intent_myDyn=new Intent(getActivity(), MyDynamicActivity.class);
                    startActivity(intent_myDyn);
                 break;
            case  R.id.fg_bmLat:
                goToMyJobActivity(AppConfig.REG_IN);
                break;
            case  R.id.fg_jsLat:
                goToMyJobActivity(AppConfig.WAIT_SETTLE);
                break;
            case  R.id.fg_pjLat:
                goToMyJobActivity(AppConfig.COMMT);
                break;
            case  R.id.fg_tgLat:
                 goToMyJobActivity(AppConfig.AlREADY_PASSWORD);
                break;
            case  R.id.fg_wcLat:
                 goToMyJobActivity(AppConfig.COMPLETE);
                 break;
            case R.id.findJobLat:
                Intent  intentWeb =new   Intent(getActivity(),WebActivity.class);
                intentWeb.putExtra("WEB_URL", AppConfig.FINDJOB+LeanchatUser.getCurrentUserId());
                startActivity(intentWeb);
                break;
        }
    }
    public  void   goToMyJobActivity(String   myJobType){
         Intent   intent=new Intent(getActivity(),MyJobActivity.class);
         intent.putExtra("myJobType",myJobType);
         getActivity().startActivity(intent);
    }
}
