package com.muki.parttimejob.event;

/**
 * Created by wli on 15/11/27.
 */
public class GroupItemClickEvent {
  public String conversationId;
  public String   conversationName;
  public GroupItemClickEvent(String id,String   conversationName) {
    conversationId = id;
    this.conversationName=conversationName;
  }
}
