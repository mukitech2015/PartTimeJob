package com.muki.parttimejob.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.LeanchatUser;

/**
 * Created by muki on 2016/10/25.
 */

public class SendCommentActivity  extends   BaseActivity   implements View.OnClickListener {
    private LinearLayout sendComtBackLat;
    private RelativeLayout  sendComtLat;
    private EditText  dynComtEt;
    private Boolean   needCallback;
    Dynamic  dynamic;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_send_dyncomt);
    }
    @Override
    protected void initViews() {
        Bundle bundle = getIntent().getExtras();
        if(bundle == null)
            return;
         needCallback = bundle.getBoolean(AppConfig.NeedCallback, false);
         dynComtEt= (EditText) findViewById(R.id.dynComtEt);
         sendComtLat= (RelativeLayout) findViewById(R.id.sendComtLat);
         sendComtBackLat= (LinearLayout) findViewById(R.id.sendComtBackLat);
         sendComtLat.setOnClickListener(this);
         sendComtBackLat.setOnClickListener(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent   sendCommentIntent=getIntent();
        dynamic= (Dynamic) sendCommentIntent.getSerializableExtra("dynamic");
     }
    @Override
    protected void initDatas() {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
           case   R.id.sendComtLat:
               final String   comtContent=dynComtEt.getText().toString().trim();
               if(TextUtils.isEmpty(comtContent)){
                   Toast.makeText(SendCommentActivity.this,"请输入内容",Toast.LENGTH_SHORT).show();
                   return;
               }
               AVQuery<Dynamic> avQuery = new AVQuery<>("Dynamic");
               avQuery.getInBackground(dynamic.getObjectId(), new GetCallback<Dynamic>() {
                   @Override
                   public void done(final Dynamic dynamic, AVException e) {
                       if(dynamic!=null){
                           final AVObject avObject=new AVObject("Dynamic_Comment");
                           avObject.put("dynamic_Comment_Content",comtContent);
                           avObject.put("dynamic_Reply_User_pointer",dynamic.getDynamicUsers());
                           avObject.put("dynamic_Comment",dynamic);
                           avObject.put("dynamic_Comment_User_pointer", LeanchatUser.getCurrentUser());
                           avObject.saveInBackground(new SaveCallback() {
                               @Override
                               public void done(AVException e) {
                                   if(e==null){
                                       AVRelation<AVObject> relation =  dynamic.getRelation("dynamic_Comment_relation");// 新建一个 AVRelation
                                       relation.add(avObject);
                                       dynamic.saveInBackground(new SaveCallback() {
                                           @Override
                                           public void done(AVException e) {
                                               if(e==null){
                                                   Toast.makeText(SendCommentActivity.this,"评论成功",Toast.LENGTH_SHORT).show();
                                                   if (needCallback) {
                                                       setResult(Activity.RESULT_OK);
                                                       finish();
                                                   }
                                               }else{
                                                   Log.e("评论失败",e.getMessage()+"");
                                               }
                                           }
                                       });
                                   }else{
                                       Log.e("==========",e.getMessage()+"");
                                   }
                               }
                           });
                       }
                   }
               });
               break;
            case  R.id.sendComtBackLat:
               finish();
             break;
        }
    }
}
