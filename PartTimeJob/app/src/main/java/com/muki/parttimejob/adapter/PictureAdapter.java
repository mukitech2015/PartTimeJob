package com.muki.parttimejob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.util.List;

/**
 * Created by muki on 2016/9/23.
 */
public class PictureAdapter  extends BaseAdapter{
    private List<AVObject> objectList;
    private Context context;
    private LayoutInflater mInflater;
    public   PictureAdapter(List<AVObject> objectList,Context context){
        this.objectList=objectList;
        this.context=context;
        mInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return objectList.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder   viewholder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.layout_pic_item, parent, false);
            viewholder=new  ViewHolder();
            convertView.setTag(viewholder);
         }	  else{
            viewholder = (ViewHolder)convertView.getTag();//取出ViewHolder对象
          }
        viewholder.gridview_image=(ImageView) convertView.findViewById(R.id.imageview_item);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        ViewGroup.LayoutParams params = viewholder.gridview_image.getLayoutParams();
        params.width = wm.getDefaultDisplay().getWidth()/3;
        params.height=params.width;
        viewholder.gridview_image.setLayoutParams(params);
        UpLoadImageUtil.loadImage(context,objectList.get(position).getAVFile("dynamic_Image").getUrl(),viewholder.gridview_image);
         return convertView;
     }
     class  ViewHolder{
        private ImageView gridview_image;
     }
}

