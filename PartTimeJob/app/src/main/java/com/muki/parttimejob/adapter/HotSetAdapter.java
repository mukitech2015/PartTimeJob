package com.muki.parttimejob.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by yuzhengang on 2016/11/25.
 */

public class HotSetAdapter extends BaseAdapter {
    private  Context   context;
    private  List<String>  strings;
    private LayoutInflater  layoutInflater;
    public  HotSetAdapter(Context   context,List<String>  strings){
        this.context=context;
        this.strings=strings;
        layoutInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return strings.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Holder holder;
        if (view== null) {
             holder = new Holder();
             view = layoutInflater.inflate(R.layout.layout_search_hot, null);
             holder.hotTv= (TextView) view.findViewById(R.id.search_item_hotTv);
             view.setTag(holder);
             } else {
             holder = (Holder) view.getTag();
             }
             holder.hotTv.setText(strings.get(i));
             return  view;
     }
    class  Holder{
        TextView   hotTv;
    }
    public    void   changeSearchHotAdapter(List<String>   strings){
        this.strings=strings;
        notifyDataSetChanged();
    }
}
