package com.muki.parttimejob.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sunfusheng on 16/4/23.
 */
public class FilterData implements Serializable {
    private List<FilterEntity> jobCitys;
    public List<FilterEntity> getJobTypes() {
        return jobTypes;
    }

    public void setJobTypes(List<FilterEntity> jobTypes) {
        this.jobTypes = jobTypes;
    }

    public List<FilterEntity> getJobCitys() {
        return jobCitys;
    }

    public void setJobCitys(List<FilterEntity> jobCitys) {
        this.jobCitys = jobCitys;
    }
    public List<FilterEntity> getJobOthers() {
        return jobOthers;
    }

    public void setJobOthers(List<FilterEntity> jobOthers) {
        this.jobOthers = jobOthers;
    }

    private List<FilterEntity> jobTypes;
    private List<FilterEntity> jobOthers;
}
