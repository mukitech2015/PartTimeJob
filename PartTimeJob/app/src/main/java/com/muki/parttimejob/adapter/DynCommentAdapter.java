package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactPersonInfoActivity;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.TimeTool;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/10/25.
 */
public class DynCommentAdapter  extends BaseAdapter<DynCommentAdapter.ViewHolder> {

    public DynCommentAdapter(Context context, List<AVObject> listDatas) {
        super(context,listDatas);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_dyncomt_item, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        AVObject  dynComtObj=listDatas.get(position);
        final LeanchatUser  leanchatUser=dynComtObj.getAVObject("dynamic_Comment_User_pointer");
        if (leanchatUser!=null){
            if(TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())){
                holder.dynComtNameTv.setText(leanchatUser.getUSERNAME());
            }else{
                holder.dynComtNameTv.setText(leanchatUser.getUSER_NICKNAME());
            }
            UpLoadImageUtil.loadCropImage(context,leanchatUser.getAvatarUrl(),holder.dynComtAvatarIv);
            SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
            SimpleDateFormat formatter = new   SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date curDatecurrenttime =  new  Date(System.currentTimeMillis());//获取当前时间
            String    strtime= null;
            try {
                strtime = formatter.format(sf.parse(dynComtObj.getCreatedAt()+""));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String    strcurrenttime= formatter.format(curDatecurrenttime);
            holder.dynComtTimeTv.setText(TimeTool.getTime(strtime,strcurrenttime));
            holder.dynComtContentTv.setText(dynComtObj.getString("dynamic_Comment_Content"));
            holder.dynComtAvatarIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!leanchatUser.getObjectId().equals(LeanchatUser.getCurrentUserId())){
                        Intent intent=new Intent(context, ContactPersonInfoActivity.class);
                        intent.putExtra(Constants.LEANCHAT_USER_ID, leanchatUser.getObjectId());
                        context.startActivity(intent);
                    }else{
                        Toast.makeText(context,"不能查看自己",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView dynComtAvatarIv;
        private TextView  dynComtNameTv;
        private TextView  dynComtTimeTv;
        private TextView  dynComtContentTv;
        public ViewHolder(View view) {
            super(view);
             dynComtAvatarIv= (ImageView) view.findViewById(R.id.dynComtAvatarIv);
             dynComtContentTv= (TextView) view.findViewById(R.id.dynComtContentTv);
             dynComtNameTv= (TextView) view.findViewById(R.id.dynComtNameTv);
             dynComtTimeTv= (TextView) view.findViewById(R.id.dynComtTimeTv);
        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
        this.listDatas=listDatas;
        notifyDataSetChanged();
    }
}
