package com.muki.parttimejob.viewholder;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactPersonInfoActivity;
import com.muki.parttimejob.event.ConversationMemberClickEvent;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.squareup.picasso.Picasso;

import cn.leancloud.chatkit.viewholder.LCIMCommonViewHolder;
import de.greenrobot.event.EventBus;

/**
 * Created by wli on 15/12/2.
 */
public class ConversationDetailItemHolder extends LCIMCommonViewHolder<LeanchatUser> {

  ImageView avatarView;
  TextView nameView;
  LeanchatUser leanchatUser;

  public ConversationDetailItemHolder(final Context context, ViewGroup root) {
    super(context, root, R.layout.conversation_member_item);
    avatarView = (ImageView)itemView.findViewById(R.id.avatar);
    nameView = (TextView)itemView.findViewById(R.id.username);
    itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (null != leanchatUser) {
          if(!leanchatUser.getObjectId().equals(LeanchatUser.getCurrentUserId())){
            Intent intent=new Intent(context, ContactPersonInfoActivity.class);
            intent.putExtra(Constants.LEANCHAT_USER_ID, leanchatUser.getObjectId());
            context.startActivity(intent);
          }else{
            Toast.makeText(context,"不能查看自己",Toast.LENGTH_SHORT).show();
          }
        }
      }
    });
    itemView.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (null != leanchatUser) {
          EventBus.getDefault().post(new ConversationMemberClickEvent(leanchatUser.getObjectId(), true));
        }
        return true;
      }
    });
  }
  @Override
  public void bindData(LeanchatUser user) {
    leanchatUser = user;
    if (null != user) {
      UpLoadImageUtil.loadCropImage(getContext(),user.getAvatarUrl(),avatarView);
      if(TextUtils.isEmpty(user.getUSER_NICKNAME())){
        nameView.setText(user.getUsername());
      }else{
        nameView.setText(user.getUSER_NICKNAME());
      }
    } else {
      avatarView.setImageResource(0);
      nameView.setText("");
    }
  }
  public static ViewHolderCreator HOLDER_CREATOR = new ViewHolderCreator<ConversationDetailItemHolder>() {
    @Override
    public ConversationDetailItemHolder createByViewGroupAndType(ViewGroup parent, int viewType) {
      return new ConversationDetailItemHolder(parent.getContext(), parent);
    }
  };
}
