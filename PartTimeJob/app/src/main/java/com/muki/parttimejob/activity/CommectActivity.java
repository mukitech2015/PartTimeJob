package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Job;

/**
 * Created by muki on 2016/10/18.
 */

public class CommectActivity  extends   BaseActivity implements View.OnClickListener{
    private RelativeLayout commCanLat;
    private RelativeLayout commSendLat;
    private EditText  commTitleEt;
    private EditText  commContentEt;
    private RatingBar   commRb;
    float currentRating;
    private  Job  job;
    private User_Job  user_job;

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_comment);
    }
    @Override
    protected void initViews() {
        commRb= (RatingBar) findViewById(R.id.commRb);
        int max=commRb.getMax();
        //得到RatingBar现在的等级
        currentRating=commRb.getRating();
        commRb.setOnRatingBarChangeListener(new RatingBarChangeListenerImpl());
        commCanLat= (RelativeLayout) findViewById(R.id.commCanLat);
        commSendLat= (RelativeLayout) findViewById(R.id.commSendLat);
        commTitleEt= (EditText) findViewById(R.id.commTitleEt);
        commContentEt= (EditText) findViewById(R.id.commContentEt);
        commCanLat.setOnClickListener(this);
        commSendLat.setOnClickListener(this);

    }
    private class RatingBarChangeListenerImpl implements RatingBar.OnRatingBarChangeListener {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            currentRating=rating;
        }
    }
    @Override
    protected void initDatas() {


    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent=getIntent();
        if(intent!=null){
            job= (Job) intent.getSerializableExtra("job");
            user_job= (User_Job) intent.getSerializableExtra("user_job");
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.commCanLat:
                 finish();
                break;
            case R.id.commSendLat:
                 saveComment();
                break;
        }
    }
    public  void   saveComment(  ){
        String  title=commTitleEt.getText().toString().trim();
        String  content=commContentEt.getText().toString().trim();
        if (currentRating==0.0){
            Toast.makeText(CommectActivity.this,"请进行评分",Toast.LENGTH_SHORT).show();
            return;
        }
        if (title.equals("")){
            Toast.makeText(CommectActivity.this,"请填写标题",Toast.LENGTH_SHORT).show();
            return;
        }
        if (content.equals("")){
            Toast.makeText(CommectActivity.this,"请填写评论内容",Toast.LENGTH_SHORT).show();
            return;
        }
        AVObject  commObj=new AVObject("CommentAndComplaint");
        commObj.put("content",content);
        commObj.put("title",title);
        commObj.put("rating",currentRating);
        commObj.put("commenter", LeanchatUser.getCurrentUser());
        commObj.put("job",job);
        commObj.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e==null){
                    Toast.makeText(CommectActivity.this,"评论成功",Toast.LENGTH_SHORT).show();
                    changeState(user_job);
                    finish();
                }else{
                    Log.e("评论失败",e.getMessage());
                }
            }
        });
    }
    public  void  changeState( User_Job  user_job){
        user_job.setState(AppConfig.COMPLETE);
        user_job.setAction_State(AppConfig.ACTION_COMPLETE);
        user_job.saveInBackground();
    }
}












