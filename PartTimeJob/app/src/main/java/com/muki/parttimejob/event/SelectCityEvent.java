package com.muki.parttimejob.event;

/**
 * Created by muki on 2016/12/21.
 */

public class SelectCityEvent {
    private String city;
    public SelectCityEvent(String city) {
        this.city=city;
    }
    public String getCity(){
        return city;
    }
}
