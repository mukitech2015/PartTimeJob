package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactPersonInfoActivity;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.util.List;

/**
 * Created by muki on 2016/10/12.
 */

public class AddFriendAdapter extends RecyclerView.Adapter<AddFriendAdapter.ViewHolder>{
    private List<LeanchatUser>  leanchatUserList;
    private RecyclerView mRecyclerView;
    private Context  context;
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView  userNameTv;
        private TextView  schoolTv;;
        private ImageView fgAvatarIv;
        private LinearLayout  addFgLayout;
        public ViewHolder(View view) {
            super(view);
            addFgLayout= (LinearLayout) view.findViewById(R.id.addFgItemLayout);
            userNameTv= (TextView) view.findViewById(R.id.addFgNameTv);
            schoolTv= (TextView) view.findViewById(R.id.addFgSchoolTv);
            fgAvatarIv= (ImageView) view.findViewById(R.id.addFgAvatarIv);
        }
    }
    public  void  changeAddFriendAdapter(List<LeanchatUser>  leanchatUsers){
        this.leanchatUserList=leanchatUsers;
        notifyDataSetChanged();
    }
    public AddFriendAdapter(RecyclerView recyclerView, List<LeanchatUser>  leanchatUsers, Context context) {
        this.context=context;
        this.leanchatUserList=leanchatUsers;
        mRecyclerView = recyclerView;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_addfriend_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
          final LeanchatUser   leanchatUser=leanchatUserList.get(position);
              holder.userNameTv.setText(leanchatUser.getUSERNAME());
          if (leanchatUser.getUSER_NICKNAME()!=null){
              holder.userNameTv.setText(leanchatUser.getUSER_NICKNAME());
          }
         holder.schoolTv.setText(leanchatUser.getUSER_SCHOOL());
         UpLoadImageUtil.loadCropImage(context,leanchatUser.getAvatarUrl(),holder.fgAvatarIv);
         holder.addFgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent=new Intent(context, ContactPersonInfoActivity.class);
                intent.putExtra(Constants.LEANCHAT_USER_ID, leanchatUser.getObjectId());
                context.startActivity(intent);
            }
         });

    }
    @Override
    public void onViewRecycled(ViewHolder holder) {

    }
    @Override
    public int getItemCount() {
        return  leanchatUserList.size();
    }

}
