package com.muki.parttimejob.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactAddFriendActivity;
import com.muki.parttimejob.activity.ConversationAddMembersActivity;
import com.muki.parttimejob.activity.IdAuthentication;
import com.muki.parttimejob.activity.MyFriendActivity;
import com.muki.parttimejob.model.LeanchatUser;


/**
 * Created by muki on 2016/9/30.
 */
public class PopWindow extends PopupWindow {
    private View conentView;

    public PopWindow(final Activity context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = inflater.inflate(R.layout.layout_popup_window, null);
        int h = context.getWindowManager().getDefaultDisplay().getHeight();
        int w = context.getWindowManager().getDefaultDisplay().getWidth();
        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth((int) (w / 2.4));
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new BitmapDrawable());
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        backgroundAlpha(context,0.5f);//0.0-1.0
        // mPopupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);
        setFocusable(true); // 设置PopupWindow可获得焦点
        setTouchable(true); // 设置PopupWindow可触摸
        setOutsideTouchable(true); // 设置非PopupWindow区域可触摸
        setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                dismiss();
                backgroundAlpha(context,1.0f);//0.0-1.0
            }
        });
        setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    dismiss();
                    backgroundAlpha(context,1.0f);//0.0-1.0
                    return true;
                }
                return false;
            }
        });
        conentView.findViewById(R.id.myFriendLat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dismiss();
                    backgroundAlpha(context,1.0f);//0.0-1.0
                    Intent  intent_myFriend=new Intent(context,MyFriendActivity.class);
                    context.startActivity(intent_myFriend);
            }
        });
        conentView.findViewById(R.id.addFriendLat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent  intent_AddFriend=new Intent(context, ContactAddFriendActivity.class);
                    context.startActivity(intent_AddFriend);
                    dismiss();
                    backgroundAlpha(context,1.0f);//0.0-1.0
              }
        });
        conentView.findViewById(R.id.initGroupChatLat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION()==1){
                    Intent  intent_GroupChatLat=new Intent(context, ConversationAddMembersActivity.class);
                    context.startActivity(intent_GroupChatLat);
                    dismiss();
                    backgroundAlpha(context,1.0f);//0.0-1.0
                }else{
                    dismiss();
                    backgroundAlpha(context,1.0f);//0.0-1.0
                    new AlertDialog.Builder(context)
                            .setIcon(R.drawable.ic_launcher)
                            .setTitle("提示")
                            .setMessage("未通过实名认证不能创建群聊,现在就去进行实名认证吗？")
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            })
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent  intentIdIsauthentcation=new Intent(context, IdAuthentication.class);
                                    context.startActivity(intentIdIsauthentcation);
                                }
                            }).show();
                }
            }
        });
      }
    /**
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(Activity context, float bgAlpha)
    {
        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        context.getWindow().setAttributes(lp);
    }
    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent,Activity context) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, parent.getLayoutParams().width / 2, 5);
            backgroundAlpha(context,0.5f);//0.0-1.0
    }
}
