package com.muki.parttimejob.event;

import java.util.List;

/**
 * Created by muki on 2016/11/1.
 */

public class ReleaseStyleImageEvent {
    private List<String> images;
    public ReleaseStyleImageEvent(List<String> images) {
          this.images=images;
    }
    public  List<String> getImages(){
        return images;
    }
}
