package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.User_Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by muki on 2016/9/27.
 */
public class MyJopAdapter extends BaseAdapter<MyJopAdapter.ViewHolder> {
    DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
    private View.OnClickListener  onClickListener;
    public  MyJopAdapter(Context context, List<AVObject> listDatas,View.OnClickListener  onClickListener) {
        super(context,listDatas);
        this.onClickListener=onClickListener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_myjob_item, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        final User_Job   user_job= (User_Job) listDatas.get(position);
        holder.stateTv.setText(user_job.getState());
        final Job  job=user_job.getJob_Pointer();
        if(job==null){
            return;
        }
        holder.stateLat.setTag(position);
        holder.date_tv.setText(job.getJob_WorkDate());
        holder.location_tv.setText(job.getJob_Address());
        holder.price_tv.setText(job.getJob_Price()+""+job.getJob_Unit());
        holder.recrutment_title_tv.setText(job.getJob_RecruitName());
        Business  business=user_job.getBusiness_pointer();holder.stateBtnTv.setText(user_job.getAction_State());
        if (business.businessStyle()){
            holder.busStyleIv.setImageResource(R.drawable.qiye);
        }else{
            holder.busStyleIv.setImageResource(R.drawable.geren);
        }
        holder.companyName_tv.setText(business.getBusiness_Name());
        if(business.IsVip()){
            holder.vipIv.setVisibility(View.VISIBLE);
        }else{
            holder.vipIv.setVisibility(View.INVISIBLE);
        }
        if(business.IsSalaryGuarantee()){
            holder.baozhangIv.setVisibility(View.VISIBLE);
        }else{
            holder.baozhangIv.setVisibility(View.INVISIBLE);
        }
        double  latitude =job.getGeoPoint().getLatitude();
        double  longitude=job.getGeoPoint().getLongitude();
        AVGeoPoint point = new AVGeoPoint(latitude,longitude);
        double distance=point.distanceInKilometersTo(new AVGeoPoint(AppConfig.mylatitude,AppConfig.mylongitude));
        holder.distanceTv.setText(decimalFormat.format(Double.valueOf(distance))+"千米");
        holder.location_tv.setText(job.getJob_Address());
        UpLoadImageUtil.loadCropImage(context,business.getComLogoUrl(),holder.company_logo_lv);
        String  actionState=user_job.getAction_State();
        holder.stateLat.setVisibility(View.VISIBLE);
        if(actionState.equals(AppConfig.ACTION_NOT_PASS)|actionState.equals(AppConfig.ACTION_WAIT_MER_PAY)|actionState.equals(AppConfig.ACTION_COMPLETE)){
           holder.stateLat.setVisibility(View.INVISIBLE);
        }
         holder.stateLat.setOnClickListener(onClickListener);
    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView  company_logo_lv;
        TextView   recrutment_title_tv;
        TextView   date_tv;
        TextView   price_tv;
        TextView   location_tv;
        TextView   stateTv;
        TextView   stateBtnTv;
        RelativeLayout  stateLat;
        TextView  companyName_tv;
        private  ImageView   busStyleIv;
        private    TextView   distanceTv;
        private    ImageView   baozhangIv;
        private    ImageView    vipIv;
        public ViewHolder(View view) {
            super(view);
            busStyleIv= (ImageView) view.findViewById(R.id.my_busStyleIv);
            vipIv= (ImageView) view.findViewById(R.id.my_vipIv);
            baozhangIv= (ImageView) view.findViewById(R.id.my_baozhangIv);
            distanceTv= (TextView) view.findViewById(R.id.my_distanceTv);
            companyName_tv= (TextView) view.findViewById(R.id.companyName_tv);
            stateLat= (RelativeLayout) view.findViewById(R.id.stateLat);
            stateBtnTv= (TextView) view.findViewById(R.id.stateBtnTv);
            company_logo_lv= (ImageView) view.findViewById(R.id.my_company_logo_lv);
            recrutment_title_tv= (TextView) view.findViewById(R.id.recrutment_title_tv);
            stateTv= (TextView) view.findViewById(R.id.stateTv);
            date_tv= (TextView) view.findViewById(R.id.date_tv);
            price_tv= (TextView) view.findViewById(R.id.my_price_tv);
            location_tv= (TextView) view.findViewById(R.id.my_location_tv);

        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
        this.listDatas=listDatas;
        notifyDataSetChanged();
    }
}




