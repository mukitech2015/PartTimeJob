package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.BalanceAdapter;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/31.
 */

public class BalanceActivity  extends AppCompatActivity implements   PullBaseView.OnHeaderRefreshListener,
        PullBaseView.OnFooterRefreshListener{
    @Bind(R.id.balancePullRv)
    PullRecyclerView  balancePullRv;
    @Bind(R.id.balaceBackLat)
    LinearLayout  balaceBackLat;
    @Bind(R.id.noBalanceLat)
    LinearLayout   noBalanceLat;
    BalanceAdapter   balanceAdapter;
    private  int  page;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_balance);
        ButterKnife.bind(this);
        balancePullRv.setLayoutManager(new LinearLayoutManager(this),this);
        balancePullRv.setOnHeaderRefreshListener(this);
        balancePullRv.setOnFooterRefreshListener(this);
        balaceBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        getDatas();
    }
    public   void    getDatas(  ){
        AVQuery<AVObject>  avObjectAVQuery=new AVQuery<>("BudgetHistory");
        avObjectAVQuery.whereEqualTo("user_pointer", LeanchatUser.getCurrentUser());
        avObjectAVQuery.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        avObjectAVQuery.setMaxCacheAge(24 * 3600); //设置缓存有效期
        avObjectAVQuery.skip(page*10);// 跳过 10 条结果
        avObjectAVQuery.include("budgetWithdrawApply_pointer");
        avObjectAVQuery.limit(10);
        avObjectAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                setBalanceAdapter(list);
            }
        });
    }
    public   void    setBalanceAdapter(List<AVObject>   avObjects){
        if(balanceAdapter==null){
            balanceAdapter=new BalanceAdapter(this,avObjects);
            balancePullRv.setAdapter(balanceAdapter);
        }else{
            balanceAdapter.changeAdapterData(avObjects);
        }
        if(balanceAdapter.getItemCount()==0){
            balancePullRv.setVisibility(View.GONE);
            noBalanceLat.setVisibility(View.VISIBLE);
        }else{
            balancePullRv.setVisibility(View.VISIBLE);
            noBalanceLat.setVisibility(View.GONE);
        }
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                getDatas();
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                getDatas();
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
}
