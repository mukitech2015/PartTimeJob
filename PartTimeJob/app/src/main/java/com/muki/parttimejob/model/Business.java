package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.SaveCallback;

import java.io.IOException;

/**
 * Created by muki on 2016/10/17.
 */

@AVClassName("Business")
public class Business  extends AVObject {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public double  getBusiness_score( ){
        return getDouble("business_score");
    }
    public void setBusiness_score(double   business_score) {
        put("business_score",business_score);
    }
    public String   getBusiness_Name( ){
        return getString("business_Name");
    }
    public void setBusiness_Name(String   business_Name) {
        put("business_Name",business_Name);
    }
    public Boolean  getBusiness_IsAuthentication( ){
        return getBoolean("business_IsAuthentication");
    }
    public void setBusiness_IsAuthentication(Boolean   business_IsAuthentication) {
        put("business_IsAuthentication",business_IsAuthentication);
    }
    public  boolean   IsSalaryGuarantee(  ){
        return  getBoolean("business_IsSalaryGuarantee");
    }
    public  void    setIsSalaryGuarantee(boolean  job_IsSalaryGuarantee){
        put("business_IsSalaryGuarantee",job_IsSalaryGuarantee);
    }
    public  boolean   IsVip(  ){
        return  getBoolean("business_IsVip");
    }
    public  void    setIsVip(boolean  business_IsVip){
        put("business_IsVip",business_IsVip);
    }

    public String getComLogoUrl() {
        AVFile  comLogo = getAVFile("business_CompanyLogo");
        if (comLogo!= null) {
            return comLogo.getUrl();
        } else {
            return null;
        }
    }
    public void saveComLogo(String path, final SaveCallback saveCallback) {
        final AVFile file;
        try {
            file = AVFile.withAbsoluteLocalPath("comLog.png", path);
            put("business_CompanyLogo", file);
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (null == e) {
                        saveInBackground(saveCallback);
                    } else {
                        if (null != saveCallback) {
                            saveCallback.done(e);
                        }
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  public   boolean  businessStyle( ){
      return   getBoolean("business_Style");
  }
}
