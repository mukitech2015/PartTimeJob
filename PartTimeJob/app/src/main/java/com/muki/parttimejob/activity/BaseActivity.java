package com.muki.parttimejob.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

/**
 * Created by muki on 2016/9/17.
 */
public  abstract class BaseActivity   extends Activity {
    protected Activity mActivity= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        onCreateView(savedInstanceState);
        mActivity=this;
        initViews();
        initDatas();

    }
    protected  abstract void  onCreateView(Bundle savedInstanceState);

    protected  abstract void  initViews( );

    protected  abstract void  initDatas( );

}















