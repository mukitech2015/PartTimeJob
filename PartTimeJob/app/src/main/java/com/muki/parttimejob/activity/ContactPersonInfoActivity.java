package com.muki.parttimejob.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.manager.FriendsManager;
import com.muki.parttimejob.model.AddRequestManager;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.utils.UserCacheUtils;

import java.util.List;

import cn.leancloud.chatkit.utils.LCIMConstants;

/**
 * 用户详情页，从对话详情页面和发现页面跳转过来
 */
public class ContactPersonInfoActivity extends Activity implements OnClickListener {
  TextView usernameView, genderView;
  ImageView avatarView, avatarArrowView;
  LinearLayout allLayout;
  Button chatBtn, addFriendBtn;
  RelativeLayout avatarLayout;
  String userId = "";
  LeanchatUser user;
  private  RelativeLayout  personBacklat;
  private RelativeLayout   reportUserLat;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onCreate(savedInstanceState);
    setContentView(R.layout.contact_person_info_activity);
    findView();
  }
  private void initData() {
    userId = getIntent().getStringExtra(Constants.LEANCHAT_USER_ID);
    user = UserCacheUtils.getCachedUser(userId);
    if(user==null){
      AVQuery<LeanchatUser>   leanchatUserAVQuery=AVQuery.getQuery(LeanchatUser.class);
      leanchatUserAVQuery.whereEqualTo("objectId",userId);
      leanchatUserAVQuery.getFirstInBackground(new GetCallback<LeanchatUser>() {
        @Override
        public void done(LeanchatUser leanchatUser, AVException e) {
             if(leanchatUser!=null){
                  user=leanchatUser;
                  initView();
             }
         }
      });
    }else{
      initView();
    }
  }
  private void findView() {
    reportUserLat= (RelativeLayout) findViewById(R.id.reportUserLat);
    personBacklat= (RelativeLayout) findViewById(R.id.personBacklat);
    allLayout = (LinearLayout) findViewById(R.id.all_layout);
    avatarView = (ImageView) findViewById(R.id.avatar_view);
    avatarArrowView = (ImageView) findViewById(R.id.avatar_arrow);
    usernameView = (TextView) findViewById(R.id.username_view);
    avatarLayout = (RelativeLayout) findViewById(R.id.head_layout);
    chatBtn = (Button) findViewById(R.id.chatBtn);
    addFriendBtn = (Button) findViewById(R.id.addFriendBtn);
    personBacklat.setOnClickListener(this);
    reportUserLat.setOnClickListener(this);
  }
  @Override
  protected void onStart() {
    super.onStart();
    initData();
  }

  private void initView() {
    LeanchatUser curUser = LeanchatUser.getCurrentUser();
    if (curUser.equals(user)) {
      avatarLayout.setOnClickListener(this);
      avatarArrowView.setVisibility(View.VISIBLE);
      chatBtn.setVisibility(View.GONE);
      addFriendBtn.setVisibility(View.GONE);
    } else {
      avatarArrowView.setVisibility(View.INVISIBLE);
      List<String> cacheFriends = FriendsManager.getFriendIds();
      boolean isFriend = cacheFriends.contains(user.getObjectId());
      if (isFriend) {
        chatBtn.setVisibility(View.VISIBLE);
        chatBtn.setOnClickListener(this);
      } else {
        chatBtn.setVisibility(View.GONE);
        addFriendBtn.setVisibility(View.VISIBLE);
        addFriendBtn.setOnClickListener(this);
      }
    }
    Log.e("=================",user.getObjectId()+"");
        updateView(user);
  }
  private void updateView(LeanchatUser user) {
    UpLoadImageUtil.loadCropImage(this,user.getAvatarUrl(),avatarView);
    if(!TextUtils.isEmpty(user.getUSER_NICKNAME())){
      usernameView.setText(user.getUSER_NICKNAME());
    }else{
      usernameView.setText("未填写");
    }
  }
  AlertDialog.Builder builderRepUser;
  public  void  ReportUserDlg( ){
    if(builderRepUser==null){
      builderRepUser=new AlertDialog.Builder(ContactPersonInfoActivity.this);
    }
    builderRepUser.setTitle("举报用户");
    builderRepUser.setIcon(R.drawable.ic_launcher);
    builderRepUser.setMessage("确定要举报该用户么？");
    builderRepUser.setNegativeButton("取消", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

      }
    }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        ReporUser();
      }
    }).show();
  }
  public  void   ReporUser(  ){
    AVObject repUserAvObj=new AVObject("Report");
    repUserAvObj.put("toReporter",user);
    repUserAvObj.put("fromReporter",LeanchatUser.getCurrentUser());
    repUserAvObj.saveInBackground(new SaveCallback() {
      @Override
      public void done(AVException e) {
        Toast.makeText(ContactPersonInfoActivity.this,"已举报该用户",Toast.LENGTH_SHORT).show();
      }
    });
  }
  @Override
  public void onClick(View v) {
    // TODO Auto-generated method stub
    switch (v.getId()) {
      case R.id.chatBtn:// 发起聊天
        Intent intent = new Intent(ContactPersonInfoActivity.this, ChatRoomActivity.class);
        intent.putExtra("CHAT_TITLE",user.getUSER_NICKNAME());
        intent.putExtra(LCIMConstants.PEER_ID, userId);
        startActivity(intent);
        finish();
        break;
      case R.id.addFriendBtn:// 添加好友
        AddRequestManager.getInstance().createAddRequestInBackground(this, user);
        break;
      case  R.id.personBacklat:
        finish();
        break;
      case R.id.reportUserLat:
        ReportUserDlg();
        break;
    }
  }
}
