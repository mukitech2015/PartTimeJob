package com.muki.parttimejob.entity;

/**
 * Created by muki on 2016/9/27.
 */
public class JobState {
    int  stateIcon;
    String  state;

    public JobState(int stateIcon, String state, String stateName) {
        this.stateIcon = stateIcon;
        this.state = state;
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getStateIcon() {
        return stateIcon;
    }

    public void setStateIcon(int stateIcon) {
        this.stateIcon = stateIcon;
    }

    String  stateName;
}
