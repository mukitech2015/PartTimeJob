package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

/**
 * Created by muki on 2016/11/2.
 */
@AVClassName("BudgetWithdrawApply")
public class BudgetWithdrawApply  extends AVObject {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public  int   getState(  ){
      return   getInt("state");
    }
    public   void   setState(int  state){
       put("state",state);
    }
    public   void  setAccount(String  account){
        put("account",account);
    }
    public   String  getAccount( ){
        return   getString("account");
    }
    public   void  setAccountType(String  accountType){
        put("accountType",accountType);
    }
    public   String  getAccountType( ){
        return getString("accountType");
    }
    public  void  setWithdrawMoney( double   withdrawMoney){
        put("withdrawMoney",withdrawMoney);
    }
    public   double getWithdrawMoney(  ){
        return  getDouble("WithdrawMoney");
    }

    public  void  setWithdrawId(String    withdrawId){
        put("order_Id",withdrawId);
    }
    public  String  getWithdrawId(  ){
        return  getString("order_Id");
    }
}
