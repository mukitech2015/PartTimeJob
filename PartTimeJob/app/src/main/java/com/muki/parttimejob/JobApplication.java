package com.muki.parttimejob;

import android.os.StrictMode;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.im.v2.AVIMMessageManager;
import com.avos.avoscloud.im.v2.AVIMTypedMessage;
import com.baidu.mapapi.SDKInitializer;
import com.muki.parttimejob.entity.FilterEntity;
import com.muki.parttimejob.manager.PushManager;
import com.muki.parttimejob.model.AddRequest;
import com.muki.parttimejob.model.BannerImage;
import com.muki.parttimejob.model.BudgetHistory;
import com.muki.parttimejob.model.BudgetWithdrawApply;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.Dynamic_Follow;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Image_Info;
import com.muki.parttimejob.model.User_Job;
import com.muki.parttimejob.utils.LeanchatUserProvider;
import com.muki.parttimejob.viewholder.LCIMMessageHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.umeng.socialize.PlatformConfig;

import org.litepal.LitePalApplication;

import java.util.ArrayList;
import java.util.List;

import cn.leancloud.chatkit.LCChatKit;



/**
 * Created by muki on 2016/9/12.
 */
public class JobApplication  extends LitePalApplication {
    private   static JobApplication instance;
    public static final String DB_NAME = "chat.db3";
    public static final int DB_VER = 4;
    public static boolean debug = true;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initImageLoader();
        SDKInitializer.initialize(this);
        AVIMMessageManager.registerMessageHandler(AVIMTypedMessage.class, new LCIMMessageHandler(getApplicationContext()));
        LCChatKit.getInstance().init(this,"3F20zSX1g5PEJY7JtCw7qbVH-9Nh9j0Va", "Bb86fJj4AnmytnEHLvwO23zd");
        AVObject.registerSubclass(Dynamic.class);
        AVObject.registerSubclass(AddRequest.class);
        AVObject.registerSubclass(BudgetHistory.class);
        AVObject.registerSubclass(BudgetWithdrawApply.class);
        AVObject.registerSubclass(Dynamic_Follow.class);
        AVObject.registerSubclass(Job.class);
        AVObject.registerSubclass(Business.class);
        AVObject.registerSubclass(User_Job.class);
        AVObject.registerSubclass(User_Image_Info.class);
        LeanchatUser.alwaysUseSubUserClass(LeanchatUser.class);
        PushManager.getInstance().init(this);
        PushManager.getInstance().unsubscribeCurrentUserChannel();
        AVOSCloud.setDebugLogEnabled(true);
        LCChatKit.getInstance().setProfileProvider(new LeanchatUserProvider());
        PlatformConfig.setWeixin("wxba8c0b75254672ca", "1ec25632308d7132bb32cf9cb6df7b99");
        PlatformConfig.setQQZone("1105854064", "VMnjVkXkbsHLFdXz");
        getBannerImages();
        getAllJobTypes();
        getJobList();
        getJobNewReleList();
        AVOSCloud.setNetworkTimeout(20 * 1000);
    }
    public  static JobApplication getInstance( ){
        return  instance;
    }
    public   void  initImageLoader( ){
        DisplayImageOptions defaultOptions = new DisplayImageOptions
                .Builder()
                .showImageForEmptyUri(R.drawable.empty_photo)
                .showImageOnFail(R.drawable.empty_photo)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .discCacheSize(50 * 1024 * 1024)//
                .discCacheFileCount(100)//缓存一百张图片
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }
    public void openStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build());
    }
    public   List<FilterEntity> jobTypes = new ArrayList<>();
    public   void   getAllJobTypes(  ){
        AVQuery<AVObject>  jobTypeQuery=new AVQuery<>("Job_Type");
        jobTypeQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(e==null){
                        jobTypes.clear();
                    for(AVObject   avObject:list){
                        jobTypes.add(new FilterEntity(avObject.getString("type_Name"),avObject.getObjectId()));
                    }
                    jobTypes.add(0,new FilterEntity("兼职类型","0"));
                }
            }
        });
    }
    public  List<BannerImage>  bannerList=new ArrayList<>();
    public   void  getBannerImages(  ){
        AVQuery<AVObject> avQuery=new AVQuery<>("Banner");
        avQuery.include("banner_Image");
        avQuery.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        avQuery.setMaxCacheAge(24 * 3600); //设置缓存有效期
        avQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if(list!=null){
                        bannerList.clear();
                    for (AVObject  avObject:list){
                        bannerList.add(new BannerImage(avObject.getAVFile("banner_Image").getUrl(),avObject.getString("url")));
                    }
                }
            }
        });
    }
    public  List<Job>  jobNewReleList=new ArrayList<>();
    public  void  getJobNewReleList(  ){
        AVQuery<Job> query = new AVQuery<>("Job");
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.include("business_pointer");
        query.whereEqualTo("job_States",true);
        query.limit(10);// 最多返回 10 条结果
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<Job>() {
            @Override
            public void done(List<Job> list, AVException e) {
                if(e==null){
                    jobNewReleList.clear();
                    jobNewReleList.addAll(list);
                }
            }
        });
    }
    public  List<Job>  jobList=new ArrayList<>();
    public  void  getJobList(  ){
        AVQuery<Job> query = new AVQuery<>("Job");
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.include("business_pointer");
        query.whereEqualTo("job_States",true);
        query.addDescendingOrder("job_Hot");
        query.findInBackground(new FindCallback<Job>() {
            @Override
            public void done(List<Job> list, AVException e) {
                if(e==null){
                  jobList.clear();
                  jobList.addAll(list);
                }
            }
        });
    }
}







