package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.JobListAdapter;
import com.muki.parttimejob.widget.BaseAdapter;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/4.
 */

public class ScreenActivity   extends AppCompatActivity implements  PullBaseView.OnHeaderRefreshListener, PullBaseView.OnFooterRefreshListener{
    @Bind(R.id.screenBackLat)
    LinearLayout  screenBackLat;
    @Bind(R.id.screenJobRv)
    PullRecyclerView  screenJobRv;
    int page=0;
    String content;
    String workTime;
    String place;
    String industry;
    private JobListAdapter screenAdapter;
    private List<AVObject>  joblist=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_screen);
        ButterKnife.bind(this);
        screenBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        screenJobRv.setLayoutManager(new LinearLayoutManager(this),this);
        screenJobRv.setOnHeaderRefreshListener(this);
        screenJobRv.setOnFooterRefreshListener(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
         Intent intent=getIntent();
         content=intent.getStringExtra("content");
         workTime=intent.getStringExtra("workTime");
         place=intent.getStringExtra("place");
         industry=intent.getStringExtra("industry");
         getScreenDatas(content,workTime,place,industry);
    }
    Date getDateWithDateString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    public  void  getScreenDatas(String content, String  workTime, final String place, String industry){
        AVQuery<AVObject> query = new AVQuery<>("Job");
        query.limit(10);
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.orderByDescending("createdAt");
        query.skip(page*10);// 跳过 10 条结果
        query.include("business_pointer");
        if (!TextUtils.isEmpty(content)){
            query.whereContains("job_RecruitName",content);
        }
        if (!TextUtils.isEmpty(workTime)){
            query.whereLessThan("job_EndTime", getDateWithDateString(workTime));
        }
        if (!TextUtils.isEmpty(place)){
            query.whereContains("job_Address",place);
        }
        if (!TextUtils.isEmpty(industry)){
            query.whereContains("job_Industy",industry);
        }
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (list!=null){
                    if(page==0){
                        joblist.clear();
                        joblist.addAll(list);
                        setScreenAdapter(joblist);
                    }else{
                        joblist.addAll(joblist.size(),list);
                        setScreenAdapter(joblist);
                    }
                }
            }
        });
    }
    public  void  setScreenAdapter(List<AVObject>  list){
        if(screenAdapter==null){
            screenAdapter=new JobListAdapter(this,list);
            screenJobRv.setAdapter(screenAdapter);
        }
        screenAdapter.changeAdapterData(list);
        screenAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent  intent=new Intent(ScreenActivity.this,JobDetailActivity.class);
                intent.putExtra("job", (Serializable)joblist.get(position));
                startActivity(intent);
            }
        });
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                getScreenDatas(content,workTime,place,industry);
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                getScreenDatas(content,workTime,place,industry);
                view.onHeaderRefreshComplete();
            }
        }, 2000);
    }
}










