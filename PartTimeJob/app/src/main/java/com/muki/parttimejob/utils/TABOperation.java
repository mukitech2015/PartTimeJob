package com.muki.parttimejob.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SignUpCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.muki.parttimejob.activity.BaseActivity;
import com.muki.parttimejob.activity.MainActivity;
import com.muki.parttimejob.model.LeanchatUser;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.leancloud.chatkit.LCChatKit;

/**
 * Created by muki on 2016/9/18.
 */
public class TABOperation {
    /**
     * 用户登录
     */
    public static void userLogin(final BaseActivity context, final String userName, final String passWord) {
        LeanchatUser.logInInBackground(userName, passWord, new LogInCallback<LeanchatUser>() {
            @Override
            public void done(LeanchatUser avUser, AVException e) {
                if (e == null) {
                      if(!LeanchatUser.getCurrentUser().getUserState()){
                          LeanchatUser.getCurrentUser().put("user_Password",passWord);
                          LeanchatUser.getCurrentUser().setMobilePhoneNumber(userName);
                          LeanchatUser.getCurrentUser().updateInstallation(AVInstallation.getCurrentInstallation());
                          LeanchatUser.getCurrentUser().saveInBackground();
                          imLogin(context);
                      }else{
                          Toast.makeText(context,"该账号已被禁止",Toast.LENGTH_SHORT).show();
                      }
                } else {
                      if (e.getMessage().equals("java.net.UnknownHostException")){
                         Toast.makeText(context,"请检查网络",Toast.LENGTH_SHORT).show();
                     }else{
                         toMeaasge(e.getCode(),context);
                     }
                }
            }
        }, LeanchatUser.class);
    }
    public static   void    toMeaasge(int code,Context   context){
              switch (code) {
                  case  1:
                  Toast.makeText(context,"登陆失败次数超过限制，请重置密码", Toast.LENGTH_SHORT).show();
                  break;
                  case 210:
                  Toast.makeText(context,"密码错误", Toast.LENGTH_SHORT).show();
                  break;
                  case 202:
                  Toast.makeText(context,"该手机号已注册", Toast.LENGTH_SHORT).show();
                  break;
                  case 211:
                  Toast.makeText(context,"用户名或密码错误", Toast.LENGTH_SHORT).show();
                  break;
              }
    }
    public static void imLogin(final BaseActivity context) {
        LCChatKit.getInstance().open(LeanchatUser.getCurrentUserId(), new AVIMClientCallback() {
            @Override
            public void done(AVIMClient avimClient, AVIMException e) {
                if (e == null) {
                        Intent intent=new Intent(context,MainActivity.class);
                        context.startActivity(intent);
                        context.finish();
                } else {
                        toMeaasge(e.getCode(),context);
                 }
            }
        });
    }
    /**
     *  修改密码
     */
    public static  void  changePassWord(final BaseActivity context, final String  userPhone, final String passWord){
        AVQuery<LeanchatUser> query = AVObject.getQuery(LeanchatUser.class);
        query.whereEqualTo("username", userPhone);
        query.getFirstInBackground(new GetCallback<LeanchatUser>() {
            @Override
            public void done(final LeanchatUser leanchatUser, AVException e) {
               if(e==null){
                    AVUser.logInInBackground(userPhone, leanchatUser.getString("user_Password"), new LogInCallback<AVUser>() {
                        @Override
                        public void done(AVUser avUser, AVException e) {
                            if (e==null){
                                avUser.setPassword(passWord);
                                avUser.put("user_Password",passWord);
                                avUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                      if (e==null){
                                          Toast.makeText(context,"重置密码成功",Toast.LENGTH_SHORT).show();
                                          Intent intent=new Intent();
                                          intent.putExtra("userPhone",userPhone);
                                          context.setResult(Activity.RESULT_OK,intent);
                                          context.finish();
                                      }else{
                                          Toast.makeText(context,"重置密码失败",Toast.LENGTH_SHORT).show();
                                      }
                                    }
                                });
                            }
                        }
                    });
               }
            }
        });
    }
    /**
     *  用户注册
     */
   public  static   void  userRegister(final BaseActivity context, final String  userPhone, final String passWord, final String  user_Referral){
       LeanchatUser.signUpByNameAndPwd(userPhone,passWord, new SignUpCallback() {
           @Override
           public void done(AVException e) {
               if (e == null) {
                   AVQuery<LeanchatUser>   userAVQuery=AVQuery.getQuery(LeanchatUser.class);
                   userAVQuery.whereEqualTo("username",userPhone);
                   userAVQuery.getFirstInBackground(new GetCallback<LeanchatUser>() {
                       @Override
                       public void done(final LeanchatUser myleanchatUser, AVException e) {
                         if (myleanchatUser!=null){
                             if (!TextUtils.isEmpty(user_Referral)){
                              AVQuery<LeanchatUser>   leanchatUserAVQuery=AVQuery.getQuery(LeanchatUser.class);
                              leanchatUserAVQuery.whereEqualTo("username",user_Referral);
                              leanchatUserAVQuery.getFirstInBackground(new GetCallback<LeanchatUser>() {
                                  @Override
                                  public void done(LeanchatUser leanchatUser, AVException e) {
                                       if (leanchatUser!=null){
                                           myleanchatUser.put("user_Referral_pointer",leanchatUser);
                                           myleanchatUser.put("user_Password",passWord);
                                           myleanchatUser.saveInBackground(new SaveCallback() {
                                               @Override
                                               public void done(AVException e) {
                                                   if (e==null){
                                                       Intent intent=new Intent();
                                                       intent.putExtra("userPhone",userPhone);
                                                       context.setResult(Activity.RESULT_OK,intent);
                                                       context.finish();
                                                   }
                                               }
                                           });
                                       }else{
                                           Toast.makeText(context,"推荐人不存在",Toast.LENGTH_SHORT).show();
                                       }
                                  }
                              });
                             }else{
                                 myleanchatUser.put("user_Password",passWord);
                                 myleanchatUser.saveInBackground(new SaveCallback() {
                                     @Override
                                     public void done(AVException e) {
                                         if (e==null){

                                             Intent intent=new Intent();
                                             intent.putExtra("userPhone",userPhone);
                                             context.setResult(Activity.RESULT_OK,intent);
                                             context.finish();
                                         }
                                     }
                                 });
                             }
                         }
                       }
                   });
               }else{
                  toMeaasge(e.getCode(),context);
               }
            }
       });
   }
  public   static   Date getDateWithDateString(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    /**
     *   更新用户信息
     * */
    public  static void   updateUserInfo(final Context  context, String university, String user_AdmissionDate,
                                         String  user_Degree, String  user_Major,
                                         String school, String   sex,  String   userHeight, String   userWeight, String  nickName,String  birth) throws AVException, FileNotFoundException {
        if(!sex.equals("-1")){
            LeanchatUser.getCurrentUser().setUSER_SEX(sex);
        }
        if (!TextUtils.isEmpty(birth)){
            LeanchatUser.getCurrentUser().setUserBirth(getDateWithDateString(birth+" 00:00:00"));
        }
        LeanchatUser.getCurrentUser().setUSER_NICKNAME(nickName);
        LeanchatUser.getCurrentUser().setUserHeight(userHeight);
        LeanchatUser.getCurrentUser().setUserWeight(userWeight);
        if(!TextUtils.isEmpty(university)&&!TextUtils.isEmpty(user_AdmissionDate)&&TextUtils.isEmpty(user_Degree)&&TextUtils.isEmpty(user_Major)&&TextUtils.isEmpty(school))
        {   if(LeanchatUser.getCurrentUser().getUSER_SCHOOL_ISAUTHENTICATION()==0||LeanchatUser.getCurrentUser().getUSER_SCHOOL_ISAUTHENTICATION()==3)
        {
            LeanchatUser.getCurrentUser().setUSER_COLLEGE(school);
            LeanchatUser.getCurrentUser().setUSER_SCHOOL(university);
            LeanchatUser.getCurrentUser().setUSER_DEGREE(user_Degree);
            LeanchatUser.getCurrentUser().setUSER_ADMISSION(user_AdmissionDate);
            LeanchatUser.getCurrentUser().setUSER_MAJOR(user_Major);
            LeanchatUser.getCurrentUser().setUSER_SCHOOL_ISAUTHENTICATION(2);
        }
        }
        LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    Toast.makeText(context,"修改个人信息成功",Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                }else{
                    Toast.makeText(context,"修改个人信息失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
