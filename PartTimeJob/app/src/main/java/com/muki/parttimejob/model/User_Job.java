package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

import java.io.Serializable;

/**
 * Created by muki on 2016/10/17.
 */


@AVClassName("User_Job")
public class User_Job   extends AVObject implements Serializable {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public   void   setAction_State(String   action_State){
       put("action_State",action_State);
    }
    public   String  getAction_State(  ){
     return   getString("action_State");
    }
    public  Job   getJob_Pointer(  ){
      return   getAVObject("job_pointer");
    }
    public   void   setJob_Pointer(Job   job){
        put("job_pointer",job);
    }
    public Business  getBusiness_pointer(     ){
        return   getAVObject("business_pointer");
    }
    public   void   setBusiness_pointer(AVObject  avObject){
        put("business_pointer",avObject);
    }
    public  String     getState(     ){
        return   getString("state");
    }
    public   void   setState(String  state){
        put("state",state);
    }
    public   void   setUser_pointer(LeanchatUser   leanchatUser){
        put("user_pointer",leanchatUser);
    }
    public   LeanchatUser  getUser_pointer( ){
        return   getAVObject("user_pointer");
    }
}



















