package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.utils.SmsSender;
import com.muki.parttimejob.utils.TABOperation;

import java.util.Random;


/**
 * Created by muki on 2016/9/18.
 */
public class RegisterActivity   extends BaseActivity implements View.OnClickListener{
    private  LinearLayout   registerBackLat;
    private  RelativeLayout   verificationCodeLat;
    private  RelativeLayout   registerLat;
    private EditText   phoneEt;
    private EditText   passwordEt;
    private EditText   verificationCodeEt;
    protected boolean needCallback;
    private TextView   verificationTv;
    private TimeCount time;
    private EditText  userReferralEt;
    private String    userReferStr;
    long rnd;
    String  passwordStr;
    Random random = new Random();
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onFinish() {// 计时完毕
            verificationTv.setText("获取验证码");
            verificationCodeLat.setClickable(true);
        }
        @Override
        public void onTick(long millisUntilFinished) {// 计时过程
            verificationTv.setClickable(false);//防止重复点击
            verificationTv.setText(millisUntilFinished / 1000 + "S");
        }
    }
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        setContentView(R.layout.layout_act_register);
        Bundle bundle = getIntent().getExtras();
        if(bundle == null)
            return;
        needCallback = bundle.getBoolean(AppConfig.NeedCallback, false);
    }
    @Override
    protected void initViews() {
        userReferralEt= (EditText) findViewById(R.id.user_Referral_et);
        verificationTv= (TextView) findViewById(R.id.verification_tv);
        verificationCodeLat= (RelativeLayout) findViewById(R.id.verification_code_lat);
        registerLat= (RelativeLayout) findViewById(R.id.register_lat);
        verificationCodeEt= (EditText) findViewById(R.id.verification_code_et);
        phoneEt= (EditText) findViewById(R.id.phone_et);
        passwordEt= (EditText) findViewById(R.id.password_et);
        registerBackLat= (LinearLayout) findViewById(R.id.register_back_lat);
        registerBackLat.setOnClickListener(this);
        registerLat.setOnClickListener(this);
        verificationCodeLat.setOnClickListener(this);
        time = new TimeCount(60000, 1000);
        phoneEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                } else {
                    if (!phoneEt.getText().toString().trim().equals("")){
                        if (isMobileNO(phoneEt.getText().toString().trim()) == false)
                        {
                            Toast.makeText(RegisterActivity.this,"手机号格式错误",Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }
            }
        });
    }
    @Override
    protected void initDatas() {

    }
    String  phoneStr;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_back_lat:
                finish();
                break;
            case  R.id.verification_code_lat:
                 phoneStr=phoneEt.getText().toString().trim();
                 passwordStr=passwordEt.getText().toString().trim();
                if (TextUtils.isEmpty(phoneStr)){
                    Toast.makeText(RegisterActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isMobileNO(phoneStr)){
                    Toast.makeText(RegisterActivity.this,"手机号格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(passwordStr)){
                    Toast.makeText(RegisterActivity.this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isPassWordNo(passwordStr)){
                    Toast.makeText(RegisterActivity.this,"密码格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                rnd = random.nextInt(9999)%(9999-1000+1)+1000;
                Thread thread=new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        SmsSender sender = new SmsSender(1400014630, "777f8d474ec29f52ea8a21479edbcb2d");
                        sender.sendMsg("86",phoneStr, "此验证码只用于注册啄米兼职App，"+rnd+"（验证码）,请勿泄露.",rnd);
                        time.start();
                    }
                });
                thread.start();
                break;
            case  R.id.register_lat:
                userReferStr=userReferralEt.getText().toString().trim();
                phoneStr=phoneEt.getText().toString().trim();
                passwordStr=passwordEt.getText().toString().trim();
                String  verificationCodeStr=verificationCodeEt.getText().toString().trim();
                if (TextUtils.isEmpty(phoneStr)){
                    Toast.makeText(RegisterActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(passwordStr)){
                    Toast.makeText(RegisterActivity.this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isPassWordNo(passwordStr)){
                    Toast.makeText(RegisterActivity.this,"密码格式错误",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(verificationCodeStr)){
                    Toast.makeText(RegisterActivity.this,"请输入验证码",Toast.LENGTH_SHORT).show();
                    return;
                }
//                if(!verificationCodeStr.equals(rnd+"")){
//                    Toast.makeText(RegisterActivity.this,"验证码不正确",Toast.LENGTH_SHORT).show();
//                    return;
//                }
                TABOperation.userRegister(RegisterActivity.this,phoneStr,passwordStr,userReferStr);
                break;
        }
    }
    public static boolean isPassWordNo(String passwords){
        String telRegex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";
        if (TextUtils.isEmpty(passwords)) return false;
        else return passwords.matches(telRegex);
    }
    public static boolean isMobileNO(String mobiles) {
        String telRegex = "[1][358]\\d{9}";//"[1]"代表�?1位为数字1�?"[358]"代表第二位可以为3�?5�?8中的�?个，"\\d{9}"代表后面是可以是0�?9的数字，�?9位�??
        if (TextUtils.isEmpty(mobiles)) return false;
        else return mobiles.matches(telRegex);
    }
}








