package com.muki.parttimejob.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.TABOperation;

/**
 * Created by muki on 2016/9/18.
 */
public class LoginActivity    extends    BaseActivity  implements View.OnClickListener{
    private RelativeLayout    registerLat;
    private static final int LOGIN_REDIRECT_INSIDE = 3001;	//注册成功后仍然在本页
    private EditText  loginPhoneEt;
    private EditText  loginPasswordEt;
    private RelativeLayout  loginLat;
    private TextView forgetPassWordTv;
    private  TextView   hideAndshowTv;
    private boolean mbDisplayFlg = false;
    @Override
    protected void onCreateView(Bundle savedInstanceState) {
        if(LeanchatUser.getCurrentUser()!=null){
            Intent  intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.layout_act_login);
    }
    @Override
    protected void initViews() {
        hideAndshowTv= (TextView) findViewById(R.id.hideAndshowTv);
        forgetPassWordTv= (TextView) findViewById(R.id.forgetPassTv);
        loginLat= (RelativeLayout) findViewById(R.id.login_lat);
        loginPhoneEt= (EditText) findViewById(R.id.login_phone_et);
        loginPasswordEt= (EditText) findViewById(R.id.login_password_et);
        registerLat= (RelativeLayout) findViewById(R.id.login_register_lat);
        registerLat.setOnClickListener(this);
        loginLat.setOnClickListener(this);
        forgetPassWordTv.setOnClickListener(this);
        hideAndshowTv.setOnClickListener(this);
    }
    @Override
    protected void initDatas() {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_register_lat:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra(AppConfig.NeedCallback, true);
                startActivityForResult(intent, LOGIN_REDIRECT_INSIDE);
                break;
            case R.id.login_lat:
                String   phoneStr=loginPhoneEt.getText().toString().trim();
                String   passwordStr=loginPasswordEt.getText().toString().trim();
                if(TextUtils.isEmpty(phoneStr)){
                    Toast.makeText(LoginActivity.this,"请输入手机号",Toast.LENGTH_SHORT);
                    return;
                }
                if(TextUtils.isEmpty(passwordStr)){
                    Toast.makeText(LoginActivity.this,"请输入密码",Toast.LENGTH_SHORT);
                    return;
                }
                TABOperation.userLogin(LoginActivity.this,phoneStr,passwordStr);
                break;
            case  R.id.forgetPassTv:
                Intent   intentFgPd=new Intent(LoginActivity.this,ForgetPassWordActivity.class);
                intentFgPd.putExtra("CHANGE_PW_TITLE","找回密码");
                intentFgPd.putExtra(AppConfig.NeedCallback, true);
                startActivityForResult(intentFgPd, LOGIN_REDIRECT_INSIDE);
                break;
            case   R.id.hideAndshowTv:
                if (!mbDisplayFlg) {
                    loginPasswordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    loginPasswordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                mbDisplayFlg = !mbDisplayFlg;
                loginPasswordEt.postInvalidate();
                break;
           }
       }
    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if(requestCode==LOGIN_REDIRECT_INSIDE) {
            String usrePhone=data.getStringExtra("userPhone");
            loginPhoneEt.setText(usrePhone);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}





