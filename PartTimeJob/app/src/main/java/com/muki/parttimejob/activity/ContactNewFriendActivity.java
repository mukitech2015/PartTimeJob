package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.HeaderListAdapter;
import com.muki.parttimejob.event.ContactRefreshEvent;
import com.muki.parttimejob.event.NewFriendItemClickEvent;
import com.muki.parttimejob.model.AddRequest;
import com.muki.parttimejob.model.AddRequestManager;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.service.PreferenceMap;
import com.muki.parttimejob.utils.ConversationUtils;
import com.muki.parttimejob.viewholder.NewFriendItemHolder;
import com.muki.parttimejob.widget.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import de.greenrobot.event.EventBus;

public class ContactNewFriendActivity extends AVBaseActivity {

  @Bind(R.id.newfriendList)
  RecyclerView recyclerView;
  LinearLayoutManager layoutManager;
  private HeaderListAdapter<AddRequest> adapter;
  private RelativeLayout  newFdBackLat;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.contact_new_friend_activity);
    initView();
  }
  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }
  private void initView() {
    layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.addItemDecoration(new SpacesItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL,15, Color.parseColor("#82858b")));
    newFdBackLat= (RelativeLayout) findViewById(R.id.newFdBackLat);
    newFdBackLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    adapter = new HeaderListAdapter<>(NewFriendItemHolder.class);
    loadMoreAddRequest(true);
    recyclerView.setAdapter(adapter);
  }
    private void loadMoreAddRequest(final boolean isRefresh) {
      AddRequestManager.getInstance().findAddRequests(isRefresh ? 0 : adapter.getDataList().size(), 20, new FindCallback<AddRequest>() {
        @Override
        public void done(List<AddRequest> list, AVException e) {
          AddRequestManager.getInstance().markAddRequestsRead(list);
          final List<AddRequest> filters = new ArrayList<AddRequest>();
          for (AddRequest addRequest : list) {
            if (addRequest.getFromUser() != null) {
              filters.add(addRequest);
            }
          }
          PreferenceMap preferenceMap = new PreferenceMap(ContactNewFriendActivity.this, LeanchatUser.getCurrentUserId());
          preferenceMap.setAddRequestN(filters.size());
          adapter.setDataList(filters);
          adapter.notifyDataSetChanged();
        }
      });
    }
    public void onEvent(NewFriendItemClickEvent event) {
    if (event.isLongClick) {
      deleteAddRequest(event.addRequest);
    } else {
      agreeAddRequest(event.addRequest);
    }
  }
  private void agreeAddRequest(final AddRequest addRequest) {
    final ProgressDialog dialog = showSpinnerDialog();
    AddRequestManager.getInstance().agreeAddRequest(addRequest, new SaveCallback() {
      @Override
      public void done(AVException e) {
          dialog.dismiss();
        if (filterException(e)) {
          if (addRequest.getFromUser() != null) {
            sendWelcomeMessage(addRequest.getFromUser().getObjectId());
          }
          loadMoreAddRequest(false);
          ContactRefreshEvent event = new ContactRefreshEvent();
          EventBus.getDefault().post(event);
        }
      }
    });
  }
  public void sendWelcomeMessage(String toUserId) {
    ConversationUtils.createSingleConversation(toUserId, new AVIMConversationCreatedCallback() {
      @Override
      public void done(AVIMConversation avimConversation, AVIMException e) {
        if (e == null) {
          AVIMTextMessage message = new AVIMTextMessage();
          message.setText(getString(R.string.message_when_agree_request));
          avimConversation.sendMessage(message, null);
        }
      }
    });
  }
  private void deleteAddRequest(final AddRequest addRequest) {
    new AlertDialog.Builder(this).setMessage(R.string.contact_deleteFriendRequest)
      .setPositiveButton(R.string.common_sure, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          addRequest.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(AVException e) {
              loadMoreAddRequest(true);
            }
          });
        }
      }).setNegativeButton(R.string.chat_common_cancel, null).show();
  }
}
