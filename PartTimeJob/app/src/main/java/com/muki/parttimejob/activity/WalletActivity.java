package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.LeanchatUser;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/9/27.
 */
public class WalletActivity  extends AppCompatActivity implements View.OnClickListener{
    @Bind(R.id.advanceLat)
    RelativeLayout advanceLat;
    @Bind(R.id.balanceDetailLat)
    RelativeLayout  balanceDetailLat;
    @Bind(R.id.explainLat)
    RelativeLayout  explainLat;
    @Bind(R.id.wallentBackLat)
    LinearLayout wallentBackLat;
    @Bind(R.id.withdrawalsLat)
    RelativeLayout  withdrawalsLat;
    @Bind(R.id.userBalanceTv)
    TextView  userBalanceTv;
    @Bind(R.id.accountSetLat)
    RelativeLayout  accountSetLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_wallet);
        ButterKnife.bind(this);
        advanceLat.setOnClickListener(this);
        balanceDetailLat.setOnClickListener(this);
        explainLat.setOnClickListener(this);
        wallentBackLat.setOnClickListener(this);
        withdrawalsLat.setOnClickListener(this);
        accountSetLat.setOnClickListener(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        userBalanceTv.setText(LeanchatUser.getCurrentUser().getBalance()+"");
    }
    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.accountSetLat:
                  Intent  intentAccount=new Intent(WalletActivity.this,AccountSetActivity.class);
                  startActivity(intentAccount);
                 break;
             case R.id.withdrawalsLat:
                  int   state=LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION();
                  if(state==1){
                      Intent  intentWithdrawals=new Intent(WalletActivity.this,WithdrawalsActivity.class);
                      startActivity(intentWithdrawals);
                  }else{
                      Toast.makeText(WalletActivity.this,"未通过身份验证,请先去个人中心进行身份认证",Toast.LENGTH_SHORT).show();
                  }
                  break;
             case R.id.wallentBackLat:
                   finish();
                 break;
             case R.id.advanceLat:
                 Intent  intentWebAdvice=new Intent(WalletActivity.this,WebActivity.class);
                 intentWebAdvice.putExtra("WEB_URL", AppConfig.YUZHIGONGZI+LeanchatUser.getCurrentUserId());
                 startActivity(intentWebAdvice);
                 break;
             case R.id.balanceDetailLat:
                 Intent  intentAdvance=new Intent(WalletActivity.this,BalanceActivity.class);
                 startActivity(intentAdvance);
                 break;
             case R.id.explainLat:
                 Intent  intentExplain=new Intent(WalletActivity.this,WebActivity.class);
                 intentExplain.putExtra("WEB_URL", AppConfig.SHIYONG);
                 startActivity(intentExplain);
                 break;
         }
    }
}









