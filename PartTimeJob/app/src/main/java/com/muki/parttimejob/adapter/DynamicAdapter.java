package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.ContactPersonInfoActivity;
import com.muki.parttimejob.entity.ImageInfo;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.Dynamic_Follow;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.Constants;
import com.muki.parttimejob.utils.TimeTool;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.BaseAdapter;
import com.muki.parttimejob.widget.NineGridView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/9/23.
 */
public class DynamicAdapter extends BaseAdapter<DynamicAdapter.ViewHolder> {
    NineGridViewAdapter pictureAdapter;
    View.OnClickListener  onClickListener;
    public  DynamicAdapter(Context context, List<AVObject> listDatas, View.OnClickListener  onClickListener) {
        super(context,listDatas);
        this.onClickListener=onClickListener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_dynamic_item, parent, false));
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        Dynamic dynamic= (Dynamic) listDatas.get(position);
        final LeanchatUser  leanchatUser=dynamic.getDynamicUsers();
        if(leanchatUser!=null){
            if(leanchatUser.getObjectId().equals(LeanchatUser.getCurrentUserId())){
                holder.dynDeleteLat.setVisibility(View.VISIBLE);
            }else {
                holder.dynDeleteLat.setVisibility(View.INVISIBLE);
            }
            holder.loveNumTv.setText(dynamic.getFollowCount()+"");
            holder.dynDeleteLat.setTag(position);
            holder.dynDeleteLat.setOnClickListener(onClickListener);
            holder.dynShareLat.setTag(position);
            holder.dynShareLat.setOnClickListener(onClickListener);
            if(dynamic.getDynJurisdiction()){
                holder.dyn_nickNameTv.setVisibility(View.GONE);
                holder.dyn_userAvatarIv.setImageResource(R.drawable.nig);
                holder.dyn_userAvatarIv.setClickable(false);
            }else{
                holder.dyn_userAvatarIv.setClickable(true);
                holder.dyn_nickNameTv.setVisibility(View.VISIBLE);
                holder.dyn_userAvatarIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!leanchatUser.getObjectId().equals(LeanchatUser.getCurrentUserId())){
                            Intent  intent=new Intent(context, ContactPersonInfoActivity.class);
                            intent.putExtra(Constants.LEANCHAT_USER_ID, leanchatUser.getObjectId());
                            context.startActivity(intent);
                        }else{
                            Toast.makeText(context,"不能查看自己",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.dyn_nickNameTv.setText(TextUtils.isEmpty(leanchatUser.getUSER_NICKNAME())?leanchatUser.getUSERNAME():leanchatUser.getUSER_NICKNAME());
                UpLoadImageUtil.loadCropImage(context,leanchatUser.getAvatarUrl(),holder.dyn_userAvatarIv);
            }
            holder.dynLoveLat.setOnClickListener(onClickListener);
            holder.find_dynComtLat.setTag(position);
            holder.find_dynComtLat.setOnClickListener(onClickListener);
            getLovePoint(dynamic,LeanchatUser.getCurrentUser(),holder.dynLoveIv,holder.dynLoveLat,position);
            SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
            SimpleDateFormat formatter = new    SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date curDatecurrenttime =  new  Date(System.currentTimeMillis());//获取当前时间
            String    strtime= null;
            try {
                strtime = formatter.format(sf.parse(dynamic.getCreatedAt()+""));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            AVObject todoFolder = AVObject.createWithoutData("Dynamic", dynamic.getObjectId());
            AVRelation<AVObject> commectRelation = todoFolder.getRelation("dynamic_Comment_relation");
            AVQuery<AVObject> commectQuery = commectRelation.getQuery();
            commectQuery.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if(e==null){
                        holder.commectNumTv.setText(list.size()+"");
                    }
                }
            });
            String    strcurrenttime= formatter.format(curDatecurrenttime);
            if(!TextUtils.isEmpty(dynamic.getDynamicContent())){
                holder.dyn_contentTv.setVisibility(View.VISIBLE);
                holder.dyn_contentTv.setText(dynamic.getDynamicContent());
            }else{
                holder.dyn_contentTv.setVisibility(View.GONE);
            }
            holder.dyn_ReleTimeTv.setText(TimeTool.getTime(strtime,strcurrenttime));
            AVRelation<AVObject> relation = dynamic.getRelation("dynamic_Image_relation");
            AVQuery<AVObject> query = relation.getQuery();
            query.include("dynamic_Image");
            query.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(final List<AVObject> list, AVException e) {
                    if (list!= null) {
                        ArrayList<ImageInfo>   imageInfos=new ArrayList<ImageInfo>();
                        for (AVObject  avObject:list){
                            ImageInfo   image=new ImageInfo();
                            image.setThumbnailUrl(avObject.getAVFile("dynamic_Thumbnail_Image").getUrl());
                            image.setBigImageUrl(avObject.getAVFile("dynamic_Image").getUrl());
                            imageInfos.add(image);
                        }
                        holder.rv_grid.setAdapter(new NineGridViewClickAdapter(context, imageInfos));
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return listDatas==null?0:listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView  dyn_nickNameTv;
        TextView  dyn_equipmentTv;
        TextView  dyn_contentTv;
        TextView  dyn_ReleTimeTv;
        ImageView   dyn_userAvatarIv;
        ImageView   dynLoveIv;
        RelativeLayout dynLoveLat;
        RelativeLayout  find_dynComtLat;
        NineGridView   rv_grid;
        RelativeLayout  dynDeleteLat;
        RelativeLayout   dynShareLat;
        TextView   loveNumTv;
        TextView   commectNumTv;
        public ViewHolder(View view) {
            super(view);
            commectNumTv= (TextView) view.findViewById(R.id.commectNumTv);
            loveNumTv= (TextView) view.findViewById(R.id.loveNum);
            dynShareLat= (RelativeLayout) view.findViewById(R.id.dynShareLat);
            dynDeleteLat= (RelativeLayout) view.findViewById(R.id.dynDeleteLat);
            find_dynComtLat= (RelativeLayout) view.findViewById(R.id.find_dynComtLat);
            dynLoveLat= (RelativeLayout) view.findViewById(R.id.dynLoveLat);
            dynLoveIv= (ImageView) view.findViewById(R.id.dynLoveIv);
            dyn_contentTv= (TextView) view.findViewById(R.id.dyn_contentTv);
            dyn_equipmentTv= (TextView) view.findViewById(R.id.dyn_equipmentTv);
            dyn_nickNameTv= (TextView) view.findViewById(R.id.dyn_nickNameTv);
            dyn_ReleTimeTv= (TextView) view.findViewById(R.id.dyn_ReleTimeTv);
            dyn_userAvatarIv= (ImageView) view.findViewById(R.id.dyn_userAvatarIv);
            rv_grid= (NineGridView) view.findViewById(R.id.rv_grid);
        }
    }
     public   void   changeAdapterData(List<AVObject> listDatas){
         this.listDatas=listDatas;
         notifyDataSetChanged();
     }
    public   void    getLovePoint(final Dynamic dynamic, LeanchatUser  leanchatUser, final ImageView imageView, final  RelativeLayout relativeLayout, final int  position){
        AVQuery<Dynamic_Follow>    dynamic_followAVQuery=AVQuery.getQuery(Dynamic_Follow.class);
        dynamic_followAVQuery.whereEqualTo("dynamic_pointer",dynamic);
        dynamic_followAVQuery.whereEqualTo("user_pointer",leanchatUser);
        dynamic_followAVQuery.getFirstInBackground(new GetCallback<Dynamic_Follow>() {
            @Override
            public void done(Dynamic_Follow dynamic_follow, AVException e) {
                if (dynamic_follow!=null){
                         relativeLayout.setTag(dynamic_follow.getState()+""+position);
                     if (dynamic_follow.getState()){
                         imageView.setImageResource(R.drawable.yidianzan);
                     }else{
                         imageView.setImageResource(R.drawable.zambia);
                     }
                }else{
                    relativeLayout.setTag(null+""+position);
                }
            }
        });
    }
}