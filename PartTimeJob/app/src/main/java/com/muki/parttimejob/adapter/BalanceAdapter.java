package com.muki.parttimejob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.BudgetHistory;
import com.muki.parttimejob.model.BudgetWithdrawApply;
import com.muki.parttimejob.widget.BaseAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/31.
 */

public class BalanceAdapter   extends BaseAdapter<BalanceAdapter.ViewHolder> {
    public BalanceAdapter(Context context, List<AVObject> listDatas) {
        super(context,listDatas);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_balance_item, parent, false));
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        BudgetHistory budgetHistory= (BudgetHistory) listDatas.get(position);
        BudgetWithdrawApply  budgetWithdrawApply=budgetHistory.getBudgetWithdrawApply();
        SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd hh:mm");
        try {
            holder.tranDateTv.setText(sdf.format(sf.parse(budgetHistory.getUpdatedAt()+"")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
         if (budgetHistory.getBudgetDescription().equals("提现")){
             holder.tranMoneyTv.setText("-"+budgetHistory.getBudgetMoney()+"");
         }else{
             holder.tranMoneyTv.setText("+"+budgetHistory.getBudgetMoney()+"");
         }
        if (budgetWithdrawApply!=null&&budgetWithdrawApply.getState()==0){
            holder.tranStateTv.setText("申请中");
        }else if((budgetWithdrawApply!=null&&budgetWithdrawApply.getState()==1)){
            holder.tranStateTv.setText("已完成");
        }
        holder.tranBalanceTv.setText("余额:"+budgetHistory.getCurrentBalance()+"");
        holder.tranExplainTv.setText(budgetHistory.getBudgetDescription()+"");
    }
    @Override
    public int getItemCount() {
        return  listDatas.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tranExplainTv)
        TextView  tranExplainTv;
        @Bind(R.id.tranStateTv)
        TextView  tranStateTv;
        @Bind(R.id.tranDateTv)
        TextView   tranDateTv;
        @Bind(R.id.tranMoneyTv)
        TextView  tranMoneyTv;
        @Bind(R.id.tranBalanceTv)
        TextView  tranBalanceTv;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
    public   void   changeAdapterData(List<AVObject> listDatas){
        this.listDatas=listDatas;
        notifyDataSetChanged();
    }
}
