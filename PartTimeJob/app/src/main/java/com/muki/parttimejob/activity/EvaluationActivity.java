package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/3.
 */

public class EvaluationActivity  extends AppCompatActivity {
    @Bind(R.id.selfComtBackLat)
    LinearLayout  selfComtBackLat;
    @Bind(R.id.selfComtLat)
    RelativeLayout  selfComtLat;
    @Bind(R.id.selfComtEt)
    EditText  selfComtEt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_evaluation);
        ButterKnife.bind(this);
        selfComtEt.addTextChangedListener(new  EditChangedListener());
        selfComtBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        selfComtLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String  content=selfComtEt.getText().toString().trim();
              if(TextUtils.isEmpty(content)){
                   Toast.makeText(EvaluationActivity.this,"请输入内容",Toast.LENGTH_SHORT).show();
                   return;
              }
                LeanchatUser.getCurrentUser().setUSER_EVALUATION(content);
                LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e==null){
                            finish();
                        }
                    }
                });
            }
        });
    }
    class EditChangedListener implements TextWatcher {
        private CharSequence temp;//监听前的文本
        private int editStart;//光标开始位置
        private int editEnd;//光标结束位置
        private final int charMaxNum = 80;
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            temp = s;
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            
        }
        @Override
        public void afterTextChanged(Editable s) {
            /** 得到光标开始和结束位置 ,超过最大数后记录刚超出的数字索引进行控制 */
            editStart = selfComtEt.getSelectionStart();
            editEnd = selfComtEt.getSelectionEnd();
            if (temp.length() > charMaxNum) {
                Toast.makeText(EvaluationActivity.this, "你输入的字数已经超过了限制", Toast.LENGTH_LONG).show();
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                selfComtEt.setText(s);
                selfComtEt.setSelection(tempSelection);
            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!TextUtils.isEmpty(LeanchatUser.getCurrentUser().getUSER_EVALUATION())){
            selfComtEt.setText(LeanchatUser.getCurrentUser().getUSER_EVALUATION());
        }
    }
}
