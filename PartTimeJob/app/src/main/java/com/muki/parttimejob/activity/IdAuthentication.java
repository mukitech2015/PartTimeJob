package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.bumptech.glide.Glide;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Image_Info;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;

import java.io.FileNotFoundException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by yuzhengang on 2016/11/28.
 */

public class IdAuthentication  extends AppCompatActivity   implements View.OnClickListener{
    @Bind(R.id.idAuthenBackLat)
    RelativeLayout   idAuthenBackLat;
    @Bind(R.id.idAuthenIdNumEt)
    EditText   idAuthenIdNumEt;
    @Bind(R.id.idAuthenNameEt)
    EditText   idAuthenNameEt;
    @Bind(R.id.userHoldCardIv)
    ImageView   userHoldCardIv;
    @Bind(R.id.userPostiveCardIv)
    ImageView   userPostiveCardIv;
    @Bind(R.id.userContraryCardIv)
    ImageView userContraryCardIv;
    @Bind(R.id.idAuthenSaveLat)
    RelativeLayout  idAuthenSaveLat;
    String   positiveStr;
    String   contayStr;
    String    positCardUrl;
    String   contaryCardUrl;
    int   width;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_id_authenication);
        ButterKnife.bind(this);
        idAuthenBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        userHoldCardIv.setOnClickListener(this);
        userContraryCardIv.setOnClickListener(this);
        userPostiveCardIv.setOnClickListener(this);
        idAuthenSaveLat.setOnClickListener(this);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels;
        initData();
    }
    @Override
    protected void onStart() {
        super.onStart();

    }
    public   void   checkAndSave(  ) {
       String  name=idAuthenNameEt.getText().toString();
       String  idCardNum=idAuthenIdNumEt.getText().toString();
       if(TextUtils.isEmpty(name)){
           Toast.makeText(this,"请填写姓名",Toast.LENGTH_SHORT).show();
           return;
       }
       if (TextUtils.isEmpty(idCardNum)){
            Toast.makeText(this,"请填写身份证信息",Toast.LENGTH_SHORT).show();
            return;
        }
        if(idCardNum.length()<15||idCardNum.length()>18){
            Toast.makeText(this,"身份证号格式有误",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(positiveStr)&&TextUtils.isEmpty(positCardUrl)){
            Toast.makeText(this,"请上传身份证正面",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(contayStr)&&TextUtils.isEmpty(contaryCardUrl)){
            Toast.makeText(this,"请上传身份证反面",Toast.LENGTH_SHORT).show();
            return;
        }
           LeanchatUser.getCurrentUser().setUSER_FAMILY_NAME(name);
           LeanchatUser.getCurrentUser().setUSER_IDCARD(idCardNum);
        if(!TextUtils.isEmpty(positiveStr)){
            final AVFile positCardFile;
            try {
                positCardFile = AVFile.withAbsoluteLocalPath("image.png",positiveStr);
                positCardFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if(e==null){
                            final AVObject positCardObj = new AVObject("User_Image_Info");
                            positCardObj.put("user_Image_Type", "身份证正面");
                            positCardObj.put("user_Image",positCardFile);
                            positCardObj.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> relation =LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");// 新建一个 AVRelation
                                    relation.add(positCardObj);
                                    LeanchatUser.getCurrentUser().saveInBackground();
                                }
                            });
                        }
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(!TextUtils.isEmpty(contayStr)){
            final AVFile contaryCardFile;
            try {
                contaryCardFile = AVFile.withAbsoluteLocalPath("image.png",contayStr);
                contaryCardFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if(e==null){
                            final AVObject contaryCardObj = new AVObject("User_Image_Info");
                            contaryCardObj.put("user_Image_Type", "身份证反面");
                            contaryCardObj.put("user_Image",contaryCardFile);
                            contaryCardObj.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> relation =LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");// 新建一个 AVRelation
                                    relation.add(contaryCardObj);
                                    LeanchatUser.getCurrentUser().saveInBackground();
                                }
                            });
                        }
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    LeanchatUser.getCurrentUser().setUSER_ID_ISAUTHENTCATION(2);
                    LeanchatUser.getCurrentUser().saveInBackground();
                    Toast.makeText(IdAuthentication.this,"认证信息已提交",Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(IdAuthentication.this,"认证信息已提交失败,请稍后再试",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
      int  whichPrcture;
    @Override
    public void onClick(View view) {
      switch (view.getId()){
          case R.id.userHoldCardIv:
          whichPrcture=1;
          setMutiSelectPicture();
          break;
          case R.id.userPostiveCardIv:
           whichPrcture=2;
           setMutiSelectPicture();
          break;
          case R.id.userContraryCardIv:
           whichPrcture=3;
           setMutiSelectPicture();
          break;
          case R.id.idAuthenSaveLat:
              checkAndSave();
              break;

        }
    }
    public void setMutiSelectPicture() {
        if (whichPrcture == 1) {

        } else if (whichPrcture == 2) {
            AndroidImagePicker.getInstance().pickSingle(IdAuthentication.this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
                @Override
                public void onImagePickComplete(List<ImageItem> items) {
                    if (items != null && items.size() > 0) {
                        positiveStr= items.get(0).path;
                        Glide.with(IdAuthentication.this).load(positiveStr).into(userPostiveCardIv);
                    }
                }
            });
        } else if (whichPrcture == 3) {
            AndroidImagePicker.getInstance().pickSingle(IdAuthentication.this, true, new AndroidImagePicker.OnImagePickCompleteListener() {
                @Override
                public void onImagePickComplete(List<ImageItem> items) {
                    if (items != null && items.size() > 0) {
                        contayStr = items.get(0).path;
                        Glide.with(IdAuthentication.this).load(contayStr).into(userContraryCardIv);
                    }
                }
            });
        }
    }

    public   void   initData(  ){
        idAuthenNameEt.setText(LeanchatUser.getCurrentUser().getUSER_FAMILY_NAME());
        idAuthenIdNumEt.setText(LeanchatUser.getCurrentUser().getUSER_IDCARD());
        AVRelation<User_Image_Info> user_image_infoAVRelation = LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
        AVQuery<User_Image_Info> query = user_image_infoAVRelation.getQuery();
        query.include("user_Image");
        query.findInBackground(new FindCallback<User_Image_Info>() {
            @Override
            public void done(List<User_Image_Info> list, AVException e) {
                if (e == null) {
                    for (User_Image_Info user_image_info : list) {
                        if (user_image_info.getUser_Image_Type().equals("身份证正面")) {
                            scaleImg(userPostiveCardIv);
                            positCardUrl = user_image_info.getUsreImageUrl();
                            UpLoadImageUtil.loadImage(IdAuthentication.this, user_image_info.getUsreImageUrl(),userPostiveCardIv);
                        }
                        if (user_image_info.getUser_Image_Type().equals("身份证反面")) {
                            scaleImg(userContraryCardIv);
                            contaryCardUrl = user_image_info.getUsreImageUrl();
                            UpLoadImageUtil.loadImage(IdAuthentication.this, user_image_info.getUsreImageUrl(), userContraryCardIv);
                        }
                    }
                }
            }
        });
    }
    public   void   scaleImg(ImageView  imageView ){
        int screenWidth = width;
        ViewGroup.LayoutParams lp =imageView.getLayoutParams();
        lp.width = screenWidth;
        lp.height =screenWidth/2;
        imageView.setLayoutParams(lp);
    }

}










