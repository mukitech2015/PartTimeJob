package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.ScrollLvAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.entity.ImageInfo;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.muki.parttimejob.widget.FullyLinearLayoutManager;
import com.muki.parttimejob.widget.QuantityView;
import com.pizidea.imagepicker.AndroidImagePicker;
import com.pizidea.imagepicker.bean.ImageItem;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/27.
 */

public class PaymentActivity  extends AppCompatActivity {
    @Bind(R.id.jobPayOrderTv)
    TextView jobPayOrderTv;
    @Bind(R.id.jobPayBackLat)
    LinearLayout  jobPayBackLat;
    @Bind(R.id.jobPayCompNameTv)
    TextView  jobPayCompNameTv;
    @Bind(R.id.jobPayIndustryTv)
    TextView  jobPayIndustryTv;
    @Bind(R.id.jobPayTitleTv)
    TextView  jobPayTitleTv;
    @Bind(R.id.jobPayLogoIv)
    ImageView  jobPayLogoIv;
    @Bind(R.id.jobPayWorkTimeTv)
    TextView    jobPayWorkTimeTv;
    @Bind(R.id.quantityView)
    QuantityView  quantityView;
    @Bind(R.id.msgContentEt)
    EditText   msgContentEt;
    @Bind(R.id.jobPayUnitTv)
    TextView   jobPayUnitTv;
    @Bind(R.id.jobPayDwTv)
    TextView  jobPayDwTv;
    @Bind(R.id.jobPayAllTv)
    TextView  jobPayAllTv;
    @Bind(R.id.jobPayMsgNumTv)
    TextView  jobPayMsgNumTv;
    @Bind(R.id.submitPayLat)
    RelativeLayout  submitPayLat;
    @Bind(R.id.alreadyPayLat)
    RelativeLayout  alreadyPayLat;
    @Bind(R.id.paymentRv)
    RecyclerView   paymentRv;
    User_Job    user_job;
    Job   job;
    SimpleDateFormat  formatter=new SimpleDateFormat("yyyyMMddhh");
    Date curDate=new  Date(System.currentTimeMillis());
    long rnd;
    Random random = new Random();
    private  static   int  quantity;
    private ScrollLvAdapter scrollLvAdapter;
    List<ImageInfo>   imageObjList=new ArrayList<>();
    private  List<String>  paymentPicList=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment);
        ButterKnife.bind(this);
        FullyLinearLayoutManager linearLayoutManager=new FullyLinearLayoutManager(this);
        paymentRv.setNestedScrollingEnabled(false);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        paymentRv.setLayoutManager(linearLayoutManager);
        setScrollLvAdapter(imageObjList);
        quantityView.setOnQuantityChangeListener(new QuantityView.OnQuantityChangeListener() {
            @Override
            public void onQuantityChanged(int newQuantity, boolean programmatically) {
                  quantity=newQuantity;
                  double  unitPrice=Double.parseDouble(jobPayUnitTv.getText().toString().trim());
                  jobPayAllTv.setText((newQuantity*unitPrice)+"");
            }
            @Override
            public void onLimitReached() {

            }
        });
        jobPayBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        alreadyPayLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(PaymentActivity.this).setIcon(R.drawable.ic_launcher).setMessage("请确认商家是否支付薪水")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        user_job.setAction_State(AppConfig.ACTION_WAIT_MER_PAY);
                        user_job.setState(AppConfig.WAIT_SETTLE);
                        user_job.put("job_Order",jobPayOrderTv.getText().toString().trim());
                        user_job.saveInBackground();
                        Intent intentComt=new Intent(PaymentActivity.this, CommectActivity.class);
                        intentComt.putExtra("job", (Serializable)job);
                        intentComt.putExtra("user_job", (Serializable) user_job);
                        startActivity(intentComt);
                        finish();
                    }
                }).show();
            }
        });
        submitPayLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String  jobPayAll=jobPayAllTv.getText().toString();
               new AlertDialog.Builder(PaymentActivity.this).setIcon(R.drawable.ic_launcher).setMessage("请确认金额为"+jobPayAll)
               .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {

                   }
               }).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {
                       submitPayOrder();
                   }
               }).show();
            }
        });
    }
    public   void   submitPayOrder(  ){
        Map<String,String>  map=new HashMap<String, String>();
        map.put("name",LeanchatUser.getCurrentUser().getUSERNAME());
        map.put("content",msgContentEt.getText().toString().trim());
        map.put("time",formatter.format(curDate));
        ArrayList<Object> arrayList = new ArrayList<>();
        arrayList.add(map);
        final AVObject   jobOrderObject=new AVObject("Job_Order");
        jobOrderObject.put("amount",quantity);
        jobOrderObject.put("money",Double.parseDouble(jobPayAllTv.getText().toString().trim()));
        jobOrderObject.put("order_id",jobPayOrderTv.getText().toString().trim());
        jobOrderObject.put("state","1");
        jobOrderObject.put("user_Job_pointer",user_job);
        jobOrderObject.put("message",arrayList);
        if(paymentPicList.size()>0){
            try {
                savePaymentPics(paymentPicList,jobOrderObject);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        jobOrderObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e==null){
                    Toast.makeText(PaymentActivity.this, "发起结款完成，等待商家付款", Toast.LENGTH_LONG).show();
                    user_job.setAction_State(AppConfig.ACTION_WAIT_MER_PAY);
                    user_job.setState(AppConfig.WAIT_SETTLE);
                    user_job.put("job_Order",jobPayOrderTv.getText().toString().trim());
                    user_job.saveInBackground();
                    finish();
                }else{

                }
            }
        });
    }
    public    void   setScrollLvAdapter(final List<ImageInfo> imageInfoList){
        if(scrollLvAdapter==null){
            scrollLvAdapter=new ScrollLvAdapter(imageInfoList,PaymentActivity.this);
            paymentRv.setAdapter(scrollLvAdapter);
        }
        scrollLvAdapter.changeAdapter(imageInfoList);
        scrollLvAdapter.setOnItemLongClickListener(new ScrollLvAdapter.OnRecycleViewItemLongClickListener() {
            @Override
            public void OnItemLongClick(View view, int position) {
                if (position!=imageInfoList.size()) {
                    deletePayPic(position);
                }
            }
        });
        scrollLvAdapter.setOnItemClickListener(new ScrollLvAdapter.OnRecycleViewItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                if (position==imageInfoList.size()) {
                    AndroidImagePicker.getInstance().pickMulti(PaymentActivity.this, true,  new AndroidImagePicker.OnImagePickCompleteListener() {
                        @Override
                        public void onImagePickComplete(List<ImageItem> items) {
                            if(items != null && items.size() > 0){
                                    paymentPicList.clear();
                                    imageObjList.clear();
                                for (ImageItem  imageItem:items){
                                    ImageInfo  imageInfo=new ImageInfo();
                                    imageInfo.setBigImageUrl(imageItem.path);
                                    imageInfo.setThumbnailUrl(imageItem.path);
                                    paymentPicList.add(imageItem.path);
                                    imageObjList.add(imageInfo);
                                    scrollLvAdapter.changeAdapter(imageObjList);
                                }
                            }
                        }
                    });
                }
            }
        });
    }
    public   void   savePaymentPics(List<String>  stringList, final AVObject   jobOrderObject) throws FileNotFoundException {
        for (String  string:stringList){
            final AVFile avFile=AVFile.withAbsoluteLocalPath("image.png",string);
            try {
                AVObject.saveFileBeforeSave(Arrays.asList(avFile), false, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (e==null){
                            final AVObject imageObj=new AVObject("Job_Order_Image");
                            imageObj.put("Image",avFile);
                            AVObject.saveAllInBackground(Arrays.asList(imageObj), new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    AVRelation<AVObject> jobOrderRelation= jobOrderObject.getRelation("job_Order_Image_relation");
                                    jobOrderRelation.add(imageObj);
                                    jobOrderObject.saveInBackground();
                                }
                            });
                        }
                    }
                });
            } catch (AVException e) {
                e.printStackTrace();
            }
        }
    }
    public   void   deletePayPic(final int  position){
        new  AlertDialog.Builder(this).setIcon(R.drawable.ic_launcher)
                .setTitle("删除照片")
                .setMessage("确定删除该照片")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        imageObjList.remove(position);
                        paymentPicList.remove(position);
                        setScrollLvAdapter(imageObjList);
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent   intent=getIntent();
        user_job= (User_Job) intent.getSerializableExtra("user_job");
        job= (Job) intent.getSerializableExtra("job");
        getDatas(user_job);
        msgContentEt.addTextChangedListener(new  EditChangedListener());
    }
    public   void    getDatas(User_Job  user_job){
        String   timeStr=formatter.format(curDate);
        rnd = random.nextInt(9999)%(9999-1000+1)+1000;
        jobPayOrderTv.setText(timeStr+rnd);
        Job job=user_job.getJob_Pointer();
        jobPayTitleTv.setText(job.getJob_RecruitName());
         Business business=user_job.getBusiness_pointer();
         jobPayCompNameTv.setText(business.getBusiness_Name());
          jobPayIndustryTv.setText(job.getJob_Industy());
         jobPayWorkTimeTv.setText(job.getJob_WorkDate());
         UpLoadImageUtil.loadFilletImage(this,business.getComLogoUrl(),jobPayLogoIv);
         jobPayUnitTv.setText(job.getJob_Price()+"");
         jobPayDwTv.setText(job.getJob_Unit());
    }
    class EditChangedListener implements TextWatcher {
        private CharSequence temp;//监听前的文本
        private int editStart;//光标开始位置
        private int editEnd;//光标结束位置
        private final int charMaxNum = 100;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            temp = s;
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            jobPayMsgNumTv.setText(s.length()+"/100");
        }
        @Override
        public void afterTextChanged(Editable s) {
            /** 得到光标开始和结束位置 ,超过最大数后记录刚超出的数字索引进行控制 */
            editStart = msgContentEt.getSelectionStart();
            editEnd =  msgContentEt.getSelectionEnd();
            if (temp.length() > charMaxNum) {
                Toast.makeText(PaymentActivity.this, "你输入的字数已经超过了限制", Toast.LENGTH_LONG).show();
                s.delete(editStart - 1, editEnd);
                int tempSelection = editStart;
                msgContentEt.setText(s);
                msgContentEt.setSelection(tempSelection);
            }
        }
    }

}
