package com.muki.parttimejob.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVRelation;
import com.avos.avoscloud.SaveCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Image_Info;
import com.muki.parttimejob.widget.CornersTransform;

/**
 * Created by muki on 2016/9/18.
 */
public class UpLoadImageUtil {
    public  static  void   loadCropImage(final Context context, String imageUrl, final ImageView  imageView){
        if(imageUrl==null){
            return;
        }
        Glide.with(context).load(imageUrl).asBitmap().centerCrop().placeholder(R.drawable.ic_launcher).error(R.drawable.ic_launcher).into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        }); //设置占位图
    }
    public  static  void  loadImage(final Context context, String imageUrl, final ImageView  imageView){
        Glide.with(context).load(imageUrl).placeholder(R.drawable.zwt).into(imageView);
    }
    public  static  void   loadFilletImage(final Context context, String imageUrl, final ImageView  imageView ){
        Glide.with(context).load(imageUrl).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CornersTransform(context))
                .placeholder(R.drawable.zwt)
                .error(R.drawable.zwt)
                .crossFade()
                .into(imageView);
    }
  public static  void  upLoadImage( String  imagePath,String  imageType){
          final User_Image_Info user_image_info=new User_Image_Info();
          user_image_info.setUser_Image_Type(imageType);
          user_image_info.saveUserImage(imagePath, new SaveCallback() {
              @Override
              public void done(AVException e) {
                  user_image_info.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(AVException e) {
                          if(e==null){
                              AVRelation<AVObject> user_image_infoAVRelation= LeanchatUser.getCurrentUser().getRelation("user_Image_Info_relation");
                              user_image_infoAVRelation.add(user_image_info);
                              LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                  @Override
                                  public void done(AVException e) {
                                      if(e!=null){

                                      }
                                  }
                              });
                          }
                      }
                  });
              }
          });
      }
}
