package com.muki.parttimejob.entity;

import org.litepal.crud.DataSupport;

/**
 * Created by muki on 2016/12/2.
 */

public class MessageInfo  extends DataSupport {
    private  String   avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private  String   name;
    private  String   nickname;
    public  MessageInfo(String   avatar, String   name,String   nickname){
        super();
        this.avatar=avatar;
        this.name=name;
        this.nickname=nickname;
    }
}
