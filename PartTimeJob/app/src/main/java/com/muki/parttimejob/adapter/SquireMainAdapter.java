package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.JobDetailActivity;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.JobData;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

/**
 * Created by sunfusheng on 16/4/20.
 */
public class SquireMainAdapter extends BaseListAdapter<JobData> {
    private boolean isNoData;
    private int mHeight;
    public static final int ONE_SCREEN_COUNT = 8; // 一屏能显示的个数，这个根据屏幕高度和各自的需求定
    public static final int ONE_REQUEST_COUNT = 10; // 一次请求的个数
    Context  context;
    public SquireMainAdapter(Context context) {
        super(context);
        this.context=context;
    }
    public SquireMainAdapter(Context context, List<JobData> list) {
        super(context, list);
        this.context=context;
    }
    // 设置数据
    public void setData(List<JobData> list) {
        clearAll();
        addALL(list);
        isNoData = false;
        if (list.size() == 1 && list.get(0).isNoData()) {
            // 暂无数据布局
            isNoData = list.get(0).isNoData();
             mHeight = list.get(0).getHeight();
        } else {
            // 添加空数据
            if (list.size() < ONE_SCREEN_COUNT) {
                addALL(createEmptyList(ONE_SCREEN_COUNT - list.size()));
            }
        }
        notifyDataSetChanged();
    }
    // 创建不满一屏的空数据
    public List<JobData> createEmptyList(int size) {
        List<JobData> emptyList = new ArrayList<>();
        if (size <= 0) return emptyList;
        for (int i=0; i<size; i++) {
            emptyList.add(new JobData());
        }
        return emptyList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // 暂无数据
        if (isNoData) {
            convertView = mInflater.inflate(R.layout.item_no_data_layout, null);
            AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mHeight);
            RelativeLayout rootView = ButterKnife.findById(convertView, R.id.rl_root_view);
            rootView.setLayoutParams(params);
            return convertView;
        }else{
            // 正常数据
            ViewHolder holder = null;
            if( convertView == null || convertView.findViewById(R.id.recrutment_title_tv) == null) {
                convertView = mInflater.inflate(R.layout.layout_item_squire, null);
            }
            if (convertView != null) {
                holder = (ViewHolder) convertView.getTag();
            }
            if (holder == null) {
                holder = new ViewHolder(  );
                holder.vipIv= (ImageView)convertView.findViewById(R.id.vipIv);
                holder.baozhangIv= (ImageView)convertView.findViewById(R.id.baozhangIv);
                holder.distanceTv= (TextView) convertView.findViewById(R.id.distanceTv);
                holder.companyLogoIv = (ImageView)convertView.findViewById(R.id.company_logo_lv);
                holder.dateTv = (TextView) convertView.findViewById(R.id.date_tv);
                holder.locationTv = (TextView)convertView.findViewById(R.id.location_tv);
                holder.jobTitleTv = (TextView) convertView.findViewById(R.id.recrutment_title_tv);
                holder.priceTv = (TextView) convertView.findViewById(R.id.price_tv);
                holder.companyNameTv= (TextView)convertView.findViewById(R.id.companyNameTv);
                holder.busStyleIv= (ImageView) convertView.findViewById(R.id.busStyleIv);
                holder.extensionIv= (ImageView)convertView.findViewById(R.id.extensionIv);
                holder.llRootView= (LinearLayout) convertView.findViewById(R.id.ll_root_view);
                convertView.setTag(holder);
            }
            DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            final JobData  jobData=getItem(position);
            if (TextUtils.isEmpty(jobData.getBusinessName())) {
                holder.llRootView.setVisibility(View.INVISIBLE);
                return convertView;
            }
            if (jobData!=null){
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(context,JobDetailActivity.class);
                        intent.putExtra("job",(Serializable)jobData.getJob());
                        context.startActivity(intent);
                    }
                });
                if (!TextUtils.isEmpty(jobData.getJobName())){
                    holder.jobTitleTv.setText(jobData.getJobName());
                }else{
                    holder.jobTitleTv.setText("");
                }

                double  latitude =jobData.getLatitude();
                double  longitude=jobData.getLongitude();
                AVGeoPoint point = new AVGeoPoint(latitude,longitude);
                double distance=point.distanceInKilometersTo(new AVGeoPoint(AppConfig.mylatitude,AppConfig.mylongitude));
                holder.distanceTv.setText(decimalFormat.format(Double.valueOf(distance))+"千米");
                holder.locationTv.setText(jobData.getAddress());
                holder.llRootView.setVisibility(View.VISIBLE);

                if (jobData.isBusinessStyle()){
                    holder.busStyleIv.setImageResource(R.drawable.qiye);
                }else{
                    holder.busStyleIv.setImageResource(R.drawable.geren);
                }
                if(jobData.isSalGuee()){
                    holder.baozhangIv.setVisibility(View.VISIBLE);
                }else {
                    holder.baozhangIv.setVisibility(View.INVISIBLE);
                }
                if(jobData.isVip()){
                    holder.vipIv.setVisibility(View.VISIBLE);
                }else {
                    holder.vipIv.setVisibility(View.INVISIBLE);
                }
                if(jobData.getHot()!=0){
                    holder.extensionIv.setVisibility(View.VISIBLE);
                }else{
                    holder.extensionIv.setVisibility(View.GONE);
                }
                holder.companyNameTv.setText(jobData.getBusinessName());
                UpLoadImageUtil.loadCropImage(context,jobData.getBusinessLogo(),holder.companyLogoIv);
                SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");

                try {
                    if( jobData.getStartTime() != null) {
                        holder.dateTv.setText(sdf.format(sf.parse(jobData.getStartTime() + "")) + "至" + sdf.format(sf.parse(jobData.getEndTime() + "")));
                    }else{
                        holder.dateTv.setText("");
                    }

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
               }
            }
        return convertView;
    }
    static class ViewHolder {
        TextView  distanceTv;
        ImageView  baozhangIv;
        ImageView   vipIv;
        TextView   jobTitleTv;
        TextView   dateTv;
        TextView   priceTv;
        TextView  locationTv;
        ImageView  companyLogoIv;
        TextView   companyNameTv;
        ImageView   busStyleIv;
        ImageView  extensionIv;
        LinearLayout llRootView;
    }
}
