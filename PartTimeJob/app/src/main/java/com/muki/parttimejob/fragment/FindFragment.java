package com.muki.parttimejob.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.muki.parttimejob.R;
import com.muki.parttimejob.activity.DynCommentActivity;
import com.muki.parttimejob.activity.DynMsgActivity;
import com.muki.parttimejob.activity.IdAuthentication;
import com.muki.parttimejob.activity.MainActivity;
import com.muki.parttimejob.activity.ReleaseDynActivity;
import com.muki.parttimejob.adapter.DynamicAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.event.ReleDynEvent;
import com.muki.parttimejob.manager.FriendsManager;
import com.muki.parttimejob.model.Dynamic;
import com.muki.parttimejob.model.Dynamic_Follow;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.utils.ButtonUtils;
import com.muki.parttimejob.widget.NineGridView;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;
import com.muki.parttimejob.widget.SegmentControl;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by muki on 2016/9/13.
 */
public class FindFragment extends Fragment implements View.OnClickListener,PullBaseView.OnHeaderRefreshListener, PullBaseView.OnFooterRefreshListener{
    private  View   view;
    DynamicAdapter dynamicAdapter;
    MainActivity mainActivity;
    PullRecyclerView dnyRv;
    private static   int  dynType=AppConfig.FRIEND_LAT;
    public ArrayList<SnsPlatform> platforms = new ArrayList<SnsPlatform>();
    List<AVObject>  objectListCopy=new ArrayList<AVObject>();
    int page=0;
    private RelativeLayout  releDynLat;
    private LinearLayout  noDynLat;
    RelativeLayout  newComtLat;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.layout_fg_find,null);
        mainActivity= (MainActivity) getActivity();
        NineGridView.setImageLoader(new GlideImageLoader());
        newComtLat= (RelativeLayout) view.findViewById(R.id.newComtLat);
        newComtLat.setOnClickListener(this);
        noDynLat= (LinearLayout) view.findViewById(R.id.noDynLat);
        dnyRv= (PullRecyclerView) view.findViewById(R.id.findDnyRv);
        releDynLat= (RelativeLayout) view.findViewById(R.id.releDynLat);
        dnyRv.setOnHeaderRefreshListener(this);
        dnyRv.setOnFooterRefreshListener(this);
        releDynLat.setOnClickListener(this);
        dnyRv.setLayoutManager(new LinearLayoutManager(getActivity()),getActivity());
        SegmentControl mSegmentHorzontal = (SegmentControl) view.findViewById(R.id.segment_control);
        mSegmentHorzontal.setOnSegmentControlClickListener(new SegmentControl.OnSegmentControlClickListener() {
            @Override
            public void onSegmentControlClick(int index) {
                  if (index==1){
                      dynType =AppConfig.FRIEND_LAT;
                      getMembers(true);
                  }else if (index==0){
                      dynType=AppConfig.ANONYMOUS_LAT;
                      getDynDates(null,dynType);
                  }else if (index==2){
                      dynType=AppConfig.SQUARE_LAT;
                      getDynDates(null,dynType);
                  }
            }
        });
        EventBus.getDefault().register(this);
        initPlatforms();
        return  view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(dynType==AppConfig.FRIEND_LAT){
            getMembers(true);
        }else {
            getDynDates(null,dynType);
        }
        getDynNewComtDatas();
    }

    public void onEventMainThread(ReleDynEvent event) {
         Log.e("event.getMsg()",event.getMsg()+"");
         if(event.getMsg().equals("发送成功")){
             if(dynType==AppConfig.FRIEND_LAT){
                 getMembers(true);
             }else {
                 getDynDates(null,dynType);
             }
         }
    }
    private void getMembers(final boolean isforce) {
        FriendsManager.fetchFriends(isforce, new FindCallback<LeanchatUser>() {
            @Override
            public void done(List<LeanchatUser> list, AVException e) {
                if(e==null){
                    getDynDates(list,dynType);
                }else{
                    setDynAdapter(null);
                }
            }
        });
    }
    public  void  getDynDates(List<LeanchatUser>    frdlists,int  dynType){
        AVQuery<AVObject> dynamicAVQuery= new AVQuery<>("Dynamic");
        if(frdlists!=null){
            frdlists.add(LeanchatUser.getCurrentUser());
            dynamicAVQuery.whereContainedIn("dynamic_Users_pointer",frdlists);
        }
        if(dynType==AppConfig.ANONYMOUS_LAT){
            dynamicAVQuery.whereEqualTo("dynamic_Jurisdiction",true);
            dynamicAVQuery.whereEqualTo("dynamic_OnlyFriendState",false);
        }else if(dynType==AppConfig.FRIEND_LAT){
            dynamicAVQuery.whereEqualTo("dynamic_Jurisdiction",false);
        }else if(dynType==AppConfig.SQUARE_LAT){
            dynamicAVQuery.whereEqualTo("dynamic_Jurisdiction",false);
            dynamicAVQuery.whereEqualTo("dynamic_OnlyFriendState",false);
        }
        dynamicAVQuery.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        dynamicAVQuery.setMaxCacheAge(24 * 3600); //设置缓存有效期
        dynamicAVQuery.orderByDescending("createdAt");
        dynamicAVQuery.skip(page*10);// 跳过 10 条结果
        dynamicAVQuery.limit(10);
        dynamicAVQuery.include("dynamic_Users_pointer");
        dynamicAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e==null){
                    if (page==0){
                        objectListCopy.clear();
                        objectListCopy.addAll(list);
                        setDynAdapter(objectListCopy);
                    }else{
                        objectListCopy.addAll(objectListCopy.size(),list);
                        setDynAdapter(objectListCopy);
                    }
                }
            }
        });
    }
    public  void   setDynAdapter(List<AVObject> list){
        if(dynamicAdapter==null){
            dynamicAdapter=new DynamicAdapter(getActivity(), list,this);
            dnyRv.setAdapter(dynamicAdapter);
        }else{
            dynamicAdapter.changeAdapterData(list);
        }
        if(dynamicAdapter.getItemCount()==0){
            noDynLat.setVisibility(View.VISIBLE);
            dnyRv.setVisibility(View.GONE);
        }else{
            noDynLat.setVisibility(View.GONE);
            dnyRv.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page++;
                if(dynType==AppConfig.FRIEND_LAT){
                    getMembers(true);
                }else {
                    getDynDates(null,dynType);
                }
                view.onFooterRefreshComplete();
            }
        }, 1000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                //execute the task
                page=0;
                if(dynType==AppConfig.FRIEND_LAT){
                    getMembers(true);
                }else{
                    getDynDates(null,dynType);
                }
                getDynNewComtDatas();
                view.onHeaderRefreshComplete();
            }
        }, 1000);
    }
    /** Glide 加载 */
    private class GlideImageLoader implements NineGridView.ImageLoader {
        @Override
        public void onDisplayImage(Context context, ImageView imageView, String url) {
            Glide.with(context).load(url)//
                    .placeholder(R.drawable.zwt)//
                    .error(R.drawable.zwt)//
                    .diskCacheStrategy(DiskCacheStrategy.ALL)//
                    .into(imageView);
        }

        @Override
        public Bitmap getCacheImage(String url) {
            return null;
        }
    }
    int  mIndex;
    android.app.AlertDialog.Builder builder;
    android.app.AlertDialog.Builder  builderIdIsauthentcation;
    public void   showToIsauthenDlg(String  msg ){
      if (builderIdIsauthentcation==null){
          builderIdIsauthentcation=new AlertDialog.Builder(getActivity());
      }
      builderIdIsauthentcation.setIcon(R.drawable.ic_launcher)
      .setTitle("提示")
      .setMessage("未通过实名认证不能"+msg+",现在就去进行实名认证吗？")
      .setNegativeButton("取消", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {

          }
      })
      .setPositiveButton("确定", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
              Intent  intentIdIsauthentcation=new Intent(getActivity(), IdAuthentication.class);
              startActivity(intentIdIsauthentcation);
          }
      }).show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.newComtLat:
                Intent  intentNewComt=new Intent(getActivity(), DynMsgActivity.class);
                startActivity(intentNewComt);
                break;
            case  R.id.releDynLat:
                if(LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION() ==1){
                    Intent  releDynIntent=new Intent(getActivity(),ReleaseDynActivity.class);
                    startActivity(releDynIntent);
                }else{
                  showToIsauthenDlg("发表动态");
               }
                break;
            case R.id.dynShareLat:
                mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                String  dynamic_content=objectListCopy.get(mIndex).getString("dynamic_content");
                if(TextUtils.isEmpty(dynamic_content)){
                   dynamic_content="啄米兼职";
                }
                String   dynamicId=objectListCopy.get(mIndex).getObjectId();
                new ShareAction(getActivity()).setDisplayList(SHARE_MEDIA.QQ,SHARE_MEDIA.QZONE,SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                        .withTitle("啄米兼职")
                        .withText(dynamic_content)
                        .withMedia(new UMImage(getActivity(),R.drawable.ic_launcher))
                        .withTargetUrl("http://www.cloudconfs.com/part/index.php/Share/Dynamic/view?objectId="+dynamicId)
                        .setCallback(umShareListener)
                        .open();
                break;
            case R.id.dynDeleteLat:
                mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                if(builder==null){
                    builder=new android.app.AlertDialog.Builder(getActivity());
                }
                builder.setTitle("删除");
                builder.setIcon(R.drawable.ic_launcher);
                builder.setMessage("确定要删除动态么？");
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        objectListCopy.get(mIndex).deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(AVException e) {
                                if (e==null){
                                    objectListCopy.remove(mIndex);
                                    setDynAdapter(objectListCopy);
                                }
                            }
                        });
                    }
                }).show();
                break;
            case R.id.find_dynComtLat:
                    mIndex=Integer.parseInt(String.valueOf(v.getTag()));
                    Dynamic  dynamic= (Dynamic) objectListCopy.get(mIndex);
                    Intent  dynComeIntent=new Intent(getActivity(), DynCommentActivity.class);
                    dynComeIntent.putExtra("dynamic", (Serializable)dynamic);
                    startActivity(dynComeIntent);
                break;
           case   R.id.dynLoveLat:
               if( !ButtonUtils.isFastDoubleClick(R.id.find_dynComtLat)){
                   String   viewTagStr= String.valueOf(v.getTag());
                   if(viewTagStr.contains("null")) {
                       Dynamic  dyn=(Dynamic) objectListCopy.get(Integer.parseInt(viewTagStr.replace("null","")));
                       createLovePoint(LeanchatUser.getCurrentUser(),dyn,dyn.getFollowCount());
                   }else if(viewTagStr.contains("false")){
                       setLovePoint(Integer.parseInt(viewTagStr.replace("false","")));
                   }else if(viewTagStr.contains("true")){
                       setLovePoint(Integer.parseInt(viewTagStr.replace("true","")));
                   }
               }
            break;
        }
    }
    public   void   setLovePoint(int  position){
      final Dynamic  dynamic= (Dynamic) objectListCopy.get(position);
      AVQuery<Dynamic_Follow>  dynamic_followAVQuery=AVQuery.getQuery(Dynamic_Follow.class);
      dynamic_followAVQuery.whereEqualTo("dynamic_pointer",dynamic);
      dynamic_followAVQuery.whereEqualTo("user_pointer",LeanchatUser.getCurrentUser());
      dynamic_followAVQuery.getFirstInBackground(new GetCallback<Dynamic_Follow>() {
          @Override
          public void done(final Dynamic_Follow dynamic_follow, AVException e) {
               if(dynamic_follow!=null){
                   if (dynamic_follow.getState()){
                       dynamic_follow.setState(false);
                       dynamic_follow.saveInBackground(new SaveCallback() {
                           @Override
                           public void done(AVException e) {
                               if (dynamic.getFollowCount()-1<0){
                                   dynamic.setFollowCount(0);
                               }else{
                                   dynamic.setFollowCount(dynamic.getFollowCount()-1);
                               }
                               dynamic.saveInBackground(new SaveCallback() {
                                   @Override
                                   public void done(AVException e) {
                                       if(e==null){
                                           dynamicAdapter.notifyDataSetChanged();
                                       }
                                   }
                               });
                           }
                       });
                   }else{
                       dynamic_follow.setState(true);
                       dynamic_follow.saveInBackground(new SaveCallback() {
                           @Override
                           public void done(AVException e) {
                               dynamic.setFollowCount(dynamic.getFollowCount()+1);
                               dynamic.saveInBackground(new SaveCallback() {
                                   @Override
                                   public void done(AVException e) {
                                       if(e==null){
                                           dynamicAdapter.notifyDataSetChanged();
                                       }
                                   }
                               });

                           }
                       });
                   }
               }
          }
      });
    }
    public     void  createLovePoint(LeanchatUser  leanchatUser, final Dynamic  dynamic, final int  followCount){
        final Dynamic_Follow  dynamic_follow=new Dynamic_Follow();
        dynamic_follow.setDynamic_pointer(dynamic);
        dynamic_follow.setUser_pointer(leanchatUser);
        dynamic_follow.setState(true);
        dynamic_follow.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    dynamic.setFollowCount(followCount+1);
                    dynamic.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if(e==null){
                                dynamicAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                }else{
                    dynamic_follow.setState(false);
                }
            }
        });
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void initPlatforms(){
        platforms.clear();
        for (SHARE_MEDIA e : SHARE_MEDIA.values()) {
            if (!e.toString().equals(SHARE_MEDIA.GENERIC.toString())){
                platforms.add(e.toSnsPlatform());
            }
        }
    }
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Log.d("plat","platform"+platform);

        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {

            if(t!=null){
                Log.d("throw","throw:"+t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {

        }
    };
    public   void   getDynNewComtDatas(){
        Log.e("==================","====");
        AVQuery<AVObject>   avObjectAVQuery=new AVQuery<>("Dynamic_Comment");
        avObjectAVQuery.whereEqualTo("isRead",false);
        avObjectAVQuery.whereEqualTo("dynamic_Reply_User_pointer", LeanchatUser.getCurrentUser());
        avObjectAVQuery.include("dynamic_Comment_User_pointer");
        avObjectAVQuery.include("dynamic_Comment");
        avObjectAVQuery.orderByDescending("createdAt");
        avObjectAVQuery.skip(page*10);// 跳过 10 条结果
        avObjectAVQuery.limit(10);
        avObjectAVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e==null){
                  Log.e("==================",list.size()+"====");
                  if (list.size()>0){
                      newComtLat.setVisibility(View.VISIBLE);
                  }else{
                      newComtLat.setVisibility(View.GONE);
                  }
                }
            }
        });
    }
}
