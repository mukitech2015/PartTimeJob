package com.muki.parttimejob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.event.GroupItemClickEvent;
import com.muki.parttimejob.utils.ConversationUtils;
import com.muki.parttimejob.viewholder.GroupItemHolder;

import java.util.List;

import butterknife.Bind;
import cn.leancloud.chatkit.adapter.LCIMCommonListAdapter;
import cn.leancloud.chatkit.cache.LCIMConversationItemCache;
import cn.leancloud.chatkit.event.LCIMConversationItemLongClickEvent;
import cn.leancloud.chatkit.utils.LCIMConstants;
import cn.leancloud.chatkit.view.LCIMDividerItemDecoration;
import de.greenrobot.event.EventBus;

/**
 * Created by lzw on 14-10-7.
 */
public class ConversationGroupListActivity extends AVBaseActivity {
  @Bind(R.id.activity_group_list_srl_view)
  protected RecyclerView recyclerView;
  LinearLayoutManager layoutManager;
  private LCIMCommonListAdapter<AVIMConversation> itemAdapter;
  private RelativeLayout   groupBackLat;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.group_list_activity);
    initView();
  }
  private void initView() {
    groupBackLat= (RelativeLayout) findViewById(R.id.groupBackLat);
    layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    this.recyclerView.addItemDecoration(new LCIMDividerItemDecoration(this));
    itemAdapter = new LCIMCommonListAdapter<>(GroupItemHolder.class);
    recyclerView.setAdapter(itemAdapter);
    groupBackLat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
           finish();
      }
    });
  }
  private void refreshGroupList() {
    ConversationUtils.findGroupConversationsIncludeMe(new AVIMConversationQueryCallback() {
      @Override
      public void done(List<AVIMConversation> conversations, AVIMException e) {
        if(filterException(e)) {
          itemAdapter.setDataList(conversations);
          itemAdapter.notifyDataSetChanged();
           }
        }
    });
  }
  public void onEvent(GroupItemClickEvent event) {
    Intent intent = new Intent(ConversationGroupListActivity.this, ChatRoomActivity.class);
    intent.putExtra(LCIMConstants.CONVERSATION_ID, event.conversationId);
    intent.putExtra("CHAT_TITLE", event.conversationName);
    startActivity(intent);
  }
  public void onEvent(LCIMConversationItemLongClickEvent event) {
    if(null != event.conversation) {
      String conversationId = event.conversation.getConversationId();
      LCIMConversationItemCache.getInstance().deleteConversation(conversationId);
      this.refreshGroupList();
    }
  }
  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
    refreshGroupList();
  }
}






