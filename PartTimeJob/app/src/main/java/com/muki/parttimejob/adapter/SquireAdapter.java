package com.muki.parttimejob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.utils.UpLoadImageUtil;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muki on 2016/10/4.
 */
public class SquireAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Job> jobList;
    DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
    public SquireAdapter(Context context, List<Job> jobList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.jobList = jobList;
    }
    @Override
    public int getCount() {
        return jobList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.layout_item_squire, null);
            holder.vipIv= (ImageView) convertView.findViewById(R.id.vipIv);
            holder.baozhangIv= (ImageView) convertView.findViewById(R.id.baozhangIv);
            holder.distanceTv= (TextView) convertView.findViewById(R.id.distanceTv);
            holder.companyLogoIv = (ImageView) convertView.findViewById(R.id.company_logo_lv);
            holder.dateTv = (TextView) convertView.findViewById(R.id.date_tv);
            holder.locationTv = (TextView) convertView.findViewById(R.id.location_tv);
            holder.jobTitleTv = (TextView) convertView.findViewById(R.id.recrutment_title_tv);
            holder.priceTv = (TextView) convertView.findViewById(R.id.price_tv);
            holder.companyNameTv= (TextView) convertView.findViewById(R.id.companyNameTv);
            holder.busStyleIv= (ImageView) convertView.findViewById(R.id.busStyleIv);
            holder.extensionIv= (ImageView) convertView.findViewById(R.id.extensionIv);
             convertView.setTag(holder);
         } else {
             holder = (Holder) convertView.getTag();
         }
         final Job job = jobList.get(position);
         if(job.jobHot()<=3){
            holder.extensionIv.setVisibility(View.VISIBLE);
         }else{
             holder.extensionIv.setVisibility(View.GONE);
         }
         holder.jobTitleTv.setText(job.getJob_RecruitName());
         holder.priceTv.setText(job.getJob_Price()+"/"+job.getJob_Unit());
         double  latitude =job.getGeoPoint().getLatitude();
         double  longitude=job.getGeoPoint().getLongitude();
         AVGeoPoint point = new AVGeoPoint(latitude,longitude);
         double distance=point.distanceInKilometersTo(new AVGeoPoint(AppConfig.mylatitude,AppConfig.mylongitude));
         holder.locationTv.setText(job.getJob_Address());
         holder.distanceTv.setText(decimalFormat.format(Double.valueOf(distance))+"千米");
         Business  business=job.getAVObject("business_pointer");
         holder.companyNameTv.setText(business.getBusiness_Name());
         if (business.businessStyle()){
             holder.busStyleIv.setImageResource(R.drawable.qiye);
         }else{
             holder.busStyleIv.setImageResource(R.drawable.geren);
         }
         if(business.IsSalaryGuarantee()){
             holder.baozhangIv.setVisibility(View.VISIBLE);
         }else {
             holder.baozhangIv.setVisibility(View.INVISIBLE);
         }
        if(business.IsVip()){
            holder.vipIv.setVisibility(View.VISIBLE);
        }else {
            holder.vipIv.setVisibility(View.INVISIBLE);
        }
         SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
            holder.dateTv.setText(sdf.format(sf.parse(job.getJob_StartTime()+""))+"至"+sdf.format(sf.parse(job.getJob_EndTime()+"")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
         UpLoadImageUtil.loadCropImage(context,business.getComLogoUrl(),holder.companyLogoIv);
         return convertView;
    }
    class  Holder{
        private  TextView  distanceTv;
        private  ImageView  baozhangIv;
        private  ImageView   vipIv;
        private TextView   jobTitleTv;
        private  TextView   dateTv;
        private  TextView   priceTv;
        private  TextView  locationTv;
        private  ImageView  companyLogoIv;
        private  TextView   companyNameTv;
        private  ImageView   busStyleIv;
        private  ImageView  extensionIv;
    }
    public  void  changeAdapterData(List<Job> jobList){
        this.jobList=jobList;
        notifyDataSetChanged();
    }
}
