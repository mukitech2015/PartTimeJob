package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/11/2.
 */

public class AccountSetActivity  extends AppCompatActivity {
    @Bind(R.id.accountBankSaveLat)
    RelativeLayout  accountBankSaveLat;
    @Bind(R.id.accountBankEt)
    EditText  accountBankEt;
    @Bind(R.id.accountBankNameEt)
    EditText  accountBankNameEt;
    @Bind(R.id.accountSetAlipayEt)
    EditText  accountSetAlipayEt;
    @Bind(R.id.accountSetIv)
    ImageView  accountSetIv;
    @Bind(R.id.accountSetNameTv)
    TextView  accountSetNameTv;
    @Bind(R.id.accountSetBackLat)
    RelativeLayout  accountSetBackLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_accent_setting);
        ButterKnife.bind(this);
        accountSetBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        accountBankSaveLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCheck()){
                    String  bankName=accountBankNameEt.getText().toString().trim();
                    String  bankAccount=accountBankEt.getText().toString().trim();
                    String  accountName=accountSetNameTv.getText().toString().trim();
                    String  alipayAccount=accountSetAlipayEt.getText().toString().trim();
                    String  name= accountSetNameTv.getText().toString().trim();
                    LeanchatUser.getCurrentUser().setBankAccount(bankAccount);
                    LeanchatUser.getCurrentUser().setAlipayAccount(alipayAccount);
                    LeanchatUser.getCurrentUser().setBankName(bankName);
                    LeanchatUser.getCurrentUser().setUSER_FAMILY_NAME(name);
                    LeanchatUser.getCurrentUser().saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if (e==null){
                                Toast.makeText(AccountSetActivity.this,"设置账户成功",Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        getAccountDatas();
    }
    public   void   getAccountDatas(  ){
         accountBankEt.setText(LeanchatUser.getCurrentUser().getBankAccount());
         accountBankNameEt.setText(LeanchatUser.getCurrentUser().getBankName());
         accountSetAlipayEt.setText(LeanchatUser.getCurrentUser().getAlipayAccount());
         if (LeanchatUser.getCurrentUser().getUSER_ID_ISAUTHENTCATION()==1){
             accountSetIv.setImageResource(R.drawable.withdrawl_tauthentication);
         }else{
             accountSetIv.setImageResource(R.drawable.withdrawl_notauthentication);
         }
        accountSetNameTv.setText(LeanchatUser.getCurrentUser().getUSER_FAMILY_NAME());
    }
     public    boolean   isCheck(  ){
        String  bankName=accountBankNameEt.getText().toString().trim();
        String  bankAccount=accountBankEt.getText().toString().trim();
        String  accountName=accountSetNameTv.getText().toString().trim();
        String  alipayName=accountSetAlipayEt.getText().toString().trim();
        if (TextUtils.isEmpty(bankName)){
            Toast.makeText(AccountSetActivity.this,"请输入银行卡开户行名称",Toast.LENGTH_SHORT).show();
            return  false;
        }
         if (TextUtils.isEmpty(bankAccount)){
             Toast.makeText(AccountSetActivity.this,"请输入银行卡号码",Toast.LENGTH_SHORT).show();
             return  false;
         }
         if (TextUtils.isEmpty(accountName)){
             Toast.makeText(AccountSetActivity.this,"请输入真实姓名",Toast.LENGTH_SHORT).show();
             return  false;
         }
         if (TextUtils.isEmpty(alipayName)){
             Toast.makeText(AccountSetActivity.this,"请输入支付宝账号",Toast.LENGTH_SHORT).show();
             return  false;
         }
         return   true;
     }
}














