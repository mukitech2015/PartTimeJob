package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.SaveCallback;

import java.io.IOException;

/**
 * Created by muki on 2016/10/12.
 */

@AVClassName("User_Image_Info")
public class User_Image_Info   extends AVObject {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public  String  getUser_Image_Type( ){
        return    getString("user_Image_Type");
    }
    public  void    setUser_Image_Type(String user_Image_Type){
        put("user_Image_Type",user_Image_Type);
    }
    public String getUsreImageUrl() {
        AVFile userImage = getAVFile("user_Image");
        if (userImage != null) {
            return userImage.getUrl();
        } else {
            return null;
        }
    }
    public void saveUserImage(String path, final SaveCallback saveCallback) {
        final AVFile file;
        try {
            file = AVFile.withAbsoluteLocalPath(path, path);
            put("user_Image", file);
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (null == e) {
                        saveInBackground(saveCallback);
                    } else {
                        if (null != saveCallback) {
                            saveCallback.done(e);
                        }
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
