package com.muki.parttimejob.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muki.parttimejob.R;
import com.muki.parttimejob.model.LeanchatUser;

/**
 * Created by muki on 2016/12/2.
 */

public class MyCommtActivity  extends  AppCompatActivity {
    RatingBar ability_rb;
    RatingBar  experience_rb;
    TextView comprehensiveTv;
    private  TextView   standNumTv;
    RelativeLayout  commtBackLat;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_comment);
        initViews();
    }
    public  void  initViews( ){
        commtBackLat= (RelativeLayout) findViewById(R.id.commtBackLat);
        standNumTv= (TextView)findViewById(R.id.userStandNumTv);
        ability_rb= (RatingBar) findViewById(R.id.ability_rb);
        experience_rb= (RatingBar)findViewById(R.id.experience_rb);
        comprehensiveTv= (TextView) findViewById(R.id.comprehensiveTv);
        commtBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        initDatas();
    }
    public  void  initDatas(  ){
        ability_rb.setRating((float) LeanchatUser.getCurrentUser().getUserAbility());
        experience_rb.setRating((float) LeanchatUser.getCurrentUser().getUserExperience());
        comprehensiveTv.setText(LeanchatUser.getCurrentUser().getUserComprehensive()+"");
        standNumTv.setText(LeanchatUser.getCurrentUser().getUserStandNum()+"");
    }
}









