package com.muki.parttimejob.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muki.parttimejob.R;

/**
 * Created by muki on 2016/9/26.
 */
public class ScreenAdapter  extends BaseAdapter{
      private Context   context;
      private String[]  sereenStrs;
      private LayoutInflater   layoutInflater;
      public    ScreenAdapter(Context  context,String[]  sereenStrs){
      this.context=context;
      this.sereenStrs=sereenStrs;
      layoutInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return  sereenStrs.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final  Holder  holder;
        if (convertView==null){
            holder=new Holder();
            convertView = layoutInflater.inflate(R.layout.layout_screen_item,null);
            holder.screenTv= (TextView) convertView.findViewById(R.id.screenTv);
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }
        holder.screenTv.setText(sereenStrs[position]);
        return convertView;
    }
    class  Holder{
         TextView   screenTv;
    }
}
