package com.muki.parttimejob.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImageUtil {
	public static Uri Scal(String filePath, Context context) {
		File file = new File(filePath);
		if (file.length() > 2073600) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
			options.inJustDecodeBounds = false;
			int w = options.outWidth;
			int h = options.outHeight;
			//现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
			float hh = 960f;//这里设置高度为800f
			float ww = 540f;//这里设置宽度为480f
			//缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
			int be = 1;//be=1表示不缩放
			if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
				be = (int) (options.outWidth / ww);
			} else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
				be = (int) (options.outHeight / hh);
			}
			if (be <= 0)
				be = 1;
			options.inSampleSize = be;//设置缩放比例
			//重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
			bitmap = BitmapFactory.decodeFile(filePath, options);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，

			int option = 100;
			while (baos.toByteArray().length / 1024 > 2000) {  //循环判断如果压缩后图片是否大于2000kb,大于继续压缩
				baos.reset();//重置baos即清空baos
				option -= 10;//每次都减少10
			}
			ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
			Bitmap newbitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
			Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), newbitmap, null, null));
			// can post image
			return uri;

		} else {
			BitmapFactory.Options options = new BitmapFactory.Options();
			Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
			return Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, null, null));
		}
	}

	public  static  String getAbsoluteImagePath(Uri uri, Activity context) {
		// can post image
		String[] proj = {MediaStore.Images.Media.DATA};
		Cursor cursor = context.managedQuery(uri,
				proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}












