package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

import java.io.Serializable;

/**
 * Created by muki on 2016/10/18.
 */
@AVClassName("Dynamic")
public class Dynamic  extends AVObject  implements Serializable {
    public static final Creator CREATOR = AVObjectCreator.instance;

    public  void   setDynamicUsers(LeanchatUser  leanchatUser){
        put("dynamic_Users_pointer",leanchatUser);
    }
    public  LeanchatUser  getDynamicUsers(   ){
        return  getAVObject("dynamic_Users_pointer");
    }
    public  void  setDynJurisdiction(boolean  dynamic_content){
        put("dynamic_Jurisdiction",dynamic_content);
    }
    public  Boolean getDynJurisdiction( ) {
        return getBoolean("dynamic_Jurisdiction");
    }

        public  void  setDynamicContent(String  dynamic_content){
            put("dynamic_content",dynamic_content);
        }
        public  String getDynamicContent( ){
            return   getString("dynamic_content");
        }
     public  void  setFollowCount(int  follow_Count){
        put("follow_Count",follow_Count);
    }
    public  int  getFollowCount(  ){
        return   getInt("follow_Count");
    }

    public  void   setOnlyFriendState(Boolean   onlyFriendState){
        put("dynamic_OnlyFriendState",onlyFriendState);
    }
    public    boolean   getOnlyFriendState( ){
     return   getBoolean("dynamic_OnlyFriendState");
    }
}










