package com.muki.parttimejob.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.baoyz.actionsheet.ActionSheet;
import com.muki.parttimejob.R;
import com.muki.parttimejob.adapter.MyJopAdapter;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.BudgetHistory;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Job;
import com.muki.parttimejob.widget.BaseAdapter;
import com.muki.parttimejob.widget.PullBaseView;
import com.muki.parttimejob.widget.PullRecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by muki on 2016/9/27.
 */
public class MyJobActivity  extends AppCompatActivity  implements View.OnClickListener,PullBaseView.OnHeaderRefreshListener,
     PullBaseView.OnFooterRefreshListener{
     private LinearLayout myBackLat;
    String   myJobType;
     private PullRecyclerView myJobRv;
    List<AVObject>  objectListCopy=new ArrayList<AVObject>();
    int page=0;
    private MyJopAdapter myJopAdapter;
    private TextView  myJobTitleTv;
    private  LinearLayout   noMyJobLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_myjob_activity);
        initViews();
    }
    protected void initViews() {
        noMyJobLat= (LinearLayout) findViewById(R.id.noMyJobLat);
        myJobTitleTv= (TextView) findViewById(R.id.myJobTitleTv);
        myBackLat= (LinearLayout) findViewById(R.id.myBackLat);
        myBackLat.setOnClickListener(this);
        myJobRv= (PullRecyclerView) findViewById(R.id.myJobLv);
        myJobRv.setLayoutManager(new LinearLayoutManager(this),this);
        myJobRv.setOnHeaderRefreshListener(this);
        myJobRv.setOnFooterRefreshListener(this);
    }
    public  void  setMyJobAdapter(List<AVObject>   userJobs){
        if(myJopAdapter==null){
            myJopAdapter=new MyJopAdapter(MyJobActivity.this,userJobs,this);
            myJobRv.setAdapter(myJopAdapter);
        }else{
            myJopAdapter.changeAdapterData(userJobs);
        }
        if(myJopAdapter.getItemCount()==0){
              myJobRv.setVisibility(View.GONE);
              noMyJobLat.setVisibility(View.VISIBLE);
        }else{
                myJobRv.setVisibility(View.VISIBLE);
                noMyJobLat.setVisibility(View.GONE);
        }
        myJopAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent=new Intent(MyJobActivity.this,JobDetailActivity.class);
                User_Job user_job= (User_Job) objectListCopy.get(position);
                intent.putExtra("job",(Serializable)user_job.getJob_Pointer());
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent  intent=getIntent();
         myJobType=intent.getStringExtra("myJobType");
         if(!TextUtils.isEmpty(myJobType)){
             myJobTitleTv.setText(myJobType);
         }
         getMyJobDate(myJobType);
    }
    public   void   getMyJobDate(String myJobType){
        AVQuery<AVObject> query = new AVQuery<>("User_Job");
        query.whereEqualTo("user_pointer", LeanchatUser.getCurrentUser());
        if (!TextUtils.isEmpty(myJobType)){
            query.whereEqualTo("state",myJobType);
        }
        query.limit(10);
        query.setCachePolicy(AVQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.setMaxCacheAge(24 * 3600); //设置缓存有效期
        query.orderByDescending("createdAt");
        query.skip(page*10);// 跳过 10 条结果
        query.include("job_pointer");
        query.include("business_pointer");
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                  if(list!=null){
                    if (page==0){
                        objectListCopy.clear();
                        objectListCopy.addAll(list);
                        setMyJobAdapter(objectListCopy);
                    }else{
                        objectListCopy.addAll(objectListCopy.size(),list);
                        setMyJobAdapter(objectListCopy);
                    }
                }
             }
        });
    }

int  mIndex;
    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case  R.id.myBackLat:
               this.finish();
               break;
           case  R.id.stateLat:
               mIndex=Integer.parseInt(String.valueOf(v.getTag()));
               btnClick(mIndex);
               break;
         }
    }
  public  void   btnClick(int  mIndex ){
      final User_Job  user_job= (User_Job) objectListCopy.get(mIndex);
      Job job=user_job.getJob_Pointer();
      if(user_job.getAction_State().equals(AppConfig.ACTION_COMMT)){
          Intent intentComt=new Intent(this, CommectActivity.class);
          intentComt.putExtra("job", (Serializable) job);
          intentComt.putExtra("user_job", (Serializable) user_job);
          startActivity(intentComt);
      }else if(user_job.getAction_State().equals("确认收款")){
          user_job.setAction_State(AppConfig.ACTION_COMMT);
          user_job.setState(AppConfig.COMMT);
          user_job.saveInBackground(new SaveCallback() {
              @Override
              public void done(AVException e) {
                  if(e==null){
                      myJopAdapter.notifyDataSetChanged();
                      AVQuery<AVObject>   avObjectAVQuery=new AVQuery<AVObject>("Job_Order");
                      avObjectAVQuery.whereEqualTo("order_id",user_job.getString("job_Order"));
                      avObjectAVQuery.getFirstInBackground(new GetCallback<AVObject>() {
                          @Override
                          public void done(final AVObject avObject, AVException e) {
                               if (e==null){
                                   BudgetHistory  budgetHistory=new BudgetHistory();
                                   budgetHistory.setUserPointer(LeanchatUser.getCurrentUser());
                                   budgetHistory.setBudgetMoney(avObject.getDouble("money"));
                                   budgetHistory.setBudgetDescription("收入");
                                   budgetHistory.setCurrentBalance(LeanchatUser.getCurrentUser().getBalance()+avObject.getDouble("money"));
                                   budgetHistory.saveInBackground(new SaveCallback() {
                                       @Override
                                       public void done(AVException e) {
                                             if(e==null){
                                           LeanchatUser.getCurrentUser().setBalance(LeanchatUser.getCurrentUser().getBalance()-avObject.getInt("money"));
                                             }
                                       }
                                   });
                               }
                          }
                      });
                  }
              }
          });

      }else if(user_job.getAction_State().equals(AppConfig.ACTION_WDW_AND_CANCEL)) {
             setMutiSelect(user_job,job);
      }else if (user_job.getAction_State().equals(AppConfig.ACTION_RETURN_POST)){
          Intent  intent=new Intent(MyJobActivity.this,PaymentActivity.class);
          intent.putExtra("user_job", (Serializable) user_job);
          intent.putExtra("job", (Serializable) job);
          startActivity(intent);
      }else if (user_job.getAction_State().equals(AppConfig.ACTION_CANCEL_REG)){
              showCancelDg(user_job,0);
      }
  }
    public    void  setMutiSelect(final User_Job   user_job, final Job job){
        ActionSheet.createBuilder(this,getSupportFragmentManager())
                .setCancelButtonTitle("取消")
                .setOtherButtonTitles("申请结款","取消兼职")
                .setCancelableOnTouchOutside(true)
                .setListener(new ActionSheet.ActionSheetListener() {
                    @Override
                    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

                    }
                    @Override
                    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                        switch (index){
                            case  0:
                              Intent  intent=new Intent(MyJobActivity.this,PaymentActivity.class);
                              intent.putExtra("user_job", (Serializable) user_job);
                              intent.putExtra("job", (Serializable) job);
                              startActivity(intent);
                            break;
                            case  1:
                               showCancelDg(user_job,1);
                                break;
                        }
                    }
                }) .show();
    }
    AlertDialog.Builder  dialog;
    public  void   showCancelDg(final User_Job  user_job,int  which){
        if(dialog==null){
            dialog=new AlertDialog.Builder(MyJobActivity.this);
        }
        if(which==0){
            dialog.setTitle("取消报名");
            dialog.setIcon(R.drawable.ic_launcher);
            dialog.setMessage("确定要取消报名吗？");
        }else{
            dialog.setTitle("取消兼职");
            dialog.setIcon(R.drawable.ic_launcher);
            dialog.setMessage("确定要取消兼职吗？");
        }
        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                user_job.setAction_State(AppConfig.ACTION_ALREADY_CANCEL);
                user_job.setState(AppConfig.COMPLETE);
                user_job.saveInBackground();
                myJopAdapter.notifyDataSetChanged();
            }
        }).show();
    }
    @Override
    public void onFooterRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page++;
                //execute the task
                 getMyJobDate(myJobType);
                view.onFooterRefreshComplete();
            }
        }, 2000);
    }
    @Override
    public void onHeaderRefresh(final PullBaseView view) {
        new Handler().postDelayed(new Runnable(){
            public void run() {
                page=0;
                //execute the task
                getMyJobDate(myJobType);
                view.onHeaderRefreshComplete();
            }
        }, 2000);
    }
}





