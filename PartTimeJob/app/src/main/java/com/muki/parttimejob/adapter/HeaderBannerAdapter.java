package com.muki.parttimejob.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.muki.parttimejob.activity.WebActivity;
import com.muki.parttimejob.model.BannerImage;

import java.util.List;

/**
 * Created by sunfusheng on 16/4/20.
 */
public class HeaderBannerAdapter extends PagerAdapter {

    private List<ImageView> ivList; // ImageView的集合
    private int count = 1; // 广告的数量
    Context  context;
    List<BannerImage> list;
    public HeaderBannerAdapter(Context context, List<ImageView> ivList,List<BannerImage> list) {
        super();
        this.list=list;
        this.context=context;
        this.ivList = ivList;
        if(ivList != null && ivList.size() > 0){
            count = ivList.size();
        }
    }

    @Override
    public int getCount() {
        if (count == 1) {
            return 1;
        } else {
            return Integer.MAX_VALUE;
        }
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final int newPosition = position % count;
        // 先移除在添加，更新图片在container中的位置（把iv放至container末尾）
        ImageView iv = ivList.get(newPosition);
        container.removeView(iv);
        container.addView(iv);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWebAdvice=new Intent(context,WebActivity.class);
                intentWebAdvice.putExtra("WEB_URL",list.get(newPosition).getBannerUrl());
                context.startActivity(intentWebAdvice);
            }
        });
        return iv;
    }
}
