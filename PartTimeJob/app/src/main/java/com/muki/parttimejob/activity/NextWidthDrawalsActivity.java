package com.muki.parttimejob.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.muki.parttimejob.R;
import com.muki.parttimejob.model.BudgetWithdrawApply;
import com.muki.parttimejob.model.LeanchatUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by muki on 2016/10/31.
 */

public class NextWidthDrawalsActivity  extends AppCompatActivity {
    @Bind(R.id.widthDrawsNextBackLat)
    RelativeLayout widthDrawsNextBackLat;
    @Bind(R.id.applyLat)
    RelativeLayout  applyLat;
    @Bind(R.id.jingaoLat)
    LinearLayout jingaoLat;
    @Bind(R.id.postWdEt)
    EditText postWdEt;
    @Bind(R.id.balanceTv)
    TextView balanceTv;
    @Bind(R.id.nextAccountTv)
    TextView  nextAccountTv;
    @Bind(R.id.nextBankNameTv)
    TextView  nextBankNameTv;
    @Bind(R.id.nextWdNameTv)
    TextView  nextWdNameTv;
    @Bind(R.id.nextBankNameLat)
    LinearLayout  nextBankNameLat;
    @Bind(R.id.accountTypeTv)
    TextView  accountTypeTv;
    String  withdrawalsType;
    Random random = new Random();
    SimpleDateFormat formatter=new SimpleDateFormat("yyyyMMddhh");
    Date curDate=new  Date(System.currentTimeMillis());
    long rnd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_next_withdrawals);
        ButterKnife.bind(this);
        jingaoLat.setVisibility(View.GONE);
        widthDrawsNextBackLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
            }
        });
        applyLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rnd = random.nextInt(9999)%(9999-1000+1)+1000;
                if (check()){
                    String  account=nextAccountTv.getText().toString().trim();
                    String   accountType=accountTypeTv.getText().toString().trim();
                    String   timeStr=formatter.format(curDate);
                    final String   postWd=postWdEt.getText().toString().trim();
                    final BudgetWithdrawApply  budgetWithdrawApply=new BudgetWithdrawApply();
                    budgetWithdrawApply.setAccount(account);
                    budgetWithdrawApply.setState(0);
                    budgetWithdrawApply.setWithdrawId(timeStr+rnd);
                    budgetWithdrawApply.setAccountType(accountType);
                    budgetWithdrawApply.setWithdrawMoney(Double.parseDouble(postWd));
                    budgetWithdrawApply.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if(e==null){
                              submit();
                            }
                        }
                    });
                }
            }
        });
    }
    android.app.AlertDialog.Builder builder;
    public void   submit(  ){
        if(builder==null){
            builder=new android.app.AlertDialog.Builder(this);
        }
        builder.setTitle("申请提现");
        builder.setMessage("您的提款申请已提交，请留意申请进度。                                              ");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               Intent   intent=new Intent(NextWidthDrawalsActivity.this,WalletActivity.class);
               startActivity(intent);
            }
        }).show();
    }
    public  Boolean   check(  ){
        String  postWdEtStr=postWdEt.getText().toString().trim();
        if (TextUtils.isEmpty(postWdEtStr)){
            Toast.makeText(this,"请填写提现金额",Toast.LENGTH_SHORT).show();
            return false;
        }
        double   money=Double.parseDouble(postWdEtStr);
        if (money<100){
            Toast.makeText(this,"提现金额不能少于100元",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(money>LeanchatUser.getCurrentUser().getBalance()){
            jingaoLat.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent=getIntent();
        withdrawalsType=intent.getStringExtra("WithdrawalsType");
        getDatas(withdrawalsType);
    }
    public   void    getDatas( String  withdrawalsType ){
        if (withdrawalsType.equals("支付宝")){
            nextBankNameLat.setVisibility(View.GONE);
            accountTypeTv.setText("支付宝账号");
            nextAccountTv.setText(LeanchatUser.getCurrentUser().getAlipayAccount());
        }else{
            accountTypeTv.setText("银行卡账号");
            nextBankNameLat.setVisibility(View.VISIBLE);
            nextBankNameTv.setText(LeanchatUser.getCurrentUser().getBankName());
            nextAccountTv.setText(LeanchatUser.getCurrentUser().getBankAccount());
        }
        nextWdNameTv.setText(LeanchatUser.getCurrentUser().getUSER_FAMILY_NAME());
        balanceTv.setText(LeanchatUser.getCurrentUser().getBalance()+" (元)");
    }
}
