package com.muki.parttimejob.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.navi.BaiduMapNavigation;
import com.baidu.mapapi.navi.NaviParaOption;
import com.muki.parttimejob.R;
import com.muki.parttimejob.config.AppConfig;
import com.muki.parttimejob.model.Business;
import com.muki.parttimejob.model.Job;
import com.muki.parttimejob.model.LeanchatUser;
import com.muki.parttimejob.model.User_Job;
import com.muki.parttimejob.utils.ButtonUtils;
import com.muki.parttimejob.utils.UpLoadImageUtil;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.SnsPlatform;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.leancloud.chatkit.utils.LCIMConstants;
import de.hdodenhof.circleimageview.CircleImageView;

;

/**
 * Created by muki on 2016/9/22.
 */
public class JobDetailActivity   extends AppCompatActivity implements View.OnClickListener{
    Job   job;
    private CircleImageView jobIconIv;
    private TextView   jobTitleTv;
    private TextView   jobPriceTv;
    private TextView   jobComNameTv;
    private ImageView   jobCertifiedIv;
    private RatingBar   jobScoreRb;
    private  TextView   jobSexTv;
    private  TextView   jobPerNumTv;
    private  TextView   job_workDateTv;
    private  TextView   job_workTimeTv;
    private  TextView   job_workPlaceTv;
    private  TextView   job_posContentTv;
    private  TextView   job_RequireTv;
    private  TextView    job_SaldescTv;
    private  TextView   job_regDatelineTv;
    private   TextView  job_SpecialNoteTv;
    private   TextView   job_conInformationTv;
    private   TextView   certificateTv;
    LinearLayout   backLat;
    private RelativeLayout   signUpLat;
    private  AVObject   busAvObj;
    private  TextView  signUpTv;
    private  RelativeLayout   jobNavigationLat;
    private  RelativeLayout   collectionLat;
    private  RelativeLayout   jobdetailshareLat;
    boolean isFirstLoc = true; // 是否首次定位
    private  ImageView  jobCollectionIv;
    AVObject  avObjectCollection;
    LinearLayout  jobComtLat;
    private   RelativeLayout  consultationLat;
    private   LinearLayout  businessLat;
    public ArrayList<SnsPlatform> platforms = new ArrayList<SnsPlatform>();
    private TextView  signUpNumTv;
    LinearLayout   jobReportLat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_parttime_detail);
        initViews();
    }
    protected void initViews() {
             initPlatforms();
            jobReportLat= (LinearLayout) findViewById(R.id.jobReportLat);
            businessLat= (LinearLayout) findViewById(R.id.businessLat);
            jobdetailshareLat= (RelativeLayout) findViewById(R.id.jobdetailshareLat);
            consultationLat= (RelativeLayout) findViewById(R.id.consultationLat);
            jobComtLat= (LinearLayout) findViewById(R.id.jobComtLat);
            jobCollectionIv= (ImageView) findViewById(R.id.jobCollectionIv);
            collectionLat= (RelativeLayout) findViewById(R.id.collectionLat);
            collectionLat.setOnClickListener(this);
            jobNavigationLat= (RelativeLayout) findViewById(R.id.jobNavigationLat);
            jobNavigationLat.setOnClickListener(this);
            consultationLat.setOnClickListener(this);
            signUpTv= (TextView) findViewById(R.id.signUpTv);
            backLat= (LinearLayout) findViewById(R.id.detail_back_lat);
            backLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
                }
            });
           jobdetailshareLat.setOnClickListener(this);
            signUpLat= (RelativeLayout) findViewById(R.id.signUpLat);
            signUpLat.setOnClickListener(this);
           jobComtLat.setOnClickListener(this);
           job_conInformationTv= (TextView) findViewById(R.id.detail_conInformationTv);
           job_SpecialNoteTv= (TextView) findViewById(R.id.detail_SpecialNoteTv);
           job_regDatelineTv= (TextView) findViewById(R.id.detail_regDatelineTv);
           job_workDateTv= (TextView) findViewById(R.id.detail_workDateTv);
           job_workTimeTv= (TextView) findViewById(R.id.detail_workTimeTv);
           job_workPlaceTv= (TextView) findViewById(R.id.detail_placeTv);
           job_posContentTv= (TextView) findViewById(R.id.detail_posContentTv);
           job_RequireTv= (TextView) findViewById(R.id.detail_RequireTv);
           job_SaldescTv= (TextView) findViewById(R.id.detail_SaldescTv);
           job_regDatelineTv= (TextView) findViewById(R.id.detail_regDatelineTv);
        jobPerNumTv= (TextView) findViewById(R.id.detail_perNumTv);
        jobSexTv = (TextView) findViewById(R.id.detail_sexTv);
        jobCertifiedIv= (ImageView) findViewById(R.id.detail_certifiedIv);
        jobComNameTv= (TextView) findViewById(R.id.detail_comNameTv);
        jobIconIv= (CircleImageView) findViewById(R.id.detail_logoIv);
        jobPriceTv= (TextView) findViewById(R.id.detail_priceTv);
        jobScoreRb= (RatingBar) findViewById(R.id.detailability_rb);
        jobTitleTv= (TextView) findViewById(R.id.detail_titleTv);
        certificateTv= (TextView) findViewById(R.id.certificateTv);
        signUpNumTv= (TextView) findViewById(R.id.signUpNumTv);
        jobReportLat.setOnClickListener(this);
        jobScoreRb.setOnRatingBarChangeListener(new RatingBarChangeListenerImpl());
        businessLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intentBusiness=new Intent(JobDetailActivity.this,WebActivity.class);
                intentBusiness.putExtra("WEB_URL", AppConfig.BUSINESS+busAvObj.getObjectId());
                startActivity(intentBusiness);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent   intent=getIntent();
        if(intent!=null){
            job= (Job) intent.getSerializableExtra("job");
            if(job!=null){
                getJobDetailData(job);
                getSignUpState();
                collection();
            }
        }
    }
    AVFile avFileLogo;
    public  void   getJobDetailData(final Job job){
        final AVObject avObj=job.getAVObject("business_pointer");
        AVQuery<AVObject> avQuery = new AVQuery<>("Business");
        avQuery.getInBackground(avObj.getObjectId(), new GetCallback<AVObject>() {
            @Override
            public void done(AVObject avObject, AVException e) {
                if(e==null){
                     busAvObj=avObj;
                     jobComNameTv.setText(avObject.getString("business_Name")+"");
                     jobScoreRb.setRating((float) avObject.getDouble("business_score"));
                       avFileLogo= (AVFile) avObject.get("business_CompanyLogo");
                     if(avFileLogo.getUrl()!=null){
                      UpLoadImageUtil.loadCropImage(JobDetailActivity.this,avFileLogo.getUrl(),jobIconIv);
                     }
                     jobPriceTv.setText(job.getJob_Price()+"");
                     if(avObject.getBoolean("business_IsAuthentication")){
                         jobCertifiedIv.setImageResource(R.drawable.alreadycertified);
                     }else{
                         jobCertifiedIv.setImageResource(R.drawable.notcertified);
                     }
                    jobTitleTv.setText(job.getJob_RecruitName());
                    job_SaldescTv.setText(job.getJob_Salary_Level());
                    jobSexTv.setText(job.getJob_SexPreference());
                    jobPerNumTv.setText(job.getJob_RecruitNum());
                    SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
                    try {
                        job_workDateTv.setText(sdf.format(sf.parse(job.getJob_StartTime()+""))+"至"+sdf.format(sf.parse(job.getJob_EndTime()+"")));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    job_workTimeTv.setText(job.getJob_WorkDate());
                    job_workPlaceTv.setText(job.getJob_Address());
                    job_RequireTv.setText(job.getJob_Requirements());
                    job_posContentTv.setText(job.getJob_Responsibility());
                    try {
                        job_regDatelineTv.setText(sdf.format(sf.parse(job.getJob_RegDeadLine()+"")));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    job_conInformationTv.setText(job.getJob_Contact());
                    job_SpecialNoteTv.setText(job.getJob_SpecialExplanation());
                    certificateTv.setText(job.getJob_Certificate());
                    alreadSignUpNum();
                 }
            }
        });
    }
    public   void   alreadSignUpNum(  ){
       AVQuery<User_Job>   user_jobAVQuery=AVQuery.getQuery(User_Job.class);
       user_jobAVQuery.whereEqualTo("job_pointer",job);
       user_jobAVQuery.whereNotEqualTo("action_State","已取消");
       user_jobAVQuery.findInBackground(new FindCallback<User_Job>() {
           @Override
           public void done(List<User_Job> list, AVException e) {
               if (e==null){
                   signUpNumTv.setText(list.size()+"人");
               }
           }
       });
    }
    AlertDialog.Builder builder;
    public  void  showSingUp( ){
        if(builder==null){
            builder=new AlertDialog.Builder(JobDetailActivity.this);
        }
        builder.setTitle("报名");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage("确定要报名么？");
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                signUp();
            }
        }).show();
    }
    public  void  getSignUpState(  ){
        AVQuery<User_Job> query =AVQuery.getQuery(User_Job.class);
        query.whereEqualTo("user_pointer",LeanchatUser.getCurrentUser());
        query.whereEqualTo("job_pointer",job);
        query.getFirstInBackground(new GetCallback<User_Job>() {
            @Override
            public void done(User_Job user_job, AVException e) {
                 if(e==null&&user_job!=null){
                     if(user_job.getState().equals("已完成")){
                         signUpTv.setText("报名");
                     }else{
                         signUpTv.setText(user_job.getState());
                     }
                 }
            }
        });
    }
    public   void   signUp( ){
        User_Job   user_job=new User_Job();
        user_job.setBusiness_pointer(busAvObj);
        user_job.setJob_Pointer(job);
        user_job.setState(AppConfig.REG_IN);
        user_job.setAction_State(AppConfig.ACTION_CANCEL_REG);
        user_job.setUser_pointer(LeanchatUser.getCurrentUser());
        user_job.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    Toast.makeText(JobDetailActivity.this,"报名成功,等待审核",Toast.LENGTH_SHORT).show();
                    signUpTv.setText(AppConfig.REG_IN);
                }
            }
        });
    }
    AlertDialog.Builder builderRepBus;
    public  void  ReportBussDlg( ){
        if(builderRepBus==null){
            builderRepBus=new AlertDialog.Builder(JobDetailActivity.this);
        }
        builderRepBus.setTitle("举报商家");
        builderRepBus.setIcon(R.drawable.ic_launcher);
        builderRepBus.setMessage("确定要举报该商家么？");
        builderRepBus.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ReportBuss();
            }
        }).show();
    }
    public  void   ReportBuss(  ){
        Business  business=job.getAVObject("business_pointer");
        AVObject   repBusAvObj=new AVObject("Report");
        repBusAvObj.put("business_pointer",business);
        repBusAvObj.put("fromReporter",LeanchatUser.getCurrentUser());
        repBusAvObj.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
               Toast.makeText(JobDetailActivity.this,"已举报该商家",Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaiduMapNavigation.finish(this);
    }
    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.jobReportLat:
               ReportBussDlg();
               break;
           case  R.id.jobdetailshareLat:
                    new ShareAction(this).setDisplayList(SHARE_MEDIA.QQ,SHARE_MEDIA.QZONE,SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                       .withTitle(job.getJob_RecruitName())
                       .withText(job.getJob_Address())
                       .withMedia(new UMImage(this,avFileLogo.getUrl()))
                       .withTargetUrl("http://www.cloudconfs.com/part/index.php/Share/Share/view?objectId="+job.getObjectId())
                       .setCallback(umShareListener)
                       .open();
              break;
           case   R.id.consultationLat:
               Intent intent = new Intent(this, ChatRoomActivity.class);
               intent.putExtra(LCIMConstants.PEER_ID, "58327eb05f7cff004f27c13a");
               intent.putExtra("CHAT_TITLE","兼职助手");
               intent.putExtra("TO_USERID","58327eb05f7cff004f27c13a");
               startActivity(intent);
                break;
           case  R.id.jobComtLat:
               Intent  jobComeIntent=new Intent(this,JobComtActivity.class);
               jobComeIntent.putExtra("job", (Serializable) job);
               startActivity(jobComeIntent);
               break;
           case R.id.signUpLat:
                   if(signUpTv.getText().toString().trim().equals("报名")){
                       if (job.jobState()){
                           showSingUp();
                       }else{
                           Toast.makeText(JobDetailActivity.this,"该兼职已过期",Toast.LENGTH_SHORT).show();
                       }
                   }else{
                       Toast.makeText(JobDetailActivity.this,"已报名该兼职，不可重复报名",Toast.LENGTH_SHORT).show();
                   }
               break;
           case  R.id.jobNavigationLat:
                   startNavi();
               break;
           case R.id.collectionLat:
               if (!ButtonUtils.isFastDoubleClick(R.id.collectionLat)) {
                        if ( avObjectCollection!=null){
                            if(avObjectCollection.getBoolean("state")){
                                avObjectCollection.put("state",false);
                                avObjectCollection.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if (e==null){
                                            jobCollectionIv.setImageResource(R.drawable.part_time_collection);
                                        }
                                    }
                                });
                            }else{
                                avObjectCollection.put("state",true);
                                avObjectCollection.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if(e==null){
                                            jobCollectionIv.setImageResource(R.drawable.part_time_collectionpress);
                                        }
                                    }
                                });
                            }
                        }else{
                            createCollection();
                        }
               }
               break;
          }
    }

    /**
     * 启动百度地图导航(Native)
     */
    public void startNavi() {
        double  mStart_Lat1=AppConfig.mylatitude;
        double  mStart_Lon1=AppConfig.mylongitude;
        double  mEndLat2=job.getGeoPoint().getLatitude();
        double  mEndLon2=job.getGeoPoint().getLongitude();
        //起点经纬坐标
        LatLng pt1 = new LatLng(mStart_Lat1, mStart_Lon1);
        //终点经纬坐标
        LatLng pt2 = new LatLng(mEndLat2, mEndLon2);
        // 构建 导航参数
        NaviParaOption para = new NaviParaOption();
        para.startPoint(pt1);
        para.startName("从这里开始");
        para.endPoint(pt2);
        para.endName ("到这里结束");
        if(isAvilible(getApplicationContext(), "com.baidu.BaiduMap")){
            try {
                Intent intent;
                try {
                    intent = Intent.getIntent("intent://map/direction?origin=latlng:"+mStart_Lat1+","+mStart_Lon1+"|name:从这里开始&destination="
                            +"latlng:"+mEndLat2+","+ mEndLon2+"|name:到这里结束"+"&mode=driving®ion=南京&referer=Autohome|GasStation#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                    startActivity(intent); //启动调用
                } catch (URISyntaxException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("您尚未安装百度地图app或app版本过低，点击确认安装？");
                builder.setTitle("提示");
                builder.create().show();
            }
        }else{
            //网页应用调起导航
            BaiduMapNavigation.openWebBaiduMapNavi(para, this);
        }
    }
    /**
     * @function:判断手机是否安装了某应用
     * @param context
     * @param packageName
     * @return
     */
    private boolean isAvilible(Context context, String packageName){
        final PackageManager packageManager = context.getPackageManager();//获取packagemanager
        List< PackageInfo> pinfo = packageManager.getInstalledPackages(0);//获取所有已安装程序的包信息
        List<String> pName = new ArrayList<String>();//用于存储所有已安装程序的包名
        //从pinfo中将包名字逐一取出，压入pName list中
        if(pinfo != null){
            for(int i = 0; i < pinfo.size(); i++){
                String pn = pinfo.get(i).packageName;
                pName.add(pn);
            }
        }
        return pName.contains(packageName);//判断pName中是否有目标程序的包名，有TRUE，没有FALSE
    }
    private class RatingBarChangeListenerImpl implements RatingBar.OnRatingBarChangeListener {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

        }
    }

    public   void   collection(  ){
        avObjectCollection=null;
       AVQuery<AVObject>   avQuery=new AVQuery<>("User_Job_Follow");
       avQuery.whereEqualTo("job_pointer",job);
       avQuery.whereEqualTo("user_pointer",LeanchatUser.getCurrentUser());
       avQuery.getFirstInBackground(new GetCallback<AVObject>() {
           @Override
           public void done(AVObject avObject, AVException e) {
                if(avObject!=null){
                        avObjectCollection=avObject;
                    if(avObject.getBoolean("state")){
                        jobCollectionIv.setImageResource(R.drawable.part_time_collectionpress);
                    }else{
                        jobCollectionIv.setImageResource(R.drawable.part_time_collection);
                    }
                }
           }
       });
    }
    public  void  createCollection(  ){
        final AVObject  avObjectCollect=new AVObject("User_Job_Follow");
        avObjectCollect.put("state",true);
        avObjectCollect.put("job_pointer",job);
        avObjectCollect.put("user_pointer",LeanchatUser.getCurrentUser());
        avObjectCollect.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if(e==null){
                    avObjectCollection=avObjectCollect;
                    jobCollectionIv.setImageResource(R.drawable.part_time_collectionpress);
                }
            }
        });
    }
    private void initPlatforms(){
        platforms.clear();
        for (SHARE_MEDIA e : SHARE_MEDIA.values()) {
            if (!e.toString().equals(SHARE_MEDIA.GENERIC.toString())){
                platforms.add(e.toSnsPlatform());
            }
        }
    }
    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            Log.d("plat","platform"+platform);

        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {

            if(t!=null){
                Log.d("throw","throw:"+t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {

        }
    };

}



