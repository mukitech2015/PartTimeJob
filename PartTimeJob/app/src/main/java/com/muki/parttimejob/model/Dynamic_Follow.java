package com.muki.parttimejob.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

/**
 * Created by muki on 2016/10/21.
 */

@AVClassName("Dynamic_Follow")
public class Dynamic_Follow  extends AVObject {
    public static final Creator CREATOR = AVObjectCreator.instance;
    public void setDynamic_pointer(Dynamic dynamic) {
        put("dynamic_pointer", dynamic);
    }
    public Dynamic getDynamic_pointer() {
       return getAVObject("dynamic_pointer");
    }
    public void setUser_pointer(LeanchatUser leanchatUser) {
        put("user_pointer", leanchatUser);
    }

    public LeanchatUser getUser_pointer() {
        return getAVObject("user_pointer");
    }
    public   void  setState(Boolean  b){
        put("state",b);
    }
    public   Boolean   getState(  ){
        return   getBoolean("state");
    }
}





